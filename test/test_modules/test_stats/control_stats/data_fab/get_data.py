import openpyxl

stats = {'P93': {}, 'C94': {}, 'C98': {}, 'W08': {}}
for v in stats.itervalues():
    for k in 'ns_site', 'Aing', 'R', 'V': v[k] = []
rD = {}

fname = 'RbarD et Ra.xlsx'
doc = openpyxl.load_workbook(fname)

# get rD
sheet = doc.get_sheet_by_name('RbarD')
header = [i.value for i in list(sheet['B1':'I1'])[0]]
for row in sheet['A2':'I5']:
    row = [i.value for i in row]
    pop = row[0]
    row = dict(zip(header, row))
    rD[pop] = row['rBarD']

# get stats
sheet = doc.get_sheet_by_name('Ra et Va sans NA')
i = 3
while True:
    frame = list(sheet['A{0}:G{1}'.format(i, i+8)])
    if frame[1][0].value == 'Normal' and frame[1][5].value == 'ending.':
        break

    print frame[0][6].value
    for j, v in enumerate(['P93', 'C94', 'C98', 'W08']):
        ns = int(frame[5+j][1].value)
        A = int(frame[5+j][2].value)
        R = float(frame[5+j][3].value)
        V = float(frame[5+j][4].value)
        stats[v]['ns_site'].append(ns)
        stats[v]['Aing'].append(A)
        stats[v]['R'].append(R)
        stats[v]['V'].append(V)
    i += 10

# export stats
f = open('stats.txt', 'w')
for pop in rD:
    f.write('{0}.gpop rD\n'.format(pop))
    f.write('    rD: {0}\n'.format(rD[pop]))
    f.write('::\n\n')

    f.write('{0}.gpop GenePop\n'.format(pop))
    for stat, value in stats[pop].iteritems():
        value = ' '.join(map(str, value))
        f.write('    {0}: {1}\n'.format(stat, value))
    f.write('::\n\n')

f.close()
