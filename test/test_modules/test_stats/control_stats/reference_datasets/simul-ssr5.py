import egglib, random, sys
from lib import to_fstat, to_sites

s = egglib.coalesce.Simulator(5)
s.params['migr_matrix'] = [
    [None, 0.90, 0.01, 0.01, 0.01],
    [0.90, None, 0.01, 0.01, 0.01],
    [0.01, 0.01, None, 0.90, 0.90],
    [0.01, 0.01, 0.90, None, 0.90],
    [0.01, 0.01, 0.90, 0.90, None]]
s.params['num_indiv'] = 8, 7, 12, 6, 8
ni = s.params['num_indiv']
s.params['theta'] = 25.0
s.params['num_sites']  = 10
s.params['mut_model'] = 'SMM'
aln = s.simul()


to_fstat('fstat001.txt', aln, ni)
to_fstat('fstat002.txt', aln, [sum(ni)])
to_sites('../sites003.txt', aln, ni, [(0, 1), (2, 3, 4)])
