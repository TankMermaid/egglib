import egglib, random
from lib import to_genepop

s = egglib.coalesce.Simulator(4, migr=0.2)
s.params['num_chrom'] = 10, 8, 12, 10
s.params['theta'] = 20.0
s.params['num_sites']  = 5
s.params['mut_model'] = 'SMM'
aln = s.simul()
struct = egglib.stats.get_structure(aln, lvl_pop=0, lvl_indiv=1)
to_genepop('genepop003.txt', aln, struct)

