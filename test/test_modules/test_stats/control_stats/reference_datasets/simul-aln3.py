import egglib

cs = egglib.stats.ComputeStats(multiple=True)
cs.add_stats('S', 'eta')

sim = egglib.coalesce.Simulator(num_pop=2, migr=3.0)
sim.params.update(
    num_chrom = [50, 50],
    recomb = 5.0,
    num_mut = 5,
    num_alleles = 4,
    rand_start = True)
aln = sim.simul()

print cs.process_align(aln, filtr=egglib.stats.filter_default)

aln.to_fasta('../simul013.fas', groups=True, mapping='ACGT')
