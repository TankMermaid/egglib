import egglib
from lib import to_genepop

s = egglib.coalesce.Simulator(4)
s.params['migr_matrix'] = [
    [None, 0.90, 0.01, 0.01],
    [0.90, None, 0.01, 0.01],
    [0.01, 0.01, None, 0.90],
    [0.01, 0.01, 0.90, None]]
s.params['num_indiv'] = 8, 7, 12, 6
s.params['theta'] = 2.5 * 15
s.params['num_sites']  = 15
s.params['mut_model'] = 'SMM'

aln = s.simul()

struct = egglib.stats.get_structure(aln, lvl_pop=0, lvl_indiv=1)
to_genepop('genepop001.txt', aln, struct)
