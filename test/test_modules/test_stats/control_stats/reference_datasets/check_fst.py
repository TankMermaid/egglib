import egglib

valid = set(map(ord, 'ACGT'))
aln = egglib.io.from_fasta('../simul006.fas', groups=True)
print 'ns: {0.ns} ls: {0.ls}'.format(aln)

Hb = 0
nb = 0
Hi = {}

for i in xrange(aln.ns):
    seq1 = aln[i]
    for j in xrange(i+1, aln.ns):
        seq2 = aln[j]
        d = 0
        for a, b in zip(seq1.sequence, seq2.sequence):
            if a not in valid: raise ValueError, 'invalid character: {0}'.format(chr(a))
            if b not in valid: raise ValueError, 'invalid character: {0}'.format(chr(b))
            if a != b: d += 1
        if seq1.group[0] == seq2.group[0]:
            p = seq1.group[0]
            if p not in Hi: Hi[p] = [0, 0, 0]
            Hi[p][1] += 1
            Hi[p][2] += d
        else:
            Hb += d
            nb += 1
    Hi[p][0] += 1

Hb = float(Hb) / nb
print 'Hb: {0}'.format(Hb)

Hw = 0
nw = 0
for i in sorted(Hi):
    hi = float(Hi[i][2]) / Hi[i][1]
    print 'pop {0} - n: {1} pairs: {2} Hi: {3}'.format(i, Hi[i][0], Hi[i][1], hi)
    Hw += hi
    nw += 1
Hw /= nw
print 'Hw: {0}'.format(Hw)
print 'Fst: {0}'.format(1-Hw/Hb)




########################

print '### try another way ###'

Hi = {}
Hd = {}

gr = aln.group_mapping()
lb = sorted(gr)

for i in xrange(len(lb)):
    ni = len(gr[lb[i]])
    num = 0
    den = 0
    for j in xrange(ni):
        sam1 = gr[lb[i]][j]
        for k in xrange(j+1, ni):
            sam2 = gr[lb[i]][k]
            assert sam1.group[0] == sam2.group[0]
            d = 0
            for a, b in zip(sam1.sequence, sam2.sequence):
                if a not in valid: raise ValueError, 'invalid character: {0}'.format(chr(a))
                if b not in valid: raise ValueError, 'invalid character: {0}'.format(chr(b))
                if a != b: d += 1
            num += d
            den += 1
    Hi[lb[i]] = float(num) / den

    for j in xrange(i+1, len(lb)):
        nj = len(gr[lb[j]])
        num = 0
        den = 0
        for m in xrange(ni):
            sam1 = gr[lb[i]][m]
            for n in xrange(nj):
                sam2 = gr[lb[j]][n]
                assert sam1.group[0] != sam2.group[0]
                d = 0
                for a, b in zip(sam1.sequence, sam2.sequence):
                    if a not in valid: raise ValueError, 'invalid character: {0}'.format(chr(a))
                    if b not in valid: raise ValueError, 'invalid character: {0}'.format(chr(b))
                    if a != b: d += 1
                num += d
                den += 1
        Hd[lb[i], lb[j]] = float(num) / den

print Hi
print Hd

Hw = sum(Hi.itervalues()) / len(Hi)
Hb = sum(Hd.itervalues()) / len(Hd)

print 1 - Hw/Hb
