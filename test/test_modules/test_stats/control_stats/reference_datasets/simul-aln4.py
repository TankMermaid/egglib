import egglib, random

cs = egglib.stats.ComputeStats(multiple=True)
cs.add_stats('S', 'eta')

sim = egglib.coalesce.Simulator(num_pop=1)
sim.params.update(
    num_chrom = [100],
    recomb = 20.0,
    num_mut = 100,
    num_alleles = 4,
    rand_start = True)
aln = sim.simul()

print cs.process_align(aln, filtr=egglib.stats.filter_default)

sites = []
for i in xrange(aln.ls):
    site = aln.column(i, outgroup=False)
    alleles = set(site)
    frq = [site.count(j) for j in alleles]
    if min(frq) > 20: sites.append(i)
aln = aln.extract(sites[:30])


aln.to_fasta('../simul014.fas', groups=True, mapping='ACGT')
print ' '.join(['{0:.2f}'.format(i) for i in sorted(filter(None, reduce(list.__add__, egglib.stats.matrix_LD(aln, stats='Dp')[1])))])
print ' '.join(['{0:.4f}'.format(i) for i in sorted(filter(None, reduce(list.__add__, egglib.stats.matrix_LD(aln, stats='D')[1])))])
print ' '.join(['{0:.4f}'.format(i) for i in sorted(filter(None, reduce(list.__add__, egglib.stats.matrix_LD(aln, stats='rsq')[1])))])

print cs.process_align(aln, filtr=egglib.stats.filter_default)
