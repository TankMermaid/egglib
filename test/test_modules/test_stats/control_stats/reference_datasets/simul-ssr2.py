import egglib
from lib import to_genepop

s1 = egglib.coalesce.Simulator(1)
s1.params['num_indiv'] = 40,
s1.params['theta'] = 20.0
s1.params['num_sites']  = 5
s1.params['mut_model'] = 'SMM'
aln1 = s1.simul()
struct1 = egglib.stats.get_structure(aln1, lvl_pop=0, lvl_indiv=1)
to_genepop('genepop002.txt', aln1, struct1)
