import egglib

# site positions from DnaSP (sites with multiple alleles have been removed)
singl = [25, 27, 91, 273, 329, 359, 382, 432, 461]
sites = [25, 27, 91, 273, 329, 359, 382, 432, 461,
         4, 6, 19, 23, 29, 54, 60, 64, 68, 72, 74, 85, 95,
         97, 101, 124, 132, 145, 150, 158, 160, 166, 167, 182, 188, 199, 212,
         223, 233, 268, 302, 309, 322, 335, 339, 349, 352, 391, 411, 421, 430,
         440, 441, 449, 472, 488]
singl = [i-1 for i in singl]
sites = [i-1 for i in sites]
singl.sort()
sites.sort()

# full alignment
aln0 = egglib.io.from_fasta('../simul006.fas', groups=True)
print 'full:', aln0.ns, aln0.no, aln0.ls

# alignment without multiple alleles
aln1 = egglib.io.from_fasta('../simul006-no_multi.fas', groups=True)
print 'full:', aln1.ns, aln1.no, aln1.ls

# determine mapping
mapping = {} # mapping is stripped-to-full
c = 0
for i in xrange(aln0.ls):
    site = aln0.column(i, True, False)
    if len(set(site)) < 3:
        mapping[c] = i
        c += 1

# check mapping
print 'sites without multiple: {0} ({1})'.format(c, aln1.ls)
for i,j in mapping.iteritems():
    assert aln1.column(i) == aln0.column(j)

# convert site positions to alignment without multiple alleles
print 'singl:', ' '.join([str(mapping[i]+1) for i in singl])
print 'sites:', ' '.join([str(mapping[i]+1) for i in sites])

