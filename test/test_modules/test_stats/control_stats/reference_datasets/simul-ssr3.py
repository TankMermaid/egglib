import egglib, random
from lib import to_genepop

ni= 20
Ho = 0, 0, 1, 2, 4, 6, 8, 10, 10, 11, 18, 19, 20, 20, 20

aln = egglib.Align(nsam=3*ni, nsit=15)
aln.ng = 1

for i, n in enumerate(Ho):
    na = random.randint(2, 5)
    alls = range(1, na+1)
    assert n <= ni and n >= 0
    print 1.0 * n / ni
    heter = random.sample(range(ni), n)
    for j in range(ni):
        if j in heter:
            a, b = random.sample(alls, 2)
            c = random.choice(alls)
        else:
            a = random.choice(alls)
            b = a
            c = a
        aln.set_i(j*3 + 0, i, a)
        aln.set_i(j*3 + 1, i, b)
        aln.set_i(j*3 + 2, i, c)


f = open('sites002.txt', 'w')
for i in xrange(aln.ls):
    f.write(','.join(['/'.join(map(str,
            (aln.get_i(j, i), aln.get_i(j+1, i), aln.get_i(j+2, i))))
                                    for j in xrange(0, ni*3, 3)]) + '\n')
f.close()
