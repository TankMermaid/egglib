import egglib

align_index_offset = 4

article_example = ('AACCAACCAA', 'TTTGGTGGGT', 'GGGAGGGGGG',
                   'TCCTTCCCCT', 'CCCCCCCGCG', 'CCACCCCCCC', 'AAAAAGGGGG')
article_example = zip(*article_example)
article_example = article_example[:5], article_example[5:]

test_data = [
    ['AGTGGC',
     'AGAGGC',
     'AGAGGA',
     'AGTGGA',
     'AGAGAA'],
    ['AGACAA',
     'AGACAA',
     'CGAGGA',
     'CTTGGC',
     'CTTGAA'],
    ['AGAAAA',
     'CGAAAA',
     'CGTCAA',
     'AGACAA',
     'AGTGAC']]

for sequences in article_example, test_data:


    aln = egglib.Align.create([('', seq, [i, j])
            for i,pop in enumerate(sequences) for j, seq in enumerate(pop)])
    struct_p = egglib.stats.get_structure(aln, lvl_pop = 0)
    struct_i = egglib.stats.get_structure(aln, lvl_pop = 1)
    pp = egglib.stats.paralog_pi(aln, struct_p, struct_i)

    fn = '../align{0:0>3}.fas'.format(align_index_offset)
    align_index_offset += 1
    aln.to_fasta(fn, groups=True)

    print 'dataset', fn

    n = set(map(len, sequences))
    assert len(n) == 1
    n = n.pop()
    L = len(sequences)

    print '    number of sequences:', n
    print '    number of genes:', L

    ls = set([len(seq) for gene in sequences for seq in gene])
    assert len(ls) == 1
    ls = ls.pop()

    sites2 = []
    sites3 = []
    for i in xrange(ls):
        na = len(set([seq[i] for gene in sequences for seq in gene]))
        if na == 1: continue
        assert na > 0 and na < 4
        sites2.append(i)
        if na == 2: sites3.append(i)

    print '    total number of sites:', ls
    print '    sites with 2+ alleles:', len(sites2)
    print '    sites with 2 alleles:', len(sites3)

    d = [[0 for k in xrange(n*L)] for j in xrange(n*L)]
    for p in xrange(L):
        for q in xrange(p, L):
            for i in xrange(n):
                for j in xrange(n):
                    a = p*n+i
                    b = q*n+j
                    a, b = sorted((a, b))
                    d[a][b] = sum([sequences[p][i][x]!=sequences[q][j][x] for x in sites2])
                    d[b][a] = sum([sequences[q][j][x]!=sequences[p][i][x] for x in sites3])

    for m in range(L):
        pi1 = 0.0
        pi2 = 0.0
        for i in xrange(n-1):
            for j in xrange(i+1, n):
                assert d[m*n+i][m*n+j] >= d[m*n+j][m*n+i]
                pi1 += d[m*n+i][m*n+j]
                pi2 += d[m*n+j][m*n+i]
        pi1 *= 2.0 / (n * (n-1))
        pi2 *= 2.0 / (n * (n-1))
        print '         pi locus {0}: {1} egglib: {3} (n: {4}) (w/o multi: {2})'.format(m+1, pi1, pi2, pp.Piw(m), pp.num_sites(m))

    for p in range(L-1):
        for q in xrange(p+1, L):
            pi1 = 0.0
            pi2 = 0.0
            for i in xrange(n):
                for j in xrange(n):
                    if i != j:
                        assert d[p*n+i][q*n+j] >= d[q*n+j][p*n+i]
                        pi1 += d[p*n+i][q*n+j]
                        pi2 += d[q*n+j][p*n+i]
            pi1 *= 1.0 / (n * (n-1))
            pi2 *= 1.0 / (n * (n-1))
            print '         pi between {0}-{1}: {2} egglib: {3} (n: {4}) (w/o multi: {5})'.format(p+1, q+1, pi1, pp.Pib(p, q), pp.num_sites(p, q), pi2)
