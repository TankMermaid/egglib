def to_genepop(fname, aln, struct):
    if struct.ploidy not in (1,2): raise ValueError, 'code only supports haploid/diploid data'
    n1 = len(str(aln.ls))
    n2 = len(str(struct.num_pop))

    scale = []
    for i in xrange(aln.ls):
        col = aln.column(i, True, False)
        mini = min(col)
        maxi = max(col)
        scale.append(mini-1)
        if maxi-scale[-1] > 999: raise ValueError, 'genotype value out of range'

    with open(fname, 'w') as f:
        f.write('Genotype data saved with EggLib\n')
        for i in xrange(aln.ls):
            f.write('locus{0}'.format(str(i+1).rjust(n1, '0')) + '\n')
        for i in xrange(struct.num_pop):
            f.write('POP\n')
            pop = struct._obj.get_population(i)
            for j in xrange(pop.num_indiv()):
                idv = pop.get_indiv(j)
                if struct.ploidy == 2:
                    x,y = [idv.get_sample(k) for k in xrange(idv.num_samples())]
                else:
                    x, = [idv.get_sample(k) for k in xrange(idv.num_samples())]

                if j == pop.num_indiv() - 1:
                    f.write('pop{0}'.format(str(i+1).rjust(n2, '0')))
                f.write(' ,')
                for k in xrange(aln.ls):
                    if struct.ploidy == 2:
                        a = aln.get_i(x, k) - scale[k]
                        b = aln.get_i(y, k) - scale[k]
                        assert a > 0 and a < 1000 and b > 0 and b < 1000
                        f.write(' {0:0>3}{1:0>3}'.format(a, b))
                    else:
                        a = aln.get_i(x, k) - scale[k]
                        assert a > 0 and a < 1000
                        f.write(' {0:0>3}'.format(a))
                f.write('\n')

def to_fstat(fname, aln, ni):
    # rescale alleles
    scale = []
    totmax = 0
    for i in range(aln.ls):
        site = aln.column(i, True, False)
        mini = min(site)
        maxi = max(site)
        site = [v-mini+1 for v in site]
        if max(site) > totmax: totmax = max(site)
        for j,v in enumerate(site):
            aln.set_i(j, i, v)

    print 'max allele:', totmax
    fs = len(str(totmax))
    if fs not in range(1,4): sys.exit('invalid allele range')

    # write a fstat file
    f = open(fname, 'w')
    f.write('{0} {1} {2} {3}\n'.format(sum(ni), aln.ls, totmax, fs))
    lns = len(str(aln.ls))
    for i in xrange(aln.ls):
        f.write('locus{0}\n'.format(str(i+1).rjust(lns, '0')))

    fps = len(str(len(ni)))
    c = 0
    for i, n in enumerate(ni):
        for j in xrange(n):
            f.write('    {0}   '.format(str(i+1).rjust(fps, '0')))
            for k in xrange(aln.ls):
                a = aln.get_i(c, k)
                b = aln.get_i(c+1, k)
                a = str(a).rjust(fs, '0')
                b = str(b).rjust(fs, '0')
                f.write(' {0}{1}'.format(a, b))
            c += 2
            f.write('\n')
    f.close()

def to_sites(fname, aln, ni, hier=None):
    assert range(len(ni)) == list(reduce(tuple.__add__, hier))

    f = open(fname, 'w')
    for i in xrange(aln.ls):
        site = aln.column(i, True, False)
        c = 0
        pops = []
        for i in ni:
            pops.append(','.join(['/'.join(map(str, site[j:j+2])) for j in range(c, c+2*i, 2)]))
            c += 2*i
        f.write(' '.join(pops))
        if hier is not None:
            f.write(' H' + ';'.join([','.join(['{0}'.format(j+1) for j in i]) for i in hier]))
        f.write('\n')
    f.close()
