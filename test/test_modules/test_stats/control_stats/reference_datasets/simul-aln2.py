import egglib

rho_list = 0.0, 0.5, 1.0, 2.0, 5.0

cs = egglib.coalesce.Simulator(2, migr=0.0)
cs.params.update(theta=5.0, num_sites=500, num_chrom=[50, 1], num_alleles=4, rand_start=True)
cs.params.add_event(cat='merge', T=3.0, src=1, dst=0)
cs.params['outgroup'] = 1

c = 7
for rho in rho_list:
    cs.params['recomb'] = rho
    aln = cs.simul()
    aln.to_fasta('../simul{0:0>3}.fas'.format(c), mapping='ACGT', groups=True)
    c += 1
