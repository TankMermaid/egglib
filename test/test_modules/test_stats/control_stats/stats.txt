FTLa.fas Align haplotypes=False,max_missing=0,lvl_pop=0
    S: 2
    Gste: 0.04995250799035187
::

simul001.fas Align haplotypes=False,max_missing=0,lvl_pop=0
    S: 38
    Gste: 0.23435086147760023
::

simul002.fas Align haplotypes=False,max_missing=0,lvl_pop=0
    S: 1
    Gste: 1.0
::

eIF4E.fas Align haplotypes=False,max_missing=0,lvl_pop=0
    S: 20
    Gste: 0.030882365672612676
::

# Below: control stats using DnaSP (thetaIAM/SMM computed by hand from EggLib's He and formula, Gste computed with a script)
FTLa.fas Align haplotypes=True,max_missing=0,lvl_pop=0
    Hst: 0.04102
    Gst: 0.04830
    Gste: 0.0922963550819114
    He: 0.16541
    thetaIAM: 0.198198867462
    thetaSMM: 0.217840262994
::

simul001.fas Align haplotypes=True,max_missing=0,lvl_pop=0
    Hst: 0.10777
    Gst: 0.10617
    Gste: 0.9999999999999999
    He: 0.90565
    thetaIAM: 9.59883412825
    thetaSMM: 55.667642439
::

simul002.fas Align haplotypes=True,max_missing=0,lvl_pop=0
    Hst: 1.00
    Gst: 1.00
    Gste: 1.00
    He: 0.513
    thetaIAM: 1.05263363158
    thetaSMM: 1.60665241275
::

eIF4E.fas Align haplotypes=True,max_missing=0,lvl_pop=0
    Hst: 0.01194
    Gst: 0.01349
    Gste: 0.1106917436544063
    He: 0.598
    thetaIAM: 1.485979078
    thetaSMM: 2.59004598813
::

001-0006534.fas Align max_missing=0
    nseff: 18
    ns_site: 18
    ns_site_o: 2.5
    lseff: 52
    S: 52
    Ss: 15
    eta: 52
    Kt: 19
    Ki: 18
    Pi: 16.63
    thetaW: 15.12
    singl: 1 4 17 19 20 21 25 33 36 37 41 44 46 
          47 48
    sites: 1 4 17 19 20 21 25 33 36 37 41 44 46
          47 48 2 3 5 6 7 8 9 10 11 12 13 14 15
          16 18 22 23 24 26 27 28 29 30 31 32
          34 35            38 39 40 42 43 45 49 50 51 52
    D: 0.41175
    Deta: 0.41175
    D*: 0.10720
    F*: 0.22782
    Fs: -7.032
    Rmin: 19
    Rintervals:    (3,5)  (6,7)  (7,8)  (11,12)  (12,13)  
          (13,15)  (15,18)  (18,22)  (22,23)  (23,24)  
          (24,27)  (28,29)  (31,32)  (34,35)  (35,38)  
          (38,39)  (40,42)  (50,51)  (51,52)  
    RminL: 52
    ZnS: 0.1332
    Za: 0.2839
    ZZ: 0.1506
    B: 0.1569
    Q: 0.2692
    R2: 0.1489
::

001-0006534.fas Align max_missing=0,lvl_pop=0
    nseff: 18
    ns_site: 18
    ns_site_o: 2.5
    lseff: 52
    S: 52
    Ss: 15
    eta: 52
    Kt: 19
    Ki: 18
    Pi: 16.63
    thetaW: 15.12
    singl: 1 4 17 19 20 21 25 33 36 37 41 44 46 
          47 48
    sites: 1 4 17 19 20 21 25 33 36 37 41 44 46
          47 48 2 3 5 6 7 8 9 10 11 12 13 14 15
          16 18 22 23 24 26 27 28 29 30 31 32
          34 35            38 39 40 42 43 45 49 50 51 52
    D: 0.41175
    Deta: 0.41175
    D*: 0.10720
    F*: 0.22782
    Fs: -7.032
    Rmin: 19
    Rintervals:    (3,5)  (6,7)  (7,8)  (11,12)  (12,13)  
          (13,15)  (15,18)  (18,22)  (22,23)  (23,24)  
          (24,27)  (28,29)  (31,32)  (34,35)  (35,38)  
          (38,39)  (40,42)  (50,51)  (51,52)  
    RminL: 52
    ZnS: 0.1332
    Za: 0.2839
    ZZ: 0.1506
    B: 0.1569
    Q: 0.2692
    R2: 0.1489
::

eIF4E.fas Align max_missing=0,lvl_pop=0
    nseff: 57
    S: 20
    eta: 20
    Ss: 15
    singl: 12 114 1310 1385 1439 1456 1458 1531 1624 1632 1635 1877 1902
           1904 1932
    sites: 12 114 1310 1385 1439 1456 1458 1531 1624 1632 1635 1877 1902
           1904 1932   255 287 1426 1583 1633
    Ki: 14
    Pi: 1.09
    thetaW: 4.34
    lseff: 1080
    numFxA: 0
    Dxy: 0.00100
    Da: 0.00005
    Kst: 0.02524
    Snn: 0.59624
    FstH: 0.05054
    ZnS: 0.065
    Za: 0.1062
    ZZ: 0.0412
    B: 0.1053
    Q: 0.2000
    Rmin: 0
    R2: 0.0406
    D*: -4.07431
    F*: -4.11613
    Fs: -10.449
    Dfl: -4.72604
    F: -4.68445
    Hns: -1.05138
    Hsd: -0.37324
    D: -2.34281
    Deta: -2.34281
::

lax4.fas Align max_missing=0
    lseff: 0
::

simul001.fas Align lvl_pop=0
    nseff: 60
    lseff: 1000
    S: 38
    Ss: 9
    singl: 80 216 228 293 681 847 862 879 902
    sites: 80 216 228 293 681 847 862 879 902
            12 25 205 248 346 356 360 458 465 485 499 521 525
           533 549 588 623 624 627 715 735 784 818 866 870 886 980
           984 990
    eta: 38
    Ki: 20
    Pi: 8.42
    thetaW: 8.15
    numFxA: 0
    Dxy: 0.01014
    Da: 0.0035
    Kst: 0.21154
    Snn: 0.98333
    FstH: 0.34540
    ZnS: 0.1038
    Za: 0.1269
    ZZ: 0.0231
    B: 0.0541
    Q: 0.1053
    Rmin: 2
    Rintervals: (588,715)  (715,784)
    R2: 0.1076
    D*: -0.16754
    F*: -0.07715
    Fs: -0.738
    Dfl: -0.22309
    F: -0.11179
    Hns: 3.75593
    Hsd: 0.72991
    D: 0.11020
::

simul002.fas Align lvl_pop=0
    nseff: 40
    lseff: 1
    S: 1
    Ss: 0
    sites: 1
    singl:
    Ki: 2
    Kt: 2
    Pi: 0.51
    thetaW: 0.24
    numFxD: 1
    Dxy: 1.00
    Da: 1.00
    Kst: 1.00
    Snn: 1.0
    FstH: 1.00
    R2: 0.2564
    D*: 0.56369
    F*: 1.01736
    Fs: 1.865
    Dfl: 0.5544
    F: 1.00866
    Hns: 0.0
    Hsd: 0.0
    D: 1.68662
::


simul003.fas  Align haplotypes=True,max_missing=0,lvl_pop=0
    Hst: 0.07519
    Gst: 0.07345
::

simul003.fas  Align haplotypes=False,max_missing=0,lvl_pop=0
    Kst: 0.93208
    Snn: 1.00000
::

simul004.fas  Align haplotypes=True,max_missing=0,lvl_pop=0
    Hst: 0.04366
    Gst: 0.06526
::

simul004.fas  Align haplotypes=False,max_missing=0,lvl_pop=0
    Kst: 0.74984
    Snn: 1.00000
::

simul005.fas Align haplotypes=False,max_missing=0,lvl_pop=0
    Kst: 0.92167
    Snn: 1.00000
::

simul005.fas Align haplotypes=True,max_missing=0,lvl_pop=0
    He: 0.74737
    Hst: 0.08175
    Gst: 0.11366
    thetaIAM: 2.95832673612
    thetaSMM: 7.33417527495
::

test001.fas Align haplotypes=False,max_missing=0,lvl_pop=0
    Kst: 0.46154
    Snn: 0.87500
::

test001.fas Align haplotypes=True,max_missing=0,lvl_pop=0
    He: 1.00000
    Gst: 0.0
    thetaIAM: None
    thetaSMM: None
::

# The following control stats are set manually
sites001.txt:1 Site
    Aing: 3
    Aotg: 1
::

sites001.txt:2 Site
    Aing: 2
    Aotg: 3
::

sites001.txt:3 Site
    Aing: 10
    As: 5
::

sites001.txt:4 Site
    Aing: 5
    Aotg: 1
    As: 3
    Asd: 2
::

sites002.txt sites
    Ho: X15L
        0.0
        0.0
        0.05
        0.1
        0.2
        0.3
        0.4
        0.5
        0.5
        0.55
        0.9
        0.95
        1.0
        1.0
        1.0
::

# Control stats using GenePop
genepop001.txt GenePop
    FistWC: X15L
       -0.029640 0.282342 0.261071
        0.087384 0.220611 0.288717
        0.116122 0.197694 0.290859
        0.176187 0.418229 0.520730
        0.098634 0.045913 0.140019
        0.049041 0.256182 0.292659
        0.044383 0.224658 0.259069
        0.153920 0.316123 0.421386
        0.036902 0.316060 0.341298
        0.147660 0.301571 0.404700
        0.213609 0.101516 0.293441
       -0.000038 0.248605 0.248576
        0.081967 0.229979 0.293095
        0.201325 0.357440 0.486803
       -0.065792 0.305438 0.259741
::

genepop002.txt GenePop
    Fis: X5L
        -0.016415261756877
        -0.184629803186504
         0.072012257405516
         0.085077951002227
        -0.192888455918169
    He: X5L
         0.713291139240506
         0.675316455696203
         0.619620253164557
         0.710443037974684
         0.649683544303797
    Ho: X5L
        0.725
        0.8
        0.575
        0.65
        0.775
::

genepop003.txt GenePop
    FstWC: X5L
        0.331827
        0.292925
        0.221228
        0.175542
        0.226425
::

# Statistics computed manually
sites004.txt:1 Site
    numSp: 1
    numSpd: 1
    numShA: 1
    numShP: 0
    numFxA: 0
    numFxD: 0
::

sites004.txt:2 Site
    numSp: 1
    numSpd: 1
    numShA: 2
    numShP: 1
    numFxA: 1
    numFxD: 0
::

sites004.txt:3 Site
    numSp: 3
    numSpd: 2
    numShA: 0
    numShP: 0
    numFxA: 3
    numFxD: 3
::

sites004.txt:4 Site
    numSp: 1
    numSpd: 0
    numShA: 1
    numShP: 0
    numFxA: 0
    numFxD: 0
::

align001.fas Align lvl_pop=0
    numSp:  13
    numSp*:  7
    numSpd:  8
    numSpd*: 5
    numShA: 15
    numShA*: 9
    numShP:  9
    numShP*: 4
    numFxA: 14
    numFxA*: 6
    numFxD: 15
    numFxD*: 6
::

sites003.txt sites
    FisctWC: X10L
        0.02328449   0.43730175   0.20477379   0.45040389
       -0.006097666  0.301246529  0.194144752  0.296985763
        0.06313224   0.28119983   0.19039514   0.32657930
        0.09441975   0.41557590   0.26762651   0.47075708
        0.03996322   0.26555177   0.22261615   0.29490268
       -0.05245615   0.38659483   0.22540717   0.35441796
        0.03058891   0.28398412   0.29945072   0.30588627
       -0.09960607   0.22646044   0.08965331   0.14941120
       -0.09924913   0.45812129   0.14102892   0.40434030
       -0.1126486    0.4034576    0.2800038    0.3362579
::

# DH, Zeng's program
simul006.fas Align lvl_pop=0,multiple=False
    Hsd: 0.2748156812256342
    E: 0.44628436768825314
    So: 54
::

# Using variscan software
simul006.fas Align lvl_pop=0,multiple=True
    So: 61
    etao: 72
    nsingld: 11
    Dfl: 0.8255424
    F: 0.8581096
    Hns: 3.8095238
::

# Using Htest (found here:http://www.genetics.wustl.edu/jflab/software.html; the web form doesn't work currently)
simul006.fas Align haplotypes=False,multiple=True,max_missing=0,lvl_pop=0
    Hsd: 0.373884
    Hns: 3.809524
    thetaH: 17.295238
::

# New DnaSP statistics (Hsd is skipped because there is an unexplained difference)
simul006.fas Align haplotypes=True,max_missing=0,lvl_pop=0,multiple=True
	He: 0.96349
	Gst: 0.09067
    Hst: 0.0891
::

simul006.fas Align lvl_pop=0,multiple=True,pop_filter=1;2
	Dxy: 0.05505
	Da: 0.02148
::

simul006.fas Align lvl_pop=0,multiple=True
	S: 64
	Ss: 9
	eta: 77
	nseff: 36
	Pi: 21.37
	thetaW: 15.43
	D: 1.40727
	Deta: 0.55550
    singl: 25 27 91 274 333 364 391 441 470
    sites: 25 27 91 274 333 364 391 441 470
           4 6 19 23 29 54 60 64 68 72 74 85 95
           97 101 124 132 145 150 158 160 166 167 182 188 199 212
           224 234 269 304 313 326 339 343 353 356 400 420 430 439
           449 450 458 481 497
           297 361 377 378 380 223 311 312 379
    sites_o: 25 27 91 274 333 364 391 441 470
           4 19 23 29 54 60 64 68 72 74 85 95
           97 101 124 132 145 150 158 160 166 167 182 188 199 212
           224 234 269 304 313 326 339 343 353 356 400 420 430 439
           449 450 458 481 497
           297 361 378 223 311 312 379
    lseff: 500
	Ki: 23
	D*: 0.72430
	F*: 0.79069
	Fs: -0.622
	Dfl: 0.82554
	F: 0.85811
    So: 61
	Hns: 3.80952
    Kst: 0.44621
	ZnS: 0.2904
	Za: 0.2147
	ZZ: -0.0757
	Rmin: 0
	RminL: 55
	FstH: 0.53224
    R2: 0.1622
    Snn: 0.81058
::
# Hsd value given by DnaSP for dataset above: 0.36425

simul006.fas Align lvl_pop=0,multiple=False
    nseff: 36
    lseff: 491
    S: 55
    Ss: 9
    singl: 25 27 91 274 333 364 391 441 470
    sites: 4 6 19 23 25 27 29 54 60 64 68 72 74 85 91 95 97 101 124 132 145 150 158 160 166 167 182 188 199 212 224 234 269 274 304 313 326 333 339 343 353 356 364 391 400 420 430 439 441 449 450 458 470 481 497
    eta: 55
    Ki: 15
    Pi: 16.28
    thetaW: 13.26
    Snn: 0.77500
    FstH: 0.53884
    Kst: 0.45038
    ZnS: 0.2904
    Za: 0.2147
    ZZ: -0.0757
    B: 0.1296
    Q: 0.1455
    Rmin: 0
    RminL: 55
    R2: 0.1439
    D*: 0.64547
    F*: 0.83940
    Fs: 3.196
    So: 54
    etao: 54
    Sso: 9
    nsingld: 9
    Dfl: 0.67257
    F: 0.87559
    Hns: 2.13333
    Hsd: 0.27482
    D: 0.82691
::
# For B and Q, DnaSP seems to remove multiple mutations

simul006.fas Align lvl_pop=0,multiple=False,pop_filter=1;2
    Dxy: 0.04711
    Da: 0.01973
::

simul006.fas Align lvl_pop=0,multiple=False,haplotypes=True
    He: 0.92698
    Gst: 0.16137
    Hst: 0.15971
    thetaIAM: 12.6956283554
    thetaSMM: 93.285118025
::

simul007.fas Align lvl_pop=0
	Rmin: 0
	RminL: 25
::

simul008.fas Align lvl_pop=0
	Rmin: 1
	RminL: 24
::

simul009.fas Align lvl_pop=0
	Rmin: 1
	RminL: 17
::

simul010.fas Align lvl_pop=0
	RminL: 18
	Rmin: 1
::

simul011.fas Align lvl_pop=0
	RminL: 19
	Rmin: 2
::

simul012.fas Align lvl_pop=0
	RminL: 37
	Rmin: 4
::

align002.fas Align max_missing=0.4
    lseff: 8
    lseffo: 5
    nseff: 7.25
    nseffo: 6.6
    nsmax: 9
    nsmaxo: 7
::

align003.fas Align multiple=False
    Ki: 3
    Kt: 5
    S: 2
::

align003.fas Align multiple=True
    Ki: 5
    Kt: 8
    S: 3
::

# MFDM controlled using the software provided by the author (see http://www.picb.ac.cn/evolgen/softwares/)
simul012.fas Align max_missing=0
    pM: 0.081633
::

simul009.fas Align max_missing=0
    pM: 0.122449
::

simul011.fas Align max_missing=0
    pM: 0.367347
::

simul010.fas Align max_missing=0
    pM: 0.040816
::

simul007.fas Align max_missing=0
    pM: 0.040816
::

# DnaSP
simul013.fas LD
    d: 1 2 3 4 1 2 3 1 2 1
    D: -0.033 -0.078 0.091 -0.004 -0.019 -0.039 -0.001 -0.09 -0.002 0.006
    Dp: -1 -1 0.431 -1 -1 -1 -1 -1 -1 1
    r: -0.241 -0.395 0.380 -0.077 -0.162 -0.273 -0.032 -0.448 -0.052 0.116
    rsq: 0.058 0.156 0.145 0.006 0.026 0.075 0.001 0.201 0.003 0.013
::

simul014.fas LD
  d: 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 1 2 3 4 5 6 7 8 9 10 11 12 13 14 1 2 3 4 5 6 7 8 9 10 11 12 13 1 2 3 4 5 6 7 8 9 10 11 12 1 2 3 4 5 6 7 8 9 10 11 1 2 3 4 5 6 7 8 9 10 1 2 3 4 5 6 7 8 9 1 2 3 4 5 6 7 8 1 2 3 4 5 6 7 1 2 3 4 5 6 1 2 3 4 5 1 2 3 4 1 2 3 1 2 1
  D: 0.075 -0.075 0.075 0.062 0.062 0.078 -0.081 -0.081 0.081 -0.007 -0.003 0.01 -0.013 0.013 -0.003 -0.017 -0.028 -0.029 0.007 0.014 -0.202 0.202 0.187 0.187 0.064 -0.154 -0.154 0.154 0.114 -0.089 0.083 0.104 -0.104 -0.028 0.036 0.016 0.065 -0.044 -0.039 -0.202 -0.187 -0.187 -0.064 0.154 0.154 -0.154 -0.114 0.089 -0.083 -0.104 0.104 0.028 -0.036 -0.016 -0.065 0.044 0.039 0.187 0.187 0.064 -0.154 -0.154 0.154 0.114 -0.089 0.083 0.104 -0.104 -0.028 0.036 0.016 0.065 -0.044 -0.039 0.192 0.06 -0.161 -0.161 0.161 0.125 -0.101 0.094 0.113 -0.113 -0.039 0.047 0.03 0.073 -0.055 -0.05 0.06 -0.161 -0.161 0.161 0.125 -0.101 0.094 0.113 -0.113 -0.039 0.047 0.03 0.073 -0.055 -0.05 -0.087 -0.087 0.087 -0.11 0.09 -0.051 -0.076 0.076 0.047 -0.053 -0.062 -0.066 0.03 0.046 0.236 -0.236 -0.092 0.148 -0.137 -0.069 0.069 0.08 -0.091 -0.063 -0.034 0.102 0.095 -0.236 -0.092 0.148 -0.137 -0.069 0.069 0.08 -0.091 -0.063 -0.034 0.102 0.095 0.092 -0.148 0.137 0.069 -0.069 -0.08 0.091 0.063 0.034 -0.102 -0.095 -0.203 0.159 0.186 -0.186 -0.098 0.114 0.1 0.137 -0.1 -0.109 -0.188 -0.145 0.145 0.126 -0.144 -0.125 -0.1 0.133 0.141 0.198 -0.198 -0.175 0.192 0.121 0.14 -0.179 -0.168 -0.248 -0.129 0.143 0.091 0.185 -0.126 -0.117 0.129 -0.143 -0.091 -0.185 0.126 0.117 -0.231 -0.157 -0.172 0.218 0.207 0.149 0.184 -0.234 -0.223 0.111 -0.14 -0.146 -0.187 -0.179 0.239
  Dp: 0.405 0.405 0.405 0.359 0.359 -1 0.383 0.383 0.383 0.038 -0.02 -0.064 -0.085 -0.085 -0.02 0.091 0.158 -0.211 0.038 0.074 1 1 1 1 -1 0.885 0.885 0.885 -0.851 -0.817 -0.675 0.675 0.675 -0.235 -0.286 -0.206 0.395 -0.33 -0.301 1 1 1 -1 0.885 0.885 0.885 -0.851 -0.817 -0.675 0.675 0.675 -0.235 -0.286 -0.206 0.395 -0.33 -0.301 1 1 -1 0.885 0.885 0.885 -0.851 -0.817 -0.675 0.675 0.675 -0.235 -0.286 -0.206 0.395 -0.33 -0.301 1 -1 1 1 1 -1 -1 -0.825 0.79 0.79 -0.359 -0.402 -0.43 0.478 -0.439 -0.415 -1 1 1 1 -1 -1 -0.825 0.79 0.79 -0.359 -0.402 -0.43 0.478 -0.439 -0.415 -1 -1 -1 -1 -1 -0.506 0.605 0.605 -0.482 -0.517 -1 0.484 -0.275 -0.433 1 1 -0.507 -1 -0.821 0.33 0.33 -0.499 -0.532 -0.61 0.153 -0.561 -0.542 1 -0.507 -1 -0.821 0.33 0.33 -0.499 -0.532 -0.61 0.153 -0.561 -0.542 -0.507 -1 -0.821 0.33 0.33 -0.499 -0.532 -0.61 0.153 -0.561 -0.542 1 0.694 -0.861 -0.861 0.451 0.487 0.715 -0.695 0.399 0.457 0.863 -0.829 -0.829 0.558 0.674 0.757 -0.625 0.655 0.668 -1 -1 0.745 0.793 0.802 -0.778 0.781 0.705 1 -0.683 -0.704 -0.753 0.823 -0.583 -0.565 -0.683 -0.704 -0.753 0.823 -0.583 -0.565 1 1 -1 1 0.912 1 -1 1 0.918 -1 1 1 -0.949 -0.947 1
  r: 0.352 0.352 0.352 0.296 0.296 -0.392 0.351 0.351 0.351 0.029 -0.011 -0.041 -0.055 -0.055 -0.012 0.072 0.134 -0.126 0.029 0.058 1 1 0.951 0.951 -0.341 0.705 0.705 0.705 -0.51 -0.407 -0.373 0.466 0.466 -0.125 -0.161 -0.078 0.295 -0.198 -0.173 1 0.951 0.951 -0.341 0.705 0.705 0.705 -0.51 -0.407 -0.373 0.466 0.466 -0.125 -0.161 -0.078 0.295 -0.198 -0.173 0.951 0.951 -0.341 0.705 0.705 0.705 -0.51 -0.407 -0.373 0.466 0.466 -0.125 -0.161 -0.078 0.295 -0.198 -0.173 1 -0.324 0.757 0.757 0.757 -0.569 -0.474 -0.434 0.518 0.518 -0.181 -0.215 -0.155 0.34 -0.25 -0.227 -0.324 0.757 0.757 0.757 -0.569 -0.474 -0.434 0.518 0.518 -0.181 -0.215 -0.155 0.34 -0.25 -0.227 -0.428 -0.428 -0.428 -0.525 -0.437 -0.245 0.365 0.365 -0.224 -0.256 -0.332 0.317 -0.145 -0.218 1 1 -0.381 -0.626 -0.569 0.286 0.286 -0.332 -0.377 -0.29 0.143 -0.422 -0.392 1 -0.381 -0.626 -0.569 0.286 0.286 -0.332 -0.377 -0.29 0.143 -0.422 -0.392 -0.381 -0.626 -0.569 0.286 0.286 -0.332 -0.377 -0.29 0.143 -0.422 -0.392 0.832 0.64 -0.748 -0.748 0.399 0.459 0.453 -0.557 0.399 0.439 0.778 -0.6 -0.6 0.524 0.596 0.576 -0.416 0.545 0.578 -0.802 -0.802 0.715 0.777 0.55 -0.575 0.721 0.677 1 -0.525 -0.576 -0.414 0.758 -0.507 -0.472 -0.525 -0.576 -0.414 0.758 -0.507 -0.472 0.941 0.715 -0.709 0.886 0.841 0.672 -0.754 0.941 0.899 -0.507 0.633 0.659 -0.76 -0.729 0.961
  rsq: 0.124 0.124 0.124 0.088 0.088 0.154 0.123 0.123 0.123 0.001 0.000 0.002 0.003 0.003 0.000 0.005 0.018 0.016 0.001 0.003 1.000 1.000 0.904 0.904 0.116 0.497 0.497 0.497 0.260 0.166 0.139 0.217 0.217 0.016 0.026 0.006 0.087 0.039 0.030 1.000 0.904 0.904 0.116 0.497 0.497 0.497 0.260 0.166 0.139 0.217 0.217 0.016 0.026 0.006 0.087 0.039 0.030 0.904 0.904 0.116 0.497 0.497 0.497 0.260 0.166 0.139 0.217 0.217 0.016 0.026 0.006 0.087 0.039 0.030 1.000 0.105 0.573 0.573 0.573 0.324 0.225 0.188 0.268 0.268 0.033 0.046 0.024 0.116 0.063 0.052 0.105 0.573 0.573 0.573 0.324 0.225 0.188 0.268 0.268 0.033 0.046 0.024 0.116 0.063 0.052 0.183 0.183 0.183 0.276 0.191 0.060 0.133 0.133 0.050 0.066 0.110 0.100 0.021 0.048 1.000 1.000 0.145 0.392 0.324 0.082 0.082 0.110 0.142 0.084 0.020 0.178 0.154 1.000 0.145 0.392 0.324 0.082 0.082 0.110 0.142 0.084 0.020 0.178 0.154 0.145 0.392 0.324 0.082 0.082 0.110 0.142 0.084 0.020 0.178 0.154 0.692 0.410 0.560 0.560 0.159 0.211 0.205 0.310 0.159 0.193 0.605 0.360 0.360 0.275 0.355 0.332 0.173 0.297 0.334 0.643 0.643 0.511 0.604 0.303 0.331 0.520 0.458 1.000 0.276 0.332 0.171 0.575 0.257 0.223 0.276 0.332 0.171 0.575 0.257 0.223 0.885 0.511 0.503 0.785 0.707 0.452 0.569 0.885 0.808 0.257 0.401 0.434 0.578 0.531 0.924
::

# two examples: one manually and one from Innan's article (with corrected mistake)
align004.fas Innan
    Piw: 2.6 2.2
    Pib: 3.4
::

align005.fas Innan
    Piw: 1.6 3.4 2.4
    Pib: 2.9 2.8 2.8
::

align006.fas Innan max_missing=0
    num_tot: 3
    num_pop: 3 3 3
    num_pair: 3 3 3
::

align006.fas Innan max_missing=0.14
    num_tot: 5
    num_pop: 5 5 5
    num_pair: 5 5 5
::

align006.fas Innan max_missing=0.33
    num_tot: 7
    num_pop: 7 7 7
    num_pair: 7 7 7
::

align006.fas Innan max_missing=1.0
    num_tot: 10
    num_pop: 10 8 9
    num_pair: 8 10 8
::

# stats below computed manually
align007.fas Align max_missing=0
    ns: 5
    ls: 56
    lseff: 43
    nseff: 5
    S: 10
    eta: 10
::

align007.fas Align max_missing=0.2,multiple=True
    ns: 5
    ls: 56
    lseff: 50
    nseff: 4.9
    S: 14
    eta: 16
::

align007.fas codon frame=0-13;25-37-2;42-56,max_missing=0,multiple_alleles=False,multiple_hits=False,consider_stop=False
    num_codons_tot: 12
    num_codons_stop: 1
    num_multiple_hits: 1
    num_multiple_alleles: 2
    num_codons_eff: 4
    num_pol_NS: 1
    num_pol_S: 2
    num_pol_single: 3
    num_sites_S: 4.0
    num_sites_NS: 8.0
::

align007.fas codon frame=0-13;25-37-2;42-56,max_missing=0.2,multiple_alleles=False,multiple_hits=False,consider_stop=False
    num_codons_tot: 12
    num_codons_stop: 1
    num_multiple_hits: 1
    num_multiple_alleles: 2
    num_codons_eff: 8
    num_pol_NS: 2
    num_pol_S: 3
    num_pol_single: 5
    num_sites_S: 6.91667
    num_sites_NS: 17.08333
::

align007.fas codon frame=0-13;25-37-2;42-56,max_missing=0,multiple_alleles=False,multiple_hits=False,consider_stop=True
    num_codons_tot: 12
    num_codons_stop: 1
    num_multiple_hits: 1
    num_multiple_alleles: 2
    num_codons_eff: 5
    num_pol_NS: 2
    num_pol_S: 2
    num_pol_single: 4
    num_sites_S: 4.06667
    num_sites_NS: 10.93333
::

align007.fas codon frame=0-13;25-37-2;42-56,max_missing=0,multiple_alleles=True,multiple_hits=False,consider_stop=False
    num_codons_tot: 12
    num_codons_stop: 1
    num_multiple_hits: 1
    num_multiple_alleles: 2
    num_codons_eff: 5
    num_pol_NS: 1
    num_pol_S: 3
    num_pol_single: 3
    num_sites_S: 5.0
    num_sites_NS: 10.0
::

align007.fas codon frame=0-13;25-37-2;42-56,max_missing=0.3,multiple_alleles=False,multiple_hits=False,consider_stop=True
    num_codons_tot: 12
    num_codons_stop: 1
    num_multiple_hits: 1
    num_multiple_alleles: 2
    num_codons_eff: 8
    num_pol_NS: 3
    num_pol_S: 3
    num_pol_single: 6
    num_sites_S: 6.23333
    num_sites_NS: 17.76667
::

align007.fas codon frame=0-13;25-37-2;42-56,max_missing=0.2,multiple_alleles=True,multiple_hits=True,consider_stop=True
    num_codons_tot: 12
    num_codons_stop: 1
    num_multiple_hits: 1
    num_multiple_alleles: 2
    num_codons_eff: 10
    num_pol_NS: 3
    num_pol_S: 5
    num_pol_single: 6
    num_sites_S: 8.43333
    num_sites_NS: 21.56667
::

align007.fas codon frame=0-13;25-37-2;42-56,max_missing=0.2,multiple_alleles=True,multiple_hits=True,consider_stop=False
    num_codons_tot: 12
    num_codons_stop: 1
    num_multiple_hits: 1
    num_multiple_alleles: 2
    num_codons_eff: 10
    num_pol_NS: 2
    num_pol_S: 5
    num_pol_single: 5
    num_sites_S: 9.15000
    num_sites_NS: 20.85000
::

# using R package rehh (Gautier and Vitalis) with their own example file
# for iES it is necessary to use option crop_EHHS because rehh sets all EHHS<threshold to 0 and EggLib does not do it by default
bta12_cgu EHH core=456,chr=12,thr=0.05,thrS=0.05,left=10,right=10,EHH_crop=True
    frq: 0.6964286
    EHH: 0.91979910 0.9594502 0.9594502 0.9594502 0.9594502
         0.9594502  0.9897436 1.0000000 1.0000000 1.0000000
             1
         1.0000000  1.0000000  1.0000000  0.9897436  0.9694422
         0.9104414  0.90066085 0.67512556 0.67512556 0.66545070
    EHHc: 0.08767507 0.1313725 0.1464986 0.1624650 0.1624650
          0.1624650  0.1661064 0.1910364 0.2764706 0.5529412
             1
          0.8879552  0.6422969 0.5955182  0.2969188  0.2036415
          0.1414566 0.08207283 0.07759104 0.07759104 0.06218487
    iHH:  284632.9601519002 2057151.6982901003 
    num: 85 195
    EHHS: 0.7876807 0.8279742 0.8303758 0.8329108 0.8329108
          0.8329108 0.8589726 0.8715588 0.8851234 0.9290193
            1.0000000
          0.9822104 0.9432066 0.9357794 0.8797421 0.8478541
          0.7883478 0.7706916 0.5802535 0.5802535 0.5696687
    iES: 1760565.177437003
::

# those are computed manually
ehh_data01 EHH core=1,chr=1,EHH_binary=False,thr=0.2,thrS=0.2,phased=True,left=0,right=7,EHH_crop=False
    Kcore: 3
    nsam: 20
    num: 5 10 5
    EHH1: 1.000000	0.400000	0.400000	0.400000	0.400000	0.200000	0.100000	0.000000
    EHH2: 1.000000	1.000000	1.000000	0.466667	0.377778	0.377778	0.377778	0.177778
    EHH3: 1.000000	1.000000	0.400000	0.400000	0.400000	0.200000	0.100000	0.100000
    EHHc1:	0.523810	0.523810	0.466667	0.238095	0.200000	0.180952	0.171429	0.0857145714
    EHHc2:	0.444444	0.311111	0.177778	0.177778	0.177778	0.088889	0.044444	0.022222
    EHHc3:	0.523810	0.466667	0.466667	0.238095	0.200000	0.180952	0.171429	0.076190
    rEHH1: 1.909091	0.763636	0.857143	1.680000	2.000000	1.105263	0.583333	0.000000
    rEHH2: 2.250000	3.214286	5.625000	2.625000	2.125000	4.250000	8.500000	8.000000
    rEHH3: 1.909091	2.142857	0.857143	1.680000	2.000000	1.105263	0.583333	1.312500
    iHH: 0.260000   0.660741    0.380000
    iHHc: 0.158095  0.044815    0.146667
    iHS: -0.497484 -2.690823 -0.952009
    decay: 1.6 2.2 1.6
    davg: 1.9
    dmax: 2.2
    EHHS: 1.000000	0.907692	0.815385	0.446154	0.384615	0.323077	0.292308	0.138462
    iES: 0.533538
    decayS: 2.2
::

ehh_data02 EHH core=1,chr=1,thr=0.3,thrS=0.25,thrG=0.4,phased=False,EHH_binary=False,EHH_crop=False
    Kcore: 3
    Kcur: 3 5 7 10
    EHH1: 1.0 0.400000	0.266667	0.133333
    EHH2: 1.0 1.000000	0.466667	0.266667
    EHH3: 1.0 0.466667	0.400000	0.200000
    decay: 5.500000 8.300000 8.300000
    davg: 7.460000
    dmax: 8.3
    EHHc1: 0.472527	0.333333	0.200000	0.109091
    EHHc2: 0.454545	0.196970	0.145455	0.072727
    EHHc3: 0.472527	0.318182	0.166667	0.090909
    rEHH1: 2.116279	1.200000	1.333333	1.222222
    rEHH2: 2.200000	5.076923	3.208333	3.666667
    rEHH3: 2.116279	1.466667	2.400000	2.200000
    iHH: 1.293750   3.377778    1.703333
    iHHc: 0.319208  0.139091    0.288791
    iHS: -1.399458 -3.189846 -1.774638
    ncur: 20 18 17 17
    EHHS: 1.000000	0.622222	0.375000	0.200000
    iES: 2.429861
    iEG: 3.06055
    decayS: 8.3
    EHHG: 1.00000	1.00000	0.63636	0.27273
    decayG: 8.3
::

# computed by Fabien (with missing data: using custom script test-rd.py)
P93.gpop rD
    rD: 0.0307719
::

P93MD.gpop rD
    rD: 0.0296664711422
::

P93.gpop GenePop
    R: 0.0512820512821 0.0769230769231 0.0512820512821 0.0512820512821 0.025641025641 0.0769230769231 0.0769230769231 0.0512820512821 0.0512820512821 0.0512820512821 0.025641025641 0.102564102564 0.102564102564 0.0512820512821 0.0769230769231 0.0512820512821 0.0512820512821 0.0769230769231 0.0769230769231 0.0769230769231 0.205128205128 0.179487179487
    ns_site: 40 40 40 40 40 40 40 40 40 40 40 40 40 40 40 40 40 40 40 40 40 40
    Aing: 3 4 3 3 2 4 4 3 3 3 2 5 5 3 4 3 3 4 4 4 9 8
    V: 1.56346153846 14.9480769231 0.461538461538 3.44615384615 0.9 9.78461538462 29.5326923077 6.36923076923 4.10192307692 18.8307692308 1.33269230769 47.025 49.8230769231 2.94807692308 67.9384615385 1.01538461538 6.91730769231 6.46153846154 28.6096153846 6.55384615385 358.130769231 35.6147435897
::

W08.gpop rD
    rD: 0.00163383
::

W08MD.gpop rD
    rD: 0.00184487429001
::

W08.gpop GenePop
    R: 0.018018018018 0.036036036036 0.045045045045 0.036036036036 0.036036036036 0.0630630630631 0.045045045045 0.045045045045 0.036036036036 0.027027027027 0.045045045045 0.0990990990991 0.0540540540541 0.027027027027 0.036036036036 0.027027027027 0.036036036036 0.036036036036 0.036036036036 0.045045045045 0.108108108108 0.108108108108
    ns_site: 112 112 112 112 112 112 112 112 112 112 112 112 112 112 112 112 112 112 112 112 112 112
    Aing: 3 5 6 5 5 8 6 6 5 4 6 12 7 4 5 4 5 5 5 6 13 13
    V: 0.162162162162 16.1344111969 5.10159266409 4.66377091377 7.51093951094 5.30107786358 22.8416988417 10.1901544402 61.4529440154 4.69369369369 16.2097007722 125.4375 108.201978764 6.39888996139 56.5662805663 1.72940797941 6.05115830116 6.79922779923 37.5803571429 15.5415057915 206.035714286 43.9008204633
::

C98.gpop rD
    rD: 0.0114581
::

C98MD.gpop rD
    rD: 0.0108678931413
::

C98.gpop GenePop
    R: 0.0181818181818 0.0545454545455 0.0363636363636 0.0545454545455 0.0363636363636 0.145454545455 0.0363636363636 0.0545454545455 0.0363636363636 0.0363636363636 0.0363636363636 0.0909090909091 0.0909090909091 0.0363636363636 0.0545454545455 0.0363636363636 0.0545454545455 0.0363636363636 0.0363636363636 0.0909090909091 0.109090909091 0.181818181818
    ns_site: 56 56 56 56 56 56 56 56 56 56 56 56 56 56 56 56 56 56 56 56 56 56
    Aing: 2 4 3 4 3 9 3 4 3 3 3 6 6 3 4 3 4 3 3 6 7 11
    V: 0.315584415584 11.2675324675 3.02727272727 7.12207792208 4.41525974026 48.8974025974 3.76071428571 5.78571428571 4.48831168831 6.06233766234 3.90097402597 159.615584416 49.2311688312 8.17012987013 18.8675324675 0.861038961039 7.51558441558 0.642857142857 3.58831168831 11.9220779221 38.4282467532 29.287987013
::

C94.gpop rD
    rD: 0.0876684
::

C94MD.gpop rD
    rD: 0.0855818905084
::

C94.gpop GenePop
    R: 0.0 0.0540540540541 0.0540540540541 0.0810810810811 0.0540540540541 0.135135135135 0.0540540540541 0.0540540540541 0.0540540540541 0.0540540540541 0.027027027027 0.135135135135 0.0540540540541 0.0540540540541 0.0540540540541 0.0540540540541 0.0810810810811 0.027027027027 0.0810810810811 0.0540540540541 0.0810810810811 0.162162162162
    ns_site: 38 38 38 38 38 38 38 38 38 38 38 38 38 38 38 38 38 38 38 38 38 38
    Aing: 1 3 3 4 3 6 3 3 3 3 2 6 3 3 3 3 4 2 4 3 4 7
    V: 0.0 15.5419630156 4.3079658606 3.39118065434 7.04765291607 59.1557610242 2.27880512091 5.58819345661 3.25817923186 4.14224751067 1.90113798009 126.460881935 30.0085348506 11.7524893314 13.6557610242 0.73186344239 2.64366998578 5.55618776671 9.70412517781 12.7318634424 25.5533428165 32.4075391181
::
