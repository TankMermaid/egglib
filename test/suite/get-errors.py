import re, sys

name, fname, outf = sys.argv[1:]

outf = open(outf, 'a')

cur = -10   
c = 0
f = open(fname)
lines = []
for idx, line in enumerate(f):
    if re.search('(?<!eggwrapper)\.[hc]pp', line):
        if idx != cur + 1:
            c += 1
            lines.append([])
        lines[-1].append(line)
        cur = idx

outf.write('{0}\n'.format(name))
outf.write('    Number: {0}\n'.format(c))
for i, line in enumerate(lines):
    outf.write('       [{0}] {1}'.format(i+1, line[0]))
    for sub in line[1:]:
        outf.write('           {0}'.format(sub))
outf.write('\n')
