echo "running all tests"

# header (get path)
set -e # stop on errors
file=`realpath $0`
path=`dirname ${file}`

# clean directory
rm -frv ${path}/output-* ${path}/error-* ${path}/output-* ${path}/TMP.err ${path}/tmp_venv .coverage/

# create virtual environment
virtualenv ${path}/tmp_venv -q
source ${path}/tmp_venv/bin/activate
pip install scipy coverage
python -c '''try: import egglib
except ImportError: pass
else: raise AssertionError("should not be able to import egglib")''' # check that egglib is not available

# install egglib in virtual environment
python setup.py clean build --debug install
python -c 'import egglib; egglib.wrappers.paths.autodetect(); egglib.wrappers.paths.save()'

# run tests and measure coverage
echo "1/3 main test suite & coverage"
coverage run --source=egglib --omit="*eggwrapper.py" test -bictswo ${path}/output-main.txt 2> ${path}/error-main.txt
coverage html -d ${path}/coverage-`python -c 'import egglib; print(egglib.__version__)'`-$(date +%Y%m%d)
rm -R .coverage/

# run the statistic control script
echo "2/3 test stats"
python test --test-stats > ${path}/output-stats.txt 2> ${path}/error-stats.txt

# run most item under valgrind
echo "3/3 memory tests"
for K in Alphabet Align Container Database LabelView SampleView \
    SequenceView Site Structure Node Tree Fasta GenBankFeatureLocation \
    GenbankFeature Genbank Genepop GFF3Feature GFF3 VcfIndex Legacy MS \
    Bed_Sliding_Window Sliding_Window VCF EventsList ParamDict \
    ParamList ParamMatrix Simulator BackalignError Outclass_tools \
    Translator Concat Random ReadingFrame Seq_Manip ProbaMisoriented \
    CodingDiversity ComputeStats Statistics EHH Freq Haplotypes LD \
    Paralog_pi SiteDiversity AlphabetAlign blastn blastp blastx \
    makeblastdb tblastn tblastx Clustal Codeml Muscle nj
do
    echo ${K}
    valgrind --leak-check=full --child-silent-after-fork=yes \
        python test -k ${K} 2> ${path}/TMP.err
    python ${path}/get-errors.py ${K} ${path}/TMP.err ${path}/output-memory.txt
    rm -f ${path}/TMP.err
done

deactivate

rm -R ${path}/tmp_venv/
