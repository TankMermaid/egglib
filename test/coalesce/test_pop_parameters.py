import egglib, lib, os
from matplotlib import pyplot

lib.showl('# Population parameters')
nr = 1000
fstream = open(lib.fname('report.rst'), 'a')
fstream.write('''
=========================
Per-Population Parameters
=========================
''')

N2 = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 1.1, 1.2, 1.3, 1.4, 1.5, 1.6, 1.7, 1.8, 1.9, 2.0]
theta = 2
ns = [20, 20, 20]
Pi1 = []
Pi2 = []
Pi3 = []
cs_array = []
for i in ns:
    cs_array.append(egglib.stats.ComputeStats())
    cs_array[-1].add_stats('Pi')
cs_array[0].configure(lib.struct(1, ns[0], init=0))
cs_array[1].configure(lib.struct(1, ns[1], init=ns[0]))
cs_array[2].configure(lib.struct(1, ns[2], init=ns[0]+ns[1]))
with lib.updater('    > population size', len(N2)*nr) as upd:
    for n2 in N2:
        acc_pi1 = 0.0
        acc_pi2 = 0.0
        acc_pi3 = 0.0
        for aln in lib.simul(num_pop=len(ns), num_chrom=ns, theta=theta,
                N=[1, n2, n2**2], migr=0, nr=nr,
                    events=[{'cat':'merge', 'src':1, 'dst':0, 'T':10},
                            {'cat':'merge', 'src':2, 'dst':0, 'T':10}]):
            upd.add_one()
            if aln.ls > 0:
                acc_pi1 += cs_array[0].process_align(aln)['Pi']
                acc_pi2 += cs_array[1].process_align(aln)['Pi']
                acc_pi3 += cs_array[2].process_align(aln)['Pi']
        Pi1.append(acc_pi1/nr)
        Pi2.append(acc_pi2/nr)
        Pi3.append(acc_pi3/nr)

pyplot.plot(N2, Pi1, 'ko', label=r'$\pi_1$')
pyplot.plot(N2, Pi2, 'ro', label=r'$\pi_2$')
pyplot.plot(N2, Pi3, 'bo', label=r'$\pi_3$')
pyplot.plot(N2, [theta] * len(N2), 'k:')
pyplot.plot(N2, [theta*n2 for n2 in N2], 'r:')
pyplot.plot(N2, [theta*(n2**2) for n2 in N2], 'b:')
pyplot.legend()
pyplot.xlabel(r'$N_2$')
pyplot.ylabel(r'$\pi$')
pyplot.savefig(lib.fname('4-1.png'))
pyplot.clf()

fstream.write(r'''
Effect of the size of populations on per-population diversity
-------------------------------------------------------------

.. figure:: 4-1.png
    :scale: 75%

    In this model, the first population has a size of 1 (the reference
    population size) while the second and third population size vary
    (the size of the third population is the square of the second's).
    Simulations are without gene flow (the two populations are merged at
    :math:`T=10`). The diversity in both populations fit the
    expectations (dotted lines give :math:`\theta` for all populations).
''')

###########

def expect(s, THETA):
    return [theta * ((2.0-s)/2) for theta in THETA]

THETA = [0.2, 0.4, 0.6, 0.8, 1, 1.2, 1.4, 1.6, 1.8, 2, 2.2, 2.4, 2.6, 2.8, 3]
theta = 2
ns = [20, 20, 20, 20]
array = []
for i in xrange(len(ns)):
                                            #  Pi  D  accPi accD n
    array.append([egglib.stats.ComputeStats(), [], [], 0.0, 0.0, 0])
    array[i][0].add_stats('Pi', 'D', 'S')
    array[i][0].configure(lib.struct(1, ns[i], init=sum(ns[:i])))
with lib.updater('    > selfing rate', len(THETA)*nr) as upd:
    for theta in THETA:
        for i in xrange(len(array)):
            array[i][3] = 0.0
            array[i][4] = 0.0
            array[i][5] = 0
        for aln in lib.simul(num_pop=len(ns), num_chrom=ns, theta=theta,
                s=[0.90, 0, 0.99, 0.5], migr=0, nr=nr,
                    events=[{'cat':'merge', 'src':1, 'dst':0, 'T':10},
                            {'cat':'merge', 'src':2, 'dst':0, 'T':10},
                            {'cat':'merge', 'src':3, 'dst':0, 'T':10}]):
            upd.add_one()
            if aln.ls > 0:
                for i in xrange(len(array)):
                    stats = array[i][0].process_align(aln)
                    array[i][3] += stats['Pi']
                    if stats['S'] > 0:
                        array[i][4] += stats['D']
                        array[i][5] += 1
        for i in xrange(len(array)):
            array[i][1].append(array[i][3]/nr)
            array[i][2].append(array[i][4]/array[i][5])

pyplot.plot(THETA, array[0][1], 'ro', label=r'$\pi_1$ ($s=0.9$)', mfc='None')
pyplot.plot(THETA, array[1][1], 'ko', label=r'$\pi_2$ ($s=0$)', mfc='None')
pyplot.plot(THETA, array[2][1], 'mo', label=r'$\pi_3$ ($s=0.99$)', mfc='None')
pyplot.plot(THETA, array[3][1], 'bo', label=r'$\pi_4$ ($s=0.5$)', mfc='None')
pyplot.plot(THETA, expect(0.90, THETA) , 'r:')
pyplot.plot(THETA, expect(0.00, THETA) , 'k:')
pyplot.plot(THETA, expect(0.99, THETA) , 'm:')
pyplot.plot(THETA, expect(0.50, THETA) , 'b:')
pyplot.legend()
pyplot.xlabel(r'$\theta$')
pyplot.ylabel(r'$\pi$')
pyplot.savefig(lib.fname('4-2.png'))
pyplot.clf()

pyplot.plot(THETA, array[0][2], 'ro', label=r'$D_1$ ($s=0.9$)', mfc='None')
pyplot.plot(THETA, array[1][2], 'ko', label=r'$D_2$ ($s=0$)', mfc='None')
pyplot.plot(THETA, array[2][2], 'mo', label=r'$D_3$ ($s=0.99$)', mfc='None')
pyplot.plot(THETA, array[3][2], 'bo', label=r'$D_4$ ($s=0.5$)', mfc='None')
pyplot.plot(THETA, [0] * len(THETA) , 'k:')
pyplot.legend()
pyplot.xlabel(r'$\theta$')
pyplot.ylabel(r'$D$')
pyplot.ylim(-1, 1)
pyplot.savefig(lib.fname('4-3.png'))
pyplot.clf()

fstream.write(r'''
Effect of selfing rate of populations on per-population diversity
-----------------------------------------------------------------

.. figure:: 4-2.png
    :scale: 75%

    The graphic represents :math:`\pi` in each population of a system
    with four populations with different selfing rates for varying
    mutation rates. In each population, the diversity fits the
    expectation (dotted lines) which is a function of :math:`\theta`
    and :math:`s`: :math:`\frac{2-s}{2} \theta`.

.. figure:: 4-3.png
    :scale: 75%

    The same for Tajima's :math:`D` whose expectation is 0 (or slightly
    less than 0).

''')

###########

fstream.close()
