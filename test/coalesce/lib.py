import sys, egglib, os, subprocess

ms_path = '/home/user/data/software/msdir-20140908/ms'
RND = egglib.tools.Random()

def fname(name):
    return os.path.join(os.path.dirname(__file__), 'output', name)

def show(s):
    sys.stdout.write(s)
    sys.stdout.flush()

def showl(s):
    show(s + '\n')

def struct(K, ns, init=0):
    d = {}
    c = init
    for i in xrange(K):
        d[i] = {}
        for j in xrange(c, c+ns):
            d[i][j] = [j]
        c += ns
    return egglib.stats.make_structure({0: d}, None)

def struct_pw(ns, K, idx):
    d = {}
    c = 0
    for i in xrange(K):
        if i in idx: d[i] = {}
        for j in xrange(c, c+ns):
            if i in idx: d[i][j] = [j]
        c += ns
    return egglib.stats.make_structure({0: d}, None)

def simul(num_pop=None, num_chrom=None, num_indiv=None, theta=0.0,
          N=None, num_mut=0, num_sites=0, nr=None, migr=0.0,
            migr_matrix=None, G=None, s=None, stop_growth=False,
            recomb=0, site_weight=None, events=[], mut_model='KAM',
            site_positions=None, num_alleles=2, trans_matrix=None,
            random_start=False, TPM_params=None):
    if num_chrom is None: num_chrom = [0] * num_pop
    if num_indiv is None: num_indiv = [0] * num_pop
    if N is None: N = [1] * num_pop
    if G is None: G = [0] * num_pop
    if s is None: s = [0] * num_pop
    coal = egglib.coalesce.Simulator(num_pop=num_pop, num_chrom=num_chrom,
        num_indiv=num_indiv, num_sites=num_sites, theta=theta,
        num_mut=num_mut, migr=migr, G=G, s=s, N=N, recomb=recomb, mut_model=mut_model,
        num_alleles=num_alleles, seed=RND.integer_32bit(), rand_start=random_start)
    if TPM_params is not None:
        coal.params['TPM_proba'] = TPM_params[0]
        coal.params['TPM_param'] = TPM_params[1]
    if trans_matrix is not None: 
        coal.params['trans_matrix'] = trans_matrix
    if migr_matrix is not None:
        assert migr == 0.0
        coal.params['migr_matrix'] = migr_matrix
    if stop_growth:
        coal.params['events'].add('growth', T=10.0, G=0.0)
        coal.params['events'].add('size', T=10.0, N=1.0)
    if site_weight is not None:
        coal.params['site_weight'] = site_weight
    if site_positions is not None:
        coal.params['site_pos'] = site_positions
    for event in events:
        coal.params.add_event(**event)
    if nr == 1:
        return coal.simul()
    else:
        return coal.iter_simul(nr)

def ms(theta, ns, nr, G=0, N=None, segsites=0, migr=0, recomb=0.0, migr_matrix=None, as_is=None):
    opt = [ms_path, str(sum(ns)), str(nr)]
    if theta == 0 and segsites == 0: raise ValueError, 'segsites OR theta'
    if theta != 0: opt.extend(['-theta', str(theta)])
    elif segsites != 0: opt.extend(['-segsites', str(segsites)])
    else: raise ValueError, 'need segsites or theta'

    if recomb != 0: opt.extend(['-r', str(recomb), '1000'])
    if G != 0: opt.extend(['-G', str(G)])

    if len(ns) > 1:
        if migr_matrix is not None:
            assert migr==0
            opt.extend(['-I', str(len(ns))] + map(str, ns))
            opt.append('-ma')
            migr_matrix = map(str, reduce(list.__add__, migr_matrix))
            opt.extend(migr_matrix)
        else:
            opt.extend(['-I', str(len(ns))] + map(str, ns) + [str(migr)])
        if N is not None:
            for i, n in enumerate(N):
                opt.extend(['-n', str(i+1), str(n)])
    else:
        if migr != 0: raise ValueError, 'migration with only one population'
        if N is not None: raise ValueError, 'cannot set pop size for unique pop'

    if as_is is not None:
        opt.extend(as_is)

    p = subprocess.Popen(opt, stdout=subprocess.PIPE)
    stdout, stderr = p.communicate()
    lines = stdout.split('\n')
    i = 3
    res = []
    while i < len(lines):
        aln = []
        assert lines[i] == '//'
        if lines[i+1] == 'segsites: 0':
            res.append(egglib.Align.create([], alphabet=ms_alphabet))
            i += 4
            continue
        i += 3
        while lines[i] != '':
            aln.append(['', map(int, lines[i])])
            i += 1
        assert len(aln) == sum(ns)
        res.append(egglib.Align.create(aln, alphabet=ms_alphabet))
        i += 1
    assert len(res) == nr
    return res
ms_alphabet = egglib.alphabets.Alphabet('int', [0, 1], [])

class updater(object):
    def __init__(self, string, num):
        self.string = string
        self.num = num

    def __enter__(self):
        show(self.string + '   0.00%')
        self.cnt = 0
        return self

    def __exit__(self, *args):
        showl('\b'*8  + ' '*8)

    def add_one(self):
        self.cnt += 1
        show('\b\b\b\b\b\b\b{0:>6.2f}%'.format(100.0 * self.cnt / self.num))
