import egglib, lib, os
from matplotlib import pyplot

lib.showl('# Base')

f = open(lib.fname('report.rst'), 'a')
f.write('''
===============
Base parameters
===============
''')

nr_base = 1000

lib.showl('    > theta=0')
for aln in lib.simul(num_pop=1, num_chrom=[20], theta=0.0, nr=nr_base/10):
    assert aln.ns == 20
    assert aln.ls == 0

for aln in lib.simul(num_pop=3, num_chrom=[20,12,5], theta=0.0, nr=nr_base/10, migr=1):
    assert aln.ns == 37
    assert aln.ls == 0

with lib.updater('    > effect of num_sites', 1000) as upd:
    for L in range(1, 1000):
        upd.add_one()
        assert lib.simul(num_pop=1, num_chrom=[20], theta=0, num_sites=L, nr=1).ls == L

with lib.updater('    > effect of num_mutations', 1000) as upd:
    for S in range(100):
        upd.add_one()
        assert lib.simul(num_pop=1, num_chrom=[20], num_mut=S, nr=1).ls == S

# EFFECT OF THETA

thetas = 0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1, 1.1, 1.2, 1.3, 1.4, 1.5, 1.6, 1.7, 1.8, 1.9, 2.0
cs = egglib.stats.ComputeStats()
cs.add_stats('thetaW', 'D', 'Pi', 'S', 'lseff')
with lib.updater('    > effect of theta', len(thetas)) as upd:
    thetaW = []
    D = []
    Pi = []
    S = []
    for theta in thetas:
        upd.add_one()
        t = 0.0
        d = 0.0
        p = 0.0
        s = 0.0
        n = 0
        for aln in lib.simul(num_pop=1, num_chrom=[20], theta=theta, nr=nr_base):
            stats = cs.process_align(aln)
            if stats['lseff'] > 0:
                t += stats['thetaW']
                d += stats['D']
                p += stats['Pi']
                s += stats['S']
                n += 1
        if n == 0: D.append(None)
        else: D.append(d/n)
        thetaW.append(t/nr_base)
        Pi.append(p/nr_base)
        S.append(s/nr_base)

pyplot.plot(thetas, thetaW, 'go-', label=r'$\hat{\theta}_W$')
pyplot.plot(thetas, Pi, 'bo-', label=r'$\pi$')
pyplot.plot(thetas, thetas, 'k-', label=r'$\theta$')
pyplot.legend()
pyplot.xlabel(r'$\theta$')
pyplot.ylabel('statistic value')
pyplot.savefig(lib.fname('1-1.png'))
pyplot.clf()

X, Y = zip(*[(i, j) for i,j in zip(thetas, D) if j is not None])
pyplot.plot(X, Y, 'ro')
pyplot.axhline(0, ls=':', c='k')
pyplot.xlabel(r'$\theta$')
pyplot.ylabel(r'$D$')
pyplot.ylim(-1, 1)
pyplot.savefig(lib.fname('1-2.png'))
pyplot.clf()

ES = []
a1 = 0.0
for i in xrange(1, 20): a1 += 1.0/i
for theta in thetas:
    ES.append(a1 * theta)
pyplot.plot(thetas, S, 'ro-', label='S')
pyplot.plot(thetas, ES, 'k-', label='E(S)')
pyplot.xlabel(r'$\theta$')
pyplot.ylabel(r'$S$')
pyplot.savefig(lib.fname('1-3.png'))
pyplot.clf()

f.write(r'''
Simplest model, variation on :math:`\theta`
--------------------------------------------

.. figure:: 1-1.png
    :scale: 75%

    Correlation of :math:`\theta` estimators with :math:`\theta`.

.. figure:: 1-2.png
    :scale: 75%

    Effect on Tajima's *D* (we expect no effect).

.. figure:: 1-3.png
    :scale: 75%

    Effect on the number of polymorphic sites, compared to the expectation.
    The expectation is :math:`E(S) = \theta \sum_{i=1}^n \frac{1}{i}`
''')

# EXPONENTIAL GROWTH RATE

alphas = [-1, -0.75, -0.5, -0.25, 0, 0.5, 1, 1.5, 2, 2.5, 3, 3.5, 4]
theta = 5
ns = [20]
cs = egglib.stats.ComputeStats()
cs.add_stats('thetaW', 'D')
D_ms = []
D_coal = []
thetaW_ms = []
thetaW_coal = []
with lib.updater('    > effect of growth rate', 2*len(alphas)) as upd:
    for alpha in alphas:
        d = 0.0
        n = 0
        t = 0.0
        for aln in lib.ms(theta=theta, G=alpha, ns=ns, nr=nr_base/2, as_is=['-eG', '10', '0', '-eN', '10', '1']):
            stats = cs.process_align(aln)
            if stats['thetaW'] != None:
                d += stats['D']
                n +=1
                t += stats['thetaW']
        D_ms.append(d/n)
        thetaW_ms.append(t/(nr_base/2))
        upd.add_one()

        d = 0.0
        n = 0
        t = 0.0
        for aln in lib.simul(num_pop=1, num_chrom=ns, theta=theta, G=[alpha], nr=nr_base/2, stop_growth=True):
            stats = cs.process_align(aln)
            if stats['thetaW'] != None:
                d += stats['D']
                n +=1
                t += stats['thetaW']
        D_coal.append(d/n)
        thetaW_coal.append(t/(nr_base/2))
        upd.add_one()

pyplot.plot(alphas, D_ms, 'bd-', mfc='None')
pyplot.plot(alphas, D_coal, 'rd-', mfc='None')
pyplot.plot([], [], 'b-', label='ms')
pyplot.plot([], [], 'r-', label='EggLib')
pyplot.xlabel(r'$\alpha$')
pyplot.ylabel(r"Tajima's $D$")
pyplot.legend()
pyplot.savefig(lib.fname('1-4.png'))
pyplot.clf()

pyplot.plot(alphas, thetaW_ms, 'bs-', mfc='None')
pyplot.plot(alphas, thetaW_coal, 'rs-', mfc='None')
pyplot.plot([], [], 'b-', label='ms')
pyplot.plot([], [], 'r-', label='EggLib')
pyplot.xlabel(r'$\alpha$')
pyplot.ylabel(r'$\hat{\theta}_W$')
pyplot.legend()
pyplot.savefig(lib.fname('1-5.png'))
pyplot.clf()

f.write(r'''
Effect of the exponential growth parameter :math:`\alpha`
---------------------------------------------------------

.. figure:: 1-4.png
    :scale: 75%

    Effect of the exponential growth/decline rate :math:`\alpha` on
    Tajima's D. There is in either case an event set at T=10 to reset
    growth rate to 0 and population size to 1. Since we do not have
    expectations at hand, we compare EggLib's output (red) to Hudson's ms (blue).

.. figure:: 1-5.png
    :scale: 75%

    The same with :math:`\hat{\theta}_W`.
''')

# SELFING

rates = [0, 0.05, 0.1, 0.15, 0.2, 0.25, 0.3, 0.35, 0.4, 0.45, 0.5,
         0.55, 0.6, 0.65, 0.7, 0.75, 0.8, 0.85, 0.9, 0.95, 1]
theta = 5
cs = egglib.stats.ComputeStats()
cs.add_stats('thetaW', 'D', 'Pi', 'lseff')
T = []
D = []
P = []
with lib.updater('    > effect of selfing rate', len(rates)) as upd:
    for s in rates:
        n = 0
        d = 0
        t = 0
        p = 0
        for aln in lib.simul(num_pop=1, num_chrom=[20], theta=theta, s=[s], nr=nr_base):
            stats = cs.process_align(aln)
            if stats['lseff'] > 0:
                n += 1
                d += stats['D']
                t += stats['thetaW']
                p += stats['Pi']
        T.append(t/nr_base)
        P.append(p/nr_base)
        D.append(d/n)
        upd.add_one()

E = [theta * ((2.0-s)/2) for s in rates]
pyplot.plot(rates, P, 'mo-', label=r'$\pi$', mfc='None')
pyplot.plot(rates, T, 'gs-', label=r'$\hat{\theta}_W$', mfc='None')
pyplot.plot(rates, E, 'k:', label='expect')
pyplot.xlabel(r'$s$')
pyplot.ylabel(r"statistics")
pyplot.legend()
pyplot.savefig(lib.fname('1-6.png'))
pyplot.clf()

pyplot.plot(rates, D, 'ko-', mfc='None')
pyplot.xlabel(r'$s$')
pyplot.ylabel(r"Tajima's $D$")
pyplot.axhline(0, c='k', ls=':')
pyplot.ylim(-1, 1)
pyplot.savefig(lib.fname('1-7.png'))
pyplot.clf()

f.write(r'''
Effect of the selfing rate (haploid samples)
--------------------------------------------

.. figure:: 1-6.png
    :scale: 75%

    Since s really just rescales the effective size (in case of haploid
    samples like here), the :math:`\theta` estimators just follow this
    rescaling (:math:`\theta_s = \frac{2}{2-s}\theta`, as dotted line).

.. figure:: 1-7.png
    :scale: 75%

    Same simulations with Tajima's :math:`D`, which should be slightly
    below 0 (dotted line).
''')

T = []
D = []
P = []
S = []
ni = 15
nr = nr_base * 5
cs = egglib.stats.ComputeStats()
cs.add_stats('thetaW', 'D', 'Pi', 'lseff', 'S')
with lib.updater('    > effect of selfing rate (diploid samples)', len(rates)) as upd:
    for s in rates:
        n = 0
        d = 0.0
        t = 0.0
        p = 0.0
        segr = 0.0
        for aln in lib.simul(num_pop=1, num_indiv=[ni], theta=theta, s=[s], nr=nr):
            stats = cs.process_align(aln)
            if stats['lseff'] > 0:
                n += 1
                d += stats['D']
                t += stats['thetaW']
                p += stats['Pi']
                segr += stats['S']
        T.append(t/nr)
        P.append(p/nr)
        D.append(d/n)
        S.append(segr/nr)
        upd.add_one()

# make a selfing haploid sample and duplicate all samples
n = 0
d = 0.0
t = 0.0
p = 0.0
segr = 0.0
for aln in lib.simul(num_pop=1, num_chrom=[ni], theta=theta/2.0, s=[0], nr=nr):
    aln2 = egglib.Align.create(aln)
    aln2.add_samples(aln)
    stats = cs.process_align(aln2)
    if stats['lseff'] > 0:
        n += 1
        d += stats['D']
        t += stats['thetaW']
        p += stats['Pi']
        segr += stats['S']
Tref = t/nr
Pref = p/nr
Dref = d/n
Sref = segr/nr

a1 = sum([1.0/i for i in xrange(1, 2*ni)]) # a1 assuming ns
a1s = [] # a1 corrected for each s value
Epi = [] # expected value of pi (accounting for error in number of samples)
for s, e in zip(rates, E):
    ns2 = int(round((2 - s/(2.0-s)) * ni))
    a1s.append(sum([1.0/i for i in xrange(1, ns2)]))
    e1 = e # real theta
    e1 /= ns2 / (ns2 - 1.0) # biased (what we would have without the correct correction based on ns2)
    e1 *= (2*ni)/(2*ni - 1.0) # unbiased with the incorrect correction based in 2ni, actually performed with this naive data)
    Epi.append(e1)
EthetaW = [e*i/a1 for (e,i) in zip(E, a1s)] # expected value of thetaW for each s (accounting for error in a1)

pyplot.plot(rates, P, 'mo-', mfc='None')
pyplot.plot(rates, E, 'k:')
pyplot.plot(rates, Epi, 'm:')
pyplot.plot([1], [Pref], 'rs', ms=10, mfc='None')
pyplot.xlabel(r'$s$')
pyplot.ylabel(r'$\pi$')
pyplot.savefig(lib.fname('1-8.png'))
pyplot.clf()

pyplot.plot(rates, T, 'gs-')
pyplot.plot([1], [Tref], 'rs', ms=10, mfc='None')
pyplot.plot(rates, E, 'k:')
pyplot.plot(rates, EthetaW, 'g:')
pyplot.xlabel(r'$s$')
pyplot.ylabel(r'$\hat{\theta}_W$')
pyplot.savefig(lib.fname('1-9.png'))
pyplot.clf()

pyplot.plot(rates, S, 'bs-')
pyplot.plot([1], [Sref], 'rs', ms=10, mfc='None')
pyplot.plot(rates, [e*i for e,i in zip(E, a1s)], 'k:')
pyplot.xlabel(r'$s$')
pyplot.ylabel(r'$S$')
pyplot.savefig(lib.fname('1-10.png'))
pyplot.clf()

pyplot.plot(rates, D, 'ko-', mfc='None')
pyplot.plot([1], [Dref], 'rs', ms=10, mfc='None')
pyplot.xlabel(r'$s$')
pyplot.ylabel(r"Tajima's $D$")
pyplot.axhline(0, c='k', ls=':')
pyplot.ylim(-1, 1)
pyplot.savefig(lib.fname('1-11.png'))
pyplot.clf()

f.write(r'''
Effect of the selfing rate (diploid samples)
--------------------------------------------

In all the plots for this section, there is a large open red square at
:math:`s=1`. This is obtained with simulations of a non-selfing population
with half number of samples and half :math:`\theta`, but where all samples
are duplicated. This mimicks the fact that all diploid samples are identical
and then all evolves like a fully selfing population. It should fit the case
with :math:`s=1`.

.. figure:: 1-8.png
    :scale: 75%

    :math:`pi` descreased with :math:`s` due to rescaling of the effective
    size like in the haploid case (black dotted line), but there is a
    slight bias due to the fact that the unbiasing of nucleotide diversity
    is a little bit overestimated. Diversity is unbiased using the factor
    :math:`\frac{n}{n-1}` where :math:`n` is number of samples, that is
    twice the number of sampled individuals. But due to immediate coalescence
    events which occur before any mutation might occur, the actual number of
    samples actually entering the coalescent process is decreased. The
    probability of immediate coalescence is :math:`\frac{s}{2-s}`. By
    computing the *effective* number of samples by applying this probability,
    and using it to computed the proper unbiasing factor, we find another
    formulation for the rescaled :math:`\theta` which fits much better
    (pink dotted line).

.. figure:: 1-9.png
    :scale: 75%

    :math:`\hat{\theta}_W` behaves like :math:`\pi` except that the modified
    correction also accounts for modification of the number of samples in
    the formula of :math:`a_1` (green dotted line). The black dotted line
    is also :math:`\theta` modified to account for the rescaling of the
    effective size (:math:`N_E = \frac{2}{2-s}N`).

.. figure:: 1-10.png
    :scale: 75%

    :math:`S` is compared with the expectation taking into account the
    correction on :math:`a_1` (blue dotted line).

.. figure:: 1-11.png
    :scale: 75%

    Effect of the selfing rate on Tajima's :math:`D`. The statistic is
    increased due to diploid samples that are immediately coalesced due
    to the separation of time scales. There is no theoretical expectation,
    but the value at :math:`s=1` is equal to what is obtained using a
    rescaled haploid coalescent with samples artificially doubled (red
    square).
''')

f.close()
