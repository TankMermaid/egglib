import egglib, lib
from matplotlib import pyplot

lib.showl('# Historical events')
fstream = open(lib.fname('report.rst'), 'a')
fstream.write('''
=================
Historical events
=================
''')

def get2(dst, cs, upd, f, **args):
    acc = {}
    n = {}
    for k in dst:
        acc[k] = 0.0
        n[k] = 0
    for aln in f(**args):
        if aln.ls > 0:
            stats = cs.process_align(aln)
            for k in dst:
                if stats[k] is not None:
                    acc[k] += stats[k]
                    n[k] += 1
        upd.add_one()
    for k in dst:
        dst[k].append(acc[k]/n[k])

nr = 1000
ns = [50]
theta = 5
T = 0.2
NA = 0.001, 0.5, 1, 1.5, 2, 2.5, 3, 3.5, 4, 4.5, 5
D_eg = []
D_ms = []
cs = egglib.stats.ComputeStats()
cs.add_stats('D')
with lib.updater('    > ancestral size', len(NA)*nr*2) as upd:
    for Na in NA:
        D = 0.0
        n = 0
        for aln in lib.simul(nr=nr, num_pop=len(ns), num_chrom=ns, theta=theta, events=[{'cat':'size', 'T':T, 'N':Na}]):
            if aln.ls > 0:
                D += cs.process_align(aln)['D']
                n += 1
            upd.add_one()
        D_eg.append(D/n)

        D = 0.0
        n = 0
        for aln in lib.ms(theta=theta, ns=ns, nr=nr, as_is=['-eN', str(T), str(Na)]):
            if aln.ls > 0:
                D += cs.process_align(aln)['D']
                n += 1
            upd.add_one()
        D_ms.append(D/n)

pyplot.plot(NA, D_ms, 'kx')
pyplot.plot(NA, D_eg, 'ko', mfc='None')
pyplot.axvline(1, ls=':', c='k')
pyplot.axhline(0, ls=':', c='k')
pyplot.legend().set_frame_on(False)
pyplot.xlabel(r'$N_{ANC}$')
pyplot.ylabel(r'Tajima\'s $D$')
pyplot.savefig(lib.fname('6-1.png'))
pyplot.clf()

fstream.write(r'''
Population size change
----------------------

.. figure:: 6-1.png
    :scale: 75%

    Population size in the past :math:`T=0.2` and effect on Tajima's :math:`D`.
    Comparison of results with EggLib and ms.
''')

###########

nr = 1000
ns = [50]
theta = 5
T = 0, 0.2, 0.4, 0.6, 0.8, 1, 1.2, 1.4, 1.6, 1.8, 2, 2.2, 2.4, 2.6, 2.8, 3
Nas = [0.2, 1, 5]
Pis = [[] for Na in Nas]

cs = egglib.stats.ComputeStats()
cs.add_stats('Pi')
with lib.updater('    > size change date', len(T)*nr*3) as upd:
    for t in T:
        for Pi, Na in zip(Pis, Nas):
            pi = 0.0
            for aln in lib.simul(nr=nr, num_pop=len(ns), num_chrom=ns, theta=theta, events=[{'cat': 'size', 'T': t, 'N': Na}]):
                if aln.ls > 0: pi += cs.process_align(aln)['Pi']
                upd.add_one()
            Pi.append(pi/nr)

pyplot.plot(T, Pis[0], 'rs:', label=r'$N_{ANC}=0.2$', mfc='w')
pyplot.plot(T, Pis[1], 'ko:', label=r'$N_{ANC}=1$', mfc='w')
pyplot.plot(T, Pis[2], 'bd:', label=r'$N_{ANC}=5$', mfc='w')
pyplot.axhline(Nas[0]*theta, ls=':', c='r')
pyplot.axhline(Nas[1]*theta, ls=':', c='k')
pyplot.axhline(Nas[2]*theta, ls=':', c='b')
pyplot.axhline(0, ls=':', c='k')
pyplot.legend().set_frame_on(False)
pyplot.xlabel(r'$T$')
pyplot.ylabel(r'$\pi$')
pyplot.savefig(lib.fname('6-2.png'))
pyplot.clf()

fstream.write(r'''
.. figure:: 6-2.png
    :scale: 75%

    Effect of the date of population size change on :math:`\pi`. The
    population has always a current size of 1 and :math:`\theta=5`
    (expected :math:`\pi=5`). Three cases are simulated with an ancestral
    size of 0.2, 1, and 5 respectively, leading to expected values of
    1, 5, and 25 for :math:`\pi` in the ancestral population. The horizontal
    lines give this values. When :math:`T=0`, the diversity is equal to
    what is expected in the ancestral population (because the event is
    immediate). When the event is very old, the diversity is equal to
    what is expected in the current population (the same in all three
    case). The middle case :math:`N_{ANC}=1` is a control.
''')

###########

nr = 1000
ns = [20, 20, 20]
theta = 1
T = 0.2
NA = 0.001, 0.5, 1, 1.5, 2, 2.5, 3, 3.5, 4, 4.5, 5
Pis = [[[], [], []],
       [[], [], []],
       [[], [], []],
       [[], [], []]]
cs = []
for i in xrange(3):
    cs.append(egglib.stats.ComputeStats())
    cs[-1].add_stats('Pi')
    cs[-1].configure(egglib.stats.make_structure({0: {0: dict([(i, [i]) for i in range(i*20, (i+1)*20)])}}, {}))

with lib.updater('    > single population size', len(NA)*nr*4) as upd:
    for Na in NA:
        for i, idx in enumerate([None, 0, 1, 2]):
            pi = [0.0, 0.0, 0.0]
            events = [{'cat': 'size', 'T': T, 'N': Na},
                      {'cat': 'merge', 'T': 10, 'src': 2, 'dst': 0},
                      {'cat': 'merge', 'T': 10, 'src': 1, 'dst': 0}]
            if idx is not None: events[0]['idx'] = idx
            for aln in lib.simul(nr=nr, num_pop=len(ns), num_chrom=ns, migr=0,
                        theta=theta, events=events):
                if aln.ls > 0:
                    pi[0] += cs[0].process_align(aln)['Pi']
                    pi[1] += cs[1].process_align(aln)['Pi']
                    pi[2] += cs[2].process_align(aln)['Pi']
                upd.add_one()
            Pis[i][0].append(pi[0]/nr)
            Pis[i][1].append(pi[1]/nr)
            Pis[i][2].append(pi[2]/nr)

pyplot.plot(NA, Pis[0][0], 'rx:', mfc='w', label='pop 1')
pyplot.plot(NA, Pis[0][1], 'gx:', mfc='w', label='pop 2')
pyplot.plot(NA, Pis[0][2], 'bx:', mfc='w', label='pop 3')
pyplot.legend().set_frame_on(False)
pyplot.xlabel(r'$N_{ANC}$')
pyplot.ylabel(r'$\pi$')
pyplot.axvline(1, ls=':', c='k')
pyplot.axhline(1, ls=':', c='k')
pyplot.savefig(lib.fname('6-3.png'))
pyplot.clf()

pyplot.plot(NA, Pis[1][0], 'rx:', mfc='w', label='pop 1')
pyplot.plot(NA, Pis[1][1], 'gx:', mfc='w', label='pop 2')
pyplot.plot(NA, Pis[1][2], 'bx:', mfc='w', label='pop 3')
pyplot.legend().set_frame_on(False)
pyplot.xlabel(r'$N_{ANC}$')
pyplot.ylabel(r'$\pi$')
pyplot.axvline(1, ls=':', c='k')
pyplot.axhline(1, ls=':', c='k')
pyplot.savefig(lib.fname('6-4.png'))
pyplot.clf()

pyplot.plot(NA, Pis[2][0], 'rx:', mfc='w', label='pop 1')
pyplot.plot(NA, Pis[2][1], 'gx:', mfc='w', label='pop 2')
pyplot.plot(NA, Pis[2][2], 'bx:', mfc='w', label='pop 3')
pyplot.legend().set_frame_on(False)
pyplot.xlabel(r'$N_{ANC}$')
pyplot.ylabel(r'$\pi$')
pyplot.axvline(1, ls=':', c='k')
pyplot.axhline(1, ls=':', c='k')
pyplot.savefig(lib.fname('6-5.png'))
pyplot.clf()

pyplot.plot(NA, Pis[3][0], 'rx:', mfc='w', label='pop 1')
pyplot.plot(NA, Pis[3][1], 'gx:', mfc='w', label='pop 2')
pyplot.plot(NA, Pis[3][2], 'bx:', mfc='w', label='pop 3')
pyplot.legend().set_frame_on(False)
pyplot.xlabel(r'$N_{ANC}$')
pyplot.ylabel(r'$\pi$')
pyplot.axvline(1, ls=':', c='k')
pyplot.axhline(1, ls=':', c='k')
pyplot.savefig(lib.fname('6-6.png'))
pyplot.clf()

fstream.write(r'''
.. figure:: 6-3.png
    :scale: 75%

    Simulations with three populations. The size of all populations is
    changed.

.. figure:: 6-4.png
    :scale: 75%

    Simulations with three populations. The size of population 1 only is
    changed.

.. figure:: 6-5.png
    :scale: 75%

    Simulations with three populations. The size of population 2 only is
    changed.

.. figure:: 6-6.png
    :scale: 75%

    Simulations with three populations. The size of population 3 only is
    changed.
''')

###########

nr = 1000
ns = [20, 20, 20]
theta = 1
T = 0, 0.2, 0.4, 0.6, 0.8, 1, 1.2, 1.4, 1.6, 1.8, 2, 2.2, 2.4, 2.6, 2.8, 3
Nas = [0.2, 1, 5]
Pi = [[] for Na in Nas]
D = [[] for Na in Nas]
Pims = [[] for Na in Nas]
Dms = [[] for Na in Nas]

cs0 = egglib.stats.ComputeStats()
cs0.add_stats('Pi', 'D', 'S')

cs = []
for pop in xrange(3):
    cs.append(egglib.stats.ComputeStats())
    cs[-1].add_stats('Pi', 'D', 'S', 'nseff', 'lseff')
    structd = {0: {0: dict([(i, [i]) for i in range(pop*20, (pop+1)*20)])}}
    cs[-1].configure(egglib.stats.make_structure(structd, {}))

with lib.updater('    > size change date', len(T)*nr*2+nr) as upd:
    for t in T:
        pi = [0.0, 0.0, 0.0]
        d = [0.0, 0.0, 0.0]
        n = [0, 0, 0]
        for aln in lib.simul(nr=nr, num_pop=len(ns), num_chrom=ns, theta=theta,
            events=[ {'cat': 'merge', 'T': 10, 'src': 0, 'dst': 2},
                     {'cat': 'merge', 'T': 10, 'src': 1, 'dst': 2},
                     {'cat': 'size', 'T': t, 'N': Nas[0], 'idx' :0},
                     {'cat': 'size', 'T': t, 'N': Nas[1], 'idx' :1},
                     {'cat': 'size', 'T': t, 'N': Nas[2], 'idx' :2}]):
            if aln.ls > 0:
                for i in xrange(3):
                    stats = cs[i].process_align(aln)
                    pi[i] += stats['Pi']
                    if stats['S'] > 0:
                        d[i] += stats['D']
                        n[i] += 1
                    sub = aln.subset(range(i*20, i*20+20))
                    stats0 = cs0.process_align(sub)
                    for k in 'Pi', 'D', 'S': assert stats[k] == stats0[k]
            upd.add_one()
        for i in xrange(3):
            Pi[i].append(pi[i]/nr)
            D[i].append(d[i]/n[i])

        pi = [0.0, 0.0, 0.0]
        d = [0.0, 0.0, 0.0]
        n = [0, 0, 0]
        for aln in lib.ms(theta=theta, ns=ns, nr=nr, as_is=[
                    '-en', str(t), '1', str(Nas[0]),
                    '-en', str(t), '2', str(Nas[1]),
                    '-en', str(t), '3', str(Nas[2]),
                    '-ej', '10', str(1), str(3),
                    '-ej', '10', str(2), str(3)]):
            if aln.ls > 0:
                for i in xrange(3):
                    stats = cs[i].process_align(aln)
                    pi[i] += stats['Pi']
                    if stats['S'] > 0:
                        d[i] += stats['D']
                        n[i] += 1
            upd.add_one()
        for i in xrange(3):
            Pims[i].append(pi[i]/nr)
            Dms[i].append(d[i]/n[i])

    Piref = [0.0, 0.0, 0.0]
    Dref = [0.0, 0.0, 0.0]
    n = [0, 0, 0]
    for aln in lib.simul(nr=nr, num_pop=len(ns), num_chrom=ns, theta=theta,
        N=Nas,
        events=[ {'cat': 'merge', 'T': 10, 'src': 0, 'dst': 2},
                 {'cat': 'merge', 'T': 10, 'src': 1, 'dst': 2}]):
        if aln.ls > 0:
            for i in xrange(3):
                stats = cs[i].process_align(aln)
                Piref[i] += stats['Pi']
                if stats['S'] > 0:
                    Dref[i] += stats['D']
                    n[i] += 1
        upd.add_one()
    for i in xrange(3):
        Piref[i] /= nr
        Dref[i] /= n[i]

pyplot.plot(T, Pi[0], 'rs:', label=r'$N_1$', mfc='w')
pyplot.plot(T, Pi[1], 'ko:', label=r'$N_2$', mfc='w')
pyplot.plot(T, Pi[2], 'bd:', label=r'$N_3$', mfc='w')
pyplot.plot(T, Pims[0], 'r+', mfc='w')
pyplot.plot(T, Pims[1], 'k+', mfc='w')
pyplot.plot(T, Pims[2], 'b+', mfc='w')
pyplot.plot([], [], 'k+', label='ms')
pyplot.axhline(Nas[0]*theta, ls=':', c='r')
pyplot.axhline(Nas[1]*theta, ls=':', c='k')
pyplot.axhline(Nas[2]*theta, ls=':', c='b')
pyplot.plot([0], [Piref[0]], 'rx')
pyplot.plot([0], [Piref[1]], 'kx')
pyplot.plot([0], [Piref[2]], 'bx')
pyplot.axhline(0, ls=':', c='k')
pyplot.legend().set_frame_on(False)
pyplot.xlabel(r'$T$')
pyplot.ylabel(r'$\pi$ per population')
pyplot.savefig(lib.fname('6-7.png'))
pyplot.clf()

pyplot.plot(T, D[0], 'rs:', label=r'$N_1$', mfc='w')
pyplot.plot(T, D[1], 'ko:', label=r'$N_2$', mfc='w')
pyplot.plot(T, D[2], 'bd:', label=r'$N_3$', mfc='w')
pyplot.plot([0], [Dref[0]], 'rx')
pyplot.plot([0], [Dref[1]], 'kx')
pyplot.plot([0], [Dref[2]], 'bx')
pyplot.axhline(0, ls=':', c='k')
pyplot.legend().set_frame_on(False)
pyplot.xlabel(r'$T$')
pyplot.ylabel(r'$D$ per population')
pyplot.ylim(-1, 1)
pyplot.savefig(lib.fname('6-8.png'))
pyplot.clf()

fstream.write(r'''
.. figure:: 6-7.png
    :scale: 75%

    Like figure 2 before but the three populations are simulated together.
    At the date :math:`T`, size of the three populations are changed to
    0.2, 1, and 5 respectively (no change for the second population).
    We can see that the results are as if the populations were simulated
    separated (note: no gene flow and very ancient merge). The cross
    simulations give results for simulations where there is no historical
    change: the three populations are directly given the ancestral size
    (giving the expectation if the change is immediate). Results are
    confirmed with ms (+ symbols).

.. figure:: 6-8.png
    :scale: 75%

    The same with Tajima's :math:`D`. No effect is no population change
    (black), positive :math:`D` if population was larger (blue), and
    negative :math:`D` if population was bigger (red). The effects on
    :math:`D` appear if :math:`T` is small but not 0.
''')

###########

nr = 1000
theta = 1
ni = 20
np = 3
NANC = 0.25, 0.5, 0.75, 1, 1.25, 1.5, 1.75, 2, 2.5, 3, 3.5, 4, 4.5, 5, 5.5, 6, 6.5, 7, 7.5, 8
cs = []
for pop in xrange(np):
    cs.append(egglib.stats.ComputeStats())
    cs[-1].add_stats('Pi', 'D', 'S')
    cs[-1].configure(egglib.stats.make_structure(
        {0: {0: dict([(i, [i]) for i in range(pop*ni, (pop+1)*ni)])}}, {}))
Pi = [[], [], []]
D = [[], [], []]

with lib.updater('    > immmediate change', len(NANC)*nr) as upd:
    for Na in NANC:
        pi = [0.0, 0.0, 0.0]
        d = [0.0, 0.0, 0.0]
        n = [0, 0, 0]
        events = []
        for i in xrange(0, np-1):
            events.append({'cat': 'merge', 'T': 10, 'src': i, 'dst': np-1})
        events.append({'cat': 'size', 'idx': 0, 'N': Na, 'T': 0.0})
        events.append({'cat': 'size', 'idx': 1, 'N': 6, 'T': 0.0})
        events.append({'cat': 'size', 'idx': 2, 'N': 8, 'T': 0.0})
        for aln in lib.simul(nr=nr, num_pop=np, num_chrom=[ni]*np,
            migr=0, theta=theta, events=events):
            if aln.ls > 0:
                for i in xrange(3):
                    stats = cs[i].process_align(aln)
                    pi[i] += stats['Pi']
                    if stats['S'] > 0:
                        d[i] += stats['D']
                        n[i] += 1
            upd.add_one()
        for i in xrange(3):
            Pi[i].append(pi[i]/nr)
            D[i].append(d[i]/n[i])

pyplot.plot(NANC, Pi[0], 'ro', mfc='w', label='pop 1')
pyplot.plot(NANC, Pi[1], 'bo', mfc='w', label='pop 2')
pyplot.plot(NANC, Pi[2], 'go', mfc='w', label='pop 3')
pyplot.xlabel(r'$N_{ANC}$')
pyplot.ylabel(r'$\pi$')
pyplot.axhline(6, ls=':', c='b')
pyplot.axhline(8, ls=':', c='g')
pyplot.axvline(1, ls=':', c='k')
pyplot.plot(NANC, NANC, 'r:')
pyplot.legend().set_frame_on(False)
pyplot.savefig(lib.fname('6-9.png'))
pyplot.clf()

pyplot.plot(NANC, D[0], 'ro', mfc='w', label='pop 1')
pyplot.plot(NANC, D[1], 'bo', mfc='w', label='pop 2')
pyplot.plot(NANC, D[2], 'go', mfc='w', label='pop 3')
pyplot.xlabel(r'$N_{ANC}$')
pyplot.ylabel(r'$D$')
pyplot.axhline(0, ls=':', c='k')
pyplot.axvline(1, ls=':', c='k')
pyplot.legend().set_frame_on(False)
pyplot.ylim(-0.5, 0.75)
pyplot.savefig(lib.fname('6-10.png'))
pyplot.clf()

fstream.write(r'''
.. figure:: 6-9.png
    :scale: 75%

    Effect of the ancestral size of population 1 (other two populations not
    changed) with an immediate change. It is like if population size of
    population 1 only was changed.

.. figure:: 6-10.png
    :scale: 75%

    The same with Tajima's :math:`D`. There is no actual change, therefore
    no effect.

''')

###########

GA = [-1, -0.75, -0.5, -0.25, 0, 0.5, 1, 1.5, 2, 2.5, 3]
nr = 2000
ns = [50]
theta = 2
Ginit = 0.5
GA0 = 1.0
T0 = 10
N0 = 1.0
T1 = 0.1
T2 = 0.3

D_ms_1 = []
D_ms_2 = []
thetaW_ms_1 = []
thetaW_ms_2 = []

D_eg_1 = []
D_eg_2 = []
thetaW_eg_1 = []
thetaW_eg_2 = []

cs = egglib.stats.ComputeStats()
cs.add_stats('thetaW', 'D')

with lib.updater('    > exponential growth change (all pops)', len(GA)*nr*4) as upd:
    for G in GA:
        d = 0.0
        n = 0
        th = 0.0
        for aln in lib.ms(theta=theta, ns=ns, nr=nr,
                as_is=['-G', str(Ginit), '-eG', str(T1), str(G), '-eG', str(T0), str(0.0), '-eN', str(T0), str(N0)]):
            upd.add_one()
            if aln.ls > 0:
                stats = cs.process_align(aln)
                th += stats['thetaW']
                if stats['thetaW'] > 0.0:
                    d += stats['D']
                    n += 1
        thetaW_ms_1.append(th/nr)
        D_ms_1.append(d/n)

        d = 0.0
        n = 0
        th = 0.0
        for aln in lib.ms(theta=theta, ns=ns, nr=nr,
                as_is=['-G', str(Ginit), '-eG', str(T2), str(G), '-eG', str(T0), str(0.0), '-eN', str(T0), str(N0)]):
            upd.add_one()
            if aln.ls > 0:
                stats = cs.process_align(aln)
                th += stats['thetaW']
                if stats['thetaW'] > 0.0:
                    d += stats['D']
                    n += 1
        thetaW_ms_2.append(th/nr)
        D_ms_2.append(d/n)

        d = 0.0
        n = 0
        th = 0.0
        for aln in lib.simul(nr=nr, num_pop=len(ns), num_chrom=ns, theta=theta, G=[Ginit],
                events=[{'cat': 'growth', 'T': T1, 'G': G},
                        {'cat': 'growth', 'T': T0, 'G': 0.0},
                        {'cat': 'size', 'T': T0, 'N': N0}]):
            upd.add_one()
            if aln.ls > 0:
                stats = cs.process_align(aln)
                th += stats['thetaW']
                if stats['thetaW'] > 0.0:
                    d += stats['D']
                    n += 1
        thetaW_eg_1.append(th/nr)
        D_eg_1.append(d/n)

        d = 0.0
        n = 0
        th = 0.0
        for aln in lib.simul(nr=nr, num_pop=len(ns), num_chrom=ns, theta=theta, G=[Ginit],
                events=[{'cat': 'growth', 'T': T2, 'G': G},
                        {'cat': 'growth', 'T': T0, 'G': 0.0},
                        {'cat': 'size', 'T': T0, 'N': N0}]):
            upd.add_one()
            if aln.ls > 0:
                stats = cs.process_align(aln)
                th += stats['thetaW']
                if stats['thetaW'] > 0.0:
                    d += stats['D']
                    n += 1
        thetaW_eg_2.append(th/nr)
        D_eg_2.append(d/n)

pyplot.plot(GA, thetaW_ms_1, 'b+:')
pyplot.plot(GA, thetaW_ms_2, 'm+--')
pyplot.plot(GA, thetaW_eg_1, 'bo:', mfc='None')
pyplot.plot(GA, thetaW_eg_2, 'mo--', mfc='None')
pyplot.plot([], [], 'k+', label='ms')
pyplot.plot([], [], 'ko', mfc='None', label='EggLib')
pyplot.plot([], [], 'b:', label='T={0}'.format(T1))
pyplot.plot([], [], 'm--', label='T={0}'.format(T2))
pyplot.legend().set_frame_on(False)
pyplot.xlabel('ancestral growth rate')
pyplot.ylabel(r'$\hat{\theta}_W$')
pyplot.savefig(lib.fname('6-11.png'))
pyplot.clf()

pyplot.plot(GA, D_ms_1, 'b+:')
pyplot.plot(GA, D_ms_2, 'm+--')
pyplot.plot(GA, D_eg_1, 'bo:', mfc='None')
pyplot.plot(GA, D_eg_2, 'mo--', mfc='None')
pyplot.plot([], [], 'k+', label='ms')
pyplot.plot([], [], 'ko', mfc='None', label='EggLib ')
pyplot.plot([], [], 'b:', label='T={0}'.format(T1))
pyplot.plot([], [], 'm--', label='T={0}'.format(T2))
pyplot.legend().set_frame_on(False)
pyplot.xlabel('ancestral growth rate')
pyplot.ylabel(r"Tajima's $D$")
pyplot.savefig(lib.fname('6-12.png'))
pyplot.clf()

fstream.write(r'''
Exponential growth rate change
------------------------------

.. figure:: 6-11.png
    :scale: 75%

    Effect of a past change of exponential growth rate in a single population.
    These simulations start with a growth rate of :math:`\alpha` = {0}.
    Then the growth rate is changed at either T=0.1 (blue) or T=0.3 (magenta).
    The comparison of ms and EggLib simulations shows no visible discrepancy.

.. figure:: 6-12.png
    :scale: 75%

    Tajima's :math:`D` for the same simulations.

'''.format(Ginit))

T = [0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1, 1.2, 1.4, 1.6, 1.8, 2]
G1 = -0.5
G2 = 1

thetaW_ms_1 = []
thetaW_ms_2 = []
thetaW_eg_1 = []
thetaW_eg_2 = []
D_ms_1 = []
D_ms_2 = []
D_eg_1 = []
D_eg_2 = []
with lib.updater('    > exponential growth change (date) (all pops)', len(T)*nr*4) as upd:
    for t in T:
        d = 0.0
        n = 0
        th = 0.0
        for aln in lib.ms(theta=theta, ns=ns, nr=nr,
                as_is=['-eG', str(t), str(G1), '-eG', str(T0), str(0.0), '-eN', str(T0), str(N0)]):
            upd.add_one()
            if aln.ls > 0:
                stats = cs.process_align(aln)
                th += stats['thetaW']
                if stats['thetaW'] > 0.0:
                    d += stats['D']
                    n += 1
        thetaW_ms_1.append(th/nr)
        D_ms_1.append(d/n)

        d = 0.0
        n = 0
        th = 0.0
        for aln in lib.ms(theta=theta, ns=ns, nr=nr,
                as_is=['-eG', str(t), str(G2), '-eG', str(T0), str(0.0), '-eN', str(T0), str(N0)]):
            upd.add_one()
            if aln.ls > 0:
                stats = cs.process_align(aln)
                th += stats['thetaW']
                if stats['thetaW'] > 0.0:
                    d += stats['D']
                    n += 1
        thetaW_ms_2.append(th/nr)
        D_ms_2.append(d/n)

        d = 0.0
        n = 0
        th = 0.0
        for aln in lib.simul(nr=nr, num_pop=len(ns), num_chrom=ns, theta=theta,
                events=[{'cat': 'growth', 'T': t, 'G': G1},
                        {'cat': 'growth', 'T': T0, 'G': 0.0},
                        {'cat': 'size', 'T': T0, 'N': N0}]):
            upd.add_one()
            if aln.ls > 0:
                stats = cs.process_align(aln)
                th += stats['thetaW']
                if stats['thetaW'] > 0.0:
                    d += stats['D']
                    n += 1
        thetaW_eg_1.append(th/nr)
        D_eg_1.append(d/n)

        d = 0.0
        n = 0
        th = 0.0
        for aln in lib.simul(nr=nr, num_pop=len(ns), num_chrom=ns, theta=theta,
                events=[{'cat': 'growth', 'T': t, 'G': G2},
                        {'cat': 'growth', 'T': T0, 'G': Ginit},
                        {'cat': 'size', 'T': T0, 'N': N0}]):
            upd.add_one()
            if aln.ls > 0:
                stats = cs.process_align(aln)
                th += stats['thetaW']
                if stats['thetaW'] > 0.0:
                    d += stats['D']
                    n += 1
        thetaW_eg_2.append(th/nr)
        D_eg_2.append(d/n)

pyplot.plot(T, thetaW_ms_1, 'b+:')
pyplot.plot(T, thetaW_ms_2, 'm+--')
pyplot.plot(T, thetaW_eg_1, 'bo:', mfc='None')
pyplot.plot(T, thetaW_eg_2, 'mo--', mfc='None')
pyplot.plot([], [], 'k+', label='ms')
pyplot.plot([], [], 'ko', mfc='None', label='EggLib')
pyplot.plot([], [], 'b:', label=r'$\alpha_{{ANC}}={0}$'.format(G1))
pyplot.plot([], [], 'm--', label=r'$\alpha_{{ANC}}={0}$'.format(G2))
pyplot.legend().set_frame_on(False)
pyplot.xlabel('change growth rate date')
pyplot.ylabel(r'$\hat{\theta}_W$')
pyplot.savefig(lib.fname('6-13.png'))
pyplot.clf()

pyplot.plot(T, D_ms_1, 'b+:')
pyplot.plot(T, D_ms_2, 'm+--')
pyplot.plot(T, D_eg_1, 'bo:', mfc='None')
pyplot.plot(T, D_eg_2, 'mo--', mfc='None')
pyplot.plot([], [], 'k+--', label='ms')
pyplot.plot([], [], 'ko--', mfc='None', label='EggLib ')
pyplot.plot([], [], 'b:', label=r'$\alpha_{{ANC}}={0}$'.format(G1))
pyplot.plot([], [], 'm--', label=r'$\alpha_{{ANC}}={0}$'.format(G2))
pyplot.legend().set_frame_on(False)
pyplot.xlabel('change growth rate date')
pyplot.ylabel(r"Tajima's $D$")
pyplot.savefig(lib.fname('6-14.png'))
pyplot.clf()

fstream.write(r'''
.. figure:: 6-13.png
    :scale: 75%

    In this simulations the ancestral :math:`\alpha` is kept constant
    (either -0.5 or 1) and the date is changed. There is no exponential
    growth at the start of simulation (contrary to the previous batch
    of simulations). The comparison of ms and EggLib simulations shows
    no visible discrepancy.

.. figure:: 6-14.png
    :scale: 75%

    Tajima's :math:`D` for the same simulations.
''')

###########

GA = [-0.5, -0.25, 0, 0.25, 0.5, 0.75, 1, 1.25, 1.5, 1.75, 2]
T1 = 0.1
T2 = 0.5
ns = [20, 20, 20, 20]
theta = 2
cs_list = []
acc = 0
nr = 2000
for n in ns:
    cs_list.append(egglib.stats.ComputeStats(
        struct=egglib.stats.make_structure({0: {0: dict([(i, [i]) for i in range(acc, acc+n)])}}, None)))
    cs_list[-1].add_stats('thetaW', 'D')
    acc += n

thetaW_ms = [[], [], [], []]
D_ms = [[], [], [], []]
thetaW_eg = [[], [], [], []]
D_eg = [[], [], [], []]

with lib.updater('    > exponential growth change (per pop)', len(GA)*nr*2) as upd:
    for G in GA:
        d = [0.0] * 4
        n = [0] * 4
        th = [0.0] * 4
        for aln in lib.ms(theta=theta, ns=ns, nr=nr,
                as_is=['-eg', str(T1), '1', str(G),
                       '-eg', str(T2), '3', str(G),
                       '-eG', '5', '0',
                       '-eN', '5', '1',
                       '-ej', '5', '2', '1',
                       '-ej', '5', '3', '1',
                       '-ej', '5', '4', '1']):
            upd.add_one()
            if aln.ls > 0:
                for i, cs in enumerate(cs_list):
                    stats = cs.process_align(aln)
                    th[i] += stats['thetaW']
                    if stats['thetaW'] > 0.0:
                        d[i] += stats['D']
                        n[i] += 1
        for i in xrange(4):
            thetaW_ms[i].append(th[i]/nr)
            D_ms[i].append(d[i]/n[i])

        d = [0.0] * 4
        n = [0] * 4
        th = [0.0] * 4
        for aln in lib.simul(num_pop=len(ns), theta=theta, num_chrom=ns, nr=nr,
                events=[{'cat': 'growth', 'T': T1, 'idx': 0, 'G': G},
                        {'cat': 'growth', 'T': T2, 'idx': 2, 'G': G},
                        {'cat': 'growth', 'T': 5, 'G': 0},
                        {'cat': 'size',   'T': 5, 'N': 1},
                        {'cat': 'merge',  'T': 5, 'src': 1, 'dst': 0},
                        {'cat': 'merge',  'T': 5, 'src': 2, 'dst': 0},
                        {'cat': 'merge',  'T': 5, 'src': 3, 'dst': 0}]):
            upd.add_one()
            if aln.ls > 0:
                for i, cs in enumerate(cs_list):
                    stats = cs.process_align(aln)
                    th[i] += stats['thetaW']
                    if stats['thetaW'] > 0.0:
                        d[i] += stats['D']
                        n[i] += 1
        for i in xrange(4):
            thetaW_eg[i].append(th[i]/nr)
            D_eg[i].append(d[i]/n[i])

pyplot.plot(GA, thetaW_ms[0], 'r+')
pyplot.plot(GA, thetaW_ms[1], 'k+')
pyplot.plot(GA, thetaW_ms[2], 'm+')
pyplot.plot(GA, thetaW_ms[3], 'b+')
pyplot.plot(GA, thetaW_eg[0], 'ro', mfc='None')
pyplot.plot(GA, thetaW_eg[1], 'ko', mfc='None')
pyplot.plot(GA, thetaW_eg[2], 'mo', mfc='None')
pyplot.plot(GA, thetaW_eg[3], 'bo', mfc='None')
pyplot.plot([], [], 'r-', label='pop 1')
pyplot.plot([], [], 'k-', label='pop 2')
pyplot.plot([], [], 'm-', label='pop 3')
pyplot.plot([], [], 'b-', label='pop 4')
pyplot.plot([], [], 'ko', mfc='None', label='EggLib')
pyplot.plot([], [], 'k+', label='ms')
pyplot.axvline(0, c='k', ls=':')
pyplot.axhline(theta, c='k', ls=':')
pyplot.legend().set_frame_on(False)
pyplot.xlabel('ancestral exponential growth rate')
pyplot.ylabel(r"$\hat{\theta}_{W}$")
pyplot.savefig(lib.fname('6-15.png'))
pyplot.clf()

fstream.write(r'''
.. figure:: 6-15.png
    :scale: 75%

    Diversity per population in a system with four populations.
    At time :math:`T={T1}` (red, for population 1) or :math:`T={T2}`
    (magenta, for population3), the exponential growth rate is set to
    the specified value. Results are compared between EggLib and ms.
''')

###########

T_list = [0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1]
s0 = 0.65
s1 = 0
s2 = 1
theta = 2
theta0 = theta * ((2.0-s0)/2)
theta1 = theta * ((2.0-s1)/2)
theta2 = theta * ((2.0-s2)/2)
ns = [20, 20, 20, 20]
cs_list = []
acc = 0
nr = 2000
for n in ns:
    cs_list.append(egglib.stats.ComputeStats(
        struct=egglib.stats.make_structure({0: {0: dict([(i, [i]) for i in range(acc, acc+n)])}}, None)))
    cs_list[-1].add_stats('thetaW', 'D')
    acc += n

thetaW_A = [[[], [], [], []], [[], [], [], []]]
D_A = [[[], [], [], []], [[], [], [], []]]
thetaW_B = [[[], [], [], []], [[], [], [], []]]
D_B = [[[], [], [], []], [[], [], [], []]]

with lib.updater('    > selfing rate', len(T_list)*nr*4) as upd:
    for T in T_list:
        for idx, s in enumerate([s1, s2]):
            d = [0.0] * 4
            n = [0] * 4
            th = [0.0] * 4
            for aln in lib.simul(num_pop=len(ns), theta=theta,
                    num_chrom=ns, nr=nr, s=[s0]*4,
                    events=[{'cat': 'selfing', 'T': T, 's': s},
                            {'cat': 'merge',  'T': 5, 'src': 1, 'dst': 0},
                            {'cat': 'merge',  'T': 5, 'src': 2, 'dst': 0},
                            {'cat': 'merge',  'T': 5, 'src': 3, 'dst': 0}]):
                upd.add_one()
                if aln.ls > 0:
                    for i, cs in enumerate(cs_list):
                        stats = cs.process_align(aln)
                        th[i] += stats['thetaW']
                        if stats['thetaW'] > 0.0:
                            d[i] += stats['D']
                            n[i] += 1
            for i in xrange(4):
                thetaW_A[idx][i].append(th[i]/nr)
                D_A[idx][i].append(d[i]/n[i])

            d = [0.0] * 4
            n = [0] * 4
            th = [0.0] * 4
            for aln in lib.simul(num_pop=len(ns), theta=theta,
                    num_chrom=ns, nr=nr, s=[s0]*4,
                    events=[{'cat': 'selfing', 'T': T, 's': s, 'idx': 1},
                            {'cat': 'selfing', 'T': T, 's': s, 'idx': 2},
                            {'cat': 'merge',  'T': 5, 'src': 1, 'dst': 0},
                            {'cat': 'merge',  'T': 5, 'src': 2, 'dst': 0},
                            {'cat': 'merge',  'T': 5, 'src': 3, 'dst': 0}]):
                upd.add_one()
                if aln.ls > 0:
                    for i, cs in enumerate(cs_list):
                        stats = cs.process_align(aln)
                        th[i] += stats['thetaW']
                        if stats['thetaW'] > 0.0:
                            d[i] += stats['D']
                            n[i] += 1
            for i in xrange(4):
                thetaW_B[idx][i].append(th[i]/nr)
                D_B[idx][i].append(d[i]/n[i])

pyplot.plot(T_list, thetaW_A[0][0], 'rd:', mfc='w')
pyplot.plot(T_list, thetaW_A[0][1], 'r^:', mfc='w')
pyplot.plot(T_list, thetaW_A[0][2], 'rv:', mfc='w')
pyplot.plot(T_list, thetaW_A[0][3], 'rs:', mfc='w')
pyplot.plot(T_list, thetaW_A[1][0], 'bd:', mfc='w')
pyplot.plot(T_list, thetaW_A[1][1], 'b^:', mfc='w')
pyplot.plot(T_list, thetaW_A[1][2], 'bv:', mfc='w')
pyplot.plot(T_list, thetaW_A[1][3], 'bs:', mfc='w')
pyplot.axhline(theta0, ls=':', c='k')
pyplot.axhline(theta1, ls=':', c='r')
pyplot.axhline(theta2, ls=':', c='b')
pyplot.plot([], [], 'r-', label=r'$s_{{ANC}}={0}$'.format(s1))
pyplot.plot([], [], 'b-', label=r'$s_{{ANC}}={0}$'.format(s2))
pyplot.plot([], [], 'kd', label='pop 1', mfc='w')
pyplot.plot([], [], 'k^', label='pop 2', mfc='w')
pyplot.plot([], [], 'kv', label='pop 3', mfc='w')
pyplot.plot([], [], 'ks', label='pop 4', mfc='w')
pyplot.legend().set_frame_on(False)
pyplot.xlabel(r'$T$')
pyplot.ylabel(r"$\hat{\theta}_{W}$")
pyplot.ylim(0.75, 2.25)
pyplot.savefig(lib.fname('6-16.png'))
pyplot.clf()

pyplot.plot(T_list, thetaW_B[0][0], 'rd:', mfc='w')
pyplot.plot(T_list, thetaW_B[0][1], 'r^:', mfc='w')
pyplot.plot(T_list, thetaW_B[0][2], 'rv:', mfc='w')
pyplot.plot(T_list, thetaW_B[0][3], 'rs:', mfc='w')
pyplot.plot(T_list, thetaW_B[1][0], 'bd:', mfc='w')
pyplot.plot(T_list, thetaW_B[1][1], 'b^:', mfc='w')
pyplot.plot(T_list, thetaW_B[1][2], 'bv:', mfc='w')
pyplot.plot(T_list, thetaW_B[1][3], 'bs:', mfc='w')
pyplot.axhline(theta0, ls=':', c='k')
pyplot.axhline(theta1, ls=':', c='r')
pyplot.axhline(theta2, ls=':', c='b')
pyplot.plot([], [], 'r-', label=r'$s_{{ANC}}={0}$'.format(s1))
pyplot.plot([], [], 'b-', label=r'$s_{{ANC}}={0}$'.format(s2))
pyplot.plot([], [], 'kd', label='pop 1', mfc='w')
pyplot.plot([], [], 'k^', label='pop 2', mfc='w')
pyplot.plot([], [], 'kd', label='pop 3', mfc='w')
pyplot.plot([], [], 'ks', label='pop 4', mfc='w')
pyplot.legend().set_frame_on(False)
pyplot.xlabel(r'$T$')
pyplot.ylabel(r"$\hat{\theta}_{W}$")
pyplot.ylim(0.75, 2.25)
pyplot.savefig(lib.fname('6-17.png'))
pyplot.clf()

pyplot.plot(T_list, D_A[0][0], 'rd:', mfc='w')
pyplot.plot(T_list, D_A[0][1], 'r^:', mfc='w')
pyplot.plot(T_list, D_A[0][2], 'rv:', mfc='w')
pyplot.plot(T_list, D_A[0][3], 'rs:', mfc='w')
pyplot.plot(T_list, D_A[1][0], 'bd:', mfc='w')
pyplot.plot(T_list, D_A[1][1], 'b^:', mfc='w')
pyplot.plot(T_list, D_A[1][2], 'bv:', mfc='w')
pyplot.plot(T_list, D_A[1][3], 'bs:', mfc='w')
pyplot.axhline(0, ls=':', c='k')
pyplot.plot([], [], 'r-', label=r'$s_{{ANC}}={0}$'.format(s1))
pyplot.plot([], [], 'b-', label=r'$s_{{ANC}}={0}$'.format(s2))
pyplot.plot([], [], 'kd', label='pop 1', mfc='w')
pyplot.plot([], [], 'k^', label='pop 2', mfc='w')
pyplot.plot([], [], 'kv', label='pop 3', mfc='w')
pyplot.plot([], [], 'ks', label='pop 4', mfc='w')
pyplot.legend().set_frame_on(False)
pyplot.xlabel(r'$T$')
pyplot.ylabel(r"$D$")
pyplot.savefig(lib.fname('6-18.png'))
pyplot.clf()

pyplot.plot(T_list, D_B[0][0], 'rd:', mfc='w')
pyplot.plot(T_list, D_B[0][1], 'r^:', mfc='w')
pyplot.plot(T_list, D_B[0][2], 'rv:', mfc='w')
pyplot.plot(T_list, D_B[0][3], 'rs:', mfc='w')
pyplot.plot(T_list, D_B[1][0], 'bd:', mfc='w')
pyplot.plot(T_list, D_B[1][1], 'b^:', mfc='w')
pyplot.plot(T_list, D_B[1][2], 'bv:', mfc='w')
pyplot.plot(T_list, D_B[1][3], 'bs:', mfc='w')
pyplot.axhline(0, ls=':', c='k')
pyplot.plot([], [], 'r-', label=r'$s_{{ANC}}={0}$'.format(s1))
pyplot.plot([], [], 'b-', label=r'$s_{{ANC}}={0}$'.format(s2))
pyplot.plot([], [], 'kd', label='pop 1', mfc='w')
pyplot.plot([], [], 'k^', label='pop 2', mfc='w')
pyplot.plot([], [], 'kv', label='pop 3', mfc='w')
pyplot.plot([], [], 'ks', label='pop 4', mfc='w')
pyplot.legend().set_frame_on(False)
pyplot.xlabel(r'$T$')
pyplot.ylabel(r"$D$")
pyplot.savefig(lib.fname('6-19.png'))
pyplot.clf()


fstream.write(r'''
Selfing rate change
-------------------

.. figure:: 6-16.png
    :scale: 75%

    Selfing rate change. The initial selfing rate (of :math:`s={{0}}`)
    of a system with 4 populations is changed to either :math:`s={{1}}`
    (red) or :math:`s={{2}}` (blue) at the specified date :math:`T`. The dotted
    lines give the corrected :math:`\theta`, that is the expected value
    for :math:`\hat{{\theta}}_W` for the initial :math:`s` (black) and the
    two ancestral :math:`s` (red and blue). If :math:`T=0`, the diversity
    reflects the ancestral :math:`s` (which is actuall immediate), and if
    :math:`T` is large, the diversity reflects the initial selfing rate.

.. figure:: 6-17.png
    :scale: 75%

    The same, but only two populations (pop 2 and 3) are affected by
    the change.

.. figure:: 6-18.png
    :scale: 75%

.. figure:: 6-19.png
    :scale: 75%

    Tajima's :math:`D` for the last two sets of simulations. We see a
    transient deviation from 0 (in the concerned populations for the
    second graphic). For the red curves, the ancestral selfing rate is
    smaller than the present so the change corresponds to an effective
    population size decrease, leading to positive Tajima's :math:`D`
    values (and all the was around for the blue curves).
'''.format(s0, s1, s2))

###########

Ma = [0.1, 0.3, 0.5, 0.7, 0.9, 1.1, 1.3, 1.5, 1.7, 1.9]
ns = 20
np = 3
nr = 1000
theta = 2.5
T = 0.2
d = {0: {0: dict([(k, [k]) for k in range(0*ns, 1*ns)]),
         1: dict([(k, [k]) for k in range(1*ns, 2*ns)]),
         2: dict([(k, [k]) for k in range(2*ns, 3*ns)])}}
cs_list = [egglib.stats.ComputeStats(struct=egglib.stats.make_structure(d, None))]
for i in xrange(3):
    for j in xrange(i+1, 3):
        d = {0: {i: dict([(k, [k]) for k in range(i*ns, (i+1)*ns)]),
                 j: dict([(k, [k]) for k in range(j*ns, (j+1)*ns)])}}
        cs_list.append(egglib.stats.ComputeStats(struct=egglib.stats.make_structure(d, None)))
Fst_ms1 = []
Fst_ms2 = []
Fst_eg1 = []
Fst_eg2 = []
for cs in cs_list:
    cs.add_stats('WCst')
    Fst_ms1.append([])
    Fst_ms2.append([])
    Fst_eg1.append([])
    Fst_eg2.append([])

with lib.updater('    > migration rate', len(Ma)*nr*4) as upd:
    for M in Ma:
        Fst_list = [0.0] * len(Fst_ms1)
        n_list = [0] * len(Fst_ms1)
        for aln in lib.ms(ns=[ns]*np, nr=nr, theta=theta, as_is=['-eM', str(T), str(M)]):
            for i, cs in enumerate(cs_list):
                Fst = cs.process_align(aln)['WCst']
                if Fst is not None:
                    Fst_list[i] += Fst
                    n_list[i] += 1
            upd.add_one()
        for i in xrange(len(Fst_ms1)):
            Fst_ms1[i].append(Fst_list[i]/n_list[i]) 

        Fst_list = [0.0] * len(Fst_ms2)
        n_list = [0] * len(Fst_ms2)
        for aln in lib.ms(ns=[ns]*np, nr=nr, theta=theta, as_is=
            ['-em', str(T), '1', '2', str(0.5*M),
             '-em', str(T), '2', '1', str(0.5*M),
             '-em', str(T), '2', '3', str(0.1*M),
             '-em', str(T), '3', '2', str(0.1*M)]):
            for i, cs in enumerate(cs_list):
                Fst = cs.process_align(aln)['WCst']
                if Fst is not None:
                    Fst_list[i] += Fst
                    n_list[i] += 1
            upd.add_one()
        for i in xrange(len(Fst_ms2)):
            Fst_ms2[i].append(Fst_list[i]/n_list[i]) 

        Fst_list = [0.0] * len(Fst_eg1)
        n_list = [0] * len(Fst_eg1)
        for aln in lib.simul(num_pop=np, num_chrom=[ns]*np, nr=nr, theta=theta, events=[{'cat': 'migr', 'T': T, 'M': M}]):
            for i, cs in enumerate(cs_list):
                Fst = cs.process_align(aln)['WCst']
                if Fst is not None:
                    Fst_list[i] += Fst
                    n_list[i] += 1
            upd.add_one()
        for i in xrange(len(Fst_eg1)):
            Fst_eg1[i].append(Fst_list[i]/n_list[i]) 

        Fst_list = [0.0] * len(Fst_eg2)
        n_list = [0] * len(Fst_eg2)
        for aln in lib.simul(num_pop=np, num_chrom=[ns]*np, nr=nr, theta=theta, events=[
            {'cat': 'pair_migr', 'T': T, 'src': 0, 'dst': 1, 'M': 0.5*M},
            {'cat': 'pair_migr', 'T': T, 'src': 1, 'dst': 0, 'M': 0.5*M},
            {'cat': 'pair_migr', 'T': T, 'src': 1, 'dst': 2, 'M': 0.1*M},
            {'cat': 'pair_migr', 'T': T, 'src': 2, 'dst': 1, 'M': 0.1*M}]):
            for i, cs in enumerate(cs_list):
                Fst = cs.process_align(aln)['WCst']
                if Fst is not None:
                    Fst_list[i] += Fst
                    n_list[i] += 1
            upd.add_one()
        for i in xrange(len(Fst_eg2)):
            Fst_eg2[i].append(Fst_list[i]/n_list[i]) 

pyplot.plot(Ma, Fst_ms1[0], 'k+')
pyplot.plot(Ma, Fst_ms1[1], 'r+')
pyplot.plot(Ma, Fst_ms1[2], 'g+')
pyplot.plot(Ma, Fst_ms1[3], 'b+')
pyplot.plot(Ma, Fst_eg1[0], 'ko', mfc='None')
pyplot.plot(Ma, Fst_eg1[1], 'ro', mfc='None')
pyplot.plot(Ma, Fst_eg1[2], 'go', mfc='None')
pyplot.plot(Ma, Fst_eg1[3], 'bo', mfc='None')
pyplot.plot([], [], 'k-', label='global')
pyplot.plot([], [], 'r-', label='pair 1-2')
pyplot.plot([], [], 'g-', label='pair 1-3')
pyplot.plot([], [], 'b-', label='pair 2-3')
pyplot.plot([], [], 'k+', label='ms')
pyplot.plot([], [], 'ko', mfc='None', label='EggLib')
pyplot.xlabel(r'ancestral $M$')
pyplot.ylabel(r'$F_{ST}$')
pyplot.legend().set_frame_on(False)
pyplot.savefig(lib.fname('6-20.png'))
pyplot.clf()

pyplot.plot(Ma, Fst_ms2[0], 'k+')
pyplot.plot(Ma, Fst_ms2[1], 'r+')
pyplot.plot(Ma, Fst_ms2[2], 'g+')
pyplot.plot(Ma, Fst_ms2[3], 'b+')
pyplot.plot(Ma, Fst_eg2[0], 'ko', mfc='None')
pyplot.plot(Ma, Fst_eg2[1], 'ro', mfc='None')
pyplot.plot(Ma, Fst_eg2[2], 'go', mfc='None')
pyplot.plot(Ma, Fst_eg2[3], 'bo', mfc='None')
pyplot.plot([], [], 'k-', label='global')
pyplot.plot([], [], 'r-', label='pair 1-2')
pyplot.plot([], [], 'g-', label='pair 1-3')
pyplot.plot([], [], 'b-', label='pair 2-3')
pyplot.plot([], [], 'k+', label='ms')
pyplot.plot([], [], 'ko', mfc='None', label='EggLib')
pyplot.xlabel(r'ancestral $M$')
pyplot.ylabel(r'$F_{ST}$')
pyplot.legend().set_frame_on(False)
pyplot.savefig(lib.fname('6-21.png'))
pyplot.clf()

fstream.write(r'''
Migration rate change
---------------------

.. figure:: 6-20.png
    :scale: 75%

    At date :math:`T={0}`, the migration rate :math:`M` is changed to the
    specified value, in a system with four poplations. Results for ms and
    EggLib are compared.

.. figure:: 6-21.png
    :scale: 75%

    Similar, but the ancestral migration rate is not homogeneous. For a
    given ancestral :math:`M`, the (symetrical) migration rate between
    populations 1 and 2 is set to :math:`\frac{{M}}{{2}}` and this between
    populations 2 and 3 to :math:`\frac{{M}}{{10}}` (no migration whatsoever
    between populations 1 and 3).
'''.format(T))

###########

nr = 5000
ns = 50
theta = 1.0
RHO = [0.0, 0.2, 0.4, 0.6, 0.8, 1, 1.2, 1.4, 1.6, 1.8, 2]
cs = egglib.stats.ComputeStats()
cs.add_stats('Rmin')

T1 = 0.1
T2 = 1.0
T3 = 0.05

def get(**args):
    Rmin_acc = 0.0
    n = 0
    for aln in lib.simul(**args):
        Rmin = cs.process_align(aln)['Rmin']
        if Rmin is not None:
            Rmin_acc += Rmin
            n += 1
        upd.add_one()
    return Rmin_acc / n

Rmin_1 = []
Rmin_2 = []
Rmin_3 = []
Rmin_4 = []
Rmin_5 = []
Rmin_6 = []
Rmin_7 = []
Rmin_8 = []

with lib.updater('    > recombination rate', len(RHO)*nr*8) as upd:
    for rho in RHO:
        Rmin_1.append(get(nr=nr, num_pop=1, num_chrom=[ns], recomb=rho, theta=theta))
        Rmin_2.append(get(nr=nr, num_pop=1, num_chrom=[ns], recomb=0.0, theta=theta, events=[{'cat': 'recombination', 'T': 0.0, 'R': rho}]))
        Rmin_3.append(get(nr=nr, num_pop=1, num_chrom=[ns], recomb=0.0, theta=theta, events=[{'cat': 'recombination', 'T': T1, 'R': rho}]))
        Rmin_4.append(get(nr=nr, num_pop=1, num_chrom=[ns], recomb=0.0, theta=theta, events=[{'cat': 'recombination', 'T': T2, 'R': rho}]))
        Rmin_5.append(get(nr=nr, num_pop=1, num_chrom=[0], recomb=0.0, theta=theta,
            events=[{'cat': 'recombination', 'T': T1, 'R': rho},
                    {'cat': 'sample', 'T': T1, 'idx': 0, 'label': 0, 'num_chrom': ns, 'num_indiv': 0}]))
        Rmin_6.append(get(nr=nr, num_pop=1, num_chrom=[0], recomb=0.0, theta=theta,
            events=[{'cat': 'recombination', 'T': T2, 'R': rho},
                    {'cat': 'sample', 'T': T2, 'idx': 0, 'label': 0, 'num_chrom': ns, 'num_indiv': 0}]))
        Rmin_7.append(get(nr=nr, num_pop=1, num_chrom=[ns], recomb=rho, theta=theta,
            events=[{'cat': 'recombination', 'T': T3, 'R': 0.0}]))
        Rmin_8.append(get(nr=nr, num_pop=1, num_chrom=[0], recomb=rho, theta=theta,
            events=[{'cat': 'recombination', 'T': T3, 'R': 0.0},
                    {'cat': 'sample', 'T': T3, 'idx': 0, 'label': 0, 'num_chrom': ns, 'num_indiv': 0}]))

pyplot.plot(RHO, Rmin_1, 'ks', mfc='None', label='ctrl')
pyplot.plot(RHO, Rmin_2, 'ko', mfc='None', label=r'$T=0$')
pyplot.plot(RHO, Rmin_3, 'kd', mfc='None', label=r'$T={0}$'.format(T1))
pyplot.plot(RHO, Rmin_4, 'k^', mfc='None', label=r'$T={0}$'.format(T2))
pyplot.plot(RHO, Rmin_5, 'bd', mfc='None')
pyplot.plot(RHO, Rmin_6, 'b^', mfc='None')
pyplot.plot(RHO, Rmin_7, 'r+', mfc='None', label='reversed')
pyplot.plot(RHO, Rmin_8, 'b+', mfc='None')
pyplot.plot([], [], 'b-', label='delayed')
pyplot.axvline(0, ls=':', c='k')
pyplot.axhline(0, ls=':', c='k')
pyplot.xlabel(r'$\rho$')
pyplot.ylabel(r'$R_{MIN}$')
pyplot.legend().set_frame_on(False)
pyplot.savefig(lib.fname('6-22.png'))
pyplot.clf()

fstream.write(r'''
Recombination rate change
-------------------------

.. figure:: 6-22.png
    :scale: 75%

    Examine the effect of the recombination rate :math:`\rho` on the
    minimal number of recombination events, Hudson's :math:`R_{MIN}`.
    The control is when the recombination rate is set as an initial
    parameters (square symbols). When the recombination rate is set
    as a historical event, but with :math:`T=0` (circle symbols),
    the outcome is identical. If the event is set later, the values for
    :math:`R_{MIN}` are decreased (diamond symbols; :math:`T=0.1`)
    down to zero for :math:`T=0.5` (triangle symbols). In addition,
    the :math:`R_{MIN}` values for the latter two sets of simulations
    are back to the control values if we set the samples to come from
    a delayed event occurring at the same time than the setting of the
    recombination rate (blue symbols). In addition, we reversed the
    change of recombination rate (plus symbols): the simulations have
    initially recombination and the rate is set to 0 at :math:`T=0.05`.
    We obtain again intermediate values in the default case (red symbols)
    and no signatures of recombination if a delayed sample is set at
    the same time that the recombination rate is set to 0 (blue symbols).
    Note that the results indicate that recombination has most of its
    effects when it occurs recently, but I see no reasons to assume
    that this doesn't make sense.
''')

###########

nr = 1000
ns = [20, 20]
theta = 1
d = {1000: {}}
d[1000][0] = dict([(i, [i]) for i in xrange(ns[0])])
d[1000][1] = dict([(i, [i]) for i in xrange(ns[0], ns[0]+ns[1])])
cs = egglib.stats.ComputeStats(struct=egglib.stats.make_structure(d, None))
cs.add_stats('WCst')

T_list = [0, 0.4, 0.8, 1.2, 1.6, 2, 2.4, 2.8, 3.2, 3.6, 4, 4.4, 4.8]
probas = [0, 0.25, 0.5, 0.75, 1]
ctrl = {'WCst': []}
res_eg = [{'WCst': []} for p in probas]
res_ms = [{'WCst': []} for p in probas]

with lib.updater('    > admixture', nr*len(T_list)*(2*len(probas)+1)) as upd:
    for T in T_list:
        get2(ctrl, cs, upd, lib.simul, nr=nr, theta=theta, num_pop=2, num_chrom=ns,
             events=[{'cat': 'merge', 'src': 1, 'dst': 0, 'T': 5}])
        for P, d, in zip(probas, res_eg):
            get2(d, cs, upd, lib.simul, nr=nr, theta=theta, num_pop=2, num_chrom=ns,
                 events=[{'cat': 'merge', 'src': 1, 'dst': 0, 'T': 5},
                         {'cat': 'admixture', 'src': 1, 'dst': 0, 'T': T, 'proba': P}])
        for P, d, in zip(probas, res_ms):
            get2(d, cs, upd, lib.ms, nr=nr, theta=theta, ns=ns,
                 as_is=['-ej', '5', '2', '1',
                        '-es', str(T), '2', str(1-P),
                        '-ej', str(T), '3', '1'])

pyplot.plot(T_list, ctrl['WCst'], 'ks', mfc='None', label='no admixt')
pyplot.plot(T_list, res_eg[0]['WCst'], 'ko', mfc='None', label='P={0}'.format(probas[0]))
pyplot.plot(T_list, res_eg[1]['WCst'], 'bo', mfc='None', label='P={0}'.format(probas[1]))
pyplot.plot(T_list, res_eg[2]['WCst'], 'go', mfc='None', label='P={0}'.format(probas[2]))
pyplot.plot(T_list, res_eg[3]['WCst'], 'ro', mfc='None', label='P={0}'.format(probas[3]))
pyplot.plot(T_list, res_eg[4]['WCst'], 'mo', mfc='None', label='P={0}'.format(probas[4]))
pyplot.plot(T_list, res_ms[0]['WCst'], 'k+', mfc='None')
pyplot.plot(T_list, res_ms[1]['WCst'], 'b+', mfc='None')
pyplot.plot(T_list, res_ms[2]['WCst'], 'g+', mfc='None')
pyplot.plot(T_list, res_ms[3]['WCst'], 'r+', mfc='None')
pyplot.plot(T_list, res_ms[4]['WCst'], 'm+', mfc='None')
pyplot.plot([], [], 'k+', label='ms')
pyplot.xlabel(r'$T$')
pyplot.ylabel(r'$F_{ST}$')
pyplot.legend().set_frame_on(False)
pyplot.savefig(lib.fname('6-23.png'))
pyplot.clf()

fstream.write(r'''
Admixture
---------

.. figure:: 6-23.png
    :scale: 75%

    Consequence of admixture on a pair of unconnected populations (they
    are merged at :math:`T=5`). Except for the control (no admixture;
    black squares), admixture is applied at different dates with five
    different probabilities (squares with different colours). With
    :math:`P=0` the result is identical to no admixture since the event
    has no effect. The higher :math:`P`, the stronger the effect (decrease
    of :math:`F_{ST}`). Wtith :math:`P=1` and :math:`T=0`, the signal of
    structure is erased and :math:`F_{ST}` is 0. With large values of
    :math:`T`, the effect of admixture vanishes. Plus symbols show results
    for simulations obtained with ms (the admixture event is emulated with
    a split event followed by an immediate merge into the target population).
''')

###########

theta = 1
nr = 1000
ns = [20, 20]
d = {1000: {}}
d[1000][0] = dict([(i, [i]) for i in xrange(ns[0])])
d[1000][1] = dict([(i, [i]) for i in xrange(ns[0], ns[0]+ns[1])])
cs = egglib.stats.ComputeStats(struct=egglib.stats.make_structure(d, None))
cs.add_stats('WCst')
T_list = [0, 0.4, 0.8, 1.2, 1.6, 2, 2.4, 2.8, 3.2, 3.6, 4, 4.4, 4.8]
ms = {'WCst': []}
eg = {'WCst': []}
with lib.updater('    > population merge', nr*len(T_list)*2) as upd:
    for T in T_list:
        get2(ms, cs, upd, lib.ms, nr=nr, theta=theta, ns=ns,
                        as_is=['-ej', str(T), '2', '1'])
        get2(eg, cs, upd, lib.simul, nr=nr, theta=theta, num_pop=2, num_chrom=ns,
                        events=[{'cat': 'merge', 'src': 1, 'dst': 0, 'T': T}])

pyplot.plot(T_list, ms['WCst'], 'k+', mfc='None', label='ms')
pyplot.plot(T_list, eg['WCst'], 'ko', mfc='None', label='EggLib')
pyplot.xlabel(r'$T$')
pyplot.ylabel(r'$F_{ST}$')
pyplot.legend().set_frame_on(False)
pyplot.savefig(lib.fname('6-24.png'))
pyplot.clf()

fstream.write(r'''
Population merge
----------------

.. figure:: 6-24.png
    :scale: 75%

    Consequences of the fusion of two unconnected populations. Comparison
    of results with ms and EggLib.
''')

###########

nr = 5000
ns = 50
theta = 5
S_list = [0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1]
T = 0.1
d1 = 0.001
d2 = 0.01
d3 = 0.05

cs = egglib.stats.ComputeStats()
cs.add_stats('D', 'thetaW')
ctrl1 = {'thetaW': [], 'D': []}
ctrl2 = {'thetaW': [], 'D': []}
ctrl3 = {'thetaW': [], 'D': []}
test = {'thetaW': [], 'D': []}

with lib.updater('    > bottleneck', nr*len(S_list)*4) as upd:
    for S in S_list:
        if S == 0:
            Nb1 = 1
            Nb2 = 1
            Nb3 = 1
        else:
            Nb1 = d1/S
            Nb2 = d2/S
            Nb3 = d3/S
        get2(ctrl1, cs, upd, lib.simul, nr=nr, theta=theta, num_pop=1, num_chrom=[ns],
             events=[{'cat': 'size', 'T': T, 'N': Nb1}, {'cat': 'size', 'T': T+d1, 'N': 1}])
        get2(ctrl2, cs, upd, lib.simul, nr=nr, theta=theta, num_pop=1, num_chrom=[ns],
             events=[{'cat': 'size', 'T': T, 'N': Nb2}, {'cat': 'size', 'T': T+d2, 'N': 1}])
        get2(ctrl3, cs, upd, lib.simul, nr=nr, theta=theta, num_pop=1, num_chrom=[ns],
             events=[{'cat': 'size', 'T': T, 'N': Nb3}, {'cat': 'size', 'T': T+d3, 'N': 1}])
        get2(test, cs, upd, lib.simul, nr=nr, theta=theta, num_pop=1, num_chrom=[ns],
             events=[{'cat': 'bottleneck', 'T': T, 'S': S}])

pyplot.plot(S_list, ctrl1['thetaW'], 'k+', mfc='None', label='control 1')
pyplot.plot(S_list, ctrl2['thetaW'], 'm+', mfc='None', label='control 2')
pyplot.plot(S_list, ctrl3['thetaW'], 'r+', mfc='None', label='control 3')
pyplot.plot(S_list, test['thetaW'], 'ko', mfc='None', label='bottleneck')
pyplot.xlabel(r'$S$')
pyplot.ylabel(r'$\hat{\theta}_W$')
pyplot.legend().set_frame_on(False)
pyplot.savefig(lib.fname('6-25.png'))
pyplot.clf()

pyplot.plot(S_list, ctrl1['D'], 'k+', mfc='None', label='control 1')
pyplot.plot(S_list, ctrl2['D'], 'm+', mfc='None', label='control 2')
pyplot.plot(S_list, ctrl3['D'], 'r+', mfc='None', label='control 3')
pyplot.plot(S_list, test['D'], 'ko', mfc='None', label='bottleneck')
pyplot.xlabel(r'$S$')
pyplot.ylabel(r"Tajima's $D$")
pyplot.legend().set_frame_on(False)
pyplot.savefig(lib.fname('6-26.png'))
pyplot.clf()

fstream.write(r'''
Bottleneck
----------

.. figure:: 6-25.png
    :scale: 75%

    Effect of a bottleneck of varying strength on :math:`\hat{theta}_W`.
    The bottleneck implemented as a single-parameter event (circle symbols)
    is  compared with a double size change implementation (plus symbols)
    following the formula :math:`S=\frac{d}{N_b}` where :math:`d` is set
    to 0.001 (black), 0.01 (magenta) and 0.05 (red). A discrepancy is
    noticeable for the latter case, probably due to the mitigating effect
    of mutations occurring between the two events (during the bottleneck).

.. figure:: 6-26.png
    :scale: 75%

    Same as previous graph, but for Tajima's :math:`D`.

''')

###########

nr = 1000
ns1 = 20
ns2 = ns1
theta = 1
T_list = [0, 0.2, 0.4, 0.6, 0.8, 1, 1.2, 1.4, 1.6]
d = {999: {}}
d[999][0] = dict([(i, [i]) for i in xrange(ns1)])
d[999][1] = dict([(i, [i]) for i in xrange(ns1, ns1+ns2)])
cs = egglib.stats.ComputeStats(struct=egglib.stats.make_structure(d, None))
cs.add_stats('WCst')
A = {'WCst': []}
B = {'WCst': []}
C = {'WCst': []}

with lib.updater('    > delayed sample', nr*len(T_list)*3) as upd:
    for T in T_list:
        get2(A, cs, upd, lib.simul, nr=nr, theta=theta, num_pop=1, num_chrom=[ns1],
             events=[{'cat': 'sample', 'T': T, 'idx': 0, 'label': 1, 'num_chrom': ns2, 'num_indiv': 0}])
        get2(B, cs, upd, lib.simul, nr=nr, theta=theta, num_pop=2, num_chrom=[ns1, 0],
             events=[{'cat': 'sample', 'T': T, 'idx': 1, 'label': 1, 'num_chrom': ns2, 'num_indiv': 0},
                     {'cat': 'merge', 'T': 1.6, 'src': 1, 'dst': 0}])
        get2(C, cs, upd, lib.simul, nr=nr, theta=theta, num_pop=2, num_chrom=[0, 0],
             events=[{'cat': 'sample', 'T': 1.6, 'idx': 0, 'label': 0, 'num_chrom': ns1, 'num_indiv': 0},
                     {'cat': 'sample', 'T': T, 'idx': 1, 'label': 1, 'num_chrom': ns2, 'num_indiv': 0},
                     {'cat': 'merge', 'T': 1.6, 'src': 1, 'dst': 0}])

pyplot.plot(T_list, A['WCst'], 'ko:', mfc='w', label='single pop')
pyplot.plot(T_list, B['WCst'], 'k^:', mfc='w', label='two pops A')
pyplot.plot(T_list, C['WCst'], 'ks:', mfc='w', label='two pops B')
pyplot.axhline(0, c='k', ls=':')
pyplot.axhline((A['WCst'][-1]+B['WCst'][-1]+C['WCst'][0])/3.0, c='k', ls=':')
pyplot.xlabel(r'$T$')
pyplot.ylabel(r'$F_{ST}$')
pyplot.legend().set_frame_on(False)
pyplot.savefig(lib.fname('6-27.png'))
pyplot.clf()

fstream.write(r'''
Delayed sample
--------------

.. figure:: 6-27.png
    :scale: 75%

    This graph shows three series of simulations involving delayed samples.
    In the single-population case (circles), a sample is taken at the start
    and a second one at an increasing date. :math:`F_{ST}` increases.
    Other simulations have two populations. In the case A (triangles),
    in the first population a sample is taken at the start and in the second
    population a sample is taken at an increasing date. This causes
    :math:`F_{ST}` to decrease because the distance between the samples
    decreases. At :math:`T=1.6`, the outcome is identical to the equivalent
    date of the first series. The last case, B, (squares) has the same
    setting except that the first population has a sample at :math:`T=1.6`
    in all cases. Then the :math:`F_{ST}` decreases until the two samples
    are at the same time. The first square point is the same as the last
    triangle (and circle) since there is 1.6 time of coalescence between
    samples in all cases, but only because the sample size are the same.
    If they are different, is appears that :math:`F_{ST}` is smaller if
    the recent sample has smaller sample size (not shown here).
''')

# assertion on the group labels of a relatively complex object
coal = egglib.coalesce.Simulator(num_pop=3, migr=1, num_mut=1,
    num_chrom=[5, 2, 8], num_indiv=[2, 0, 6])
coal.params.add_event(cat='sample', idx=0, T=1, label=1, num_chrom=5, num_indiv=2)
coal.params.add_event(cat='sample', idx=1, T=1, label=3, num_chrom=1, num_indiv=4)
coal.params.add_event(cat='sample', idx=2, T=1, label=2, num_chrom=3, num_indiv=1)
expect = []
for i in xrange(2): expect.extend([[0, i], [0, i]])
for i in xrange(5): expect.append([0, 2+i])
for i in xrange(2): expect.append([1, 7+i])
for i in xrange(6): expect.extend([[2, 9+i], [2, 9+i]])
for i in xrange(8): expect.append([2, 15+i])
for i in xrange(2): expect.extend([[1, 23+i], [1, 23+i]])
for i in xrange(5): expect.append([1, 25+i])
for i in xrange(4): expect.extend([[3, 30+i], [3, 30+i]])
for i in xrange(1): expect.append([3, 34+i])
for i in xrange(1): expect.extend([[2, 35+i], [2, 35+i]])
for i in xrange(3): expect.append([2, 36+i])
aln = coal.simul()
assert expect==[list(sam[2]) for sam in aln]

##########

fstream.close()
