import egglib, lib
from matplotlib import pyplot

lib.showl('# Recombination')
nr = 1000
fstream = open(lib.fname('report.rst'), 'a')
fstream.write('''
=============
Recombination
=============
''')

RHO = [0, 0.25, 0.5, 0.75, 1, 1.25, 1.5, 1.75, 2, 2.25, 2.5]
theta = 5
ns = [40]
R_eg = [[], [], [], []]
R_ms = [[], [], [], []]
cs = egglib.stats.ComputeStats()
cs.add_stats('Rmin')
with lib.updater('    > effect on Rmin', len(RHO)*nr*8) as upd:
    for rho in RHO:
        for i in xrange(4):
            R = 0.0
            n = 0
            for aln in lib.simul(num_pop=1, num_chrom=ns, theta=theta, recomb=rho, nr=nr):
                upd.add_one()
                if aln.ls > 1:
                    R += cs.process_align(aln)['Rmin']
                    n += 1
            R_eg[i].append(R/n)
            R = 0.0
            n = 0
            for aln in lib.ms(theta=theta, ns=ns, nr=nr, recomb=rho):
                upd.add_one()
                if aln.ls > 1:
                    R += cs.process_align(aln)['Rmin']
                    n += 1
            R_ms[i].append(R/n)

for i in xrange(4):
    pyplot.plot(RHO, R_ms[i], 'kx')
    pyplot.plot(RHO, R_eg[i], 'ko', mfc='None')
pyplot.plot([],[], 'kx', label='ms')
pyplot.plot([],[], 'ko', mfc='None', label='EggLib')
pyplot.legend()
pyplot.xlabel(r'$\rho$')
pyplot.ylabel('Rmin')
pyplot.savefig(lib.fname('5-1.png'))
pyplot.clf()

fstream.write(r'''
Effect of the recombination rate on the minimal number of recombination events
------------------------------------------------------------------------------

.. figure:: 5-1.png
    :scale: 75%

    Hudson's minimal number of recombination events against the
    recombination rate: comparision betweem EggLib and ms. Four
    different batches are run for both programs to account for high
    variance.
''')

###########

P = [0.001, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0]
L = 100
theta = 1

nr = 5000
S = [[], [], []]
R = [[], [], []]

cs = egglib.stats.ComputeStats()
cs.add_stats('Rmin', 'S')
with lib.updater('    > effect of region restriction', len(P)*nr*3) as upd:
    for p in P:
        pos = [1.0*i/(L-1)*p for i in range(L)]
        for rho in xrange(3):
            rmin = 0.0
            s = 0.0
            n = 0
            args = {'num_pop': 1, 'num_chrom': [40], 'theta': theta,
                    'recomb': rho, 'nr': nr, 'num_sites': L, 'site_positions': pos}
            for aln in lib.simul(**args):
                if aln.ls > 0:
                    stats = cs.process_align(aln)
                    s += stats['S']
                    if stats['S'] > 1:
                        rmin += stats['Rmin']
                        n += 1
                upd.add_one()
            S[rho].append(s/nr)
            R[rho].append(rmin/n)
R_ = []
S_ = []
for rho in xrange(3):
    rmin = 0.0
    s = 0.0
    n = 0
    args = {'num_pop': 1, 'num_chrom': [40], 'theta': theta, 'recomb': rho, 'nr': nr}
    for aln in lib.simul(**args):
        if aln.ls > 0:
            stats = cs.process_align(aln)
            s += stats['S']
            if stats['S'] > 1:
                rmin += stats['Rmin']
                n += 1
    S_.append(s/nr)
    R_.append(rmin/n)

pyplot.plot(P, R[0], 'ko-', label=r'$\rho=0$')
pyplot.plot(P, R[1], 'bo-', label=r'$\rho=1$')
pyplot.plot(P, R[2], 'ro-', label=r'$\rho=2$')
pyplot.plot([0], [R_[0]], 'kd', mfc='None')
pyplot.plot([0], [R_[1]], 'bd', mfc='None')
pyplot.plot([0], [R_[2]], 'rd', mfc='None')
pyplot.plot([], [], 'kd', mfc='None', label='ISM')
pyplot.legend()
pyplot.xlabel('Sub-region')
pyplot.ylabel(r'$R_{min}$')
pyplot.savefig(lib.fname('5-2.png'))
pyplot.clf()

pyplot.plot(P, S[0], 'ko-', label=r'$\rho=0$')
pyplot.plot(P, S[1], 'bo-', label=r'$\rho=1$')
pyplot.plot(P, S[2], 'ro-', label=r'$\rho=2$')
pyplot.plot([0], [S_[0]], 'kd', mfc='None')
pyplot.plot([0], [S_[1]], 'bd', mfc='None')
pyplot.plot([0], [S_[2]], 'rd', mfc='None')
pyplot.plot([], [], 'kd', mfc='None', label='ISM')
pyplot.legend()
pyplot.xlabel('Sub-region')
pyplot.ylabel(r'$S$')
pyplot.ylim(3, 5)
pyplot.savefig(lib.fname('5-3.png'))
pyplot.clf()

fstream.write(r'''
Effect of the site localisation on Rmin and S
---------------------------------------------

.. figure:: 5-2.png
    :scale: 75%

    Effect of the recombination rate on :math:`R_{min}`. The values are
    presented as a function of the proportion of the simulated segment
    in which sites are restricted. For example, at 50%, sites are evenly
    distributed between 0 and 0.5. The first value is near 0. Three
    recombination rates are tested, and cases with infinite (diamonds)
    and finite (solid lines) number of sites are tested as well. The basis
    value (black diamond) is 0 as expected without recombination or
    homoplasy. Adding recombination (blue and red diamonds) increases
    the number of recombination events. Similarly, adding homoplasy even
    without recombination (black line), increases :math:`Rmin` but there
    is no effect of site position (because no recombination). Adding both
    homoplasy and recombination (blue and red lines) further increases
    the number of recombination events and there is an effect of site
    positions (the more clustered the sites, the more mitigated the effect
    of recombination, because many actual recombination events have no effect
    because they don't occur between sites.

.. figure:: 5-3.png
    :scale: 75%

    Same simulations for the number of polymorphic sites. No visible
    effect of site positions of recombination rate but the effect of
    homoplasy is visible (diamonds -- without homoplasy -- a little
    bit higher).

''')

###########

fstream.close()
