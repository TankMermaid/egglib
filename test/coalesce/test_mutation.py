import egglib, lib, os
from matplotlib import pyplot

lib.showl('# Mutation')

f = open(lib.fname('report.rst'), 'a')
f.write('''
========
Mutation
========
''')

nr = 1000
NSITES = [10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150]
THETA = [1, 2, 3]
S = [[], [], []]
D = [[], [], []]
R = [[], [], []]
cs = egglib.stats.ComputeStats(multi_hits=True)
cs.add_stats('S', 'lseff', 'D', 'Rmin')
with lib.updater('    > number of sites and homoplasy', len(NSITES)*nr*3) as upd:
    for nsites in NSITES:
        for theta, s, d, r in zip(THETA, S, D, R):
            acc_s = 0.0
            acc_d = 0.0
            acc_r = 0.0
            n1 = 0
            n2 = 0
            for aln in lib.simul(num_pop=1, num_chrom=[40], theta=theta, num_sites=nsites, nr=nr):
                upd.add_one()
                stats = cs.process_align(aln)
                if stats['lseff'] != 0:
                    acc_s += stats['S']
                if stats['S'] > 0:
                    n1 += 1
                    acc_d += stats['D']
                if stats['S'] > 1:
                    n2 += 1
                    acc_r += stats['Rmin']
            s.append(acc_s/(nr))
            d.append(acc_d/n1)
            r.append(acc_r/n2)

pyplot.plot(NSITES, S[0], 'go:', mfc='w', label=r'$\theta={0}$'.format(THETA[0]))
pyplot.plot(NSITES, S[1], 'bo:', mfc='w', label=r'$\theta={0}$'.format(THETA[1]))
pyplot.plot(NSITES, S[2], 'ro:', mfc='w', label=r'$\theta={0}$'.format(THETA[2]))
pyplot.legend().set_frame_on(False)
pyplot.xlabel(r'$L$')
pyplot.ylabel(r'$S$')
pyplot.savefig(lib.fname('2-A.png'))
pyplot.clf()

pyplot.plot(NSITES, D[0], 'go:', mfc='w', label=r'$\theta={0}$'.format(THETA[0]))
pyplot.plot(NSITES, D[1], 'bo:', mfc='w', label=r'$\theta={0}$'.format(THETA[1]))
pyplot.plot(NSITES, D[2], 'ro:', mfc='w', label=r'$\theta={0}$'.format(THETA[2]))
pyplot.legend().set_frame_on(False)
pyplot.ylim(-1, 1)
pyplot.xlabel(r'$L$')
pyplot.ylabel(r'$D$')
pyplot.savefig(lib.fname('2-B.png'))
pyplot.clf()

pyplot.plot(NSITES, R[0], 'go:', mfc='w', label=r'$\theta={0}$'.format(THETA[0]))
pyplot.plot(NSITES, R[1], 'bo:', mfc='w', label=r'$\theta={0}$'.format(THETA[1]))
pyplot.plot(NSITES, R[2], 'ro:', mfc='w', label=r'$\theta={0}$'.format(THETA[2]))
pyplot.legend().set_frame_on(False)
pyplot.xlabel(r'$L$')
pyplot.ylabel(r'$R_{MIN}$')
pyplot.savefig(lib.fname('2-C.png'))
pyplot.clf()

f.write('''
Number of sites and homoplasy
-----------------------------

.. figure:: 2-A.png
    :scale: 75%

    Effect of the number of sites on the number of polymorphic sites.
    The increase reflects the diminution of the effect of homoplasy.

.. figure:: 2-B.png
    :scale: 75%

    Effect on Tajima's :math:`D`. An effect of homoplasy toward positive
    Tajima's :math:`D` is visible.

.. figure:: 2-C.png
    :scale: 75%

    Effect on the minimal number of recombination events. The effect is
    clearly visible.

''')

#######

nr = 1000
NMUT = [0, 5, 10, 15, 20, 30, 40, 50, 60, 70, 80, 90, 100]
S1 = []
S2 = []
S3 = []
D1 = []
D2 = []
D3 = []
cs = egglib.stats.ComputeStats()
cs.add_stats('S', 'lseff', 'D')
with lib.updater('    > number of mutations and homoplasy', len(NMUT)*3*nr) as upd:
    for nmut in NMUT:
        s1 = 0.0
        s2 = 0.0
        s3 = 0.0
        d1 = 0.0
        d2 = 0.0
        d3 = 0.0
        n1 = 0
        n2 = 0
        n3 = 0
        for aln in lib.simul(num_pop=1, num_chrom=[20], theta=0, num_sites=0, num_mut=nmut, nr=nr):
            upd.add_one()
            stats = cs.process_align(aln)
            if stats['lseff'] != 0:
                s1 += stats['S']
            if stats['S'] > 0:
                n1 += 1
                d1 += stats['D']
        for aln in lib.simul(num_pop=1, num_chrom=[20], theta=0, num_sites=200, num_mut=nmut, nr=nr):
            upd.add_one()
            stats = cs.process_align(aln)
            if stats['lseff'] != 0:
                s2 += stats['S']
            if stats['S'] > 0:
                n2 += 1
                d2 += stats['D']
        for aln in lib.simul(num_pop=1, num_chrom=[20], theta=0, num_sites=50, num_mut=nmut, nr=nr):
            upd.add_one()
            stats = cs.process_align(aln)
            if stats['lseff'] != 0:
                s3 += stats['S']
            if stats['S'] > 0:
                n3 += 1
                d3 += stats['D']
        S1.append(s1/(nr))
        S2.append(s2/(nr))
        S3.append(s3/(nr))
        if nmut > 0:
            D1.append(d1/n1)
            D2.append(d2/n2)
            D3.append(d3/n3)

pyplot.plot(NMUT, NMUT, 'k:')
pyplot.plot(NMUT, S1, 'mo:', mfc='w', label='L=INF')
pyplot.plot(NMUT, S2, 'ro:', mfc='w', label='L=200')
pyplot.plot(NMUT, S3, 'bo:', mfc='w', label='L=50')
pyplot.axhline(50, c='k', ls=':')
pyplot.legend().set_frame_on(False)
pyplot.xlabel('number of mutations')
pyplot.ylabel(r'$S$')
pyplot.savefig(lib.fname('2-1.png'))
pyplot.clf()

pyplot.plot(NMUT[1:], D1, 'mo:', mfc='w', label='L=INF')
pyplot.plot(NMUT[1:], D2, 'ro:', mfc='w', label='L=200')
pyplot.plot(NMUT[1:], D3, 'bo:', mfc='w', label='L=50')
pyplot.axhline(0, c='k', ls=':')
pyplot.legend().set_frame_on(False)
pyplot.ylim(-1, 1)
pyplot.xlabel('number of mutations')
pyplot.ylabel(r'$D$')
pyplot.savefig(lib.fname('2-2.png'))
pyplot.clf()

f.write('''
Fixed number of mutations and homoplasy
---------------------------------------

.. figure:: 2-1.png
    :scale: 75%

    Show number of polymorphic sites with fixed number of mutations, with
    either the ISM or two different numbers of mutable sites. S is equal to the
    number of mutation in ISM (first series), and decreased otherwise.

.. figure:: 2-2.png
    :scale: 75%

    The same with Tajima's :math:`D`, which shows that homoplasy causes
    and increase of Tajima's :math:`D`.
''')

#######

cs = egglib.stats.ComputeStats()
cs.add_stats('sites')

W = [0.001, 0.05, 0.1, 0.15, 0.2, 0.25, 0.3, 0.35, 0.4, 0.45, 0.5,
     0.55, 0.6, 0.65, 0.7, 0.75, 0.8, 0.85, 0.9, 0.95, 0.999]
NMUT = [1, 2, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50]
F = 5

with lib.updater('    > site weight', len(W)*nr*F+len(NMUT)*nr*F) as upd:
    Y = []

    for w in W:
        n = 0.0
        for aln in lib.simul(num_pop=1, num_chrom=[20], num_sites=2, num_mut=1, nr=nr*F, site_weight=[1-w, w]):
            upd.add_one()
            [x] = cs.process_align(aln)['sites']
            if x == 1: n += 1
            else: assert x == 0
        Y.append(n / (nr*F))

    pyplot.plot(W, Y, 'ro', mfc='w', label='EggLib')
    pyplot.plot(W, W, 'k-', label='expectation')
    pyplot.xlabel(r'$w_2$')
    pyplot.ylabel('Probability that second site is muted')
    pyplot.legend().set_frame_on(False)
    pyplot.axhline(0, ls=':', c='k')
    pyplot.axhline(1, ls=':', c='k')
    pyplot.savefig(lib.fname('2-3.png'))
    pyplot.clf()

    cs.configure(multi_hits=True)
    cs.add_stats('S')
    N1 = []
    N2 = []
    N3 = []

    weights = [0.1, 1, 2]
    for nmut in NMUT:
        n1 = 0.0
        n2 = 0.0
        n3 = 0.0
        for aln in lib.simul(num_pop=1, num_chrom=[20], num_sites=3,
            num_mut=nmut, nr=nr*F, site_weight=weights, mut_model='IAM'):
            upd.add_one()
            hits = cs.process_align(aln)['sites']
            assert len(hits) <= 3 and len(hits) > 0

            for i in hits:
                if i == 0: n1 += 1
                elif i == 1: n2 += 1
                elif i == 2: n3 += 1
                else: raise AssertionError
        N1.append(n1/(nr*F))
        N2.append(n2/(nr*F))
        N3.append(n3/(nr*F))

    p1 = 1.0*weights[0]/sum(weights)
    p2 = 1.0*weights[1]/sum(weights)
    p3 = 1.0*weights[2]/sum(weights)

    E1 = [1-((1-p1)**n) for n in NMUT]
    E2 = [1-((1-p2)**n) for n in NMUT]
    E3 = [1-((1-p3)**n) for n in NMUT]

    pyplot.plot(NMUT, N1, 'bo', mfc='w', label=r'$w_1 = {0}$'.format(weights[0]))
    pyplot.plot(NMUT, N2, 'go', mfc='w', label=r'$w_2 = {0}$'.format(weights[1]))
    pyplot.plot(NMUT, N3, 'ro', mfc='w', label=r'$w_3 = {0}$'.format(weights[2]))
    pyplot.plot(NMUT, E1, 'b:')
    pyplot.plot(NMUT, E2, 'g:')
    pyplot.plot(NMUT, E3, 'r:')
    pyplot.plot([1], [p1], 'bx')
    pyplot.plot([1], [p2], 'gx')
    pyplot.plot([1], [p3], 'rx')
    pyplot.xlabel(r'Number of mutations')
    pyplot.ylabel('Probability that site is muted')
    pyplot.legend().set_frame_on(False)
    pyplot.axhline(0, ls=':', c='k')
    pyplot.axhline(1, ls=':', c='k')
    pyplot.savefig(lib.fname('2-4.png'))
    pyplot.clf()

f.write('''
Effect of site weight
---------------------

.. figure:: 2-3.png
    :scale: 75%

    There are two sites: first site has weight 1 and second site has a
    weigth :math:`w_2` ranging from {0} to {1}. Simulations are run with
    a fixed number of mutations set to 1 and we monitor the proportion
    of times where the second site (rather than the first) is hit by the
    single mutation. The result is plotted against the expectation which
    is equal to :math:`w_2`. Dotted lines are placed at 0 and 1.

.. figure:: 2-4.png
    :scale: 75%

    In this case there are three sites with fixed weights ({2[0]}, {2[1]}, and
    {2[2]}, respectively) and the number of mutations varies. The proportion
    of simulations where a site is muted at least once is recorded
    (since there can be multiple mutations, several sites can be muted
    in the same simulation). The dotted lines give the theoretical
    expectaction, as :math:`1-(1-p)^n` where :math:`p` is the relative
    weight of the site under consideration, and :math:`n` is the number
    of mutations. A cross symbol is placed at :math:`n=1` to the expected
    value (equal to the relative weight of each site). To avoid homoplasy
    (which would reduce the probabily of a site to be muted), the infinite
    allele mutation model is used.
'''.format(min(W), max(W), weights))

#######

cs = egglib.stats.ComputeStats()
cs.add_stats('S', 'D')
RHO = [0, 0.25, 0.5, 0.75, 1, 1.25, 1.5, 1.75, 2, 2.25, 2.5, 2.75, 3]
S1 = []
S2 = []
S3 = []
S4 = []
S5 = []
S6 = []
D1 = []
D2 = []
D3 = []
D4 = []
D5 = []
D6 = []

F = 5
theta = 8
eta = 30
L = 100
pos = [i/200.+0.20 for i in range(100)]

def get(destS, destD, **kwargs):
    s = 0.0
    d = 0.0
    n = 0.0
    for aln in lib.simul(**kwargs):
        upd.add_one()
        if aln.ls > 0:
            stats = cs.process_align(aln)
            s += stats['S']
            if stats['S'] > 0:
                n += 1
                d += stats['D']
    destS.append(s/(nr*F))
    destD.append(d/n)

with lib.updater('    > independence from rho', len(RHO)*nr*F*6) as upd:
    for rho in RHO:
        get(S1, D1, num_pop=1, num_chrom=[20], theta=theta, nr=nr*F, recomb=rho)
        get(S2, D2, num_pop=1, num_chrom=[20], num_mut=eta, nr=nr*F, recomb=rho)
        get(S3, D3, num_pop=1, num_chrom=[20], num_sites=L, theta=theta, nr=nr*F, recomb=rho)
        get(S4, D4, num_pop=1, num_chrom=[20], num_sites=L, num_mut=eta, nr=nr*F, recomb=rho)
        get(S5, D5, num_pop=1, num_chrom=[20], num_sites=L, theta=theta, nr=nr*F, recomb=rho, site_positions=pos)
        get(S6, D6, num_pop=1, num_chrom=[20], num_sites=L, num_mut=eta, nr=nr*F, recomb=rho, site_positions=pos)

pyplot.plot(RHO, S1, 'ro-', mfc='w', label=r'$\theta={0}$'.format(theta))
pyplot.plot(RHO, S2, 'rs-', mfc='w', label=r'$\eta={0}$'.format(eta))
pyplot.plot(RHO, S3, 'bo-', mfc='w', label=r'$\theta={0}$, $L={1}$'.format(theta, L))
pyplot.plot(RHO, S4, 'bs-', mfc='w', label=r'$\eta={0}$, $L={1}$'.format(eta, L))
pyplot.plot(RHO, S5, 'go-', mfc='w', label=r'$\theta={0}$, $L={1}*$'.format(theta, L))
pyplot.plot(RHO, S6, 'gs-', mfc='w', label=r'$\eta={0}$, $L={1}*$'.format(eta, L))
pyplot.xlabel(r'$\rho$')
pyplot.ylabel(r'$S$')
pyplot.legend(loc=2)
pyplot.axhline(25.2, c='k', ls=':')
pyplot.axhline(25.5, c='k', ls=':')
pyplot.axhline(23.5, c='k', ls=':')
pyplot.axhline(23.8, c='k', ls=':')
pyplot.axhline(28.3, c='k', ls=':')
pyplot.axhline(28.5, c='k', ls=':')
pyplot.savefig(lib.fname('2-5.png'))
pyplot.clf()

pyplot.plot(RHO, D1, 'ro-', mfc='w', label=r'$\theta={0}$'.format(theta))
pyplot.plot(RHO, D2, 'rx-', mfc='w', label=r'$\eta={0}$'.format(eta))
pyplot.plot(RHO, D3, 'bo-', mfc='w', label=r'$\theta={0}$, $L={1}$'.format(theta, L))
pyplot.plot(RHO, D4, 'bx-', mfc='w', label=r'$\eta={0}$, $L={1}$'.format(eta, L))
pyplot.plot(RHO, D5, 'go-', mfc='w', label=r'$\theta={0}$, $L={1}*$'.format(theta, L))
pyplot.plot(RHO, D6, 'gx-', mfc='w', label=r'$\eta={0}$, $L={1}*$'.format(eta, L))
pyplot.xlabel(r'$\rho$')
pyplot.ylabel(r'$D$')
pyplot.axhline(0, c='k', ls=':')
pyplot.ylim(-1, 1)
pyplot.legend().set_frame_on(False)
pyplot.savefig(lib.fname('2-6.png'))
pyplot.clf()

f.write(r'''
Check independence from recombination
-------------------------------------

.. figure:: 2-5.png
    :scale: 75%

    The correlation of the number of polymorphic sites with the rate of
    recombination :math:`\rho` is examined under several sets of parameters:
    with a fixed number of mutations :math:`\eta` or not, with an infinite
    site model (the default) or a finite number of mutable sites, and with
    a particular case (noted with :math:`*`) where the sites are located in a subset
    of the simulated region. There is no effect of :math:`\rho` under either
    of these conditions. However, a subtle decrease of :math:`S` is perceptible
    with the fixed number of mutations and finite number of sites (square
    symbols). The dotted lines help visualising this decrease. The reason
    is probably that, with recombination, the unbalance of tree lengths
    at different sites make more likely that several mutations hit the
    same site (therefore increasing homoplasy). This effect should be
    visible if :math:`\theta` rather than :math:`\eta` is set (see below).

.. figure:: 2-6.png
    :scale: 75%

    The same with Tajima's :math:`D`, which does not react on the recombination
    rate or the localization of sizes (only on the finite number of sites,
    because of homoplasy, as seen before).

''')

cs = egglib.stats.ComputeStats(multi_hits=True)
cs.add_stats('sites', 'S', 'eta')
RHO = [0, 0.3, 0.6, 0.9, 1.2, 1.5, 1.8, 2.1, 2.4, 2.7, 3]
mutedIAM = []
S = []
eta = []
L = 2
theta=0.7
F = 75
with lib.updater('    > independence from rho (theta)', len(RHO)*nr*F) as upd:
    for rho in RHO:
        muted = [0,0,0,0]
        s = 0.0
        e = 0.0
        for aln in lib.simul(num_pop=1, num_chrom=[20], num_sites=2,
            theta=theta, recomb=rho, nr=nr*F, mut_model='IAM'):
            stats = cs.process_align(aln)
            if stats['sites'] == []: muted[0] += 1
            elif stats['sites'] == [0]: muted[1] += 1
            elif stats['sites'] == [1]: muted[2] += 1
            elif stats['sites'] == [0, 1]: muted[3] += 1
            else: raise ValueError, stats
            s += stats['S']
            e += stats['eta']
            upd.add_one()
        mutedIAM.append([1.0*i/(nr*F) for i in muted])
        S.append(s/(nr*F))
        eta.append(e/(nr*F))

A, B, C, D = zip(*mutedIAM)
pyplot.plot(RHO, A, 'ko', mfc='w', label='[]')
pyplot.plot(RHO, B, 'go', mfc='w', label='[0]')
pyplot.plot(RHO, C, 'bo', mfc='w', label='[1]')
pyplot.plot(RHO, D, 'ro', mfc='None', label='[0, 1]')
pyplot.axhline(0.11, c='k', ls=':')
pyplot.axhline(0.2, c='k', ls=':')
pyplot.axhline(0.48, c='k', ls=':')
pyplot.legend().set_frame_on(False)
pyplot.xlabel(r'$\rho$')
pyplot.ylabel('proportion of sites')
pyplot.savefig(lib.fname('2-7.png'))
pyplot.clf()

pyplot.plot(RHO, S, 'ko-', mfc='None', label=r'$S$')
pyplot.plot(RHO, eta, 'rs-', mfc='None', label=r'$\hat{\eta}$')
pyplot.legend().set_frame_on(False)
pyplot.xlabel(r'$\rho$')
pyplot.ylabel('number of sites')
pyplot.axhline(1.37, c='k', ls=':')
pyplot.axhline(2.18, c='k', ls=':')
pyplot.savefig(lib.fname('2-8.png'))
pyplot.clf()

f.write(r'''
.. figure:: 2-7.png
    :scale: 75%

    This figure is generated with a fixed number of sites of 2 and
    :math:`\theta` set to {0} and with the IAM model mutation allowing
    to detect all mutations. We count the number of simulations where
    neither site is muted (black curve), both sites are muted (red)
    and either one is muted (blue and green). Arbitrary dotted lines are
    placed to ease seeing trends. We observe that the number of case where
    only one site, rather than both sites, is muted tends to increase,
    as expected due to the tree length imbalance due to tree unphasing.
    There is also a decrease of the number of simulations without any
    mutation, possibly due to the increase of variance making this
    unlikely event less frequent.

.. figure:: 2-8.png
    :scale: 75%

    Same as above: the number of polymorphic sites (:math:`S`) and
    the number of observed mutations (:math:`\hat{{\eta}}`) show no
    visible trend.

'''.format(theta))

#######

nr = 1000
theta = 5.0
NALL = [2, 3, 4, 5, 6, 7, 8, 9, 10]
cs = egglib.stats.ComputeStats()
cs.add_stats('Rmin')
Rmin = {0: [], 200: [], 50: []}
with lib.updater('    > homoplasy', len(NALL)*nr*len(Rmin)) as upd:
    for nall in NALL:
        for ls in Rmin:
            n = 0
            rmin = 0.0
            for aln in lib.simul(num_pop=1, num_chrom=[20], num_sites=ls,
                    theta=theta, nr=nr, mut_model='KAM', num_alleles=nall):
                X = cs.process_align(aln)['Rmin']
                if X is not None:
                    n += 1
                    rmin += X
                upd.add_one()
            Rmin[ls].append(rmin/n)

pyplot.plot(NALL, Rmin[0], 'ko:', mfc='w', label='ISM')
pyplot.plot(NALL, Rmin[200], 'bs:', mfc='w', label='ls=200')
pyplot.plot(NALL, Rmin[50], 'r^:', mfc='w', label='ls=50')
pyplot.xlabel('Number of alleles')
pyplot.ylabel(r'$R_{min}$')
pyplot.legend().set_frame_on(False)
pyplot.savefig(lib.fname('2-9.png'))
pyplot.clf()

f.write(r'''
Number of alleles and homoplasy
-------------------------------

.. figure:: 2-9.png
    :scale: 75%

    In this graphic, :math:`R_{min}` reflects homoplasy. There is no
    homoplasy, therefore :math:`R_{min}=0` if the number of sites is
    infinite (ISM), and otherwise homoplasy is stronger with less
    alleles and less sites.

''')

#######

nr = 1000
theta = 5.0
KAPPA = [1, 1.5, 2, 2.5, 3, 3.5, 4, 4.5, 5]
cs = egglib.stats.ComputeStats()
cs.add_stats('Rmin', 'S')
Rmin = {0: [], 100: [], 50: []}
with lib.updater('    > transition bias', len(KAPPA)*nr*len(Rmin)) as upd:
    for k in KAPPA:
        for ls in Rmin:
            n = 0
            rmin = 0.0
            for aln in lib.simul(num_pop=1, num_chrom=[20], num_sites=ls,
                    theta=theta, nr=nr, mut_model='KAM', num_alleles=4,
                    trans_matrix=[[None, 1, k, 1], [1, None, 1, k], [k, 1, None, 1], [1, k, 1, None]]):
                X = cs.process_align(aln)['Rmin']
                if X is not None:
                    n += 1
                    rmin += X
                GC = 0
                t = 0
                upd.add_one()
            Rmin[ls].append(rmin/n)
    
pyplot.plot(KAPPA, Rmin[0], 'ko:', mfc='w', label='ISM')
pyplot.plot(KAPPA, Rmin[100], 'bs:', mfc='w', label='ls=100')
pyplot.plot(KAPPA, Rmin[50], 'r^:', mfc='w', label='ls=50')
pyplot.xlabel(r'$\kappa$')
pyplot.ylabel(r'$R_{min}$')
pyplot.legend().set_frame_on(False)
pyplot.savefig(lib.fname('2-10.png'))
pyplot.clf()

f.write(r'''
Transition-transversion rate ratio
----------------------------------

.. figure:: 2-10.png
    :scale: 75%

    Simulations under the KAM with 4 alleles. A transition-transversion
    rate ratio is implemented. Since there is no recombination, with the
    ISM, no effect on :math:`R_{min}` is expected since there is no
    homoplasy. With :math:`L=100` and even more with :math:`L=50` there
    is homoplasy and therefore :math:`R_{min}` increases. There is a
    positive effect of the rate ratio :math:`\kappa` on :math:`R_{min}`
    because the bias increases the chances of homoplasy.

''')

#######

nr = 1000
theta = 50.0
L = 100
BIAS = [0.00001, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 0.99999]
PROP1 = []
PROP2 = []
with lib.updater('    > non-reversible mutation', len(BIAS)*nr*2) as upd:
    for b in BIAS:
        P = 0.0
        for aln in lib.simul(num_pop=1, num_chrom=[20], num_sites=L,
                theta=theta, nr=nr, mut_model='KAM', num_alleles=4,
                trans_matrix=[[None, 0.5, b, b], [0.5, None, b, b],
                              [1-b, 1-b, None, 0.5], [1-b, 1-b, 0.5, None]]):
            p = 0.0
            t = 0
            for seq in aln:
                for x in seq.sequence:
                    if aln.alphabet.get_code(x) < 2: p += 1
                    t += 1
            P += p/t
            upd.add_one()
        PROP1.append(P/nr)

        P = 0.0
        for aln in lib.simul(num_pop=1, num_chrom=[20], num_sites=L,
                theta=theta, nr=nr, mut_model='KAM', num_alleles=4,
                random_start=True,
                trans_matrix=[[None, 0.5, b, b], [0.5, None, b, b],
                              [1-b, 1-b, None, 0.5], [1-b, 1-b, 0.5, None]]):
            p = 0.0
            t = 0
            for seq in aln:
                for x in seq.sequence:
                    if aln.alphabet.get_code(x) < 2: p += 1
                    t += 1
            P += p/t
            upd.add_one()
        PROP2.append(P/nr)

pyplot.plot(BIAS, PROP1, 'ro-', mfc='w', label='default start')
pyplot.plot(BIAS, PROP2, 'bo-', mfc='w', label='random start')
pyplot.xlabel(r'mutation bias')
pyplot.ylabel(r'base proportion')
pyplot.axvline(0.5, ls=':', c='k')
pyplot.axhline(0.5, ls=':', c='k')
pyplot.legend().set_frame_on(False)
pyplot.ylim(-0.1, 1.1)
pyplot.savefig(lib.fname('2-11.png'))
pyplot.clf()

f.write(r'''
Non-reversible mutations
------------------------

.. figure:: 2-11.png
    :scale: 75%

    In this model we simulate four alleles with a custom matrix where
    the mutation weights between alleles 1 and 2 on one hand and 3 and 4
    on the other hand are kept constant and equal to 0.5, whereas mutations
    from 1 or 2 to 3 or 4 have a bias set to a value (the bias) ranging from
    nearly 0 to nearly 1 while the reverse mutation have the opposite bias.
    The proportion of bases 1 and 2 (cumulated) is represented against
    the mutation bias in the direction of the other two bases. Simulations
    using the default starting allele (first allele) are shown in red. In this
    case the proportion of 1/2 bases is close to 1 when the bias is close to
    0 (meaning all bases are 1 or 2). This proportion decreases with increasing
    bias. Using random start alleles, if the bias is 0.5 (no bias, actually),
    the proportion of 1/2 bases is also half. Otherwise, the proportion is
    larger or smaller than 0.5 according to the direction of the bias.
''')

######

nr = 1000
THETA = [0, 0.5, 1, 1.5, 2, 2.5, 3, 3.5, 4, 4.5, 5, 5.5, 6, 6.5, 7, 7.5, 8, 8.5, 9, 9.5, 10]
thetaIAM1 = []
thetaIAM2 = []
thetaSMM1 = []
thetaSMM2 = []
theta2V1 = []
theta2V2 = []

cs = egglib.stats.ComputeStats()
cs.add_stats('thetaIAM', 'thetaSMM', 'V')

with lib.updater('    > theta estimators', len(THETA)*nr*2) as upd:
    for theta in THETA:
        thetaIAM = 0.0
        thetaSMM = 0.0
        theta2V = 0.0
        for aln in lib.simul(num_pop=1, num_chrom=[80], num_sites=1,
                             theta=theta, nr=nr, mut_model='IAM'):
            stats = cs.process_align(aln)
            thetaIAM += stats['thetaIAM']
            thetaSMM += stats['thetaSMM']
            theta2V += 2 * stats['V']
            upd.add_one()
        thetaIAM1.append(thetaIAM / nr)
        thetaSMM1.append(thetaSMM / nr)
        theta2V1.append(theta2V / nr)

        thetaIAM = 0.0
        thetaSMM = 0.0
        theta2V = 0.0
        for aln in lib.simul(num_pop=1, num_chrom=[80], num_sites=1,
                             theta=theta, nr=nr, mut_model='SMM'):
            stats = cs.process_align(aln)
            thetaIAM += stats['thetaIAM']
            thetaSMM += stats['thetaSMM']
            theta2V += 2 * stats['V']
            upd.add_one()
        thetaIAM2.append(thetaIAM / nr)
        thetaSMM2.append(thetaSMM / nr)
        theta2V2.append(theta2V / nr)

pyplot.plot(THETA, thetaIAM1, 'ks-', mfc='k')
pyplot.plot(THETA, thetaSMM1, 'k^-', mfc='k')
pyplot.plot(THETA, theta2V1, 'kv-', mfc='k')
pyplot.plot(THETA, thetaIAM2, 'ks-', mfc='w')
pyplot.plot(THETA, thetaSMM2, 'k^-', mfc='w')
pyplot.plot(THETA, theta2V2, 'kv-', mfc='w')
pyplot.plot([], [], 'ko', mfc='k', label='simul IAM')
pyplot.plot([], [], 'ko', mfc='0.5', label='simul SMM')
pyplot.plot([], [], 'k^', mfc='0.5', label=r'$\hat{\theta}_{IAM}$')
pyplot.plot([], [], 'kv', mfc='0.5', label=r'$\hat{\theta}_{SMM}$')
pyplot.plot([], [], 'kd', mfc='0.5', label=r'$2V$')
pyplot.plot(THETA, THETA, 'k:')
pyplot.legend().set_frame_on(False)
pyplot.xlabel(r'$\theta$')
pyplot.ylabel(r'$\theta$ estimators')
pyplot.ylim(0, 15)
pyplot.savefig(lib.fname('2-12.png'))
pyplot.clf()

f.write(r'''
Theta estimators
----------------

.. figure:: 2-12.png
    :scale: 75%

    We compare the behaviour of three :math:`\theta` estimators:
    :math:`\hat{\theta}_{IAM}`, :math:`\hat{\theta}_{SMM}`, and
    :math:`2V` (two times the allele size variance). The latter is
    expected to be precise under the SMM. The three estimators are
    computed from data simulated under the IAM (solid symbols) and the
    SMM (open symbols). Only :math:`2V` under the SMM is reliable.
''')

######

nr = 1000
theta = 2
PROBA = 0, 0.1, 0.2, 0.3, 0.4, 0.5,0.6, 0.7, 0.8, 0.9, 1
SHAPE = None, 0.75, 0.5, 0.25
V2 = [[], [], [], []]

cs = egglib.stats.ComputeStats()
cs.add_stats('V')
with lib.updater('    > effect TPM parameters', len(PROBA)*len(SHAPE)*nr) as upd:
    for P in PROBA:
        for idx, S in enumerate(SHAPE):
            params = {'num_pop': 1, 'num_chrom':[80], 'num_sites':1,
                      'theta':theta, 'nr':nr}
            if S is None: params.update({'mut_model': 'SMM'})
            else: params.update({'mut_model': 'TPM', 'TPM_params': (P, S)})
            v2 = 0.0
            for aln in lib.simul(**params):
                v2 += 2*cs.process_align(aln)['V']
                upd.add_one()
            V2[idx].append(v2/nr)

pyplot.plot(PROBA, V2[0], 'ks-', mfc='k', label='SMM')
pyplot.plot(PROBA, V2[1], 'kd-', mfc='w', label='TPM P={0}'.format(SHAPE[1]))
pyplot.plot(PROBA, V2[2], 'kv-', mfc='w', label='TPM P={0}'.format(SHAPE[1]))
pyplot.plot(PROBA, V2[3], 'k^-', mfc='w', label='TPM P={0}'.format(SHAPE[2]))
pyplot.axhline(theta, c='k', ls=':')
pyplot.legend().set_frame_on(False)
pyplot.xlabel(r'$P$')
pyplot.ylabel(r'$S$')
pyplot.ylim(0, 15)
pyplot.savefig(lib.fname('2-13.png'))
pyplot.clf()

f.write(r'''
Effect of TPM params
--------------------

.. figure:: 2-13.png
    :scale: 75%

    We analyse the effect of TPM parameters :math:`P` (probability) and
    :math:`S` (shape) on the :math:`\theta` estimator :math:`2V`. The
    estimator is reliable for the SMM only (as before) or when the TPM
    reduces to the TPM (probability equal to 0). Otherwise, the variance
    is inflated proportionally to both parameters.
''')

######

f.close()
