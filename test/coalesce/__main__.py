import sys, subprocess, os, lib

if len(sys.argv) < 2:
    sys.exit("""usage: python {0} CODES

where CODES is one of several strings containing at least one of the
following letters:
    A   -- all tests
    b   -- basic model
    m   -- mutation
    s   -- structure
    p   -- per-population parameters
    r   -- recombination
    h   -- historical events
""".format(sys.argv[0]))
codes = 'bmsprh'

tasks = set()
for arg in sys.argv[1:]:
    for code in arg:
        if code == 'A': tasks.update(codes)
        elif code in codes: tasks.update(code)
        else: sys.exit('invalid code: {0}'.format(code))

f = open(lib.fname('report.rst'), 'w')
f.write('''.. contents::
    :depth: 3
''')
f.close()

if 'b' in tasks: import test_base
if 'm' in tasks: import test_mutation
if 's' in tasks: import test_structure
if 'p' in tasks: import test_pop_parameters
if 'r' in tasks: import test_recombination
if 'h' in tasks: import test_historical

# generate report
path = os.getcwd()
os.chdir(os.path.join(os.path.dirname(__file__), 'output'))
preamble = '''--latex-preamble=\usepackage{geometry}
\geometry{a4paper, portrait, left=30mm, right=30mm, bottom=30mm, top=20mm}'''
subprocess.call(['rst2latex', preamble, 'report.rst', 'report.tex'], stdout=subprocess.PIPE)
subprocess.call(['pdflatex', 'report.tex'], stdout=subprocess.PIPE)
subprocess.call(['pdflatex', 'report.tex'], stdout=subprocess.PIPE)
if os.path.isfile('report.tex'): os.unlink('report.tex')
if os.path.isfile('report.log'): os.unlink('report.log')
if os.path.isfile('report.toc'): os.unlink('report.toc')
if os.path.isfile('report.aux'): os.unlink('report.aux')
if os.path.isfile('report.rst'): os.unlink('report.rst')
os.chdir(path)

if os.path.isfile('seedms'): os.unlink('seedms')
print('# All tasks completed')
