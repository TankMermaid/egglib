import egglib, lib, os
from matplotlib import pyplot

lib.showl('# Structure')
nr = 1000

fstream = open(lib.fname('report.rst'), 'a')
fstream.write('''
=======================
Structure and Migration
=======================
''')

MIGRp = [0.2, 0.4, 0.6, 0.8, 1, 1.2, 1.4, 1.6, 1.8, 2.0]
theta = 1
Ks = [2, 4, 10]
ns_tot = 60
F_c = [[], [], []]
F_ms = [[], [], []]
cs_array = []
for K in Ks:
    cs_array.append(egglib.stats.ComputeStats())
    cs_array[-1].add_stats('WCst', 'lseff')
    cs_array[-1].configure(lib.struct(K, ns_tot/K))
with lib.updater('    > island model', len(MIGRp)*6*nr) as upd:
    for Mp in MIGRp:
        for Fc, Fms, K, cs in zip(F_c, F_ms, Ks, cs_array):
            f = 0.0
            n = 0
            for aln in lib.simul(num_pop=K, num_chrom=[ns_tot/K]*K, theta=theta, migr=Mp*(K-1), nr=nr):
                upd.add_one()
                stats = cs.process_align(aln)
                if stats['lseff'] != 0:
                    f += stats['WCst']
                    n += 1
            Fc.append(f/n)

            f = 0.0
            n = 0
            for aln in lib.ms(theta, [ns_tot/K]*K, nr, migr=Mp*(K-1), as_is=None):
                upd.add_one()
                if aln.ns == 0: continue
                stats = cs.process_align(aln)
                if stats['lseff'] != 0:
                    f += stats['WCst']
                    n += 1
            Fms.append(f/n)

pyplot.plot(MIGRp, F_ms[0], 'b+')
pyplot.plot(MIGRp, F_ms[1], 'g+')
pyplot.plot(MIGRp, F_ms[2], 'r+')
pyplot.plot(MIGRp, F_c[0], 'bo', mfc='None')
pyplot.plot(MIGRp, F_c[1], 'go', mfc='None')
pyplot.plot(MIGRp, F_c[2], 'ro', mfc='None')
pyplot.plot([], [], 'k+', label='ms')
pyplot.plot([], [], 'ko', mfc='None', label='EggLib')
pyplot.plot([], [], 'b-', label='K={0}'.format(Ks[0]))
pyplot.plot([], [], 'g-', label='K={0}'.format(Ks[1]))
pyplot.plot([], [], 'r-', label='K={0}'.format(Ks[2]))
EFst2 = [1.0/(1+Mp) for Mp in [0.1] + MIGRp + [2.1]]
EFst4 = [1.0/(1+Mp*3) for Mp in [0.1] + MIGRp + [2.1]]
EFst10 = [1.0/(1+Mp*9) for Mp in [0.1] + MIGRp + [2.1]]
pyplot.plot([0.1] + MIGRp + [2.1], EFst2, 'b:', label=r'$E(F_{ST}), K=2$')
pyplot.plot([0.1] + MIGRp + [2.1], EFst4, 'g:', label=r'$E(F_{ST}), K=4$')
pyplot.plot([0.1] + MIGRp + [2.1], EFst10, 'r:', label=r'$E(F_{ST}), K=10$')
pyplot.legend()
pyplot.xlabel(r'$M$ (pairwise)')
pyplot.ylabel(r'$F_{ST}$')
pyplot.ylim(0, 1)
pyplot.savefig(lib.fname('3-1.png'))
pyplot.clf()

fstream.write(r'''
Effect of the migration rate on :math:`F_{ST}`
----------------------------------------------

.. figure:: 3-1.png
    :scale: 75%

    The :math:`F_{ST}` is compared for different rates of migration, and
    different numbers of populations, between EggLib and ms. The rate of
    migration is expressed per pair of populations. The expectation
    is :math:`F_{ST} = \frac{1}{1+M}` but assumes an island model with an
    infinite number of demes, so only simulations with many populations
    would fit. The expectation depends on the global :math:`M` (per population
    rather than pairwise) so it depends on the number of demes since :math:`M`
    is expressed pairwise is simulations.
''')

###########

MIGRp = [0.2, 0.4, 0.6, 0.8, 1, 1.2, 1.4, 1.6, 1.8, 2.0]
theta = 1
ns_tot = 60
F_c = [[], [], []]
F_ms = [[], [], []]
cs_array = []
for p in (0,1), (0,2), (1,2):
    cs_array.append(egglib.stats.ComputeStats())
    cs_array[-1].add_stats('WCst')
    cs_array[-1].configure(lib.struct_pw(20, 4, p))
with lib.updater('    > custom structure', len(MIGRp)*2*nr) as upd:
    for M in MIGRp:
        f_acc = [0.0] * 3
        n_acc = [0] * 3
        migr_matrix = [[None, 5*M, M], [5*M, None, 0], [M, 0, None]]
        for aln in lib.simul(num_pop=3, num_chrom=[ns_tot/3]*3, theta=theta, migr_matrix=migr_matrix, nr=nr):
            for i, cs in enumerate(cs_array):
                stats = cs.process_align(aln)
                if stats['WCst'] is not None:
                    f_acc[i] += stats['WCst']
                    n_acc[i] += 1
            upd.add_one()
        for i, Fc in enumerate(F_c): Fc.append(f_acc[i]/n_acc[i])
        f_acc = [0.0] * 3
        n_acc = [0] * 3
        for aln in lib.ms(theta=theta, ns=[ns_tot/3]*3, nr=nr, G=0, segsites=0, migr=0, migr_matrix=migr_matrix, as_is=None):
            for i, cs in enumerate(cs_array):
                stats = cs.process_align(aln)
                if stats['WCst'] is not None:
                    f_acc[i] += stats['WCst']
                    n_acc[i] += 1
            upd.add_one()
        for i, Fms in enumerate(F_ms): Fms.append(f_acc[i]/n_acc[i])

pyplot.plot(MIGRp, F_ms[0], 'r+')
pyplot.plot(MIGRp, F_ms[1], 'g+')
pyplot.plot(MIGRp, F_ms[2], 'b+')
pyplot.plot(MIGRp, F_c[0], 'ro', mfc='None')
pyplot.plot(MIGRp, F_c[1], 'go', mfc='None')
pyplot.plot(MIGRp, F_c[2], 'bo', mfc='None')
pyplot.plot([], [], 'k+', label='ms')
pyplot.plot([], [], 'ko', mfc='None', label='EggLib')
pyplot.plot([], [], 'r-', label=r'$F_{ST}$ (1<->2)')
pyplot.plot([], [], 'g-', label=r'$F_{ST}$ (1<->3)')
pyplot.plot([], [], 'b-', label=r'$F_{ST}$ (2<->3)')
pyplot.legend()
pyplot.xlabel(r'$M$ (pairwise)')
pyplot.ylabel(r'$F_{ST}$')
pyplot.ylim(0, 1)
pyplot.savefig(lib.fname('3-2.png'))
pyplot.clf()

fstream.write(r'''
Pairwise :math:`F_{ST}` with a custom structure
-----------------------------------------------

.. figure:: 3-2.png
    :scale: 75%

    The :math:`F_{ST}` between pairs of populations for different rates
    of migration in a model with a custom migration rates matrix. The
    rates between populations 1 and 2 is 5 times the rates between populations
    1 and 2, and the rates between populations 2 and 3 is null. All rates
    are symetrical and results are compared between EggLib and ms.
''')

###########

MIGRp = [0.2, 0.4, 0.6, 0.8, 1, 1.2, 1.4, 1.6, 1.8, 2.0]
theta = 1
ns = 20
N = 4, 1
Pi1_eg_A = []
Pi2_eg_A = []
Fst_eg_A = []
Pi1_ms_A = []
Pi2_ms_A = []
Fst_ms_A = []
Pi1_eg_B = []
Pi2_eg_B = []
Fst_eg_B = []
Pi1_ms_B = []
Pi2_ms_B = []
Fst_ms_B = []
Pi1_eg_C = []
Pi2_eg_C = []
Fst_eg_C = []
Pi1_ms_C = []
Pi2_ms_C = []
Fst_ms_C = []

cs0 = egglib.stats.ComputeStats()
cs0.add_stats('S')
cs1 = egglib.stats.ComputeStats()
cs1.add_stats('Pi')
cs1.configure(lib.struct_pw(ns, 2, [0]))
cs2 = egglib.stats.ComputeStats()
cs2.add_stats('Pi')
cs2.configure(lib.struct_pw(ns, 2, [1]))
cs3 = egglib.stats.ComputeStats()
cs3.add_stats('WCst')
cs3.configure(lib.struct(2, ns))
with lib.updater('    > asymetrical gene flow', len(MIGRp)*nr*3*2) as upd:
    for M in MIGRp:
        for (migr_matrix, Pi1_eg, Pi2_eg, Fst_eg, Pi1_ms, Pi2_ms, Fst_ms) in [
                      ([[None, 1.0*M], [1.0*M, None]], Pi1_eg_A, Pi2_eg_A, Fst_eg_A, Pi1_ms_A, Pi2_ms_A, Fst_ms_A),
                      ([[None, 1.0*M], [0.1*M, None]], Pi1_eg_B, Pi2_eg_B, Fst_eg_B, Pi1_ms_B, Pi2_ms_B, Fst_ms_B),
                      ([[None, 0.1*M], [1.0*M, None]], Pi1_eg_C, Pi2_eg_C, Fst_eg_C, Pi1_ms_C, Pi2_ms_C, Fst_ms_C)]:
            Pi1 = 0.0
            Pi2 = 0.0
            Fst = 0.0
            n = 0
            for aln in lib.simul(num_pop=2, num_chrom=[ns, ns], theta=theta, N=[4,1], migr_matrix=migr_matrix, nr=nr):
                stats0 = cs0.process_align(aln)
                stats1 = cs1.process_align(aln)
                stats2 = cs2.process_align(aln)
                stats3 = cs3.process_align(aln)
                if stats0['S'] > 0: 
                    Pi1 += stats1['Pi']
                    Pi2 += stats2['Pi']
                    Fst += stats3['WCst']
                    n += 1
                upd.add_one()
            Pi1_eg.append(Pi1/nr)
            Pi2_eg.append(Pi2/nr)
            Fst_eg.append(Fst/n)

            Pi1 = 0.0
            Pi2 = 0.0
            Fst = 0.0
            n = 0
            for aln in lib.ms(theta=theta, ns=[ns, ns], nr=nr, G=0, segsites=0, migr=0, migr_matrix=migr_matrix, N=[4,1], as_is=None):
                upd.add_one()
                if aln.ns == 0: continue
                stats0 = cs0.process_align(aln)
                stats1 = cs1.process_align(aln)
                stats2 = cs2.process_align(aln)
                stats3 = cs3.process_align(aln)
                if stats0['S'] > 0: 
                    Pi1 += stats1['Pi']
                    Pi2 += stats2['Pi']
                    Fst += stats3['WCst']
                    n += 1
            Pi1_ms.append(Pi1/nr)
            Pi2_ms.append(Pi2/nr)
            Fst_ms.append(Fst/n)

pyplot.plot(MIGRp, Pi1_ms_A, 'r+')
pyplot.plot(MIGRp, Pi1_eg_A, 'ro', mfc='None')
pyplot.plot(MIGRp, Pi1_ms_B, 'b+')
pyplot.plot(MIGRp, Pi1_eg_B, 'bo', mfc='None')
pyplot.plot(MIGRp, Pi1_ms_C, 'g+')
pyplot.plot(MIGRp, Pi1_eg_C, 'go', mfc='None')
pyplot.plot([], [], 'k+', label='ms')
pyplot.plot([], [], 'ko', label='EggLib', mfc='None')
pyplot.plot([], [], 'r-', label='1<->2')
pyplot.plot([], [], 'g-', label='1->2')
pyplot.plot([], [], 'b-', label='1<-2')
pyplot.legend()
pyplot.xlabel(r'$M$ (pairwise)')
pyplot.ylabel(r'$\pi$ (pop 1)')
pyplot.savefig(lib.fname('3-3.png'))
pyplot.clf()

pyplot.plot(MIGRp, Pi2_ms_A, 'r+')
pyplot.plot(MIGRp, Pi2_eg_A, 'ro', mfc='None')
pyplot.plot(MIGRp, Pi2_ms_B, 'b+')
pyplot.plot(MIGRp, Pi2_eg_B, 'bo', mfc='None')
pyplot.plot(MIGRp, Pi2_ms_C, 'g+')
pyplot.plot(MIGRp, Pi2_eg_C, 'go', mfc='None')
pyplot.plot([], [], 'k+', label='ms')
pyplot.plot([], [], 'ko', label='EggLib', mfc='None')
pyplot.plot([], [], 'r-', label='1<->2')
pyplot.plot([], [], 'g-', label='1->2')
pyplot.plot([], [], 'b-', label='1<-2')
pyplot.legend()
pyplot.xlabel(r'$M$ (pairwise)')
pyplot.ylabel(r'$\pi$ (pop 2)')
pyplot.savefig(lib.fname('3-4.png'))
pyplot.clf()

pyplot.plot(MIGRp, Fst_ms_A, 'r+')
pyplot.plot(MIGRp, Fst_eg_A, 'ro', mfc='None')
pyplot.plot(MIGRp, Fst_ms_B, 'b+')
pyplot.plot(MIGRp, Fst_eg_B, 'bo', mfc='None')
pyplot.plot(MIGRp, Fst_ms_C, 'g+')
pyplot.plot(MIGRp, Fst_eg_C, 'go', mfc='None')
pyplot.plot([], [], 'k+', label='ms')
pyplot.plot([], [], 'ko', label='EggLib', mfc='None')
pyplot.plot([], [], 'r-', label='1<->2')
pyplot.plot([], [], 'g-', label='1->2')
pyplot.plot([], [], 'b-', label='1<-2')
pyplot.legend()
pyplot.xlabel(r'$M$ (pairwise)')
pyplot.ylabel(r'$F_{ST}$')
pyplot.savefig(lib.fname('3-5.png'))
pyplot.clf()

fstream.write(r'''
Asymetrical gene flow
---------------------

In the next three figures, ms and EggLib are used to simulate a model
with two populations: the first population has a relative size of
:math:`N_1=4` and the second population has a relative size of
:math:`N_2=1`. The migration rate is either bidirectional (red symbols),
mainly from the first to the second population (big-to-small; green symbols)
or mainly from the second to the first population (small-to-big; blue
symbols). When migration rate is reduced in either direction, it is
divided by 10. The statistics are :math:`\pi` for the first population
(first figure), for the second population (second figure), and :math:`F_{ST}`
(third figure).

.. figure:: 3-3.png
    :scale: 75%

.. figure:: 3-4.png
    :scale: 75%

.. figure:: 3-5.png
    :scale: 75%
''')

fstream.close()
