import sys, os, re, subprocess, tempfile
import egglib_coalclonal as egglib, random

def path(fname):
    return os.path.join(os.path.dirname(__file__), 'pic', fname)

def show(msg):
    sys.stdout.write(msg)
    sys.stdout.flush()

class Simuls:
    def get(self, repet, locus):
        return self._repets[repet][locus]
    @property
    def dim(self):
        nr = len(self._repets)
        if nr == 0: return 0, 0
        ls = map(len, self._repets)
        assert len(set(ls)) == 1
        return nr, ls[0]

class Locus:
    pass

def run(*arguments, **kwargs):
    f1, fname1 = tempfile.mkstemp()
    f2, fname2 = tempfile.mkstemp()
    f3, fname3 = tempfile.mkstemp()
    f4, fname4 = tempfile.mkstemp()
    os.close(f1)
    os.close(f2)
    os.close(f3)
    os.close(f4)
    os.unlink(fname1)
    os.unlink(fname2)
    os.unlink(fname3)
    os.unlink(fname4)

    try:
        args = ['python', '-W', 'ignore', '-m', 'egglib_coalclonal.coalclonal']
        args.extend(map(str, arguments))
        args.extend(['-fgenotypes', fname1])
        if '-locus-stats' in args or '-site-stats' in args or '-global-stats' in args:
            args.extend(['-fstats', fname2])
        assert '-seed' not in arguments
        args.extend(['-seed', str(random.randint(999999, 999999999))])

        pipes = subprocess.PIPE
        for key, val in kwargs.iteritems():
            if key == 'feed':
                f = open(fname3, 'w')
                for line in val:
                    f.write(' '.join(map(str, line)) + '\n')
                f.close()
                args.extend(['-fvars', fname3])
            elif key == 'bypass':
                pipes = None
                print( ' '.join(args))
            elif key == 'var':
                f = open(fname4, 'w')
                for line in val:
                    f.write(' '.join(map(str, line)) + '\n')
                f.close()
                args.extend(['-fvars', fname4])
            else: raise ValueError, 'invalid argument to run: {0}'.format(key)

        simuls = Simuls()

        simuls.cmd = ' '.join(args)
        p = subprocess.Popen(args, stdout=pipes, stderr=pipes)
        simuls.stdout, simuls.stderr = p.communicate()
        simuls.ret = p.returncode
        simuls._repets = []

        if simuls.ret != 0: return simuls

        if os.path.isfile(fname1):
            f = open(fname1)
            line1 = f.readline()
            line2 = f.readline()
            bits = f.read().split('//')
            f.close()

            simuls.nr, simuls.nl, spacers = re.match('coalclonal -nr (\d+) -nl (\d+) -spacer (.+)', line1).groups()
            simuls.nr = int(simuls.nr)
            simuls.nl = int(simuls.nl)
            simuls.seed = re.match('(\d+)', line2)
            spacers = [i == 'Y' for i in spacers.split(',')]
            bits = map(str.strip, bits)
            assert bits[0] == ''
            del bits[0]
            idx = 0
            if len(bits) > 0:
                for rep in xrange(simuls.nr):
                    if idx == len(bits):
                        sys.stderr.write('not enough simulated datasets!\n')
                        break
                    simuls._repets.append([])
                    for loc in xrange(simuls.nl):
                        lines = bits[idx].split('\n')
                        locus = Locus()
                        simuls._repets[-1].append(locus)
                        locus.ss = int(re.match('segsites: (\d+)', lines[0]).group(1))
                        if locus.ss == 0:
                            assert len(lines) == 1
                            locus.pos = []
                            locus.samples = []
                            locus.ns = 0
                        else:
                            locus.pos = map(float, re.match('positions: (.+)', lines[1]).group(1).split())
                            assert locus.ss == len(locus.pos)
                            locus.samples = []
                            for idv in lines[2:]:
                                if spacers[loc]: locus.samples.append(map(int, idv.split()))
                                else: locus.samples.append(map(int, idv))
                                assert len(locus.samples[-1]) == locus.ss
                            locus.ns = len(locus.samples)
                        idx += 1
                assert idx == len(bits)

        if os.path.isfile(fname2):
            sys.stdout.flush()
            f = open(fname2)
            header = f.readline().split()
            simuls.stats = {}
            for k in header: simuls.stats[k] = []
            for line in f:
                line = line.split()
                assert len(line) == len(header)
                for k, v in zip(header, line):
                    if re.search('\[\d+\]\*$', k) is not None:
                        v = v.split(',')
                        w = []
                        for x in v:
                            if x == 'NA':
                                if k == 'thetaW' or k == 'Pi': x = 0.0
                                else: x = None
                            else:
                                try: x = int(x)
                                except ValueError: x = float(x)
                        w.append(x)
                        v = w
                    else:
                        if v == 'NA':
                            if k == 'thetaW' or k == 'Pi': v = 0.0
                            else: v = None
                        else:
                            try: v = int(v)
                            except ValueError: v = float(v)
                    simuls.stats[k].append(v)
            f.close()
            simuls.average_stats = {}
            for k, val in simuls.stats.iteritems():
                n = len(val) - val.count(None)
                if n > 0:
                    if not isinstance(val[0], list):
                        simuls.average_stats[k] = float(sum(filter(None, val))) / n
                else: simuls.average_stats[k] = None

        hits = re.findall('# locus=(\d+) pops=\* structure=(.+)', simuls.stdout)
        if len(hits) > 0:
            simuls.sample_sizes = []
            for locus, struct in hits:
                assert int(locus) - 1 == len(simuls.sample_sizes)
                pops = struct.split('|')
                ss = []
                for pop in pops:
                    lbl, indivs = re.match('(\d+)\:(.*)', pop).groups()
                    if len(indivs) == 0: ss.append(0)
                    else: ss.append(len(indivs.split(';')))
                simuls.sample_sizes.append(ss)

    finally:
        if os.path.isfile(fname1): os.unlink(fname1)
        if os.path.isfile(fname2): os.unlink(fname2)
        if os.path.isfile(fname3): os.unlink(fname3)
        if os.path.isfile(fname4): os.unlink(fname4)

    return simuls

def run_ms(*args):
    args = [os.path.expanduser('~/data/software/msdir-20140908/ms')] + list(args)
    if '-I' in args:
        idx = args.index('-I')
        np = args[idx+1]
        struct = {0: {}}
        acc = 0
        for i, n in enumerate(args[idx+2:idx+2+np]):
            struct[0][i] = dict([(i, (i,)) for i in range(acc, acc+n)])
            acc += n
        struct = egglib.stats.make_structure(struct, None)
    else:
        struct = None

    args = map(str, args)
    p = subprocess.Popen(args, stdout=subprocess.PIPE)
    stdout, stderr = p.communicate()
    simuls = stdout.split('//')
    del simuls[0]
    cs = egglib.stats.ComputeStats()
    cs.add_stats('WCst')
    WCst = []
    for sim in simuls:
        sim = sim.strip().split('\n')
        ss = int(re.match('segsites: (\d+)', sim[0]).group(1))
        aln = egglib.Align()
        for sam in sim[2:]:
            aln.add_sample('', map(int, sam))
        stats = cs.process_align(aln, filtr=egglib.stats.filter_default, struct=struct)
        if stats['WCst'] is not None:
            WCst.append(stats['WCst'])
    if os.path.isfile('seedms'): os.unlink('seedms')
    if len(WCst) > 0:
        return {'WCst': 1.0 * sum(WCst) / len(WCst)}
    else:
        return {'WCst': None}
