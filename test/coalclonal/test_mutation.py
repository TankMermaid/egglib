from egglib_coalclonal import coalclonal
from matplotlib import pyplot
import lib, random, math

print('# Test mutation')

nr = 1000

# -sitew
lib.show('    - -nmut option')
S1 = []
S2 = []
S3 = []
NMUT = [0, 5, 10, 15, 20, 30, 40, 50]
for nmut in NMUT:
    lib.show(' {0}'.format(nmut))
    sim = lib.run('-nr', nr, '-nl', 1, '-nmut', 1, nmut,
        '-np', 1, '-ns', 1, 20,
        '-sexual', 1, '-global-stats', 'S')
    assert sim.ret == coalclonal.Program.SUCCESS, sim.stderr
    S1.append(sim.average_stats['S'])

    sim = lib.run('-nr', nr, '-nl', 1, '-nmut', 1, nmut,
        '-np', 1, '-ns', 1, 20, '-ls', 1, 200,
        '-sexual', 1, '-global-stats', 'S')
    assert sim.ret == coalclonal.Program.SUCCESS, sim.stderr
    S2.append(sim.average_stats['S'])

    sim = lib.run('-nr', nr, '-nl', 1, '-nmut', 1, nmut,
        '-np', 1, '-ns', 1, 20, '-ls', 1, 50,
        '-sexual', 1, '-global-stats', 'S')
    assert sim.ret == coalclonal.Program.SUCCESS, sim.stderr
    S3.append(sim.average_stats['S'])

fname = lib.path('pic5-1-nmut.png')
pyplot.plot(NMUT, NMUT, 'kx', label='S=nmut')
pyplot.plot(NMUT, S1, 'bo:', mfc='None', mec='b', label='ls=INF')
pyplot.plot(NMUT, S2, 'mo:', mfc='None', mec='m', label='ls=200')
pyplot.plot(NMUT, S3, 'ro:', mfc='None', mec='r', label='ls=50')
pyplot.xlabel('nmut')
pyplot.ylabel('S')
pyplot.title('S vs. nmut')
pyplot.legend(loc=2, numpoints=1)
pyplot.xlim(-1, 51)
pyplot.ylim(-1, 51)
pyplot.savefig(fname)
pyplot.clf()
lib.show('\n        -> {0}\n'.format(fname))


lib.show('    - -sitew option')
UNB = [0, 1, 2, 3, 5, 7, 10, 15, 20, 25, 30, 35, 40, 45, 50]
S = []
weights = [1.0] * 100
for unb in UNB:
    lib.show(' {0}'.format(unb))
    w = weights[:]
    for i in random.sample(range(100), unb):
        w[i] += 100.0/unb

    sim = lib.run('-nr', nr, '-nl', 1, '-nmut', 1, 30,
        '-np', 1, '-ns', 1, 20, '-ls', 1, 100,
        '-sexual', 1, '-global-stats', 'S',
        '-sitew', 1, *w)
    assert sim.ret == coalclonal.Program.SUCCESS, sim.stderr
    S.append(sim.average_stats['S'])

fname = lib.path('pic5-2-sitew.png')
pyplot.plot(UNB, S, 'ko', mfc='None', mec='k')
pyplot.xlabel('Number of sites sharing 100% weight increase')
pyplot.ylabel('S')
pyplot.title('S vs. weight unbalance')
pyplot.xlim(-1, 51)
pyplot.ylim(0, 31)
pyplot.axhline(30, ls=':', c='k')
pyplot.axvline(0, ls=':', c='k')
pyplot.savefig(fname)
pyplot.clf()
lib.show('\n        -> {0}\n'.format(fname))

# recomb & S
lib.show('    - -rho effect on S')
theta=10
nmut =50
RHO = [0, 0.25, 0.5, 0.75, 1, 1.25, 1.5, 1.75, 2, 2.25, 2.5, 2.75, 3]
S1 = []
S2 = []
S3 = []
S4 = []
S5 = []
S6 = []
pos = [i/99*0.1 for i in xrange(100)]
for rho in RHO:
    lib.show(' {0}'.format(rho))
    sim = lib.run('-nr', nr, '-nl', 1, '-theta', 1, theta,
        '-np', 1, '-ns', 1, 20, '-rho', 1, rho,
        '-sexual', 1, '-global-stats', 'S')
    assert sim.ret == coalclonal.Program.SUCCESS, sim.stderr
    S1.append(sim.average_stats['S'])

    sim = lib.run('-nr', nr, '-nl', 1, '-nmut', 1, nmut,
        '-np', 1, '-ns', 1, 20, '-rho', 1, rho,
        '-sexual', 1, '-global-stats', 'S')
    assert sim.ret == coalclonal.Program.SUCCESS, sim.stderr
    S2.append(sim.average_stats['S'])

    sim = lib.run('-nr', nr, '-nl', 1, '-theta', 1, theta,
        '-np', 1, '-ns', 1, 20, '-ls', 1, 100,
         '-rho', 1, rho,
        '-sexual', 1, '-global-stats', 'S')
    assert sim.ret == coalclonal.Program.SUCCESS, sim.stderr
    S3.append(sim.average_stats['S'])

    sim = lib.run('-nr', nr, '-nl', 1, '-nmut', 1, nmut,
        '-np', 1, '-ns', 1, 20, '-ls', 1, 100,
         '-rho', 1, rho,
        '-sexual', 1, '-global-stats', 'S')
    assert sim.ret == coalclonal.Program.SUCCESS, sim.stderr
    S4.append(sim.average_stats['S'])

    sim = lib.run('-nr', nr, '-nl', 1, '-theta', 1, theta,
        '-np', 1, '-ns', 1, 20, '-ls', 1, 100,
         '-rho', 1, rho,
        '-sexual', 1, '-global-stats', 'S',
        '-sitep', 1, *pos)
    assert sim.ret == coalclonal.Program.SUCCESS, sim.stderr
    S5.append(sim.average_stats['S'])

    sim = lib.run('-nr', nr, '-nl', 1, '-nmut', 1, nmut,
        '-np', 1, '-ns', 1, 20, '-ls', 1, 100,
         '-rho', 1, rho,
        '-sexual', 1, '-global-stats', 'S',
        '-sitep', 1, *pos)
    assert sim.ret == coalclonal.Program.SUCCESS, sim.stderr
    S6.append(sim.average_stats['S'])

fname = lib.path('pic5-3-rho.png')
pyplot.plot(RHO, S1, 'ko', mfc='None', mec='r', label='theta={0}'.format(theta))
pyplot.plot(RHO, S2, 'ko', mfc='None', mec='b', label='nmut={0}'.format(nmut))
pyplot.plot(RHO, S3, 'kv', mfc='None', mec='r')
pyplot.plot(RHO, S4, 'kv', mfc='None', mec='b')
pyplot.plot([], [], 'kv', mfc='None', mec='k', label='FSM: ls=100')
pyplot.plot(RHO, S5, 'k^', mfc='None', mec='r')
pyplot.plot(RHO, S6, 'k^', mfc='None', mec='b')
pyplot.plot([], [], 'k^', mfc='None', mec='k', label='FSM: ls=100 restricted 10%')
pyplot.xlabel('rho')
pyplot.ylabel('S')
pyplot.title('S vs. rho')
pyplot.legend(loc=1, numpoints=1, framealpha=0.5)
pyplot.xlim(-0.2, 3.2)
pyplot.ylim(math.floor(min(S1+S2+S3+S4+S5+S6))-0.1, math.ceil(max(S1+S2+S3+S4+S5+S6))+0.1)
pyplot.savefig(fname)
pyplot.clf()
lib.show('\n        -> {0}\n'.format(fname))

lib.show('    - -rho effect on S (simple)')
theta=5.0
RHO = [0, 0.25, 0.5, 0.75, 1, 1.25, 1.5, 1.75, 2, 2.25, 2.5, 2.75, 3]
S1 = []
S2 = []
D1 = []
D2 = []
thetaW1 = []
thetaW2 = []
pos = [i/99*0.1 for i in xrange(100)]
for rho in RHO:
    lib.show(' {0}'.format(rho))
    sim = lib.run('-nr', nr, '-nl', 1, '-theta', 1, theta,
        '-np', 1, '-ns', 1, 20, '-rho', 1, rho,
        '-sexual', 1, '-global-stats', 'S', 'thetaW', 'D')
    assert sim.ret == coalclonal.Program.SUCCESS, sim.stderr
    S1.append(sim.average_stats['S'])
    D1.append(sim.average_stats['D'])
    thetaW1.append(sim.average_stats['thetaW'])

    sim = lib.run('-nr', nr, '-nl', 1, '-nmut', 1, 10,
        '-np', 1, '-ns', 1, 20, '-rho', 1, rho,
        '-sexual', 1, '-global-stats', 'S', 'thetaW', 'D')
    assert sim.ret == coalclonal.Program.SUCCESS, sim.stderr
    S2.append(sim.average_stats['S'])
    D2.append(sim.average_stats['D'])
    thetaW2.append(sim.average_stats['thetaW'])

fname = lib.path('pic5-4-rho.png')
pyplot.plot(RHO, S1, 'ko', mfc='None', mec='m', label='S')
pyplot.plot(RHO, S2, 'k^', mfc='None', mec='m', label='S nmut')
pyplot.plot(RHO, thetaW1, 'ko', mfc='None', mec='b', label='thetaW')
pyplot.plot(RHO, thetaW2, 'k^', mfc='None', mec='b', label='thetaW nmut')
pyplot.plot(RHO, D1, 'ko', mfc='None', mec='r', label='D')
pyplot.plot(RHO, D2, 'k^', mfc='None', mec='r', label='D nmut')
pyplot.xlabel('rho')
pyplot.ylabel('thetaW / D')
pyplot.title('thetaW/D vs. rho')
pyplot.legend(loc=1, numpoints=1, framealpha=0.5)
pyplot.xlim(-0.2, 3.2)
pyplot.savefig(fname)
pyplot.clf()
lib.show('\n        -> {0}\n'.format(fname))

# -sitep (no recomb)
lib.show('    - -sitep option (no recombination)')
FRACT = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0]
Rmin1 = []
Rmin2 = []
Rmin3 = []
Rmin4 = []
Rmin5 = []
Rmin6 = []
S1 = []
S2 = []
S3 = []
S4 = []
S5 = []
S6 = []
positions = [i/99.0 for i in xrange(100)]

for f in FRACT:
    lib.show(' {0}'.format(f))
    pos = [i*f for i in positions[:]]

    sim = lib.run('-nr', nr, '-nl', 1, '-nmut', 1, 10,
        '-np', 1, '-ns', 1, 20, '-ls', 1, 100, '-rstart', 1,
        '-sexual', 1, '-global-stats', 'Rmin', 'S', '-rho', 1, 0.0,
        '-sitep', 1, *pos)
    assert sim.ret == coalclonal.Program.SUCCESS, sim.stderr
    Rmin1.append(sim.average_stats['Rmin'])
    S1.append(sim.average_stats['S'])

    sim = lib.run('-nr', nr, '-nl', 1, '-nmut', 1, 50,
        '-np', 1, '-ns', 1, 20, '-ls', 1, 100, '-rstart', 1,
        '-sexual', 1, '-global-stats', 'Rmin', 'S', '-rho', 1, 0.0,
        '-sitep', 1, *pos)
    assert sim.ret == coalclonal.Program.SUCCESS, sim.stderr
    Rmin2.append(sim.average_stats['Rmin'])
    S2.append(sim.average_stats['S'])

    sim = lib.run('-nr', nr, '-nl', 1, '-nmut', 1, 10,
        '-np', 1, '-ns', 1, 20, '-ls', 1, 100, '-K', 1, 4,
        '-rstart', 1,
        '-sexual', 1, '-global-stats', 'Rmin', 'S', '-rho', 1, 0.0,
        '-sitep', 1, *pos)
    assert sim.ret == coalclonal.Program.SUCCESS, sim.stderr
    Rmin3.append(sim.average_stats['Rmin'])
    S3.append(sim.average_stats['S'])

    sim = lib.run('-nr', nr, '-nl', 1, '-nmut', 1, 50,
        '-np', 1, '-ns', 1, 20, '-ls', 1, 100, '-K', 1, 4,
        '-rstart', 1,
        '-sexual', 1, '-global-stats', 'Rmin', 'S', '-rho', 1, 0.0,
        '-sitep', 1, *pos)
    assert sim.ret == coalclonal.Program.SUCCESS, sim.stderr
    Rmin4.append(sim.average_stats['Rmin'])
    S4.append(sim.average_stats['S'])

    sim = lib.run('-nr', nr, '-nl', 1, '-nmut', 1, 10,
        '-np', 1, '-ns', 1, 20, '-ls', 1, 100, '-K', 1, 8,
        '-rstart', 1,
        '-sexual', 1, '-global-stats', 'Rmin', 'S', '-rho', 1, 0.0,
        '-sitep', 1, *pos)
    assert sim.ret == coalclonal.Program.SUCCESS, sim.stderr
    Rmin5.append(sim.average_stats['Rmin'])
    S5.append(sim.average_stats['S'])

    sim = lib.run('-nr', nr, '-nl', 1, '-nmut', 1, 50,
        '-np', 1, '-ns', 1, 20, '-ls', 1, 100, '-K', 1, 8,
        '-rstart', 1,
        '-sexual', 1, '-global-stats', 'Rmin', 'S', '-rho', 1, 0.0,
        '-sitep', 1, *pos)
    assert sim.ret == coalclonal.Program.SUCCESS, sim.stderr
    Rmin6.append(sim.average_stats['Rmin'])
    S6.append(sim.average_stats['S'])

fname = lib.path('pic5-5-sitep-rho0.png')
pyplot.plot(FRACT, Rmin1, 'ko', mfc='None', mec='k', label='nmut=10')
pyplot.plot(FRACT, Rmin2, 'ko', mfc='k', mec='k', label='nmut=50')
pyplot.plot(FRACT, Rmin3, 'k^', mfc='None', mec='k', label='K=4')
pyplot.plot(FRACT, Rmin4, 'k^ ', mfc='k', mec='k')
pyplot.plot(FRACT, Rmin5, 'kv ', mfc='None', mec='k', label='K=8')
pyplot.plot(FRACT, Rmin6, 'kv ', mfc='k', mec='k')
pyplot.legend(loc=1, numpoints=1, framealpha=0.5)
pyplot.xlabel('Proportion of the interval where sites are restricted')
pyplot.ylabel('Rmin')
pyplot.title('Rmin vs. region restriction (no recombination)')
pyplot.xlim(-0.1, 1.1)
pyplot.savefig(fname)
pyplot.clf()
lib.show('\n        -> {0}\n'.format(fname))

fname = lib.path('pic5-6-sitep-rho0.png')
pyplot.plot(FRACT, S1, 'ko', mfc='None', mec='k', label='nmut=10')
pyplot.plot(FRACT, S2, 'ko', mfc='k', mec='k', label='nmut=50')
pyplot.plot(FRACT, S3, 'k^', mfc='None', mec='k', label='K=4')
pyplot.plot(FRACT, S4, 'k^', mfc='k', mec='k')
pyplot.plot(FRACT, S5, 'kv', mfc='None', mec='k', label='K=8')
pyplot.plot(FRACT, S6, 'kv', mfc='k', mec='k')
pyplot.legend(loc=1, numpoints=1, framealpha=0.5)
pyplot.xlabel('Proportion of the interval where sites are restricted')
pyplot.ylabel('S')
pyplot.title('S vs. region restriction (no recombination)')
pyplot.xlim(-0.1, 1.1)
pyplot.savefig(fname)
pyplot.clf()
lib.show('        -> {0}\n'.format(fname))

# -sitep (recomb)
lib.show('    - -sitep option')
FRACT = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0]
Rmin1 = []
Rmin2 = []
Rmin3 = []
Rmin4 = []
Rmin5 = []
Rmin6 = []
S1 = []
S2 = []
S3 = []
S4 = []
S5 = []
S6 = []
positions = [i/99.0 for i in xrange(100)]

for f in FRACT:
    lib.show(' {0}'.format(f))
    pos = [i*f for i in positions[:]]

    sim = lib.run('-nr', nr, '-nl', 1, '-nmut', 1, 10,
        '-np', 1, '-ns', 1, 20, '-ls', 1, 100, '-rstart', 1,
        '-sexual', 1, '-global-stats', 'Rmin', 'S', '-rho', 1, 4.0,
        '-sitep', 1, *pos)
    assert sim.ret == coalclonal.Program.SUCCESS, sim.stderr
    Rmin1.append(sim.average_stats['Rmin'])
    S1.append(sim.average_stats['S'])

    sim = lib.run('-nr', nr, '-nl', 1, '-nmut', 1, 50,
        '-np', 1, '-ns', 1, 20, '-ls', 1, 100, '-rstart', 1,
        '-sexual', 1, '-global-stats', 'Rmin', 'S', '-rho', 1, 4.0,
        '-sitep', 1, *pos)
    assert sim.ret == coalclonal.Program.SUCCESS, sim.stderr
    Rmin2.append(sim.average_stats['Rmin'])
    S2.append(sim.average_stats['S'])

    sim = lib.run('-nr', nr, '-nl', 1, '-nmut', 1, 10,
        '-np', 1, '-ns', 1, 20, '-ls', 1, 100, '-K', 1, 4,
        '-rstart', 1,
        '-sexual', 1, '-global-stats', 'Rmin', 'S', '-rho', 1, 4.0,
        '-sitep', 1, *pos)
    assert sim.ret == coalclonal.Program.SUCCESS, sim.stderr
    Rmin3.append(sim.average_stats['Rmin'])
    S3.append(sim.average_stats['S'])

    sim = lib.run('-nr', nr, '-nl', 1, '-nmut', 1, 50,
        '-np', 1, '-ns', 1, 20, '-ls', 1, 100, '-K', 1, 4,
        '-rstart', 1,
        '-sexual', 1, '-global-stats', 'Rmin', 'S', '-rho', 1, 4.0,
        '-sitep', 1, *pos)
    assert sim.ret == coalclonal.Program.SUCCESS, sim.stderr
    Rmin4.append(sim.average_stats['Rmin'])
    S4.append(sim.average_stats['S'])

    sim = lib.run('-nr', nr, '-nl', 1, '-nmut', 1, 10,
        '-np', 1, '-ns', 1, 20, '-ls', 1, 100, '-K', 1, 8,
        '-rstart', 1,
        '-sexual', 1, '-global-stats', 'Rmin', 'S', '-rho', 1, 4.0,
        '-sitep', 1, *pos)
    assert sim.ret == coalclonal.Program.SUCCESS, sim.stderr
    Rmin5.append(sim.average_stats['Rmin'])
    S5.append(sim.average_stats['S'])

    sim = lib.run('-nr', nr, '-nl', 1, '-nmut', 1, 50,
        '-np', 1, '-ns', 1, 20, '-ls', 1, 100, '-K', 1, 8,
        '-rstart', 1,
        '-sexual', 1, '-global-stats', 'Rmin', 'S', '-rho', 1, 4.0,
        '-sitep', 1, *pos)
    assert sim.ret == coalclonal.Program.SUCCESS, sim.stderr
    Rmin6.append(sim.average_stats['Rmin'])
    S6.append(sim.average_stats['S'])

fname = lib.path('pic5-7-sitep-recomb.png')
pyplot.plot(FRACT, Rmin1, 'ko', mfc='None', mec='k', label='nmut=10')
pyplot.plot(FRACT, Rmin2, 'ko', mfc='k', mec='k', label='nmut=50')
pyplot.plot(FRACT, Rmin3, 'k^', mfc='None', mec='k', label='K=4')
pyplot.plot(FRACT, Rmin4, 'k^ ', mfc='k', mec='k')
pyplot.plot(FRACT, Rmin5, 'kv ', mfc='None', mec='k', label='K=8')
pyplot.plot(FRACT, Rmin6, 'kv ', mfc='k', mec='k')
pyplot.legend(loc=1, numpoints=1, framealpha=0.5)
pyplot.xlabel('Proportion of the interval where sites are restricted')
pyplot.ylabel('Rmin')
pyplot.title('Rmin vs. region restriction')
pyplot.xlim(-0.1, 1.1)
pyplot.savefig(fname)
pyplot.clf()
lib.show('\n        -> {0}\n'.format(fname))

fname = lib.path('pic5-8-sitep-recomb.png')
pyplot.plot(FRACT, S1, 'ko', mfc='None', mec='k', label='nmut=10')
pyplot.plot(FRACT, S2, 'ko', mfc='k', mec='k', label='nmut=50')
pyplot.plot(FRACT, S3, 'k^', mfc='None', mec='k', label='K=4')
pyplot.plot(FRACT, S4, 'k^', mfc='k', mec='k')
pyplot.plot(FRACT, S5, 'kv', mfc='None', mec='k', label='K=8')
pyplot.plot(FRACT, S6, 'kv', mfc='k', mec='k')
pyplot.legend(loc=1, numpoints=1, framealpha=0.5)
pyplot.xlabel('Proportion of the interval where sites are restricted')
pyplot.ylabel('S')
pyplot.title('S vs. region restriction')
pyplot.xlim(-0.1, 1.1)
pyplot.savefig(fname)
pyplot.clf()
lib.show('        -> {0}\n'.format(fname))

