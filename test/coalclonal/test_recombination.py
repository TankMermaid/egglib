from egglib_coalclonal import coalclonal
from matplotlib import pyplot
import lib

print('# Test recombination')
lib.show('    - -rho option')

nr = 1000
RHO = [0.0, 0.5, 1, 1.5, 2, 2.5, 3, 3.5, 4, 4.5, 5]
theta = 3

Rmin_sexual = []
Rmin_clonal = []

for rho in RHO:
    lib.show(' {0}'.format(rho))
    sim = lib.run('-nr', nr, '-nl', 1, '-theta', 1, theta,
        '-np', 1, '-ns', 1, 30,
        '-sexual', 1, '-global-stats', 'Rmin', '-rho', 1, rho)
    assert sim.ret == coalclonal.Program.SUCCESS, sim.stderr
    Rmin_sexual.append(sim.average_stats['Rmin'])

    sim = lib.run('-nr', nr, '-nl', 1, '-theta', 1, theta,
        '-np', 1, '-ns', 1, 30,
        '-S', 1, 1.0, '-global-stats', 'Rmin', '-rho', 1, rho)
    assert sim.ret == coalclonal.Program.SUCCESS, sim.stderr
    Rmin_clonal.append(sim.average_stats['Rmin'])


fname = lib.path('pic4-1-rho.png')
pyplot.plot(RHO, Rmin_sexual, 'kx', mfc='None', mec='b', label='sexual')
pyplot.plot(RHO, Rmin_clonal, 'ko', mfc='None', mec='m', label='clonal')
pyplot.xlabel('rho')
pyplot.ylabel('Rmin')
pyplot.title('Rmin vs. rho')
pyplot.axhline(0.0, ls=':', c='k')
pyplot.legend(loc=2, numpoints=1)
pyplot.plot([0.0], [1.0], 'w,')
pyplot.xlim(-0.1, 5.1)
pyplot.ylim(-0.1, 1.1)
pyplot.savefig(fname)
pyplot.clf()
lib.show('\n        -> {0}\n'.format(fname))


lib.show('    - -rec option')
REC = [0.0, 0.1, 0.2,0.3, 0.4, 0.5,0.6, 0.7,0.8, 0.9,1.0]

Rmin_sexual = []
Rmin_clonal = []

for rec in REC:
    lib.show(' {0}'.format(rec))
    sim = lib.run('-nr', nr, '-nl', 1, '-theta', 1, theta,
        '-np', 1, '-ns', 1, 30,
        '-sexual', 1, '-global-stats', 'Rmin', '-rec', 1, rec)
    assert sim.ret == coalclonal.Program.SUCCESS, sim.stderr
    Rmin_sexual.append(sim.average_stats['Rmin'])

    sim = lib.run('-nr', nr, '-nl', 1, '-theta', 1, theta,
        '-np', 1, '-ns', 1, 30,
        '-S', 1, 1.0, '-global-stats', 'Rmin', '-rec', 1, rec)
    assert sim.ret == coalclonal.Program.SUCCESS, sim.stderr
    Rmin_clonal.append(sim.average_stats['Rmin'])


fname = lib.path('pic4-2-rec.png')
pyplot.plot(REC, Rmin_sexual, 'kx', mfc='None', mec='b', label='sexual')
pyplot.plot(REC, Rmin_clonal, 'ko', mfc='None', mec='m', label='clonal')
pyplot.xlabel('rec')
pyplot.ylabel('Rmin')
pyplot.title('Rmin vs. re')
pyplot.axhline(0.0, ls=':', c='k')
pyplot.legend(loc=2, numpoints=1)
pyplot.xlim(-0.1, 1.1)
pyplot.ylim(-0.1, 1.1)
pyplot.savefig(fname)
pyplot.clf()
lib.show('\n        -> {0}\n'.format(fname))
