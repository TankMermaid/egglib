from egglib_coalclonal import coalclonal
from matplotlib import pyplot
import lib, os, sys, subprocess, re, egglib

def run(nr, ns, theta=0.0, G=0.0, s=0.0):
    args = ['-nr', nr, '-np', 1, '-ns', 1, 2*ns, '-nl', 1,
        '-sexual', 1, '-theta', 1, theta, '-G', 1, G, '-s', 1, s,
        '-global-stats', 'S', 'thetaW', 'Pi', 'D']
    args.append('-all-flags')
    args.extend(['YN'] * (2*ns))
    if G < 0:
        args.extend(['-event', 'G', 10.0, 1, 0, '-event', 'N', 10.0, 1, 1.0])
    simuls = lib.run(*args)
    assert simuls.ret == coalclonal.Program.SUCCESS, simuls.stderr
    S = 0.0
    thetaW = 0.0
    Pi = 0.0
    D = 0.0
    nD = 0
    for i in xrange(nr):
        if simuls.stats['S'][i] is not None:
            S += simuls.stats['S'][i]
            thetaW += simuls.stats['thetaW'][i]
            Pi += simuls.stats['Pi'][i]
        if simuls.stats['D'][i] is not None:
            nD += 1
            D += simuls.stats['D'][i]
    return {'S': S/nr, 'thetaW': thetaW/nr, 'Pi': Pi/nr, 'D': D/nD if nD>0 else None}

def run_ms(nr, ns, theta=0.0, G=0.0):
    args = [os.path.expanduser('~/data/software/msdir-20140908/ms'), str(2*ns), str(nr),
        '-t', str(theta), '-G', str(G)]
    if G < 0:
        args.extend(['-eG', '10', '0', '-eN', '10', '1'])
    p = subprocess.Popen(args, stdout=subprocess.PIPE)
    stdout, stderr = p.communicate()
    simuls = stdout.split('//')
    del simuls[0]
    cs = egglib.stats.ComputeStats()
    cs.add_stats('S', 'thetaW', 'Pi', 'D')
    S = 0.0
    thetaW = 0.0
    Pi = 0.0
    nD = 0
    D = 0.0
    for sim in simuls:
        sim = sim.strip().split('\n')
        ss = int(re.match('segsites: (\d+)', sim[0]).group(1))
        aln = egglib.Align()
        for sam in sim[2:]:
            aln.add_sample('', map(int, sam))
        stats = cs.process_align(aln, filtr=egglib.stats.filter_default)
        S += stats['S']
        thetaW += stats['thetaW']
        Pi += stats['Pi']
        if stats['S'] > 0:
            D += stats['D']
            nD += 1
    if os.path.isfile('seedms'): os.unlink('seedms')
    return {'S': S/nr, 'thetaW': thetaW/nr, 'Pi': Pi/nr, 'D': D/nD if nD>0 else None}

print('# Test simple model')

# FLAGS
print('    - flags')
simuls = lib.run('-np', 2, '-nl', 1, '-ns', '.', 10, '-sexual', '.', '-M', 1.0, '-nmut', '.', 10)
assert simuls.ret == coalclonal.Program.SUCCESS, simuls.stderr
assert simuls.get(0, 0).ns == 40

simuls = lib.run('-np', 2, '-nl', 1, '-ns', '.', 10, '-sexual', '.', '-M', 1.0, '-nmut', '.', 10,
    '-all-flags', * ['YY'] * 20)
assert simuls.ret == coalclonal.Program.SUCCESS, simuls.stderr
assert simuls.get(0, 0).ns == 40

simuls = lib.run('-np', 2, '-nl', 1, '-ns', '.', 10, '-sexual', '.', '-M', 1.0, '-nmut', '.', 10,
    '-flags', 1, 5, 'YN')
assert simuls.ret == coalclonal.Program.SUCCESS, simuls.stderr
assert simuls.get(0, 0).ns == 39

simuls = lib.run('-np', 2, '-nl', 1, '-ns', '.', 10, '-sexual', '.', '-M', 1.0, '-nmut', '.', 10,
    '-all-flags', * ['YN']* 20)
assert simuls.ret == coalclonal.Program.SUCCESS, simuls.stderr
assert simuls.get(0, 0).ns == 20

simuls = lib.run('-np', 2, '-nl', 1, '-ns', '.', 10, '-sexual', '.', '-M', 1.0, '-nmut', '.', 10, '-global-stats', 'Fis')
assert simuls.ret == coalclonal.Program.SUCCESS, simuls.stderr
assert simuls.stats['Fis'][0] is not None

simuls = lib.run('-np', 2, '-nl', 1, '-ns', '.', 10, '-sexual', '.', '-M', 1.0, '-nmut', '.', 10,
    '-global-stats', 'Fis', '-all-flags', * ['YN'] * 20)
assert simuls.ret == coalclonal.Program.SUCCESS, simuls.stderr
assert simuls.stats['Fis'][0] is None

simuls = lib.run('-np', 2, '-nl', 1, '-ns', '.', 10, '-sexual', '.', '-M', 1.0, '-nmut', '.', 10,
    '-global-stats', 'Fis', '-flags', 1, 5, 'YN', '-flags', 2, 2, 'NY')
assert simuls.ret == coalclonal.Program.FAIL, simuls.stderr
assert 'invalid ploidy' in simuls.stderr

# THETA
lib.show('    - theta')
THETA = []
THETAW = []
S = []
ES = []
PI = []

ns = 20
nr = 1000
for theta in [0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1, 1.5, 2,
              2.5, 3, 3.5, 4, 4.5, 5]:
    stats = run(nr, ns, theta=theta)
    S.append(stats['S'])
    THETAW.append(stats['thetaW'])
    THETA.append(theta)
    ES.append(theta * sum([1.0/i for i in xrange(1, 2*ns)]))
    PI.append(stats['Pi'])
    lib.show(' {0}'.format(theta))

fname = lib.path('pic1-1-theta.png')
pyplot.plot(THETA, THETAW, 'ko', mfc='None', mec='r', label='thetaW')
pyplot.plot(THETA, PI, 'ko', mfc='None', mec='g', label='Pi')
pyplot.plot(THETA, S, 'ko', mfc='None', mec='b', label='S')
pyplot.plot(THETA, THETA, 'k-', mfc='None', mec='k')
pyplot.plot(THETA, ES, 'b-')
pyplot.legend(loc=2, numpoints=1)
pyplot.xlabel('THETA')
pyplot.ylabel('statistics')
pyplot.title('thetaW, Pi, and S against theta and E(S)')
pyplot.xlim(-0.1, 5.1)
pyplot.ylim(-0.1, 25)
pyplot.savefig(fname)
pyplot.clf()
lib.show('\n        -> {0}\n'.format(fname))

# GROWTH RATE
lib.show('    - growth rate')
G = []
D = []
THETAW = []
D_MS = []
THETAW_MS = []

nr = 1000
for g in [-1, -0.75, -0.5, -0.25, 0, 1, 2, 3, 4]:
    stats = run(nr, ns, theta=theta, G=g)
    G.append(g)
    THETAW.append(stats['thetaW'])
    D.append(stats['D'])
    stats = run_ms(nr, ns, theta=theta, G=g)
    THETAW_MS.append(stats['thetaW'])
    D_MS.append(stats['D'])
    lib.show(' {0}'.format(g))


fname = lib.path('pic1-2-G.png')
pyplot.plot(G, THETAW, 'ko', mfc='None', mec='b', label='thetaW')
pyplot.plot(G, THETAW_MS, 'ks', mfc='None', mec='b')
pyplot.plot(G, D, 'ks', mfc='None', mec='r', label='D')
pyplot.plot(G, D_MS, 'ko', mfc='None', mec='r')
pyplot.plot([], [], 'ks', mfc='None', mec='k', label='with ms')
pyplot.legend(loc=1, numpoints=1)
pyplot.xlabel('G')
pyplot.ylabel('statistics')
pyplot.title('thetaW and D against G and ms')
pyplot.xlim(-2.1, 4.1)
pyplot.axhline(0, c='k', ls='-')
pyplot.axhline(-1, c='k', ls=':')
pyplot.savefig(fname)
pyplot.clf()
lib.show('\n        -> {0}\n'.format(fname))

# SELFING RATE
lib.show('    - selfing')
S = []
THETA_NE = []
THETAW = []
SELF = []
ES = []
PI = []
D = []

nr = 4000
theta = 5
for s in [0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1]:
    stats = run(nr, ns, theta=theta, s=s)
    SELF.append(s)
    S.append(stats['S'])
    THETAW.append(stats['thetaW'])
    Ne = 1 * (2.0-s)/2
    thetaNe = theta * Ne
    THETA_NE.append(thetaNe)
    PI.append(stats['Pi'])
    ES.append(thetaNe * sum([1.0/i for i in xrange(1, 2*ns)]))
    D.append(stats['D'])
    lib.show(' {0}'.format(s))

fname = lib.path('pic1-3-s.png')
pyplot.plot(SELF, THETAW, 'ko', mfc='None', mec='r', label='thetaW')
pyplot.plot(SELF, PI, 'ko', mfc='None', mec='g', label='Pi')
pyplot.plot(SELF, D, 'ko', mfc='None', mec='m', label='D')
pyplot.axhline(0.0, c='k', ls='-')
pyplot.plot(SELF, THETA_NE, 'k-', mfc='None', mec='k')
pyplot.legend(loc=6, numpoints=1)
pyplot.xlabel('s')
pyplot.ylabel('statistics')
pyplot.title('thetaW, Pi, and S against s, Ne and E(S)')
pyplot.xlim(-0.1, 1.1)
pyplot.ylim(-1, 5.1)
pyplot.savefig(fname)
pyplot.clf()
lib.show('\n        -> {0}\n'.format(fname))
