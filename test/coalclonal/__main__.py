import sys, subprocess, os

if len(sys.argv) < 2:
    sys.exit("""usage: python test/coalclonal CODES

where CODES is one of several strings containing at least one of the
following letters:
    A   -- all tests
    i   -- interface
    c   -- clonal
    b   -- basic model
    s   -- substructure
    r   -- recombination
    u   -- mutation
    m   -- mutation models
    e   -- events
    v   -- variable arguments
    R   -- generate pdf report
""")
codes = 'Ribscrumev'

tasks = set()
for arg in sys.argv[1:]:
    for code in arg:
        if code == 'A': tasks.update(codes)
        elif code in codes: tasks.update(code)
        else: sys.exit('invalid code: {0}'.format(code))

if 'i' in tasks: import test_interface
if 'b' in tasks: import test_simple
if 's' in tasks: import test_differentiation
if 'c' in tasks: import test_clonal
if 'r' in tasks: import test_recombination
if 'u' in tasks: import test_mutation
if 'm' in tasks: import test_mutation_models
if 'e' in tasks: import test_events
if 'v' in tasks: import test_variables

# keep this for the end
if 'R' in tasks:
    path = os.getcwd()
    try:
        print( '# Generate pdf report')
        os.chdir(os.path.join(os.path.dirname(__file__), 'report'))
        subprocess.call(['pdflatex', 'report.tex'], stdout=subprocess.PIPE)
    except:
        print( '    -> an error occurred!')
    else:
        print( '    -> report.pdf generated')
    finally:
        os.chdir(path)

print( '# All tasks completed')
