from egglib_coalclonal import coalclonal
from matplotlib import pyplot
import lib

print('# Test demographic events')
nr = 1000

# -event N
lib.show('    - change N (Na)')
D = []
NA = [1/8., 1/4., 1/2., 1, 2, 4, 8]
for Na in NA:
    lib.show(' {0}'.format(Na))
    sim = lib.run('-nr', nr, '-nl', 1, '-theta', 1, 5.0,
        '-np', 1, '-ns', 1, 40,
        '-sexual', 1, '-global-stats', 'D',
        '-event', 'N', 0.2, 1, Na)
    assert sim.ret == coalclonal.Program.SUCCESS, sim.stderr
    D.append(sim.average_stats['D'])

fname = lib.path('pic7-1-eN.png')
pyplot.plot(NA, D, 'ko:', mfc='None', mec='k')
pyplot.xlabel('Na')
pyplot.ylabel('D')
pyplot.title('D vs. eN anc. size')
pyplot.axhline(0, ls=':', c='k')
pyplot.axvline(1, ls=':', c='k')
pyplot.xlim(0, 8.1)
pyplot.savefig(fname)
pyplot.clf()
lib.show('\n        -> {0}\n'.format(fname))

lib.show('    - change N (T)')
Pi1 = []
Pi2 = []
T = [0, 0.3, 0.6, 0.9, 1.2, 1.5, 1.8, 2.1, 2.4, 2.7, 3, 3.5, 4, 5]
for t in T:
    lib.show(' {0}'.format(t))
    sim = lib.run('-nr', nr, '-nl', 1, '-theta', 1, 5.0,
        '-np', 1, '-ns', 1, 40,
        '-sexual', 1, '-global-stats', 'Pi',
        '-event', 'N', t, 1, 0.2)
    assert sim.ret == coalclonal.Program.SUCCESS, sim.stderr
    Pi1.append(sim.average_stats['Pi'])

    sim = lib.run('-nr', nr, '-nl', 1, '-theta', 1, 5.0,
        '-np', 1, '-ns', 1, 40,
        '-sexual', 1, '-global-stats', 'Pi',
        '-event', 'N', t, 1, 5.0)
    assert sim.ret == coalclonal.Program.SUCCESS, sim.stderr
    Pi2.append(sim.average_stats['Pi'])

fname = lib.path('pic7-2-eN.png')
pyplot.plot(T, Pi1, 'ro-', mfc='None', mec='r')
pyplot.plot(T, Pi2, 'bo-', mfc='None', mec='b')
pyplot.xlabel('T')
pyplot.ylabel('Pi')
pyplot.title('Pi vs. eN date')
pyplot.axhline(5*1.0, ls=':', c='k')
pyplot.axhline(5*0.2, ls=':', c='r')
pyplot.axhline(5*5.0, ls=':', c='b')
pyplot.xlim(-0.1, 5.1)
pyplot.savefig(fname)
pyplot.clf()
lib.show('\n        -> {0}\n'.format(fname))

# -event G
lib.show('    - change G (Ga)')
G = [-2, -1.5, -1, -0.5, 0, 0.5, 1, 2, 2.5, 3]
D1 = []
D2 = []
for g in G:
    lib.show(' {0}'.format(g))

    sim = lib.run('-nr', nr, '-nl', 1, '-theta', 1, 1.0,
        '-np', 1, '-ns', 1, 40,
        '-sexual', 1, '-global-stats', 'D',
        '-G', 1, g,
        '-event', 'G', 2.0, 1, 0.0)
    assert sim.ret == coalclonal.Program.SUCCESS, sim.stderr
    D1.append(sim.average_stats['D'])

    sim = lib.run('-nr', nr, '-nl', 1, '-theta', 1, 1.0,
        '-np', 1, '-ns', 1, 40,
        '-sexual', 1, '-global-stats', 'D',
        '-event', 'G', 0.3, 1, g,
        '-event', 'G', 2.0, 1, 0.0)
    assert sim.ret == coalclonal.Program.SUCCESS, sim.stderr
    D2.append(sim.average_stats['D'])

fname = lib.path('pic7-3-eG.png')
pyplot.plot(G, D1, 'ko:', mfc='None', mec='k', label='-G')
pyplot.plot(G, D2, 'ro:', mfc='None', mec='r', label='-eG 0.3')
pyplot.xlabel('G')
pyplot.ylabel('D')
pyplot.title('D vs. eG Ga')
pyplot.xlim(-2.1, 3.1)
pyplot.legend(loc=1, numpoints=1, framealpha=0.75)
pyplot.axhline(0, ls=':', c='k')
pyplot.axvline(0, ls=':', c='k')
pyplot.savefig(fname)
pyplot.clf()
lib.show('\n        -> {0}\n'.format(fname))

# -event S
lib.show('    - change S')
Sa = [0.10, 0.25, 0.5, 0.75, 1, 1.25, 1.5, 1.75, 2, 2.5, 3]
Fis1 = []
Fis2 = []
rD1 = []
rD2 = []
for S in Sa:
    lib.show(' {0}'.format(S))

    sim = lib.run('-nr', nr, '-nl', 3, '-theta', '.', 2.0,
        '-np', 1, '-ns', 1, 40,
        '-sexual', 1, '-global-stats', 'Fis', 'rD',
        '-event', 'switch', 0.5, 1,
        '-event', 'S', 0.5, 1, S)
    assert sim.ret == coalclonal.Program.SUCCESS, sim.stderr
    Fis1.append(sim.average_stats['Fis'])
    rD1.append(sim.average_stats['rD'])

    sim = lib.run('-nr', nr, '-nl', 3, '-theta', '.', 2.0,
        '-np', 1, '-ns', 1, 40,
        '-global-stats', 'Fis', 'rD',
        '-event', 'S', 0.5, 1, S)
    assert sim.ret == coalclonal.Program.SUCCESS, sim.stderr
    Fis2.append(sim.average_stats['Fis'])
    rD2.append(sim.average_stats['rD'])

sim = lib.run('-nr', nr, '-nl', 3, '-theta', '.', 2.0,
    '-np', 1, '-ns', 1, 40,
    '-sexual', 1, '-global-stats', 'Fis', 'rD')
assert sim.ret == coalclonal.Program.SUCCESS, sim.stderr
Fis1_ = sim.average_stats['Fis']
rD1_ = sim.average_stats['rD']

sim = lib.run('-nr', nr, '-nl', 3, '-theta', '.', 2.0,
    '-np', 1, '-ns', 1, 40,
    '-global-stats', 'Fis', 'rD',
    '-event', 'switch', 0.5, 1)
Fis2_ = sim.average_stats['Fis']
rD2_ = sim.average_stats['rD']

fname = lib.path('pic7-4-eS-Fis.png')
pyplot.plot(Sa, Fis1, 'bo:', mfc='None', mec='b', label='initial sexual')
pyplot.plot(Sa, Fis2, 'ro:', mfc='None', mec='r', label='initial clonal')
pyplot.plot([4], Fis1_, 'bs', mfc='None', mec='b')
pyplot.plot([4], Fis2_, 'rs', mfc='None', mec='r')
pyplot.xticks([0, 0.5, 1, 1.5, 2, 2.5, 3, 4], [0, 0.5, 1, 1.5, 2, 2.5, 3, 'sexual'])
pyplot.xlabel('Sa')
pyplot.ylabel('Fis')
pyplot.title('Fis vs. eS S')
pyplot.xlim(-0.1, 4.1)
pyplot.legend(loc=1, numpoints=1, framealpha=0.75)
pyplot.axhline(0, ls=':', c='k')
pyplot.savefig(fname)
pyplot.clf()
lib.show('\n        -> {0}\n'.format(fname))

fname = lib.path('pic7-5-eS-rD.png')
pyplot.plot(Sa, rD1, 'bo:', mfc='None', mec='b', label='initial sexual')
pyplot.plot(Sa, rD2, 'ro:', mfc='None', mec='r', label='initial clonal')
pyplot.plot([4], rD1_, 'bs', mfc='None', mec='b')
pyplot.plot([4], rD2_, 'rs', mfc='None', mec='r')
pyplot.xticks([0, 0.5, 1, 1.5, 2, 2.5, 3, 4], [0, 0.5, 1, 1.5, 2, 2.5, 3, 'sexual'])
pyplot.xlabel('Sa')
pyplot.ylabel('RbarD')
pyplot.title('RbarD vs. eS S')
pyplot.xlim(-0.1, 4.1)
pyplot.legend(loc=1, numpoints=1, framealpha=0.75)
pyplot.savefig(fname)
pyplot.clf()
lib.show('        -> {0}\n'.format(fname))

# -event s
lib.show('    - change s')
T = [0, 0.01, 0.025, 0.05, 0.075, 0.1, 0.25, 0.5, 0.75, 1, 1.25, 1.5, 1.75, 2, 2.25, 2.5, 2.75, 3, 3.25, 3.5, 3.75, 4]
tW1 = []
tW2 = []
tW3 = []
tW4 = []
tW5 = []
Pi1 = []
Pi2 = []
Pi3 = []
Pi4 = []
Pi5 = []
D1 = []
D2 = []
D3 = []
D4 = []
D5 = []
for t in T:
    lib.show(' {0}'.format(t))

    sim = lib.run('-nr', 5*nr, '-nl', 1, '-theta', 1, 1.0,
        '-np', 1, '-ns', 1, 40,
        '-global-stats', 'thetaW', 'Pi', 'D',
        '-sexual', 1, '-s', 1, 0.1,
        '-event', 's', t, 1, 0.9, '-all-flags', * ('YN',) * 40)
    assert sim.ret == coalclonal.Program.SUCCESS, sim.stderr
    tW1.append(sim.average_stats['thetaW'])
    Pi1.append(sim.average_stats['Pi'])
    D1.append(sim.average_stats['D'])

    sim = lib.run('-nr', 5*nr, '-nl', 1, '-theta', 1, 1.0,
        '-np', 1, '-ns', 1, 40,
        '-global-stats', 'thetaW', 'Pi', 'D',
        '-sexual', 1, '-s', 1, 0.9,
        '-event', 's', t, 1, 0.1, '-all-flags', * ('YN',) * 40)
    assert sim.ret == coalclonal.Program.SUCCESS, sim.stderr
    tW2.append(sim.average_stats['thetaW'])
    Pi2.append(sim.average_stats['Pi'])
    D2.append(sim.average_stats['D'])

    sim = lib.run('-nr', 5*nr, '-nl', 1, '-theta', 1, 1.0,
        '-np', 1, '-ns', 1, 40,
        '-global-stats', 'thetaW', 'Pi', 'D',
        '-sexual', 1, '-s', 1, 0.9,
        '-event', 's', t, 1, 0.9, '-all-flags', * ('YN',) * 40)
    assert sim.ret == coalclonal.Program.SUCCESS, sim.stderr
    tW3.append(sim.average_stats['thetaW'])
    Pi3.append(sim.average_stats['Pi'])
    D3.append(sim.average_stats['D'])

    sim = lib.run('-nr', 5*nr, '-nl', 1, '-theta', 1, 1.0,
        '-np', 1, '-ns', 1, 40, '-G', 1, 0.0,
        '-global-stats', 'thetaW', 'Pi', 'D',
        '-sexual', 1, '-s', 1, 0.9, '-all-flags', * ('YN',) * 40)
    assert sim.ret == coalclonal.Program.SUCCESS, sim.stderr
    tW4.append(sim.average_stats['thetaW'])
    Pi4.append(sim.average_stats['Pi'])
    D4.append(sim.average_stats['D'])

    sim = lib.run('-nr', 5*nr, '-nl', 1, '-theta', 1, 1.0,
        '-np', 1, '-ns', 1, 40,
        '-global-stats', 'thetaW', 'Pi', 'D',
        '-sexual', 1, '-s', 1, 0.1,
        '-event', 's', t, 1, 0.1, '-all-flags', * ('YN',) * 40)
    assert sim.ret == coalclonal.Program.SUCCESS, sim.stderr
    tW5.append(sim.average_stats['thetaW'])
    Pi5.append(sim.average_stats['Pi'])
    D5.append(sim.average_stats['D'])

fname = lib.path('pic7-6-es.png')
pyplot.plot(T, tW1, 'bo:', mfc='None', mec='b', label='allo->auto')
pyplot.plot(T, tW2, 'ro:', mfc='None', mec='r', label='auto->allo')
pyplot.plot(T, tW3, 'yo:', mfc='None', mec='y', label='auto->auto')
pyplot.plot(T, tW4, 'mo:', mfc='None', mec='m', label='auto')
pyplot.plot(T, tW5, 'go:', mfc='None', mec='g', label='allo->allo')
pyplot.plot(T, Pi1, 'bs:', mfc='None', mec='b')
pyplot.plot(T, Pi2, 'rs:', mfc='None', mec='r')
pyplot.plot(T, Pi3, 'ys:', mfc='None', mec='y')
pyplot.plot(T, Pi4, 'ms:', mfc='None', mec='m')
pyplot.plot(T, Pi5, 'gs:', mfc='None', mec='g')
pyplot.plot([], [], 'ks', label='Pi')
pyplot.xlabel('T')
pyplot.ylabel('thetaW/Pi')
pyplot.title('thetaW/Pi vs. es')
pyplot.xlim(-0.1, 4.1)
pyplot.legend(loc=5, numpoints=1, framealpha=0.25)
pyplot.axhline(1 * (2.0-0.1)/2, ls=':', c='b')
pyplot.axhline(1 * (2.0-0.9)/2, ls=':', c='r')
pyplot.savefig(fname)
pyplot.clf()
lib.show('\n        -> {0}\n'.format(fname))

fname = lib.path('pic7-7-es-D.png')
pyplot.plot(T, D1, 'bo:', mfc='None', mec='b', label='allo->auto')
pyplot.plot(T, D2, 'ro:', mfc='None', mec='r', label='auto->allo')
pyplot.plot(T, D3, 'yo:', mfc='None', mec='y', label='auto->auto')
pyplot.plot(T, D4, 'mo:', mfc='None', mec='m', label='auto')
pyplot.plot(T, D5, 'go:', mfc='None', mec='g', label='allo->allo')
pyplot.xlabel('T')
pyplot.ylabel('D')
pyplot.title('D vs. es')
pyplot.xlim(-0.1, 4.1)
pyplot.legend(loc=1, numpoints=1, framealpha=0.25)
pyplot.axhline(0, ls=':', c='k')
pyplot.savefig(fname)
pyplot.clf()
lib.show('        -> {0}\n'.format(fname))
lib.show('        -> {0}\n'.format(fname))

# -event M
lib.show('    - change M')
FstB = []
FstR = []
Ts = [0, 0.1, 0.2, 0.3, 0.5, 0.8, 1.1, 1.5, 2, 2.5, 3]
for T in Ts:
    lib.show(' {0}'.format(T))
    sim = lib.run('-nr', nr, '-nl', 1, '-theta', 1, 5.0,
        '-np', 2, '-ns', 1, 40, '-ns', 2, 40, '-M', 0.1,
        '-sexual', 1, '-global-stats', 'WCst',
        '-event', 'M', T, 1, 2, 5.0,
        '-event', 'M', T, 2, 1, 5.0)
    assert sim.ret == coalclonal.Program.SUCCESS, sim.stderr
    FstB.append(sim.average_stats['WCst'])
    sim = lib.run('-nr', nr, '-nl', 1, '-theta', 1, 5.0,
        '-np', 2, '-ns', 1, 40, '-ns', 2, 40, '-M', 5.0,
        '-sexual', 1, '-global-stats', 'WCst',
        '-event', 'M', T, 1, 2, 0.1,
        '-event', 'M', T, 2, 1, 0.1)
    assert sim.ret == coalclonal.Program.SUCCESS, sim.stderr
    FstR.append(sim.average_stats['WCst'])

sim = lib.run('-nr', nr, '-nl', 1, '-theta', 1, 5.0,
    '-np', 2, '-ns', 1, 40, '-ns', 2, 40, '-M', 0.1,
    '-sexual', 1, '-global-stats', 'WCst')
assert sim.ret == coalclonal.Program.SUCCESS, sim.stderr
Fst0R = sim.average_stats['WCst']

sim = lib.run('-nr', nr, '-nl', 1, '-theta', 1, 5.0,
    '-np', 2, '-ns', 1, 40, '-ns', 2, 40, '-M', 5.0,
    '-sexual', 1, '-global-stats', 'WCst')
assert sim.ret == coalclonal.Program.SUCCESS, sim.stderr
Fst0B = sim.average_stats['WCst']

fname = lib.path('pic7-8-eM-T.png')
pyplot.plot(Ts, FstB, 'bo:', mfc='None', mec='b', label='M: 5.0->0.1')
pyplot.plot(Ts, FstR, 'ro:', mfc='None', mec='r', label='M: 0.1->5.0')
pyplot.plot([0], [Fst0B], 'bs', label='M=5.0')
pyplot.plot([0], [Fst0R], 'rs', label='M=0.1')
pyplot.axhline(Fst0B, c='b', ls=':')
pyplot.axhline(Fst0R, c='r', ls=':')
pyplot.xlabel('T')
pyplot.ylabel('Fst')
pyplot.title('Fst vs. eM date')
pyplot.legend()
pyplot.savefig(fname)
pyplot.clf()
lib.show('\n        -> {0}\n'.format(fname))

lib.show('    - change M (2)')
T = 0.2
Fst = []
Mas = [0.2, 0.4, 0.6, 0.8, 1, 1.2, 1.4, 1.6, 1.8]
for Ma in Mas   :
    lib.show(' {0}'.format(Ma))
    sim = lib.run('-nr', 2*nr, '-nl', 1, '-theta', 1, 5.0,
        '-np', 4, '-ns', '.', 40, '-M', 1,
        '-sexual', 1, '-global-stats', 'WCst',
        '-event', 'M', T, 1, 2, Ma,
        '-event', 'M', T, 1, 3, Ma,
        '-event', 'M', T, 1, 4, Ma,
        '-event', 'M', T, 2, 3, Ma,
        '-event', 'M', T, 2, 4, Ma,
        '-event', 'M', T, 3, 4, Ma,
        '-event', 'M', T, 2, 1, Ma,
        '-event', 'M', T, 3, 1, Ma,
        '-event', 'M', T, 4, 1, Ma,
        '-event', 'M', T, 3, 2, Ma,
        '-event', 'M', T, 4, 2, Ma,
        '-event', 'M', T, 4, 3, Ma)
    assert sim.ret == coalclonal.Program.SUCCESS, sim.stderr
    Fst.append(sim.average_stats['WCst'])

sim = lib.run('-nr', 2*nr, '-nl', 1, '-theta', 1, 5.0,
    '-np', 4, '-ns', '.', 40, '-M', 1,
    '-sexual', 1, '-global-stats', 'WCst')
assert sim.ret == coalclonal.Program.SUCCESS, sim.stderr
Fst0 = sim.average_stats['WCst']

fname = lib.path('pic7-9-eM-Ma.png')
pyplot.plot(Mas, Fst, 'ko:', mfc='None', mec='k', label='change at T={0}'.format(T))
pyplot.plot([1.0], [Fst0], 'rs', label='no change (M=1.0)')
pyplot.axhline(Fst0, c='r', ls=':')
pyplot.xlabel('Ma')
pyplot.ylabel('Fst')
pyplot.title('Fst vs. eM Ma')
pyplot.legend()
pyplot.savefig(fname)
pyplot.clf()
lib.show('\n        -> {0}\n'.format(fname))

# -events rho/rec
lib.show('    - change rho/rec')
results = [
    {'change': None, 'regime': 'sexual', 'param': 'rho', 'value': 4.0, 'symb': 's', 'c': 'r'},
    {'change': None, 'regime': 'clonal', 'param': 'rho', 'value': 4.0, 'symb': 's', 'c': 'b'},
    {'change': None, 'regime': 'sexual', 'param': 'rec', 'value': 0.8, 'symb': 'o', 'c': 'r'},
    {'change': None, 'regime': 'clonal', 'param': 'rec', 'value': 0.8, 'symb': 'o', 'c': 'b'},
    {'change': 0.0,  'regime': 'sexual', 'param': 'rho', 'value': 4.0, 'symb': '^', 'c': 'r'},
    {'change': 0.0,  'regime': 'clonal', 'param': 'rho', 'value': 4.0, 'symb': '^', 'c': 'b'},
    {'change': 0.0,  'regime': 'sexual', 'param': 'rec', 'value': 0.8, 'symb': 'v', 'c': 'r'},
    {'change': 0.0,  'regime': 'clonal', 'param': 'rec', 'value': 0.8, 'symb': 'v', 'c': 'b'},
    {'change': 4.0,  'regime': 'sexual', 'param': 'rho', 'value': 0.0, 'symb': '>', 'c': 'r'},
    {'change': 4.0,  'regime': 'clonal', 'param': 'rho', 'value': 0.0, 'symb': '>', 'c': 'b'},
    {'change': 0.8,  'regime': 'sexual', 'param': 'rec', 'value': 0.0, 'symb': '<', 'c': 'r'},
    {'change': 0.8,  'regime': 'clonal', 'param': 'rec', 'value': 0.0, 'symb': '<', 'c': 'b'}]
for res in results: res['Y'] = []
Ts = [0.0, 0.05, 0.1, 0.15, 0.2, 0.25, 0.3, 0.35]

for T in Ts:
    lib.show(' {0}'.format(T))
    for res in results:
        params = ['-nr', nr, '-nl', 1, '-theta', 1, 5.0,
            '-np', 1, '-ns', 1, 40, '-global-stats', 'Rmin',
            '-' + res['param'], '1', str(res['value'])]
        if res['regime'] == 'sexual': params.extend(['-sexual', '1'])
        else: params.extend(['-S', '1', '0.8'])
        if res['change'] is not None: params.extend(['-event', res['param'], T, 1, res['change']])
        sim = lib.run(*params)
        assert sim.ret == coalclonal.Program.SUCCESS, sim.stderr
        res['Y'].append(sim.average_stats['Rmin'])
        lib.show('.')

fname = lib.path('pic7-10-erecomb-T.png')
for idx, res in enumerate(results):
    X = Ts[:]
    Y = res['Y'][:]
    if Y[-1] == 0.0:
        X.append(X[-1]+0.1)
        Y.append((idx+1)*0.05)
        
    pyplot.plot(X, Y, res['c']+res['symb']+':', mfc='None', mec=res['c'])
pyplot.xlabel('T')
pyplot.ylabel('Rmin')
pyplot.title('Rmin vs. T to recomb change')
pyplot.savefig(fname)
pyplot.clf()
lib.show('\n        -> {0}\n'.format(fname))

# -event switch
lib.show('    - change switch sex->asex error checking\n')

# sexual: no error
sim = lib.run('-nr', nr/100, '-nl', 1, '-theta', 1, 5.0,
    '-np', 1, '-ns', 1, 40,
    '-sexual', 1)
assert sim.ret == coalclonal.Program.SUCCESS, sim.stderr

# asexual: error
sim = lib.run('-nr', nr/100, '-nl', 1, '-theta', 1, 5.0,
    '-np', 1, '-ns', 1, 40)
assert sim.ret == coalclonal.Program.FAIL and 'infinite coalescent time' in sim.stderr

# sexual->asexual: error
sim = lib.run('-nr', nr/100, '-nl', 1, '-theta', 1, 5.0,
    '-np', 1, '-ns', 1, 40,
    '-sexual', 1, '-event', 'switch', 0.1, 1)
assert sim.ret == coalclonal.Program.FAIL and 'infinite coalescent time' in sim.stderr, sim.stderr

# sexual->partially asexual: no error
sim = lib.run('-nr', nr/100, '-nl', 1, '-theta', 1, 5.0,
    '-np', 1, '-ns', 1, 40,
    '-sexual', 1, '-event', 'switch', 0.1, 1, '-event', 'S', 0.2, 1, 1)
assert sim.ret == coalclonal.Program.SUCCESS, sim.stderr

lib.show('    - change switch')
Ts = 0, 0.1, 0.2, 0.4, 0.6, 0.8, 1
Fis1 = []
Fis2 = []
rD1 = []
rD2 = []
for T in Ts:
    lib.show(' {0}'.format(T))
    sim = lib.run('-nr', nr, '-nl', 5, '-theta', '.', 1.0,
        '-np', 1, '-ns', 1, 40,
        '-global-stats', 'Fis', 'rD',
        '-event', 'switch', T, 1)
    assert sim.ret == coalclonal.Program.SUCCESS, sim.stderr
    Fis1.append(sim.average_stats['Fis'])
    rD1.append(sim.average_stats['rD'])

    sim = lib.run('-nr', nr, '-nl', 5, '-theta', '.', 1.0,
        '-np', 1, '-ns', 1, 40,
        '-global-stats', 'Fis', 'rD',
        '-sexual', 1, '-S', 1, 0.8,
        '-event', 'switch', T, 1)
    assert sim.ret == coalclonal.Program.SUCCESS, sim.stderr
    Fis2.append(sim.average_stats['Fis'])
    rD2.append(sim.average_stats['rD'])

sim = lib.run('-nr', nr, '-nl', 5, '-theta', '.', 1.0,
    '-np', 1, '-ns', 1, 40,
    '-global-stats', 'Fis', 'rD', '-S', 1, 0.8)
assert sim.ret == coalclonal.Program.SUCCESS, sim.stderr
Fis_clonal = [sim.average_stats['Fis']]
rD_clonal = [sim.average_stats['rD']]

sim = lib.run('-nr', nr, '-nl', 5, '-theta', '.', 1.0,
    '-np', 1, '-ns', 1, 40,
    '-global-stats', 'Fis', 'rD', '-sexual', 1)
assert sim.ret == coalclonal.Program.SUCCESS, sim.stderr
Fis_sexual = [sim.average_stats['Fis']]
rD_sexual = [sim.average_stats['rD']]

fname = lib.path('pic7-11-switch-Fis.png')
pyplot.plot(Ts, Fis1, 'ro:', mfc='None', mec='r', label='asex->sex')
pyplot.plot(Ts, Fis2, 'bo:', mfc='None', mec='b', label='sex->quasi asex')
pyplot.plot([-0.05], Fis_sexual, 'ks', mfc='None', label='sexual')
pyplot.plot([-0.05], Fis_clonal, 'ks', mfc='k', label='partially clonal')
pyplot.xlabel('T')
pyplot.ylabel('Fis')
pyplot.title('Fis vs. switch date')
pyplot.axhline(0, ls=':', c='k')
pyplot.legend()
pyplot.savefig(fname)
pyplot.clf()
lib.show('\n        -> {0}\n'.format(fname))

fname = lib.path('pic7-12-switch-rD.png')
pyplot.plot(Ts, rD1, 'ro:', mfc='None', mec='r', label='asex->sex')
pyplot.plot(Ts, rD2, 'bo:', mfc='None', mec='b', label='sex->quasi asex')
pyplot.plot([-0.05], rD_sexual, 'ks', mfc='None', label='sexual')
pyplot.plot([-0.05], rD_clonal, 'ks', mfc='k', label='partially clonal')
pyplot.xlabel('T')
pyplot.ylabel('rD')
pyplot.title('rD vs. switch date')
pyplot.legend()
pyplot.savefig(fname)
pyplot.clf()
lib.show('        -> {0}\n'.format(fname))

# event admixture
lib.show('    - change admixture')
Ts = 0, 0.2, 0.4, 0.6, 0.8, 1, 1.2, 1.4
Ps = 0, 0.2, 0.5, 0.8, 1
Fst = [[], [], [], [], []]
for T in Ts:
    lib.show(' {0}'.format(T))
    for i, P in enumerate(Ps):
        sim = lib.run('-nr', nr, '-nl', 1, '-theta', 1, 5.0,
            '-np', 2, '-ns', '.', 40, '-event', 'M', 1.5, 1, 2, 5.0,
            '-event', 'M', 1.5, 2, 1, 5.0,
            '-event', 'admixt', T, 1, 2, P, '-global-stats', 'WCst',
            '-sexual', 1, '-sexual', 2)
        assert sim.ret == coalclonal.Program.SUCCESS, sim.stderr
        Fst[i].append(sim.average_stats['WCst'])

sim = lib.run('-nr', nr, '-nl', 1, '-theta', 1, 5.0,
    '-np', 2, '-ns', '.', 40, '-event', 'M', 1.5, 1, 2, 5.0,
    '-event', 'M', 1.5, 2, 1, 5.0,
    '-global-stats', 'WCst',
    '-sexual', 1, '-sexual', 2)
assert sim.ret == coalclonal.Program.SUCCESS, sim.stderr
Fst0 = sim.average_stats['WCst']

fname = lib.path('pic7-13-admixt-T.png')
pyplot.axhline(Fst0, ls=':', c='k')
pyplot.plot([], [], 'k:', label='no admixt')
for Y, P, c in zip(Fst, Ps, 'bgymr'):
    pyplot.plot(Ts, Y, c+'o:', mfc='None', mec=c, label='proba={0}'.format(P))
pyplot.legend()
pyplot.xlabel('T')
pyplot.ylabel('Fst')
pyplot.title('Fst vs. admixt date')
pyplot.savefig(fname)
pyplot.clf()
lib.show('\n        -> {0}\n'.format(fname))

# event bottleneck
lib.show('    - bottleneck')
Ss = 0.0, 0.05, 0.1, 0.8, 10
Ts = 0, 0.01, 0.02, 0.05, 0.1, 0.2, 0.3, 0.5, 0.7, 1, 1.5, 2, 2.5, 3
D = [[], [], [], [], []]
Th = [[], [], [], [], []]
for T in Ts:
    lib.show(' {0}'.format(T))
    for i, S in enumerate(Ss):
        sim = lib.run('-nr', nr, '-nl', 1, '-theta', 1, 5.0,
            '-np', 1, '-ns', '.', 40, '-sexual', 1,
            '-event', 'bot', T, 1, S,
            '-global-stats', 'D', 'thetaW')
        assert sim.ret == coalclonal.Program.SUCCESS, sim.stderr
        D[i].append(sim.average_stats['D'])
        Th[i].append(sim.average_stats['thetaW'])

fname = lib.path('pic7-14-bot-D.png')
for Y, S, c in zip(D, Ss, 'bgymr'):
    pyplot.plot(Ts, Y, c+'o-', mfc='None', mec=c, label='S={0}'.format(S))
pyplot.legend()
pyplot.axhline(0, c='k', ls=':')
pyplot.xlabel('T')
pyplot.ylabel('D')
pyplot.title('D vs. bottleneck date')
pyplot.savefig(fname)
pyplot.clf()
lib.show('\n        -> {0}\n'.format(fname))

fname = lib.path('pic7-15-bot-thetaW.png')
for Y, S, c in zip(Th, Ss, 'bgymr'):
    pyplot.plot(Ts, Y, c+'o-', mfc='None', mec=c, label='S={0}'.format(S))
pyplot.legend()
pyplot.xlabel('T')
pyplot.ylabel('thetaW')
pyplot.title('thetaW vs. bottleneck date')
pyplot.savefig(fname)
pyplot.clf()
lib.show('\n        -> {0}\n'.format(fname))

# event delayed
lib.show('    - delayed')
Ts = 0.0, 0.2, 0.4, 0.6, 0.8, 1.0, 1.2, 1.4, 1.6
Fst1 = []
Fst2 = []
Fst3 = []
for T in Ts:
    lib.show(' {0}'.format(T))
    sim = lib.run('-nr', nr, '-nl', 2, '-theta', '.', 2.0,
        '-np', 1, '-ns', '1', 20, '-sexual', 1,
        '-event', 'delayed', T, 1, 20, 2, 'YY', 'YY',
        '-global-stats', 'WCst')
    assert sim.ret == coalclonal.Program.SUCCESS, sim.stderr
    assert sim.nl == 2
    assert sim.nr == nr
    for i in range(sim.nr):
        for j in xrange(sim.nl):
            if sim.get(i, j).ss != 0: assert sim.get(i, j).ns == 20*2+20*2
            ds = sim.get(i,j).samples
    Fst1.append(sim.average_stats['WCst'])

    sim = lib.run('-nr', nr, '-nl', 2, '-theta', '.', 2.0,
        '-np', 2, '-ns', '1', 20, '-sexual', 1, '-M', 0,
        '-event', 'delayed', T, 2, 20, 2, 'YY', 'YY',
        '-event', 'admixt', Ts[-1], 2, 1, 1.0,
        '-global-stats', 'WCst')
    assert sim.ret == coalclonal.Program.SUCCESS, sim.stderr
    Fst2.append(sim.average_stats['WCst'])

    sim = lib.run('-nr', nr, '-nl', 2, '-theta', '.', 2.0,
        '-np', 2, '-sexual', 1, '-M', 0,
        '-event', 'delayed', Ts[-1], 1, 20, 100, 'YY', 'YY',
        '-event', 'delayed', T, 2, 20, 2, 'YY', 'YY',
        '-event', 'admixt', Ts[-1], 2, 1, 1.0,
        '-global-stats', 'WCst')
    assert sim.ret == coalclonal.Program.SUCCESS, sim.stderr
    Fst3.append(sim.average_stats['WCst'])

fname = lib.path('pic7-16-delayed-T.png')
pyplot.plot(Ts, Fst1, 'ro:', mfc='None', mec='r', label='same pop')
pyplot.plot(Ts, Fst2, 'go:', mfc='None', mec='g', label='two pops 1')
pyplot.plot(Ts, Fst3, 'bo:', mfc='None', mec='b', label='two pops 2')
pyplot.legend()
pyplot.xlabel('T')
pyplot.ylabel('Fst')
pyplot.title('Fst vs. delayed sampling date')
pyplot.axvline(0, c='k', ls=':')
pyplot.axvline(Ts[-1], c='r', ls='-')
pyplot.axhline(0, c='k', ls=':')
pyplot.savefig(fname)
pyplot.clf()
lib.show('\n        -> {0}\n'.format(fname))

# test flags per population (should be moved to one of the earliest test scripts, not related to events)
sim = lib.run('-nr', 1, '-nl', 2, '-theta', '.', 1.0,
    '-np', 3, '-ns', 1, 20, '-ns', 2, 12, '-ns', 3, 18,
    '-sexual', 1, '-M', 1, '-show-struct')
assert sim.ret == coalclonal.Program.SUCCESS, sim.stderr
assert len(sim.sample_sizes) == 2
assert sim.sample_sizes[0] == [20, 12, 18]
assert sim.sample_sizes[1] == [20, 12, 18]

sim = lib.run('-nr', 1, '-nl', 2, '-theta', '.', 1.0,
    '-np', 3, '-ns', 1, 20, '-ns', 2, 12, '-ns', 3, 18,
    '-sexual', 1, '-M', 1,
    '-flags', 1, 1, 'YY', 'NN', '-flags', 1, 18, 'YY', 'NN', '-flags', 1, 19, 'YY', 'NN',
    '-flags', 2, 6, 'YY', 'NN', '-flags', 2, 8, 'NN', 'NN', '-flags', 2, 10, 'NN', 'YY',
    '-flags', 3, 6, 'NN', 'NN', '-flags', 3, 4, 'YY', 'NN', '-show-struct')
assert sim.ret == coalclonal.Program.SUCCESS, sim.stderr
assert len(sim.sample_sizes) == 2
assert sim.sample_sizes[0] == [20, 10, 17]
assert sim.sample_sizes[1] == [17, 10, 16]

sim = lib.run('-nr', 1, '-nl', 3, '-theta', '.', 1.0,
    '-np', 3, '-ns', 1, 10, '-ns', 2, 5, '-ns', 3, 8,
    '-sexual', 1, '-M', 1, '-show-struct',
    '-event', 'delayed', 0.5, 1, 5, 3, 'NN', 'YY', 'NN',
    '-event', 'delayed', 0.6, 2, 4, 4, 'NN', 'NN', 'YY')
assert sim.ret == coalclonal.Program.SUCCESS, sim.stderr
assert len(sim.sample_sizes) == 3
assert sim.sample_sizes[0] == [10, 5, 8, 0]
assert sim.sample_sizes[1] == [10, 5, 13, 0]
assert sim.sample_sizes[2] == [10, 5, 8, 4]


