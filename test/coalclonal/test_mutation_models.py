from egglib_coalclonal import coalclonal
from matplotlib import pyplot
import lib, random, math

print('# Test mutation models')
nr = 1000

# -K
lib.show('    - KAM\n')
lib.show('        - -K option')
Rmin1 = []
Rmin2 = []
Rmin3 = []
NALL = [2, 3, 4, 5, 6, 7, 8, 9, 10]
for nall in NALL:
    lib.show(' {0}'.format(nall))
    sim = lib.run('-nr', nr, '-nl', 1, '-nmut', 1, 49,
        '-np', 1, '-ns', 1, 20,
        '-sexual', 1, '-global-stats', 'S', 'Rmin',
        '-mut', 1, 'KAM', '-K', 1, nall)
    assert sim.ret == coalclonal.Program.SUCCESS, sim.stderr
    Rmin1.append(sim.average_stats['Rmin'])

    sim = lib.run('-nr', nr, '-nl', 1, '-nmut', 1, 49,
        '-np', 1, '-ns', 1, 20,
        '-sexual', 1, '-global-stats', 'S', 'Rmin',
        '-ls', 1, 200,
        '-mut', 1, 'KAM', '-K', 1, nall)
    assert sim.ret == coalclonal.Program.SUCCESS, sim.stderr
    Rmin2.append(sim.average_stats['Rmin'])

    sim = lib.run('-nr', nr, '-nl', 1, '-nmut', 1, 49,
        '-np', 1, '-ns', 1, 20,
        '-sexual', 1, '-global-stats', 'S', 'Rmin',
        '-ls', 1, 50,
        '-mut', 1, 'KAM', '-K', 1, nall)
    assert sim.ret == coalclonal.Program.SUCCESS, sim.stderr
    Rmin3.append(sim.average_stats['Rmin'])

fname = lib.path('pic6-1-K.png')
pyplot.plot(NALL, Rmin1, 'ko:', mfc='None', mec='k', label='ls=INF')
pyplot.plot(NALL, Rmin2, 'go:', mfc='None', mec='g', label='ls=200')
pyplot.plot(NALL, Rmin3, 'bo:', mfc='None', mec='b', label='ls=50')
pyplot.xlabel('number of alleles')
pyplot.ylabel('Rmin')
pyplot.title('Rmin vs. number of alleles')
pyplot.legend(loc=1, numpoints=1, framealpha=0.75)
pyplot.xlim(1.5, 10.5)
pyplot.ylim(-0.1, int(math.ceil(max(Rmin3)))+0.1)
pyplot.savefig(fname)
pyplot.clf()
lib.show('\n        -> {0}\n'.format(fname))

# -trans
lib.show('        - -trans option')
Rmin = [[],[],[]]
LS = 0, 200, 50
BIAS = [1, 1.5, 2, 2.5, 3, 3.5, 4, 4.5, 5]
for bias in BIAS:
    lib.show(' {0}'.format(bias))
    for i, ls in enumerate(LS):
        sim = lib.run('-nr', nr, '-nl', 1, '-nmut', 1, 50,
            '-np', 1, '-ns', 1, 20,
            '-sexual', 1, '-global-stats', 'Rmin',
            '-mut', 1, 'KAM', '-K', 1, 4,
            '-ls', 1, ls,
            '-trans', 1,
                'x', 1, bias, bias,
                1, 'x', bias, bias,
                bias, bias, 'x', 1,
                bias, bias, 1, 'x')
        assert sim.ret == coalclonal.Program.SUCCESS, sim.stderr
        Rmin[i].append(sim.average_stats['Rmin'])

fname = lib.path('pic6-2-trans.png')
pyplot.plot(BIAS, Rmin[0], 'ko:', mfc='None', mec='k', label='ls={0}'.format(LS[0]))
pyplot.plot(BIAS, Rmin[1], 'go:', mfc='None', mec='g', label='ls={0}'.format(LS[1]))
pyplot.plot(BIAS, Rmin[2], 'bo:', mfc='None', mec='b', label='ls={0}'.format(LS[2]))
pyplot.xlabel('-trans bias')
pyplot.ylabel('Rmin')
pyplot.title('Rmin vs. trans bias')
pyplot.legend(loc=1, numpoints=1, framealpha=0.25)
pyplot.plot([1], [-0.1], 'w,')
pyplot.xlim(0.5, 5.5)
pyplot.axhline(0, c='k', ls=':')
pyplot.savefig(fname)
pyplot.clf()
lib.show('\n        -> {0}\n'.format(fname))

def count(sim, na):
    counts = [0] * na
    for i in xrange(sim.nr):
        for sam in sim.get(i, 0).samples:
            for i in sam:
                counts[i] += 1
    counts = [float(i) / sum(counts) for i in counts]
    return counts


lib.show('        - -trans option (equilibrium)')
GC1 = []
GC2 = []
GC3 = []
GC0 = []
BIAS = [1, 1.5, 2, 2.5, 3, 3.5, 4, 4.5, 5]
for bias in BIAS:
    lib.show(' {0}'.format(bias))

    trans = [ 'x', 0.5*bias, 0.5*bias, 1,
            0.5, 'x', bias, 0.5,
            0.5, bias, 'x', 0.5,
            1, 0.5*bias, 0.5*bias, 'x']

    sim = lib.run('-nr', nr, '-nl', 1, '-nmut', 1, 10,
        '-np', 1, '-ns', 1, 100, '-ls', 1, 1,
        '-sexual', 1, '-mut', 1, 'KAM', '-K', 1, 4,
        '-trans', 1, *trans)
    assert sim.ret == coalclonal.Program.SUCCESS, sim.stderr
    GC0.append(sum(count(sim, 4)[1:3]))

    sim = lib.run('-nr', nr, '-nl', 1, '-nmut', 1, 10,
        '-np', 1, '-ns', 1, 100, '-ls', 1, 1,
        '-sexual', 1, '-mut', 1, 'KAM', '-K', 1, 4,
        '-rstart', 1, '-trans', 1, *trans)
    assert sim.ret == coalclonal.Program.SUCCESS, sim.stderr
    GC1.append(sum(count(sim, 4)[1:3]))

    sim = lib.run('-nr', nr, '-nl', 1, '-nmut', 1, 20,
        '-np', 1, '-ns', 1, 100, '-ls', 1, 1,
        '-sexual', 1, '-mut', 1, 'KAM', '-K', 1, 4,
        '-rstart', 1, '-trans', 1, *trans)
    assert sim.ret == coalclonal.Program.SUCCESS, sim.stderr
    GC2.append(sum(count(sim, 4)[1:3]))

    sim = lib.run('-nr', nr, '-nl', 1, '-nmut', 1, 40,
        '-np', 1, '-ns', 1, 100, '-ls', 1, 1,
        '-sexual', 1, '-mut', 1, 'KAM', '-K', 1, 4,
        '-rstart', 1,    '-trans', 1, *trans)
    assert sim.ret == coalclonal.Program.SUCCESS, sim.stderr
    GC3.append(sum(count(sim, 4)[1:3]))

fname = lib.path('pic6-3-trans.png')
pyplot.plot(BIAS, GC0, 'ko:', mfc='None', mec='k', label='10 mut (start: A)')
pyplot.plot(BIAS, GC1, 'bo:', mfc='None', mec='b', label='10 mut')
pyplot.plot(BIAS, GC2, 'go:', mfc='None', mec='g', label='20 mut')
pyplot.plot(BIAS, GC3, 'ro:', mfc='None', mec='r', label='40 mut')
pyplot.legend(loc=4, numpoints=1, framealpha=0.25)
pyplot.xlabel('-trans bias')
pyplot.ylabel('GC%')
pyplot.title('GC% vs. trans bias')
pyplot.ylim(-0.1, 1.1)
pyplot.xlim(0.5, 5.5)
pyplot.axhline(0.5, c='k', ls=':')
pyplot.savefig(fname)
pyplot.clf()
lib.show('\n        -> {0}\n'.format(fname))

# -mut
lib.show('    - SMM/IAM\n')
lib.show('        - theta estimators with different models')
thetaIAM1 = []
thetaIAM2 = []
thetaSMM1 = []
thetaSMM2 = []
doubleV1 = []
doubleV2 = []
THETA = [0, 0.5, 1, 1.5, 2, 2.5, 3, 3.5, 4, 4.5, 5, 6, 7, 8, 9, 10]
for theta in THETA:
    lib.show(' {0}'.format(theta))

    sim = lib.run('-nr', nr, '-nl', 1, '-theta', 1, theta,
        '-np', 1, '-ns', 1, 200, '-ls', 1, 1,
        '-sexual', 1, '-mut', 1, 'IAM',
        '-locus-stats', 'thetaIAM', 'thetaSMM', 'V')
    assert sim.ret == coalclonal.Program.SUCCESS, sim.stderr
    thetaIAM1.append(sim.average_stats['thetaIAM[1]'])
    thetaSMM1.append(sim.average_stats['thetaSMM[1]'])
    doubleV1.append(2 * sim.average_stats['V[1]'])

    sim = lib.run('-nr', nr, '-nl', 1, '-theta', 1, theta,
        '-np', 1, '-ns', 1, 200, '-ls', 1, 1,
        '-sexual', 1, '-mut', 1, 'SMM',
        '-locus-stats', 'thetaIAM', 'thetaSMM', 'V')
    assert sim.ret == coalclonal.Program.SUCCESS, sim.stderr
    thetaIAM2.append(sim.average_stats['thetaIAM[1]'])
    thetaSMM2.append(sim.average_stats['thetaSMM[1]'])
    doubleV2.append(2 * sim.average_stats['V[1]'])

fname = lib.path('pic6-4-IAM-SMM.png')
pyplot.plot(THETA, THETA, 'k:')
pyplot.plot(THETA, thetaIAM1, 'rs-', mfc='None', mec='r')
pyplot.plot(THETA, thetaIAM2, 'bs-', mfc='None', mec='b')
pyplot.plot(THETA, thetaSMM1, 'r^-', mfc='None', mec='r')
pyplot.plot(THETA, thetaSMM2, 'b^-', mfc='None', mec='b')
pyplot.plot(THETA, doubleV1,  'rv-', mfc='None', mec='r')
pyplot.plot(THETA, doubleV2,  'bv-', mfc='None', mec='b')
pyplot.plot([], [], 'r-', label='simul IAM')
pyplot.plot([], [], 'b-', label='simul SMM')
pyplot.plot([], [], 'ks-', mfc='None', mec='k', label='theta IAM')
pyplot.plot([], [], 'k^-', mfc='None', mec='k', label='theta SMM')
pyplot.plot([], [], 'kv-', mfc='None', mec='k', label='2V')
pyplot.legend(loc=2, numpoints=1, framealpha=0.25)
pyplot.xlabel('theta')
pyplot.ylabel('value')
pyplot.title('thetaIAM / thetaSMM / 2V vs. theta')
pyplot.xlim(-0.5, 10.5)
pyplot.ylim(-0.5, 15.5)
pyplot.savefig(fname)
pyplot.clf()
lib.show('\n        -> {0}\n'.format(fname))

# -TPM
lib.show('    - TPM')
doubleV1 = []
doubleV2 = []
doubleV3 = []
PROBA = [0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1]
theta = 8
for proba in PROBA:
    lib.show(' {0}'.format(proba))

    sim = lib.run('-nr', nr, '-nl', 1, '-theta', 1, theta,
        '-np', 1, '-ns', 1, 200, '-ls', 1, 1,
        '-sexual', 1, '-mut', 1, 'SMM',
        '-locus-stats', 'thetaIAM', 'thetaSMM', 'V')
    assert sim.ret == coalclonal.Program.SUCCESS, sim.stderr
    doubleV1.append(2 * sim.average_stats['V[1]'])

    sim = lib.run('-nr', nr, '-nl', 1, '-theta', 1, theta,
        '-np', 1, '-ns', 1, 200, '-ls', 1, 1,
        '-sexual', 1, '-mut', 1, 'TPM', '-TPMparams', 1, proba, 0.5,
        '-locus-stats', 'thetaIAM', 'thetaSMM', 'V')
    assert sim.ret == coalclonal.Program.SUCCESS, sim.stderr
    doubleV2.append(2 * sim.average_stats['V[1]'])

    sim = lib.run('-nr', nr, '-nl', 1, '-theta', 1, theta,
        '-np', 1, '-ns', 1, 200, '-ls', 1, 1,
        '-sexual', 1, '-mut', 1, 'TPM', '-TPMparams', 1, proba, 0.8,
        '-locus-stats', 'thetaIAM', 'thetaSMM', 'V')
    assert sim.ret == coalclonal.Program.SUCCESS, sim.stderr
    doubleV3.append(2 * sim.average_stats['V[1]'])

fname = lib.path('pic6-5-TPM.png')
pyplot.axhline(theta, ls=':', c='k')
pyplot.plot(PROBA, doubleV1,  'ko-', mfc='None', mec='k', label='SMM')
pyplot.plot(PROBA, doubleV2,  'mo-', mfc='None', mec='m', label='TPM par. 0.5')
pyplot.plot(PROBA, doubleV3,  'ro-', mfc='None', mec='r', label='TPM par. 0.8')
pyplot.legend(loc=2, numpoints=1, framealpha=0.25)
pyplot.xlabel('TPM proba')
pyplot.ylabel('2V')
pyplot.title('2V vs. TPM proba')
pyplot.xlim(-0.1, 1.1)
pyplot.savefig(fname)
pyplot.clf()
lib.show('\n        -> {0}\n'.format(fname))
