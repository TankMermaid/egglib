from egglib_coalclonal import coalclonal
from matplotlib import pyplot
import lib

print('# Test structuration')
idx = 0
nr = 1000
ns = 20
M = [0.1, 0.2, 0.4, 0.6, 0.8, 1, 1.2, 1.4, 1.6, 1.8, 2]

for np in 2, 4, 10:
    idx += 1
    lib.show('    - -M option (K={0})'.format(np))
    Fst = []
    msFst = []
    EFst = []
    for m in M:
        lib.show(' {0}'.format(m))
        sim = lib.run('-nr', nr, '-np', np, '-ns', '.', ns, '-nl', 1,
            '-nmut', 1, 20, '-M', m, '-sexual', '.', '-global-stats', 'WCst')
        Fst.append(sim.average_stats['WCst'])
        EFst.append(1.0 / (1 + m * (np-1)))
        stats = lib.run_ms(*[ns*2*np, nr, '-I', np] + [ns*2]*np + [m*(np-1), '-s', 20])
        msFst.append(stats['WCst'])

    fname = lib.path('pic2-{0}-M-K={1}.png'.format(idx, np))
    pyplot.plot(M, Fst, 'ko', mfc='None', mec='r', label='Fst')
    pyplot.plot(M, msFst, 'ko', mfc='None', mec='g', label='Fst (ms)')
    pyplot.plot(M, EFst, 'r:', label='expectation')
    pyplot.legend(loc=1, numpoints=1)
    pyplot.xlabel('M = 4Nm')
    pyplot.ylabel('Fst (Weir & Cockerham')
    pyplot.title('Fst vs. M K={0}'.format(np))
    pyplot.xlim(0, 2.1)
    pyplot.ylim(0, 1)
    pyplot.savefig(fname)
    pyplot.clf()
    lib.show('\n        -> {0}\n'.format(fname))

lib.show('    - -Mm option')
Fst1 = []
Fst2 = []
Fst3 = []
for m in M:
    lib.show(' {0}'.format(m))
    sim = lib.run('-nr', nr, '-np', 3, '-ns', '.', 20, '-nl', 2,
        '-nmut', '.', 10, '-Mm', 'x', 5*m, m, 5*m, 'x', 0, m, 0, 'x',
        '-flags', 1, 1, 'YY', 'NN',
        '-flags', 2, 1, 'YY', 'NN',
        '-flags', 2, 5, 'NN', 'YY',
        '-flags', 3, 5, 'YY', 'NN', # add flags to test locus/structure combination
        '-sexual', '.', '-global-stats', 'WCst:1,2', 'WCst:1,3', 'WCst:2,3', 'WCst')
    assert sim.ret == coalclonal.Program.SUCCESS, sim.stderr
    Fst1.append(sim.average_stats['WCst<1,2>'])
    Fst2.append(sim.average_stats['WCst<1,3>'])
    Fst3.append(sim.average_stats['WCst<2,3>'])

fname = lib.path('pic2-4-Mm.png')
pyplot.plot(M, Fst1, 'ko', mfc='None', mec='r', label='Fst 1-2 (5xM)')
pyplot.plot(M, Fst2, 'ko', mfc='None', mec='g', label='Fst 1-3 (1xM)')
pyplot.plot(M, Fst3, 'ko', mfc='None', mec='b', label='Fst 2-3 (0)')
pyplot.legend(loc=1, numpoints=1)
pyplot.xlabel('M = 4Nm')
pyplot.ylabel('Fst (Weir & Cockerham')
pyplot.title('pairwise Fst vs. Mp')
pyplot.xlim(0, 2.1)
pyplot.ylim(-0.1, 1.1)
pyplot.savefig(fname)
pyplot.clf()
lib.show('\n        -> {0}\n'.format(fname))

lib.show('    - -Mp option')
Fst1 = []
Fst2 = []
Fst3 = []
for m in M:
    lib.show(' {0}'.format(m))
    sim = lib.run('-nr', nr, '-np', 3, '-ns', '.', 20, '-nl', 2,
        '-nmut', '.', 10, '-M', m, '-Mp', 1, 2, 5*m, '-Mp', 2, 1, 5*m,
        '-flags', 1, 1, 'YY', 'NN',
        '-flags', 2, 1, 'YY', 'NN',
        '-flags', 2, 5, 'NN', 'YY',
        '-flags', 3, 5, 'YY', 'NN', # add flags to test locus/structure combination
        '-sexual', '.', '-global-stats', 'WCst:1,2', 'WCst:1,3', 'WCst:2,3', 'WCst')
    assert sim.ret == coalclonal.Program.SUCCESS, sim.stderr
    Fst1.append(sim.average_stats['WCst<1,2>'])
    Fst2.append(sim.average_stats['WCst<1,3>'])
    Fst3.append(sim.average_stats['WCst<2,3>'])

fname = lib.path('pic2-5-Mp.png')
pyplot.plot(M, Fst1, 'ko', mfc='None', mec='r', label='Fst 1-2 (5xM)')
pyplot.plot(M, Fst2, 'ko', mfc='None', mec='g', label='Fst 1-3 (1xM)')
pyplot.plot(M, Fst3, 'ko', mfc='None', mec='b', label='Fst 2-3 (1xM)')
pyplot.legend(loc=1, numpoints=1)
pyplot.xlabel('M = 4Nm')
pyplot.ylabel('Fst (Weir & Cockerham')
pyplot.title('pairwise Fst vs. Mp')
pyplot.xlim(0, 2.1)
pyplot.ylim(-0.1, 1.1)
pyplot.savefig(fname)
pyplot.clf()
lib.show('\n        -> {0}\n'.format(fname))

lib.show('    - -Mp option (assymetrical)')
N1 = 4
N2 = 1
theta = 2
Pi1a = []
Pi2a = []
Pi1b = []
Pi2b = []
Pi1c = []
Pi2c = []
for m in M:
    lib.show(' {0}'.format(m))
    sim = lib.run('-nr', nr, '-np', 2, '-ns', '.', 20, '-nl', 1,
        '-theta', '.', 2, '-N', 1, 4, '-Mp', 1, 2, m, '-Mp', 2, 1, m,
        '-sexual', '.', '-global-stats', 'Pi', 'Pi:1', 'Pi:2')
    assert sim.ret == coalclonal.Program.SUCCESS, sim.stderr
    Pi1a.append(sim.average_stats['Pi<1>'])
    Pi2a.append(sim.average_stats['Pi<2>'])

    sim = lib.run('-nr', nr, '-np', 2, '-ns', '.', 20, '-nl', 1,
        '-theta', '.', 2, '-N', 1, 4, '-Mp', 1, 2, m, '-Mp', 2, 1, 0,
        '-sexual', '.', '-global-stats', 'Pi', 'Pi:1', 'Pi:2')
    assert sim.ret == coalclonal.Program.SUCCESS, sim.stderr
    Pi1b.append(sim.average_stats['Pi<1>'])
    Pi2b.append(sim.average_stats['Pi<2>'])

    sim = lib.run('-nr', nr, '-np', 2, '-ns', '.', 20, '-nl', 1,
        '-theta', '.', 2, '-N', 1, 4, '-Mp', 1, 2, 0, '-Mp', 2, 1, m,
        '-sexual', '.', '-global-stats', 'Pi', 'Pi:1', 'Pi:2')
    assert sim.ret == coalclonal.Program.SUCCESS, sim.stderr
    Pi1c.append(sim.average_stats['Pi<1>'])
    Pi2c.append(sim.average_stats['Pi<2>'])

fname = lib.path('pic2-6-Mp-assym.png')
pyplot.plot(M, Pi1a, 'ks', mfc='None', mec='r')
pyplot.plot(M, Pi2a, 'k^', mfc='None', mec='r')
pyplot.plot(M, Pi1b, 'ks', mfc='None', mec='g')
pyplot.plot(M, Pi2b, 'k^', mfc='None', mec='g')
pyplot.plot(M, Pi1c, 'ks', mfc='None', mec='b')
pyplot.plot(M, Pi2c, 'k^', mfc='None', mec='b')
pyplot.plot([], [], 'ks--', mfc='None', label='Pi1 (N=4)')
pyplot.plot([], [], 'k^:', mfc='None', label='Pi2 (N=1)')
pyplot.plot([], [], 'rx', label='1<->2')
pyplot.plot([], [], 'gx', label='1->2')
pyplot.plot([], [], 'bx', label='1<-2')
pyplot.axhline(theta*N1, c='k', ls='--')
pyplot.axhline(theta*N2, c='k', ls=':')
pyplot.legend(loc=1, numpoints=1)
pyplot.xlabel('M = 4Nm')
pyplot.ylabel('Pi')
pyplot.title('Pi vs. Mp assymetrical')
pyplot.xlim(0, 2.1)
pyplot.savefig(fname)
pyplot.clf()
lib.show('\n        -> {0}\n'.format(fname))
