from egglib_coalclonal import coalclonal
from matplotlib import pyplot
import lib

print( '# Test clonality')
lib.show('    - -S option')
nr = 1000
ns = 20
S = [0, 0.2, 0.4, 0.8, 1.2, 1.6, 2, 2.4, 2.8, 3.2, 3.6, 4]

Fis = []
rD_intra = []
rD_global = []
Z_intra = []
Z_global = []
Rmin_intra = []
Rmin_global = []
for s in S:
    lib.show(' {0}'.format(s))
    args = ['-nr', nr, '-np', 1, '-ns', '.', ns, '-nl', 4,
                    '-nmut', '.', 20, '-S', 1, s,
                    '-global-stats', 'rD', 'ZnS', 'Rmin',
                    '-locus-stats', 'Fis', 'rD', 'ZnS', 'Rmin']
    if s == 0: args.extend(['-event', 'switch', 10.0, 1])
    sim = lib.run(*args)
    assert sim.ret == coalclonal.Program.SUCCESS, sim.stderr
    Fis.append(sim.average_stats['Fis[1]'])
    rD_global.append(sim.average_stats['rD'])
    rD_intra.append(sim.average_stats['rD[1]'])
    Z_intra.append(sim.average_stats['ZnS[1]'])
    Z_global.append(sim.average_stats['ZnS'])
    Rmin_intra.append(sim.average_stats['Rmin[1]'])
    Rmin_global.append(sim.average_stats['Rmin'])

lib.show(' sexual')
args = ['-nr', nr, '-np', 1, '-ns', '.', ns, '-nl', 4,
                '-nmut', '.', 20, '-sexual', 1,
                    '-global-stats', 'rD', 'ZnS', 'Rmin',
                    '-locus-stats', 'Fis', 'rD', 'ZnS', 'Rmin']
sim = lib.run(*args)
assert sim.ret == coalclonal.Program.SUCCESS, sim.stderr
Fis_sexual = sim.average_stats['Fis[1]']
rD_intra_sexual = sim.average_stats['rD[1]']
rD_global_sexual = sim.average_stats['rD']
Z_global_sexual = sim.average_stats['ZnS']
Z_intra_sexual = sim.average_stats['ZnS[1]']
Rmin_intra_sexual = sim.average_stats['Rmin[1]']
Rmin_global_sexual = sim.average_stats['Rmin']

fname = lib.path('pic3-1-S.png')
pyplot.plot(S, Fis, 'ko', mfc='b', mec='b', label='Fis')
pyplot.plot([5.0], Fis_sexual,       'ko', mfc='b', mec='b')
pyplot.xlabel('S = 4N(1-c)')
pyplot.ylabel('Fis')
pyplot.title('Fis vs. S')
pyplot.xlim(-0.1, 5.1)
pyplot.ylim(-1.1, +0.1)
pyplot.axhline(0, ls=':', c='k')
pyplot.xticks([0,1,2,3,4,5], [0,1,2,3,4, 'sexual'])
pyplot.savefig(fname)
pyplot.clf()
lib.show('\n        -> {0}\n'.format(fname))

fname = lib.path('pic3-2-S.png')
pyplot.plot(S, rD_intra, 'ko', mfc='g', mec='g', label='rD local')
pyplot.plot(S, rD_global, 'ks', mfc='None', mec='g', label='rD global')
pyplot.plot(S, Z_intra, 'ko', mfc='m', mec='m', label='ZnS local')
pyplot.plot(S, Z_global, 'ks', mfc='None', mec='m', label='ZnS global')
pyplot.plot([5.0], rD_intra_sexual,  'ko', mfc='g', mec='g')
pyplot.plot([5.0], rD_global_sexual, 'ks', mfc='None', mec='g')
pyplot.plot([5.0], Z_intra_sexual,  'ko', mfc='m', mec='m')
pyplot.plot([5.0], Z_global_sexual, 'ks', mfc='None', mec='m')
pyplot.legend(loc=3, numpoints=1)
pyplot.xlabel('S = 4N(1-c)')
pyplot.ylabel('rD / ZnS')
pyplot.title('rD / ZnS vs. S')
pyplot.xlim(-0.1, 5.1)
pyplot.ylim(-0.1, +0.51)
pyplot.xticks([0,1,2,3,4,5], [0,1,2,3,4, 'sexual'])
pyplot.savefig(fname)
pyplot.clf()
lib.show('        -> {0}\n'.format(fname))

fname = lib.path('pic3-3-S.png')
pyplot.plot(S, Rmin_intra, 'ko', mfc='r', mec='r', label='Rmin local')
pyplot.plot(S, Rmin_global, 'ks', mfc='y', mec='y', label='Rmin global')
pyplot.plot([5.0], Rmin_intra_sexual,  'ko', mfc='r', mec='y')
pyplot.plot([5.0], Rmin_global_sexual, 'ks', mfc='y', mec='y')
pyplot.legend(loc=2, numpoints=1)
pyplot.axhline(0.0, ls=':', c='k')
pyplot.xlabel('S = 4N(1-c)')
pyplot.ylabel('Rmin')
pyplot.title('Rmin vs. S')
pyplot.xlim(-0.1, 5.1)
pyplot.ylim(-0.1, 5.1)
pyplot.xticks([0,1,2,3,4,5], [0,1,2,3,4, 'sexual'])
pyplot.savefig(fname)
pyplot.clf()
lib.show('        -> {0}\n'.format(fname))
