from egglib_coalclonal import coalclonal
import lib, re, os, tempfile

print('# Test interface')

# HELP
print('    - invoking help')
simuls = lib.run('-help')
assert simuls.ret == coalclonal.Program.MANUAL, simuls.stdout+simuls.stderr
assert re.match('CoalClonal -- Coalescent simulator for partially clonal populations\n', simuls.stdout), simuls.stdout
assert simuls.dim == (0, 0)

# ONE SITE
print('    - simulate a single site')
simuls = lib.run('-nr', 1, '-np', 1, '-ns', 1, 10, '-nl', 1,
    '-nmut', 1, 1, '-sexual', 1)
assert simuls.ret == coalclonal.Program.SUCCESS, simuls.stderr
assert simuls.stdout == '', simuls.stdout
assert simuls.nr == 1
assert simuls.nl == 1
assert simuls.get(0, 0).ss == 1
assert len(simuls.get(0, 0).samples) == 20
assert set([i[0] for i in simuls.get(0, 0).samples]) == set([0, 1])

# NR / STEP
print('    - test nr and step options')
simuls = lib.run('-nr', 550, '-np', 1, '-ns', 1, 20,
    '-nl', 2, '-nmut', 1, 10, '-theta', 2, 0.0, '-sexual', 1,
    '-step', 120)
assert simuls.ret == coalclonal.Program.SUCCESS, simuls.stderr
assert simuls.nr == 550
assert simuls.nl == 2
assert simuls.stdout.count('replicate') == 4
for i in xrange(550):
    assert simuls.get(i, 0).ss == 10 and simuls.get(i, 1).ss == 0 and simuls.get(i, 0).ns == 40

# NS
print('    - test ns')
simuls = lib.run('-np', 4, '-M', 1.0, '-S', 1, 1.0,
    '-S', 2, 1.0, '-S', 3, 1.0, '-S', 4, 1.0, '-ns', 1, 1, '-ns', 2, 4,
    '-ns', 3, 2, '-ns', 4, 1, '-nl', 1, '-nmut', 1, 1)
assert simuls.ret == coalclonal.Program.SUCCESS, simuls.stderr
assert simuls.stdout == ''
assert simuls.nr == 1
assert simuls.nl == 1
assert simuls.get(0, 0).ns == 16

# MIGRATION
print('    - test migration rates')
    # No migration, samples in just one pop -> success
simuls = lib.run('-np', 4, '-nl', 1, '-ns', 1, 10, '-S', 1, 1.0)
assert simuls.ret == coalclonal.Program.SUCCESS, simuls.stderr

    # No migration -> failure
simuls = lib.run('-np', 4, '-nl', 1, '-ns', 1, 10, '-ns', 2, 10, '-sexual', 1, '-sexual', 2)
assert simuls.ret == coalclonal.Program.FAIL
assert 'infinite coalescent time' in simuls.stderr

    # Migration -> success
simuls = lib.run('-np', 4, '-nl', 1,
    '-ns', 1, 10, '-ns', 2, 10, '-ns', 3, 10, '-ns', 4, 10,
    '-sexual', 1, '-sexual', 2, '-sexual', 3, '-sexual', 4,
    '-M', 1.0)
assert simuls.ret == coalclonal.Program.SUCCESS, simuls.stderr

    # One population not connected -> failure
simuls = lib.run('-np', 4, '-nl', 1,
    '-ns', 1, 10, '-ns', 2, 10, '-ns', 3, 10, '-ns', 4, 10,
    '-sexual', 1, '-sexual', 2, '-sexual', 3, '-sexual', 4,
    '-Mp', 1, 2, 1.0, '-Mp', 2, 3, 1.0, '-Mp', 3, 4, 0.0)
assert simuls.ret == coalclonal.Program.FAIL, simuls.stderr
assert 'infinite coalescent time' in simuls.stderr

    # All populations connected -> success
simuls = lib.run('-np', 4, '-nl', 1,
    '-ns', 1, 10, '-ns', 2, 10, '-ns', 3, 10, '-ns', 4, 10,
    '-sexual', 1, '-sexual', 2, '-sexual', 3, '-sexual', 4,
    '-Mp', 1, 2, 1.0, '-Mp', 2, 3, 1.0, '-Mp', 3, 4, 1.0)
assert simuls.ret == coalclonal.Program.SUCCESS, simuls.stderr

    # The same in matrix mode
simuls = lib.run('-np', 4, '-nl', 1,
    '-ns', 1, 10, '-ns', 2, 10, '-ns', 3, 10, '-ns', 4, 10,
    '-sexual', 1, '-sexual', 2, '-sexual', 3, '-sexual', 4,
    '-Mm', 'x', 1, 0, 0,  0, 'x', 1, 0,  0, 0, 'x', 1,  0, 0, 0, 'x')
assert simuls.ret == coalclonal.Program.SUCCESS, simuls.stderr

# VARIOUS POPULATION PARAMETERS
print('    - set population properties and population indexing')
simuls = lib.run('-np', 4, '-nl', 1,     '-sexual', '.', '-M', 1,
    '-ns', 1, 10, '-ns', 2, 10, '-ns', 3, 10, '-ns', 4, 10,
    '-N', '.', 2, '-N', 1, 0.5,
    '-G', '1,2', 0.5, '-G', 3, 1.0)
assert simuls.ret == coalclonal.Program.SUCCESS, simuls.stderr

simuls = lib.run('-np', 4, '-nl', 1, '-ns', '.', 5,
    '-S', '.', 0.0, '-S', 1, 1.0, '-M', 1.0)
assert simuls.ret == coalclonal.Program.SUCCESS, simuls.stderr

simuls = lib.run('-np', 4, '-nl', 1, '-ns', '.', 5, '-M', 1.0,
    '-s', '1,2', 0.9, '-sexual', '3,4')
assert simuls.ret == coalclonal.Program.SUCCESS, simuls.stderr

# FLAGS
print('    - test sampling flags')
simuls = lib.run('-np', 2, '-ns', '.', 4, '-M', 1, '-sexual', '.',
        '-nl', 2, '-nmut', '.', 1)
assert simuls.ret == coalclonal.Program.SUCCESS, simuls.stderr
assert simuls.get(0, 0).ns == 16 and simuls.get(0, 1).ns == 16

simuls = lib.run('-np', 2, '-ns', '.', 4, '-M', 1, '-sexual', '.',
        '-nl', 2, '-nmut', '.', 1,
        '-flags', 1, 1, 'YN', 'NN',
        '-flags', 1, 4, 'YY', 'NN',
        '-flags', 2, 3, 'YN', 'NY')
assert simuls.ret == coalclonal.Program.SUCCESS, simuls.stderr
assert simuls.get(0, 0).ns == 14 and simuls.get(0, 1).ns == 11

simuls = lib.run('-np', 2, '-ns', '.', 4, '-M', 1, '-sexual', '.',
        '-nl', 2, '-nmut', '.', 1,
        '-all-flags', 'YY', 'YY', 'NN', 'YY', 'YN', 'YY', 'YN', 'YN',
                      'YY', 'YY', 'YY', 'YY', 'NN', 'YY', 'NN', 'YN')
assert simuls.ret == coalclonal.Program.SUCCESS, simuls.stderr
assert simuls.get(0, 0).ns == 8 and simuls.get(0, 1).ns == 14

# LOCUS PARAMETERS
print('    - test locus properties')
simuls = lib.run('-np', 1, '-ns', 1, 20, '-sexual', 1,
        '-nl', 4, '-ls', '1,3', 1000, '-ls', 2, 800, '-ls', 4, 0,
        '-rho', '.', 1.1, '-rec', '.', 0.1, '-rec', 3, 0.2,
        '-theta', '.', 3.0, '-theta', 3, 2.1, '-nmut', 4, 17)
assert simuls.ret == coalclonal.Program.SUCCESS, simuls.stderr
assert simuls.get(0, 3).ss == 17

# SITE PARAMETERS
print('    - test site properties')
simuls = lib.run('-np', 1, '-ns', 1, 20, '-sexual', 1,
    '-nl', 1, '-ls', 1, 0,
    '-sitep', 1, 0.01, 0.02, 0.05, 0.25, 0.45, 0.055, 0.065, 0.88, 0.89, 0.90)
assert simuls.ret == coalclonal.Program.FAIL_ARGUMENT

simuls = lib.run('-np', 1, '-ns', 1, 20, '-sexual', 1,
    '-nl', 1, '-ls', 1, 20,
    '-sitep', 1, 0.01, 0.02, 0.05, 0.25, 0.45, 0.055, 0.065, 0.88, 0.89, 0.90)
assert simuls.ret == coalclonal.Program.FAIL_ARGUMENT
assert 'expect 20 values for option `-sitep`' in simuls.stderr

simuls = lib.run('-np', 1, '-ns', 1, 20, '-sexual', 1,
    '-nl', 1, '-ls', 1, 10,
    '-sitep', 1, 0.01, 0.02, 0.05, 0.25, 0.45, 0.55, 0.65, 0.88, 0.89, 0.90)
assert simuls.ret == coalclonal.Program.SUCCESS, simuls.stderr
assert simuls.get(0, 0).pos == [0.01, 0.02, 0.05, 0.25, 0.45, 0.55, 0.65, 0.88, 0.89, 0.90]

simuls = lib.run('-np', 1, '-ns', 1, 20, '-sexual', 1,
    '-nl', 1, '-ls', 1, 10,
    '-sitep', 1, 0.01, 0.02, 0.05, 0.25, 0.45, 0.55, 0.65, 0.88, 0.89, 0.90,
    '-sitew', 1, 0, 0, 0, 1, 1.2, 1, 0.2, 0, 1, 1, '-theta', 1, 5.0)
assert simuls.ret == coalclonal.Program.SUCCESS, simuls.stderr

# MUTATION MODELS
print('    - test mutation models')
print('        - KAM')
simuls = lib.run('-np', 1, '-ns', 1, 20, '-sexual', 1,
    '-nl', 1, '-ls', 1, 1000, '-theta', 1, 5.0,
    '-mut', 1, 'KAM', '-K', 1, 4, '-rstart', 1,
    '-trans', 1,  'x', 1.4, 1.0, 1.0,
                  1.4, 'x', 1.0, 1.0,
                  1.0, 1.0, 'x', 1.4,
                  1.0, 1.0, 1.4, 'x')
assert simuls.ret == coalclonal.Program.SUCCESS, simuls.stderr
assert set([j for i in simuls.get(0, 0).samples for j in i]) <= set(range(4))

simuls = lib.run('-np', 1, '-ns', 1, 20, '-sexual', 1,
    '-nl', 1, '-ls', 1, 1000, '-theta', 1, 5.0,
    '-mut', 1, 'KAM', '-K', 1, 8, '-rstart', 1)
assert simuls.ret == coalclonal.Program.SUCCESS, simuls.stderr
assert set([j for i in simuls.get(0, 0).samples for j in i]) <= set(range(8))

print('        - IAM')
simuls = lib.run('-np', 1, '-ns', 1, 20, '-sexual', 1,
    '-nl', 1, '-ls', 1, 1, '-nmut', 1, 17, '-mut', 1, 'IAM')
assert simuls.ret == coalclonal.Program.SUCCESS, simuls.stderr
assert set([j for i in simuls.get(0, 0).samples for j in i]) <= set(range(17))

print('        - SMM')
simuls = lib.run('-np', 1, '-ns', 1, 20, '-sexual', 1,
    '-nl', 1, '-ls', 1, 1, '-theta', 1, 5.0, '-mut', 1, 'SMM')
assert simuls.ret == coalclonal.Program.SUCCESS, simuls.stderr

print('        - TPM')
simuls = lib.run('-np', 1, '-ns', 1, 20, '-sexual', 1,
    '-nl', 1, '-ls', 1, 1, '-theta', 1, 5.0, '-mut', 1, 'TPM', '-TPMparams', 1, 1, 0.5)
assert simuls.ret == coalclonal.Program.SUCCESS, simuls.stderr

print('        - multi-model')
simuls = lib.run('-np', 1, '-ns', 1, 20, '-sexual', 1,
    '-nl', 5, '-ls', 1, 0, '-ls', 2, 1000, '-ls', '3,4,5', 1,
    '-theta', '1,2', 2.2, '-theta', '3,4,5', 5.0,
    '-mut', '1,2', 'KAM', '-mut', 3, 'IAM', '-mut', 4, 'SMM', '-mut', 5, 'TPM',
    '-K', 1, 2, '-K', 2, 60, '-rstart', 2, '-TPMparams', 5, 0.25, 0.8)
assert simuls.ret == coalclonal.Program.SUCCESS, simuls.stderr

# HISTORICAL EVENTS
print('    - test historical events')
print('        - change N & bottleneck')
simuls = lib.run('-np', 1, '-ns', 1, 20, '-sexual', 1, '-nl', 1, '-theta', 1, 2.0,
    '-event', 'N', 0.7, 1, 1, '-event', 'N', 0.5, 1, 0.1)
assert simuls.ret == coalclonal.Program.SUCCESS, simuls.stderr

simuls = lib.run('-np', 2, '-ns', 1, 20, '-sexual', 1, '-nl', 1, '-theta', 1, 2.0, '-M', 0.2,
    '-event', 'bot', 0.2, 1, 0.4)
assert simuls.ret == coalclonal.Program.SUCCESS, simuls.stderr

print('        - change G')
simuls = lib.run('-np', 1, '-ns', 1, 20, '-sexual', 1, '-nl', 1, '-theta', 1, 2.0,
    '-G', 1, -100)
assert simuls.ret == coalclonal.Program.FAIL
assert 'infinite coalescent time' in simuls.stderr, simuls.stderr

simuls = lib.run('-np', 1, '-ns', 1, 20, '-sexual', 1, '-nl', 1, '-theta', 1, 2.0,
    '-G', 1, -100, '-event', 'G', 4.0, 1, 10)
assert simuls.ret == coalclonal.Program.SUCCESS, simuls.stderr

print('        - change S and sexual regime')
simuls = lib.run('-np', 1, '-ns', 1, 20, '-nl', 1, '-theta', 1, 2.0)
assert simuls.ret == coalclonal.Program.FAIL
assert 'infinite coalescent time' in simuls.stderr, simuls.stderr

simuls = lib.run('-np', 1, '-ns', 1, 20, '-nl', 1, '-theta', 1, 2.0,
        '-event', 'S', 1.0, 1, 0.1)
assert simuls.ret == coalclonal.Program.SUCCESS, simuls.stderr

print('        - change s')
simuls = lib.run('-np', 5, '-ns', '.', 5, '-nl', 1, '-theta', 1, 2.0,
        '-S', '.', 0.1, '-s', '.', 1.0, '-M', 0.1,
        '-event', 's', 1.0, 1, 0.1, '-event', 's', 1.0, 2, 0.1,
        '-event', 's', 1.0, 3, 0.1)
assert simuls.ret == coalclonal.Program.SUCCESS, simuls.stderr

print('        - change M'
simuls = lib.run('-np', 3, '-ns', '.', 20, '-nl', 1, '-theta', 1, 2.0, '-M', 0, '-sexual', '.')
assert simuls.ret == coalclonal.Program.FAIL
assert 'infinite coalescent time' in simuls.stderr, simuls.stderr

simuls = lib.run('-np', 3, '-ns', '.', 20, '-nl', 1, '-theta', 1, 2.0, '-M', 0, '-sexual', '.',
         '-event', 'M', 1.5, 1, 2, 1.0, '-event', 'M', 1.5, 2, 1, 1.0)
assert simuls.ret == coalclonal.Program.FAIL, simuls.stderr
assert 'maximum number of iterations' in simuls.stderr, simuls.stderr

simuls = lib.run('-np', 3, '-ns', 1, 20, '-nl', 1, '-theta', 1, 2.0, '-M', 0, '-sexual', '.',
         '-event', 'M', 1.5, 1, 2, 1.0, '-event', 'M', 1.5, 2, 1, 1.0,
         '-event', 'M', 1.5, 2, 3, 1.0, '-event', 'M', 1.5, 3, 2, 1.0)
assert simuls.ret == coalclonal.Program.SUCCESS, simuls.stderr

print('        - recombination parameters & switch')
simuls = lib.run('-np', 1, '-ns', '.', 20, '-nl', 2, '-theta', '.', 2.0, '-sexual', 1,
    '-event', 'rho', 0.4, 1, 1.0, '-event', 'rho', 0.8, 1, 0.2,
    '-event', 'switch', 0.6, 1, '-event', 'S', 0.6, 1, 0.95,
    '-event', 'rec', 0.6, 1, 0.2)
assert simuls.ret == coalclonal.Program.SUCCESS, simuls.stderr

simuls = lib.run('-np', 1, '-ns', '.', 20, '-nl', 1, '-theta', '.', 2.0, '-sexual', 1,
    '-rho', 1, 0.2, '-event', 'rho', 0.4, 1, 1.0, '-event', 'rho', 0.8, 1, 0.2)
assert simuls.ret == coalclonal.Program.SUCCESS, simuls.stderr

print('        - admixt')
simuls = lib.run('-np', 2, '-ns', '.', 20, '-M', 0,
     '-nl', 1, '-theta', '.', 1.0, '-sexual', '.',
     '-event', 'admixt', 0.1, 1, 2, 1.0)
assert simuls.ret == coalclonal.Program.SUCCESS, simuls.stderr

simuls = lib.run('-np', 2, '-ns', '.', 20, '-M', 0.2,
     '-nl', 1, '-theta', '.', 1.0, '-sexual', '.',
     '-event', 'admixt', 0.5, 1, 2, 0.5)
assert simuls.ret == coalclonal.Program.SUCCESS, simuls.stderr

print('        - delayed')
simuls = lib.run('-nr', 2, '-np', 2, '-ns', 1, 10, '-M', 1.0,
     '-nl', 2, '-theta', '.', 1.0, '-sexual', '.',
     '-event', 'delayed', 0.1, 2, 10, 1, 'YN', 'YY',
     '-event', 'delayed', 0.6, 1, 10, 1, 'YY', 'NN')
assert simuls.ret == coalclonal.Program.SUCCESS, simuls.stderr
assert simuls.nr == 2
assert simuls.nl == 2
assert simuls.get(0, 0).ns == 50
assert simuls.get(0, 1).ns == 40
assert simuls.get(1, 0).ns == 50
assert simuls.get(1, 1).ns == 40

# STATS
print('    - test statistics'
simuls = lib.run('-nr', 100, '-np', 2, '-ns', '.', 25, '-sexual', '.',
    '-event', 'admixt', 2.5, 1, 2, 1.0,
    '-nl', 2, '-theta', 1, 2.4, '-theta', 2, 3.8,
    '-locus-stats', 'D', 'S', 'thetaW', 'Pi',
    '-site-stats', 'He', 'WCst',
    '-global-stats', 'D', 'WCst')
assert simuls.ret == coalclonal.Program.SUCCESS, simuls.stderr

args = ['-nr', 10, '-np', 1, '-ns', '.', 10, '-sexual', '.', '-nl', 1, '-theta', '.', 1.4]
args.append('-locus-stats')
args.extend(coalclonal.Program._stats)
args.append('-global-stats')
args.extend(coalclonal.Program._stats)
args.append('-site-stats')
args.extend(coalclonal.Program._site_stats)
simuls = lib.run(*args)
assert simuls.ret == coalclonal.Program.SUCCESS, simuls.stderr

simuls = lib.run('-nr', 100, '-np', 4, '-ns', '.', 25, '-sexual', '.',
    '-M', 1.0, '-nl', 2, '-theta', 1, 2.4, '-theta', 2, 3.8,
    '-locus-stats', 'D:1,2', 'S:1,2,3,4', 'thetaW:2', 'Pi:3,4',
    '-site-stats', 'He:1,2', 'WCst:1,2,4',
    '-global-stats', 'D:1,4', 'WCst:1,4')
assert simuls.ret == coalclonal.Program.SUCCESS, simuls.stderr

# VARIABLE PARAMETER FEED
print('    - test VAR')
params = [[1.0, 0.1, 0.0, 1.2],
          [1.5, 0.3, 1.0, 1.0],
          [0.5, 0.2, 1.6, 0.0],
          [0.8, 0.1, 0.0, 0.0],
          [1.4, 0.8, 1.4, 3.2]]
simuls = lib.run('-nr', 5, '-np', 1, '-ns', '.', 10, '-sexual', '.',
    '-N', 1, 'VAR-1', '-M', 'VAR-2', '-nl', 2, '-theta', 1, 'VAR-3',
    '-theta', 2, 'VAR-4',
        feed=params)
assert simuls.ret == coalclonal.Program.SUCCESS, simuls.stderr
assert simuls.get(0, 0).ss == 0
assert simuls.get(2, 1).ss == 0
assert simuls.get(3, 0).ss == 0
assert simuls.get(3, 1).ss == 0
assert max([simuls.get(i, j).ss for i in range(5) for j in range(2)]) > 0

params = [[1.2, 0.96, 1.5, 0.1, 0.9, 3, 0.5, 0.2, 0.1, 3.2],
          [1.5, 0.98, 1.0, 0.1, 0.5, 2, 0.7, 0.8, 2.2, 0.5],
          [0.6, 0.00, 0.1, 0.2, 0.7, 4, 0.2, 0.1, 1.4, 2.8]]
simuls = lib.run('-nr', 3, '-np', 3, '-ns', '.', 10, '-sexual', '.',
    '-N', 2, 'VAR-10', '-M', 0.0, '-nl', 2, '-N', 2, 'VAR-1', '-s', '.', 'VAR-2',
    '-rho', 1, 'VAR-3', '-rec', '.', 'VAR-4', '-mut', 2, 'TPM',
    '-theta', 1, 1.5, '-theta', 2, 8.0,    '-TPMparams', 2, 1.0, 'VAR-5', 
    '-ls', 1, 100, '-sitew', 1, 1,1,1,1,1,1,1,1,1,1,1,1,1,1,'VAR-6',1,1,1,1,1,1,1,1,1,1,
                               1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,
                               1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,'VAR-6',1,1,1,1,1,1,1,
                               1,1,1,1,1,1,1,'VAR-6',1,1,1,1,1,1,'VAR-6',1,1,1,1,1,1,1,1,1,1,
     '-event', 'N', 'VAR-7', 1, 'VAR-8',
     '-event', 'N', 'VAR-7', 2, 0.5,
     '-event', 'M',  2.0, 1, 2, 'VAR-9',
     '-event', 'M',  2.0, 2, 1, 'VAR-9',
     '-event', 'M',  2.0, 1, 3, 'VAR-9',
     '-event', 'M',  2.0, 3, 1, 'VAR-9',
     '-event', 'M',  2.0, 2, 3, 'VAR-9',
     '-event', 'M',  2.0, 3, 2, 'VAR-9', feed=params)
assert simuls.ret == coalclonal.Program.SUCCESS, simuls.stderr

simuls = lib.run('-nr', 2, '-np', 2, '-nl', 1, '-ns', 1, 10,
    '-M', 'VAR-1', '-Mm', 'x', 'VAR-2', 'VAR-2', 'x', '-Mp', 1, 2, 'VAR-3',
    '-N', 2, 'VAR-4', '-G', 1, 'VAR-5', '-S', 1, 'VAR-6', '-S', 2, 'VAR-6',
    '-s', 1, 'VAR-7', '-rho', 1, 'VAR-8', '-rec', '.', 'VAR-9',
    '-TPMparams', 1, 'VAR-10', 'VAR-11', '-theta', 1, 'VAR-12',
    '-K', 1, 4, '-trans', 1, 'x', 'VAR-13', 1, 1, 'VAR-13', 'x', 1, 1, 1, 1, 'x', 'VAR-14', 1, 1, 'VAR-14', 'x',
    '-ls', 1, 10, '-sitew', 1, 1, 'VAR-15', 'VAR-15', 'VAR-15', 'VAR-16', 'VAR-16', 'VAR-15', 'VAR-15', 1, 1,
    '-event', 'N',      'VAR-17', 2, 'VAR-18',
    '-event', 'G',      'VAR-17', 2, 'VAR-19',
    '-event', 'S',      'VAR-17', 2, 'VAR-20',
    '-event', 's',      'VAR-17', 2, 'VAR-21',
    '-event', 'rho',    'VAR-17', 1, 'VAR-22',
    '-event', 'rec',    'VAR-17', 1, 'VAR-23',
    '-event', 'bot',    'VAR-17', 2, 'VAR-24',
    '-event', 'M',      'VAR-17', 1, 2, 'VAR-25',
    '-event', 'admixt', 'VAR-17', 2, 1, 'VAR-26',
        feed=zip(*[ (1.0, 1.4), #VAR-1
                    (0.5, 0.3), #VAR-2
                    (1.7, 2.1), #VAR-3
                    (1.5, 0.9), #VAR-4
                    (0.1, 0.3), #VAR-5
                    (0.98, 0.88), #VAR-6
                    (0.78, 0.92), #VAR-7
                    (0.8, 0.7), #VAR-8
                    (0.12, 0.32), #VAR-9
                    (0.1, 0.9), #VAR-10
                    (0.5, 0.66), #VAR-11
                    (4.2, 2.6), #VAR-12
                    (1.2, 1.44), #VAR-13
                    (2.1, 1.7), #VAR-14
                    (1.5, 1.7), #VAR-15
                    (4.2, 6.2), #VAR-16
                    (0.8, 0.6), #VAR-17
                    (0.2, 0.36), #VAR-18
                    (5.2, 1.1), #VAR-19
                    (0.9, 1.6), #VAR-20
                    (0.98, 0.99), #VAR-21
                    (3.4, 1.8), #VAR-22
                    (0.7, 0.4), #VAR-23
                    (4.4, 0.2), #VAR-24
                    (1.0, 0.7), #VAR-25
                    (0.5, 0.9)]))  #VAR-26
assert simuls.ret == coalclonal.Program.SUCCESS, simuls.stderr
