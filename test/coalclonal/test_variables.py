import math
from egglib_coalclonal import coalclonal
from matplotlib import pyplot
import lib

print('# Test variable arguments')
nrep = 100

if False:
    lib.show('    - var theta/N')
    theta_N = [(0.0, 1.0), (0.2, 1.0), (0.4, 1.0), (0.6, 1.0), (0.8, 1.0),
        (1.0, 1.0), (1.2, 1.0), (1.4, 1.0), (1.6, 1.0), (1.8, 1.0),
        (0.0, 3.0), (0.2, 3.0), (0.4, 3.0), (0.6, 3.0), (0.8, 3.0),
        (1.0, 3.0), (1.2, 3.0), (1.4, 3.0), (1.6, 3.0), (1.8, 3.0),
        (0.0, 5.0), (0.2, 5.0), (0.4, 5.0), (0.6, 5.0), (0.8, 5.0),
        (1.0, 5.0), (1.2, 5.0), (1.4, 5.0), (1.6, 5.0), (1.8, 5.0)]

    sim = lib.run('-nr', len(theta_N), '-nl', nrep, '-theta', 1, 'VAR-1',
        '-np', 1, '-ns', 1, 40, '-N', 1, 'VAR-2',
        '-sexual', 1, '-global-stats', 'thetaW', 'Pi', var=theta_N)
    assert sim.ret == coalclonal.Program.SUCCESS, sim.stderr

    theta = [i for i,j in theta_N[:10]]
    N1 = theta_N[0][1]
    N2 = theta_N[10][1]
    N3 = theta_N[20][1]
    thetaW_1 = sim.stats['thetaW'][:10]
    thetaW_2 = sim.stats['thetaW'][10:20]
    thetaW_3 = sim.stats['thetaW'][20:30]
    Pi_1 = sim.stats['Pi'][:10]
    Pi_2 = sim.stats['Pi'][10:20]
    Pi_3 = sim.stats['Pi'][20:30]

    fname = lib.path('pic8-1-var-theta-N.png')
    pyplot.plot(theta, thetaW_1, 'ro:', mfc='None', mec='r')
    pyplot.plot(theta, thetaW_2, 'go:', mfc='None', mec='g')
    pyplot.plot(theta, thetaW_3, 'bo:', mfc='None', mec='b')
    pyplot.plot(theta, Pi_1, 'rs:', mfc='None', mec='r')
    pyplot.plot(theta, Pi_2, 'gs:', mfc='None', mec='g')
    pyplot.plot(theta, Pi_3, 'bs:', mfc='None', mec='b')
    pyplot.plot([],[], 'r:', label='N={0}'.format(N1))
    pyplot.plot([],[], 'g:', label='N={0}'.format(N2))
    pyplot.plot([],[], 'b:', label='N={0}'.format(N3))
    pyplot.plot([],[], 'ko', label='thetaW')
    pyplot.plot([],[], 'ks', label='Pi')
    pyplot.plot(theta, [i*N1 for i in theta], 'r-')
    pyplot.plot(theta, [i*N2 for i in theta], 'g-')
    pyplot.plot(theta, [i*N3 for i in theta], 'b-')
    pyplot.plot([], [], 'k-', label='expectations')
    pyplot.legend()
    pyplot.title('thetaW/Pi vs. theta and N using VAR')
    pyplot.xlabel('theta')
    pyplot.ylabel('thetaW/Pi')
    pyplot.savefig(fname)
    pyplot.clf()
    lib.show('\n        -> {0}\n'.format(fname))


if False:
    lib.show('    - var Ni/T')
    nrep = 500
    N_T = [(0.2, 0.5), (0.6, 0.5), (1.0, 0.5), (1.4, 0.5), (1.8, 0.5),
           (2.2, 0.5), (2.6, 0.5), (3.0, 0.5), (3.4, 0.5), (3.8, 0.5),
           (0.2, 2.0), (0.6, 2.0), (1.0, 2.0), (1.4, 2.0), (1.8, 2.0),
           (2.2, 2.0), (2.6, 2.0), (3.0, 2.0), (3.4, 2.0), (3.8, 2.0)]
    sim = lib.run('-nr', len(N_T), '-nl', nrep, '-theta', 1, 4.0,
    '-np', 3, '-ns', '.', 20, '-N', 1, 'VAR-1', '-N', 2, 1, '-N', 3, 'VAR-1',
    '-event', 'admixt', 'VAR-2', 2, 1, 1.0, '-event', 'admixt', 'VAR-2', 3, 1, 1.0,
    '-sexual', 1, '-sexual', 2, '-sexual', 3,
    '-global-stats', 'thetaW:1', 'thetaW:2', 'thetaW:3', 'WCst', var=N_T)
    assert sim.ret == coalclonal.Program.SUCCESS, sim.stderr
    N = [i for i,j in N_T[:10]]
    T1 = N_T[0][1]
    T2 = N_T[10][1]
    thetaW1a = sim.stats['thetaW<1>'][:10]
    thetaW2a = sim.stats['thetaW<2>'][:10]
    thetaW3a = sim.stats['thetaW<3>'][:10]
    thetaW1b = sim.stats['thetaW<1>'][10:]
    thetaW2b = sim.stats['thetaW<2>'][10:]
    thetaW3b = sim.stats['thetaW<3>'][10:]
    Fst_a = sim.stats['WCst'][:10]
    Fst_b = sim.stats['WCst'][10:]

    fname = lib.path('pic8-2-var-N-T-thetaW.png')
    pyplot.plot(N, thetaW1a, 'ro:', mfc='None', mec='r')
    pyplot.plot(N, thetaW2a, 'go:', mfc='None', mec='g')
    pyplot.plot(N, thetaW3a, 'bo:', mfc='None', mec='b')
    pyplot.plot(N, thetaW1b, 'rs:', mfc='None', mec='r')
    pyplot.plot(N, thetaW2b, 'gs:', mfc='None', mec='g')
    pyplot.plot(N, thetaW3b, 'bs:', mfc='None', mec='b')
    pyplot.plot([],[], 'r:', label='pop 1')
    pyplot.plot([],[], 'g:', label='pop 2')
    pyplot.plot([],[], 'b:', label='pop 3')
    pyplot.plot([],[], 'ko', label='T={0}'.format(T1))
    pyplot.plot([],[], 'ks', label='T={0}'.format(T2))
    pyplot.legend()
    pyplot.title('thetaW vs. N using VAR')
    pyplot.xlabel('N (pops 1 and 2)')
    pyplot.ylabel('thetaW per pop')
    pyplot.savefig(fname)
    pyplot.clf()
    lib.show('\n        -> {0}\n'.format(fname))

    fname = lib.path('pic8-3-var-N-T-Fst.png')
    pyplot.plot(N, Fst_a, 'ro:', mfc='None', mec='r', label='T={0}'.format(T1))
    pyplot.plot(N, Fst_b, 'bo:', mfc='None', mec='b', label='T={0}'.format(T2))
    pyplot.legend()
    pyplot.title('Fst vs. N using VAR')
    pyplot.xlabel('N (pops 1 and 2)')
    pyplot.ylabel('Fst')
    pyplot.savefig(fname)
    pyplot.clf()
    lib.show('        -> {0}\n'.format(fname))


if False:
    lib.show('    - var M')
    nrep = 100
    M = [(0.05,), (0.1,), (0.2,), (0.4,), (0.8,), (1.6,), (3.2,), (6.4,), (8.0,), (10.0,)]
    Fst12 = [[] for i in M]
    Fst13 = [[] for i in M]
    Fst23 = [[] for i in M]
    Fst = [[] for i in M]
    for i in range(nrep):
        if (i+1)%10==0: lib.show(' ' + str(i+1))
        sim = lib.run('-nr', len(M), '-nl', 1, '-theta', 1, 4.0,
        '-np', 3, '-ns', '.', 20,
        '-Mm', None, 'VAR-1', 1, 'VAR-1', None, 1, 1, 1, None,
        '-sexual', 1, '-sexual', 2, '-sexual', 3,
        '-global-stats', 'WCst:1,2', 'WCst:1,3', 'WCst:2,3', 'WCst', var=M)
        assert sim.ret == coalclonal.Program.SUCCESS, sim.stderr
        [i.append(j) for i,j in zip(Fst12, sim.stats['WCst<1,2>'])]
        [i.append(j) for i,j in zip(Fst13, sim.stats['WCst<1,3>'])]
        [i.append(j) for i,j in zip(Fst23, sim.stats['WCst<2,3>'])]
        [i.append(j) for i,j in zip(Fst, sim.stats['WCst'])]
    Fst12 = [1.0*sum(i)/len(i) for i in Fst12]
    Fst13 = [1.0*sum(i)/len(i) for i in Fst13]
    Fst23 = [1.0*sum(i)/len(i) for i in Fst23]
    Fst = [1.0*sum(i)/len(i) for i in Fst]

    M = map(math.log10, [i for i, in M])
    fname = lib.path('pic8-4-var-M.png')
    pyplot.plot(M, Fst12, 'ro:', mfc='None', mec='r', label='Fst: 1-2')
    pyplot.plot(M, Fst13, 'go:', mfc='None', mec='g', label='Fst: 1-3')
    pyplot.plot(M, Fst23, 'bo:', mfc='None', mec='b', label='Fst: 2-3')
    pyplot.plot(M, Fst, 'ko:', mfc='None', mec='k', label='Fst')
    pyplot.legend()
    pyplot.title('Fst vs. M using VAR')
    pyplot.xlabel('M (between pop 1 ent 2)')
    pyplot.ylabel('Fst')
    pos = [0.03, 0.1, 0.3, 1, 3, 10]
    pyplot.xticks(map(math.log10, pos), pos)
    pyplot.savefig(fname)
    pyplot.clf()
    lib.show('\n        -> {0}\n'.format(fname))



if True:
    lib.show('    - var bot')
    nrep = 100
    var = [(0.0, 0.1), (0.01, 0.1), (0.02, 0.1), (0.05, 0.1), (0.1, 0.1),
           (0.2, 0.1), (0.3, 0.1), (0.5, 0.1), (0.7, 0.1), (1.0, 0.1),
           (0.0, 0.8), (0.01, 0.8), (0.02, 0.8), (0.05, 0.8), (0.1, 0.8),
           (0.2, 0.8), (0.3, 0.8), (0.5, 0.8), (0.7, 0.8), (1.0, 0.8),
           (0.0, 0.1), (0.01, 10.0), (0.02, 10.0), (0.05, 10.0), (0.1, 10.0),
           (0.2, 10.0), (0.3, 10.0), (0.5, 10.0), (0.7, 10.0), (1.0, 10.0)]
    D1 = [[] for i in xrange(10)]
    D2 = [[] for i in xrange(10)]
    D3 = [[] for i in xrange(10)]
    for i in range(nrep):
        if (i+1)%10==0: lib.show(' ' + str(i+1))
        sim = lib.run('-nr', len(var), '-nl', 1, '-theta', 1, 4.0,
        '-np', 1, '-ns', '.', 40,
        '-event', 'bot', 'VAR-1', 1, 'VAR-2',
        '-sexual', 1, '-global-stats', 'D', var=var)
        assert sim.ret == coalclonal.Program.SUCCESS, sim.stderr
        [i.append(j) for i,j in zip(D1, sim.stats['D'][:10]) if j is not None]
        [i.append(j) for i,j in zip(D2, sim.stats['D'][10:20]) if j is not None]
        [i.append(j) for i,j in zip(D3, sim.stats['D'][20:]) if j is not None]
    D1 = [1.0*sum(i)/len(i) for i in D1]
    D2 = [1.0*sum(i)/len(i) for i in D2]
    D3 = [1.0*sum(i)/len(i) for i in D3]

    T = [i for i,j in var[:10]]
    S1 = var[0][1]
    S2 = var[10][1]
    S3 = var[20][1]
    fname = lib.path('pic8-5-var-bottleneck.png')
    pyplot.plot(T, D1, 'yo:', mfc='None', mec='y', label='S={0}'.format(S1))
    pyplot.plot(T, D2, 'mo:', mfc='None', mec='m', label='S={0}'.format(S2))
    pyplot.plot(T, D3, 'ro:', mfc='None', mec='r', label='S={0}'.format(S3))
    pyplot.legend()
    pyplot.title('Tajima\'s vs. bottleneck params using VAR')
    pyplot.xlabel('T')
    pyplot.ylabel('D')
    pyplot.axhline(0, c='k', ls='-')
    pyplot.savefig(fname)
    pyplot.clf()
    lib.show('\n        -> {0}\n'.format(fname))

