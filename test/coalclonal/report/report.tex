\documentclass{article}
\usepackage{graphicx}
\usepackage{latexsym}
\usepackage{wasysym}

\newcommand{\cpp}[1]{{\color{blue}{\texttt{#1}}}}

\begin{document}

\title{Coalclonal testing}
\author{St\'ephane De Mita}
\date{\today}

\maketitle
\tableofcontents
\newpage


\section{Effect of basic parameters}
\begin{center}
    \includegraphics[width=1\textwidth,keepaspectratio=true]{../pic/pic1-1-theta.png}
\end{center}

\noindent $S$, $\hat{\theta}_W$, and $\pi$ against simulated $\theta$ in the standard neutral
model. The three statistics are compared to their theoretical expectation.

\begin{center}
    \includegraphics[width=1\textwidth,keepaspectratio=true]{../pic/pic1-2-G.png}
\end{center}

\noindent $\hat{\theta}_W$ and Tajima's $D$ against the exponential growth/decline
rate. The horizontal lines give $D$ = 0 (solid) and 1 (dotted). Values are
compared with equivalent simulations using Hudson's \verb|ms|.

\begin{center}
    \includegraphics[width=1\textwidth,keepaspectratio=true]{../pic/pic1-3-s.png}
\end{center}

\noindent Effect of the self-fertilization rate. $\hat{\theta}_W$ and $\pi$
are compared with their expectation based on $\theta$ and the population
size rescaling due to $s$, and $D$ is compared with $D$ = 0 (solid horizontal
line).

\section{Population structure}

\begin{center}
    \includegraphics[width=1\textwidth,keepaspectratio=true]{../pic/pic2-1-M-K=2.png}
\end{center}

\begin{center}
    \includegraphics[width=1\textwidth,keepaspectratio=true]{../pic/pic2-2-M-K=4.png}
\end{center}

\begin{center}
    \includegraphics[width=1\textwidth,keepaspectratio=true]{../pic/pic2-3-M-K=10.png}
\end{center}

\noindent Weir and Cockerham's $\hat{\theta}$ against the $M$ parameter. Comparison of
\verb|coalclonal| and \verb|ms| with equivalent parameters. The theoretical
expectation assumes an infinite number of demes and shows significant
deviation with the number of deme is small.

\begin{center}
    \includegraphics[width=1\textwidth,keepaspectratio=true]{../pic/pic2-4-Mm.png}
\end{center}

\noindent Structure with three populations 1, 2, and 3 and a custom migration
matrix. 1 and 2 are strongly connected, 1 and 3 much less and 2 and 3 not
at all (but they are through 1). $\hat{\theta}_{1,2}$ is the lowest and
$\hat{\theta}|{2,3}$ the highest. $\hat{\theta}_{2,3}$ is not much higher
than $\hat{\theta}_{1,3}$ probably because 1 and 2 are very similar,
with respect to 3, due
to their high exchange rate (1 $\leftrightarrow$ 2 $\gg$ 2 $\leftrightarrow$ 3).

\begin{center}
    \includegraphics[width=1\textwidth,keepaspectratio=true]{../pic/pic2-5-Mp.png}
\end{center}

\noindent A slightly simpler structure set using the \verb|-Mp| option.

\begin{center}
    \includegraphics[width=1\textwidth,keepaspectratio=true]{../pic/pic2-6-Mp-assym.png}
\end{center}

\noindent Consequences of assymetrical gene flow. There are two populations:
the first is large and the second is small. The expected (if populations
were isolated) diversity is shown by horizontal lines (dashed line for the
big population and dotted line for the small one). There are three modalities.

\begin{enumerate}
    \item In red, the case when migration is bidirectional. With low migration,
        the diversity in the big population is increased due to occasional samples
        than may go to the small populations and stay there long time. With increasing
        migration, this effect diminishes (samples go to the small population but
        don't stay thee long). It can be expected that the diversity goes even
        lower than the expected $\pi$ because pairs of samples that go to the
        other population would coalesce faster there but this effect might be
        compensated by the time needed for migration.

        In the small population, the diversity is increased both single samples
        and pairs of samples sent to the big population increase the overall
        within-population diversity. It seems the later effect is dominant because
        the increase is positively correlated to migration rate.
    \item In green, migration only from the big to the small population
        (thinking backwards). The small population is like it was isolated
        (its samples can never migrate), so its diversity is not affected by
        $M$ and equal to the expected value. The big population has an inflated
        diversity with low $M$ (due to samples that go to the other population
        and cause a longer time to the MRCA because the final coalescence must
        happen there, required at least two migrations). With increasing migration,
        more samples from the big population go to the small one and experience
        higher coalescence rate there, causing diminished diversity in the
        big population (because actually many of its samples come from the
        small population).
    \item In blue, migration only from the small to the big population. In
        this case the big population is as it would be if completely isolated.
        The small population has consistently an inflated diversity because
        its individuals may originate from the bigger population and have
        larger coalescent times there (with big $M$) or because of long
        coalescent times due to small migration rates (small $M$).
\end{enumerate}

\section{Sex and recombination}

\begin{center}
    \includegraphics[width=1\textwidth,keepaspectratio=true]{../pic/pic3-1-S.png}
\end{center}

\noindent Effect of the sexual rate (in quasi-clonal models) on $F_{IS}$,
which increases as expected but still far from the purely sexual level (0).
$S$ = 4 is still a very small value.

\begin{center}
    \includegraphics[width=1\textwidth,keepaspectratio=true]{../pic/pic3-2-S.png}
\end{center}

\noindent Effect of $S$ on within-locus and between-locus linkage disequilibrium
measures $\bar{r}_D$ and $ZnS$. Local LD is always higher than between-locus
and LD is always higher in a clonal model than in a sexual one. LD decreased
with increasing $S$ expect with very small $S$ where it decreases.

\begin{center}
    \includegraphics[width=1\textwidth,keepaspectratio=true]{../pic/pic3-3-S.png}
\end{center}

\noindent Effect of $S$ on the minimum number of recombination events (in
the absence of recombination. Segregation between loci caused an increase of
Rmin with increasing sexual rate while within-locus Rmin is always 0.

\begin{center}
    \includegraphics[width=1\textwidth,keepaspectratio=true]{../pic/pic4-1-rho.png}
\end{center}

\begin{center}
    \includegraphics[width=1\textwidth,keepaspectratio=true]{../pic/pic4-2-rec.png}
\end{center}

\noindent The two recombination parameters have a positive effect on the
minimum number of recombination events. Note that each one applies in
a different context: one (the coalescent-scaled rate) for the purely sexual
model, and the other (the probability) for the quasi-clonal case.

\section{Mutation parameters}

\begin{center}
    \includegraphics[width=1\textwidth,keepaspectratio=true]{../pic/pic5-1-nmut.png}
\end{center}

\noindent The number of polymorphic sites (\verb|S|) increases with the
number of mutations (\verb|nmut|) but there is a saturation effect with
smaller values of \verb|ls| due to homoplasy.

\begin{center}
    \includegraphics[width=1\textwidth,keepaspectratio=true]{../pic/pic5-2-sitew.png}
\end{center}

\noindent Unbalanced weights (that is, if few sites have a bigger weight)
increase homoplasy and therefore decrease the number of polymorphic sites.
The value of 0 is a control: all sites have the same weight (but there is
still homoplasy because the number of sites is still finite).

\begin{center}
    \includegraphics[width=1\textwidth,keepaspectratio=true]{../pic/pic5-3-rho.png}
\end{center}

\noindent This graphic shows the absence of response of $S$ to the recombination
rate, regardless of whether we set $\theta$ or the number of mutations, or we use ISM or FSM
(even with homoplasy), or we set the sites to a restricted area of the recombinating
segment.

\begin{center}
    \includegraphics[width=1\textwidth,keepaspectratio=true]{../pic/pic5-4-rho.png}
\end{center}

\noindent There isn't, either, any response to $\rho$ of $\hat{\theta}_W$ or $D$,
regardless of wheter we set $\theta$ or the number of mutations.

\begin{center}
    \includegraphics[width=1\textwidth,keepaspectratio=true]{../pic/pic5-5-sitep-rho0.png}
\end{center}

\noindent This is the response of Rmin to homoplasy only (no recombination).
Rmin is much larger with K=2 (the default), then decreased with K=4 and
further with K=8 because homoplasy decreases. With much less mutations,
homoplasy decreases accordingly (such as there is no visible difference
between K=4 and 8). In contrast, there is no response to the restriction
of sites, as expected (no recombination).

\begin{center}
    \includegraphics[width=1\textwidth,keepaspectratio=true]{../pic/pic5-6-sitep-rho0.png}
\end{center}

\noindent Same as previous, but for $S$. With less mutations we have less polymorphic
sites and K has no visible effect. But with more mutations there is a slight
increase of the number of polymorphic sites with larger values of K.
This is because homoplasy is less visible. No response to the restriction
of sites as expected.

\begin{center}
    \includegraphics[width=1\textwidth,keepaspectratio=true]{../pic/pic5-7-sitep-recomb.png}
\end{center}

\begin{center}
    \includegraphics[width=1\textwidth,keepaspectratio=true]{../pic/pic5-8-sitep-recomb.png}
\end{center}

\noindent These two graphics repeat the two previous, but with non-null recombination
($\rho=4$). The former shows that site restriction (when site are restricted
to a smaller interval of the full recombining segment) decreases Rmin
(because the effective recombination is decreases). Both homoplasy and
recombination increase Rmin but we don't investigate this quantitatively.
The latter shows still no effect in S, as expected.

\begin{center}
    \includegraphics[width=1\textwidth,keepaspectratio=true]{../pic/pic6-1-K.png}
\end{center}

\noindent This graphic shows again (and better) that more alleles decrease Rmin
because they decrease homoplasy, especially if there are few sites (and
therefore much homoplasy initially). No effect with ISM because no
homoplasy.

\begin{center}
    \includegraphics[width=1\textwidth,keepaspectratio=true]{../pic/pic6-2-trans.png}
\end{center}

\noindent Here we show that transition bias also increases homoplasy.

\begin{center}
    \includegraphics[width=1\textwidth,keepaspectratio=true]{../pic/pic6-3-trans.png}
\end{center}

\noindent The transition bias we implement a bias towards C and G. The bigger the
bias, the bigger GC percentage. The bias is worsened by the number of
mutations until the equilibrium is reached. By starting with the first
allele only (0=A), there is a tendency toward lower GC percentage.

\section{Mutation models}

\begin{center}
    \includegraphics[width=1\textwidth,keepaspectratio=true]{../pic/pic6-4-IAM-SMM.png}
\end{center}

\noindent This graphic shows the behaviour of $\theta$ estimators according to the
simulation models. Only 2V (simulated under the stepwise mutation model)
seems to be reliable.

\begin{center}
    \includegraphics[width=1\textwidth,keepaspectratio=true]{../pic/pic6-5-TPM.png}
\end{center}

\noindent In the TPM models, the allelic variance is increased proportionnaly to
both the probability and the shape parameter.

\section{Historical events}

\begin{center}
    \includegraphics[width=1\textwidth,keepaspectratio=true]{../pic/pic7-1-eN.png}
\end{center}

\noindent In this model there is a population size change at some point in the
recent past (T=0.2). Tajima's $D$ is decreased in case of a population
reduction and conversely (thinking backwise).

\begin{center}
    \includegraphics[width=1\textwidth,keepaspectratio=true]{../pic/pic7-2-eN.png}
\end{center}

\noindent Here $\theta$ is set to 5, and the current population size is
the default (1) so the excepted value (without historical change)
of $\pi$ is 5. For the blue line,
the ancestral size is 5 (so $\pi$ should be 25) and for the blue line
the ancestral size is 0.2 (so $\pi$ should be 1). The dotted lines show
these expected values. If the size change is immediate, it is like the
ancestral size was the current one. And progressively, $\pi$ converge to
the current/starting value (value at T=0, therefore 5) as the date increases
(meaning that, in the end, the historical event is too old to have any
effect).

\begin{center}
    \includegraphics[width=1\textwidth,keepaspectratio=true]{../pic/pic7-3-eG.png}
\end{center}

\noindent The effect of the exponential growth rate $G$ on Tajima's $D$
is in black: $D$ is larger than 0 for negative values (exponential decrease
when we go backward in time) and conversely. This curve is compared with
a delayed event where $G$ is set to the same value at time 0.3. The curve
with the delayed event is similar to the one with the constant parameter
except that it is attenuated. The slightly negative $D$ at the equilibrium
is expected.

\begin{center}
    \includegraphics[width=1\textwidth,keepaspectratio=true]{../pic/pic7-4-eS-Fis.png}
\end{center}

\noindent This graphic examinates the effect of the ancestral clonality rate on $F_{IS}$.
Remember that the parameter is actually the sexual rate (0 is totally clonal).
The red curve shows the response of $F_{IS}$ on the ancestral sexual rate
with a change from 0 to $S_a$ at time 0.5. Obviously, the smaller the sexual rate,
the more negative $F_{IS}$ goes. The square point is equivalent to an
infinitely large ancestral sexual rate (the model actually switches to
purely sexual. This is the top limit of $F_{IS}$ knowning that we have
the most recent period of time ($T-0.5$) under a strictly clonal regime.

The blue curve is the reverse: the population is initially sexual and
switches to a partially clonal regime with the given sexual rate at time
0.5. The square point is purely sexual all along. In either case $F_{IS}$
is always 0 because this period of time is far more than enough to restore
gametic equilibrium.

\begin{center}
    \includegraphics[width=1\textwidth,keepaspectratio=true]{../pic/pic7-5-eS-rD.png}
\end{center}

\noindent This is based on the same simulations as above, but represents
$\bar{r_D}$. There are three separate (unlinked) loci so only clonal
reproduction caused linkage, and this linkage doesn't disappear after
switching to the clonal regime (for the blue line; this time speaking
forward in time). So higher values of $\bar{r_D}$ are found for smaller
values of the ancestral sexual rate. The purely sexual control has the
smallest (although non-zero, probably because the statistic is not
centered on zero) value. The red curve (where the regime switches,
speaking backward, from partially clonal to sexual) shows little or no
tendency, because the latest clonal period of time is enough to general
linkage.

\begin{center}
    \includegraphics[width=1\textwidth,keepaspectratio=true]{../pic/pic7-6-es.png}
\end{center}

\begin{center}
    \includegraphics[width=1\textwidth,keepaspectratio=true]{../pic/pic7-7-es-D.png}
\end{center}

\noindent These two graphs show the effect of changing the selfing rate in a
sexual population. The population starts with a selfing rate of 0.1
(blue curve) or 0.9 (red curve) and switches to 0.9 or 0.1 (respectively)
at a time given by $T$. Three statistics are represented: $\hat{\theta}_W$
and $\pi$ on the first graph, and $D$ on the second one. If $T$ is 0, it means
that the ancestral rate is the actual one (s=0.9 for the blue line and
s=0.1 for the red line), so the blue line starts with low diversity (high
selfing) and the red line starts with high diversity. As the time to the
event increased, they swap and converge to the opposite equilibrium when
the event is too old to have an effect. As controls, the graphics include
permanently allogame/autogame populations (without change, or with a
change to the same regime). Also, $D$ is transiently positive for the
red curve (decrease of selfing rate: population size increase, thinking
backward) and negative for the blue curve (population size decrease, or
bottleneck without recovery.

\begin{center}
    \includegraphics[width=1\textwidth,keepaspectratio=true]{../pic/pic7-8-eM-T.png}
\end{center}

\noindent There are two populations. For the blue line, $M$ starts with
0.1 and is changed to 5.0 in the past. For the blue line, it is opposite.
Therefore for the blue line, the time to the event is correlated with more
ancient high migration (bigger $F_{ST}$) and for the red line, the time
to the event is correlated with more ancient low migration (lower $F_{ST}$).
The blue square gives the $F_{ST}$ with $M=5.0$ and the red square with
$M=0.1$ (the squares are supported by a horizontal line).

\begin{center}
    \includegraphics[width=1\textwidth,keepaspectratio=true]{../pic/pic7-9-eM-Ma.png}
\end{center}

\noindent There are four populations. The starting $M$ is 1 and it is changed
to different values at $T=0.2$. The control (red square and dotted line)
is a simulation without change.

\begin{center}
    \includegraphics[width=1\textwidth,keepaspectratio=true]{../pic/pic7-10-erecomb-T.png}
\end{center}

\noindent This graph shows Rmin (which is a function of recombination only
in this model) as a function of the time to a change of the recombination
rate/probability. For the curves at 0, there is an additional, arbitrary
point that allows to identify them all.

\begin{itemize}
    \item[$\Box$] Constant $\rho$ fixed to 4. In the sexual case
    (red) there is recombination (Rmin around 1.6) but in the sexual
    case (blue) no recombination (Rmin fixed to 0) which is expected
    because $\rho$ has an effect only in the purely sexual regime. Rmin
    must be constant.
    \item[$\Circle$] Constant \verb|rec| fixed to 0.8. Here there is
    recombination only in the mostly clonal case ($S=0.8$) (blue) and
    none in the sexual case (red). Rmin is constant.
    \item[$\triangle$] $\rho$ is changed from 4 to 0 (there is only
    recent recombination. In the clonal case, there is no effect (blue).
    If the change date is recent, no recombination occurs, and Rmin
    converge with constant $\rho$ as the event is ancient enough.
    \item[$\nabla$] \verb|rec| is changed from 0.8 to 0. Analogous to
    the previous except that no recombination occurs in the sexual case
    (red).
    \item[$\rhd$] $\rho$ is changed from 0 to 4 (there is only ancient
    recombination). No recombination altogether in the clonal case (blue).
    Identical to constant $\rho=4$ if $T=0$ and otherwise the amount of
    recombination decreases when the date of the event is too ancient to
    have caused meaningfull (\textit{ie} recent) recombination.
    \item[$\lhd$] \verb|rec| is changed from 0 to 0.8. Analogous to the
    previous except that only partially clonal case is affected. In red,
    there is no recombination at all.

\end{itemize}

\begin{center}
    \includegraphics[width=1\textwidth,keepaspectratio=true]{../pic/pic7-11-switch-Fis.png}
\end{center}

\noindent The graph shows the effect of switch from purely clonal to
sexual case (red curve).  At $T=0$, $F_{is}$ is like under
purely sexual (0), and then the excess of heterozygote increased ($F_{IS}$
decreases below 0) due to the increasing period of time under clonal
reproduction while the ancestral sexual phase allows the ultimate
coalescent event. The blue curve show the switch from purely sexual
to quasi clonal. In that case, the start point of the simulation is therefore
always sexual which means individuals are reshuffled and $F_{IS}$ is cancelled.
This means that even one generation of sex (\textit{it} an infinitely small
amount of time on the coalescent time) is sufficient to cancel the genotypic
association due to clonality. The two squares show the controls without
any switch: $F_{IS}=0$ in the sexual case, and negative in the clonal case.

\begin{center}
    \includegraphics[width=1\textwidth,keepaspectratio=true]{../pic/pic7-12-switch-rD.png}
\end{center}

\noindent The same, but with $\bar{R}_d$ (there have been several loci
all the way). If population is sexual, $\bar{R}_d$ is minimal. If population
switches immediately to sexual (start of red curve) it is the same, and
if the population has been clonal before, $\bar{R}_d$ increases
dramatically due to recent clonality. Then there is a decrease of the
switch is old (maybe a side effect but I'm not sure). Conversely, a
clonal population has a higher $\bar{R}_d$ (filled square). If a sexual
population switches to asexual (blue curve), there is a immediate decrease
of $\bar{R}_d$ probably due to the reshuffling of genotypes due to the
infinitesimal sexual phase. Then, older changes to asexual phase don't change much
but tend to decrease $\bar{R}_d$ (strong effect of clonality on between-locus
linkage).

\begin{center}
    \includegraphics[width=1\textwidth,keepaspectratio=true]{../pic/pic7-13-admixt-T.png}
\end{center}

\noindent In this simulation, there are two isolated populations. At
$T=1.5$, the pairwise migration rate is set to 5. The dotted line shows
the $F_{ST}$ if no additional event is set. The curves give the $F_{ST}$
as a function of the date of an admixture event between the two
populations with different instant migration probability. With $P=0$ 
(blue curve) the event has no effect, and with increasing probability
the event has a bigger effect (reduced $F_{ST}$). With $P=1$ and $T=0$,
the limit, all samples are taken to the other population immediately,
allowing no differentiation between populations. When $T$ increases,
the event is more ancient and has less effects.

\begin{center}
    \includegraphics[width=1\textwidth,keepaspectratio=true]{../pic/pic7-14-bot-D.png}
\end{center}

\begin{center}
    \includegraphics[width=1\textwidth,keepaspectratio=true]{../pic/pic7-15-bot-thetaW.png}
\end{center}

\noindent Effect of bottleneck in a single population. Very recent bottlenecks
cause a positive $D$ and strong decrease of $\hat{\theta}_W$ except for
exceedingly strong bottlenecks ($S=10$) which are always complete. For
$S=10$ and $T=0$, $D$ is actually not computable because there is no
polymorphism at all. When $T$ increases, the diversity increases
steadily and $D$ passes through a negative phase (more marked for stronger
bottlenecks). When $T$ increases further, both statistics go back to
the normal values as the effects of the bottlenecks vanish. The blue
curve ($S=0$) serves as a control.

\begin{center}
    \includegraphics[width=1\textwidth,keepaspectratio=true]{../pic/pic7-16-delayed-T.png}
\end{center}

\noindent Red curve: In a single population, 20 individuals are sampled
immediately, and 12 other sampled with an increasing delay. $F_{ST}$
increases with sample delay.

Green curve: There are two populations without migration. In
pop 1, 20 individuals are sampled immediately. In pop 2, 12 individuals
are sampled at a date comprised between 0 and 1.6. At 1.6, the two
populations are merged. This time, we expect Fst to decrease with
increasing sampling date. The value at $T=1.6$ should be the same as the
last point of the red curve because it reflects 1.6 coalescent time of
drift in the same population.

Blue curve: Another variant of the previous. In the first
population, the sample is taked at 1.6. We expect that $F_{ST}$ goes to
0 when the two samples are at the same time.

\begin{center}
    \includegraphics[width=1\textwidth,keepaspectratio=true]{../pic/pic8-1-var-theta-N.png}
\end{center}

\noindent We replicate the graphic of $\hat{theta}_W$ and $\Pi$ against
$\theta$ but using VAR to specify the different values of $\theta$ and $N$.
There are 100 loci to make an average, and one replication per value.

\begin{center}
    \includegraphics[width=1\textwidth,keepaspectratio=true]{../pic/pic8-2-var-N-T-thetaW.png}
\end{center}

\begin{center}
    \includegraphics[width=1\textwidth,keepaspectratio=true]{../pic/pic8-3-var-N-T-Fst.png}
\end{center}

\noindent In a setting with three populations. We set the size of
populations 1 and 3 (population 2 has the default size of 1) using a
variable argument. Populations are merge at two simultaneous events with
a date set using another variable argument.

\begin{center}
    \includegraphics[width=1\textwidth,keepaspectratio=true]{../pic/pic8-4-var-M.png}
\end{center}

\noindent  With three populations, the migration rate between populations
1 and 2 (both directions) is set by the variable parameter (beween 0.05
and 10) and all other pairwise rates are set to 1. With increasing $M$,
the pairwise $F_{ST}$ between populations 1 and 2 decreased sharply, and
the other pairwise $F_{ST}$ much less, and the global $F_{ST}$ something
in between, due to side effect. True replicates are implemented here
(instead of using several loci).

\begin{center}
    \includegraphics[width=1\textwidth,keepaspectratio=true]{../pic/pic8-5-var-bottleneck.png}
\end{center}

\noindent A bottleneck in a single population. We reproduce the "D vs.
bottleneck date" shown earlier (in the events section) using variable
parameters (both time and strength), obtaining the same result.

\end{document}
