print "bazzaz"
import random

f = open('example1.fas', 'w')
for i in range(10):
    f.write('>sequence%s\n' %(str(i+1).rjust(2, '0')))
    r = random.uniform(0.2,0.8)
    L = random.randint(500,5000)
    for j in range(L):
        if random.random()<0.01:
            f.write(random.choice('NWKRSVHBDY'))
        else:
            if random.random()<r:
                if random.random()<0.5: f.write('A')
                else: f.write('T')
            else:
                if random.random()<0.5: f.write('C')
                else: f.write('G')
    f.write('\n')
    print i, r, L
f.close()
