# EXAMPLE 1.1

f = open('example1.fas')
for i in range(10):
    header = f.readline()
    header = header.strip()
    name = header[1:]
    sequence = f.readline()
    sequence = sequence.strip()
    cA = 0
    cC = 0
    cG = 0
    cT = 0
    cOthers = 0
    for j in sequence:
        if j=='A':
            cA += 1
        elif j=='C':
            cC += 1
        elif j=='G':
            cG += 1
        elif j=='T':
            cT += 1
        else:
            cOthers += 1
    print '{}: A={}, C={}, G={}, T={}, others={}'.format(name, cA, cC, cG, cT, cOthers)
f.close()



f = open('example1.fas')
for i in range(10):
    header = f.readline()
    header = header.strip()
    name = header[1:]
    sequence = f.readline()
    sequence = sequence.strip()
    cA = sequence.count('A')
    cC = sequence.count('C')
    cG = sequence.count('G')
    cT = sequence.count('T')
    cOthers = len(sequence) - (cA+cC+cG+cT)
    print '{}: A={}, C={}, G={}, T={}, others={}'.format(name, cA, cC, cG, cT, cOthers)
f.close()



# EXAMPLE 1.2

f = open('example1.fas')
outfile = open('example1.txt', 'w')
for i in range(10):
    header = f.readline()
    header = header.strip()
    name = header[1:]
    sequence = f.readline()
    sequence = sequence.strip()
    cA = sequence.count('A')
    cC = sequence.count('C')
    cG = sequence.count('G')
    cT = sequence.count('T')
    cOthers = len(sequence) - (cA+cC+cG+cT)
    outfile.write('{}: A={}, C={}, G={}, T={}, others={}\n'.format(name, cA, cC, cG, cT, cOthers))
f.close()
outfile.close()



