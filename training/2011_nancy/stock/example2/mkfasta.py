import egglib, random


ps = egglib.simul.CoalesceParamSet(40)
m = egglib.simul.CoalesceFiniteAlleleMutator(4.)
simuls = egglib.simul.coalesce(ps, m, 1000)
f = open('data1.txt', 'w')
for i in simuls:
    pol = i.polymorphism()
    results = ['D={}'.format(pol['D']),
               'S={}'.format(pol['S']),
               'thetaW={}'.format(pol['thetaW']*pol['S']/1000),
               'Pi={}'.format(pol['Pi']*pol['S']/1000),
               'K={}'.format(pol['K']),
               'He={}'.format(pol['He'])]
    random.shuffle(results)
    f.write(' '.join(results) + '\n')
f.close()




ps = egglib.simul.CoalesceParamSet(40)
m = egglib.simul.CoalesceFiniteAlleleMutator(1.)
simuls = egglib.simul.coalesce(ps, m, 1000)
f = open('data2.txt', 'w')
for i in simuls:
    pol = i.polymorphism()
    if 'thetaW' not in pol: pol['thetaW'] = 0
    if 'Pi' not in pol: pol['Pi'] = 0
    results = ['S={}'.format(pol['S']),
               'thetaW={}'.format(pol['thetaW']*pol['S']/1000),
               'Pi={}'.format(pol['Pi']*pol['S']/1000),
               'K={}'.format(pol['K']),
               'He={}'.format(pol['He'])]
    if 'D' in pol: results.append('D={}'.format(pol['D']))
    random.shuffle(results)
    f.write(' '.join(results) + '\n')
f.close()



ps = egglib.simul.CoalesceParamSet(40)
ps.bottleneck(0.5, 0.4)
m = egglib.simul.CoalesceFiniteAlleleMutator(3.)
simuls = egglib.simul.coalesce(ps, m, 1000)
f = open('data3.txt', 'w')
for i in simuls:
    pol = i.polymorphism()
    if 'thetaW' not in pol: pol['thetaW'] = 0
    if 'Pi' not in pol: pol['Pi'] = 0
    results = ['S={}'.format(pol['S']),
               'thetaW={}'.format(pol['thetaW']*pol['S']/1000),
               'Pi={}'.format(pol['Pi']*pol['S']/1000),
               'K={}'.format(pol['K']),
               'He={}'.format(pol['He'])]
    if 'D' in pol: results.append('D={}'.format(pol['D']))
    random.shuffle(results)
    f.write(' '.join(results) + '\n')
f.close()





ps = egglib.simul.CoalesceParamSet([20,20], M=0.2)
m = egglib.simul.CoalesceFiniteAlleleMutator(3.)
simuls = egglib.simul.coalesce(ps, m, 1000)
f = open('data4.txt', 'w')
for i in simuls:
    pol = i.polymorphism()
    if 'thetaW' not in pol: pol['thetaW'] = 0
    if 'Pi' not in pol: pol['Pi'] = 0
    results = ['S={}'.format(pol['S']),
               'thetaW={}'.format(pol['thetaW']*pol['S']/1000),
               'Pi={}'.format(pol['Pi']*pol['S']/1000),
               'K={}'.format(pol['K']),
               'Fst={}'.format(pol['Fst']),
               'He={}'.format(pol['He'])]
    if 'D' in pol: results.append('D={}'.format(pol['D']))
    random.shuffle(results)
    f.write(' '.join(results) + '\n')
f.close()



