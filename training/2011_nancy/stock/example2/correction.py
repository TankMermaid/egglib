import os, re, glob

os.chdir('../example2')
fnames = glob.glob('data*.txt')
fnames.sort()


for fname in fnames:
    f = open(fname)
    c1 = 0
    c2 = 0
    S = 0.
    D = 0.
    for line in f:
        c1 += 1
        match = re.search('S=(\d+)', line)
        S += int(match.group(1))
        match = re.search('D=(-?\d\.\d+)', line)
        if match != None:
            D += float(match.group(1))
            c2 += 1
    f.close()
    print fname
    print '   average S = {}'.format(1.*S/c1)
    print '   average D = {}'.format(1.*D/c2)
    print '   number of alignments without polymorphism: {}'.format(c1-c2)
