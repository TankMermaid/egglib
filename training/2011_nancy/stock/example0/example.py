import random

data = [415, 64, 28, 351, 616]
print data
print data[0]
print data[-1]
print data[2:4]
print data[-2:]
print data[::-1]
data.reverse()
print data

for i in data:
    print i

data.sort()
print data
print data.index(351)

data.append(822)
print data

data = map(float, data)
print data

data = filter(lambda x: x<60, data)
print data
print len(data)

data = {'a': 8, 'b': 16, 'c': 32}
data = {(12,1):42, (5,6):63, (4,8):37}
for i in data:
    print i

print set("Les sanglots longs des violons de l'automne blessent mon coeur d'une langueur monotone.")

A = set([1,3,5,7,9])
B = set([1,2,3,4])

print A & B
print A | B
A |= B
print A


