import egglib, os

# Importe alignements

aligns = {}
for i in os.listdir("eagle_files"):
    aligns[i] = egglib.Align("eagle_files/" + i)
print len(aligns)

# Definit groupes

group1 = ["L0049B", "L0144B", "L0154C", "L0163C", "L0174B", "L0198C",
          "L0245B", "L0263C", "L0290B", "L0310C", "L0321B", "L0337C",
          "L0401C", "L0530B", "L0542B", "L0549B", "L0550B", "L0552D",
          "L0554B", "L0555B", "L0557B", "L0651C", "L0732D", "L0734D",
          "L0736D"]

group2 = ["L0166B", "L0213D", "L0216B", "L0233C", "L0239B", "L0306C",
          "L0330B", "L0357B", "L0368D", "L0369B", "L0372B", "L0400C",
          "L0404B", "L0410B", "L0414C", "L0421B", "L0425C", "L0430B",
          "L0448B", "L0482C", "L0514C", "L0526C", "L0543B", "L0544A",
          "L0545C", "L0546B", "L0547B", "L0640C", "L0648D", "L0673C",
          "L0679C", "L0738D"]
          
outgroup = "L0750D"

# Assigne labels

for locus in aligns:
    for seq in aligns[locus]:
        if seq.name in group1:
            seq.group = 1
        elif seq.name in group2:
            seq.group = 2
        else:
            if seq.name!=outgroup:
                print "unexpected sequence name in", locus, ":", seq.name
                exit()
            seq.group = 999

# Analyse de polymorphisme

for locus in aligns:
    pol = aligns[locus].polymorphism()
    print locus
    print "    S:", pol["S"]

    if pol["S"]>0:
        print "    D:", pol["D"]
        print "    Fst:", pol["Fst"]
    else:
        print "    D: -"
        print "    Fst: -"
    if "H" in pol:
        print "    H:", pol["H"]
    else:
        print "    H: -"

# Function to compute mean and variance

def meanvar(data):
    s=0.
    s2=0.
    for i in data:
        s+=i
        s2+=i**2
    m = s/len(data)
    m2 = s2/len(data)
    v = m2-m
    return m, v


# Use differents thresholds

from matplotlib import pyplot, rcParams
a, b = rcParams['figure.figsize']
rcParams['figure.figsize'] = a, 3*b

T = [1, 0.8, 0.6, 0.4, 0.2, 0]
S = []
D = []
F = []

print "testing thresholds"

for t in T:
    print t
    s = []
    d = []
    f = 0
    for locus in aligns:
        pol = aligns[locus].polymorphism(minimumExploitableData=t)
        s.append(pol['S'])
        if "D" in pol: d.append(pol['D'])
        if pol['S']==0: f+=1
    S.append(meanvar(s))
    D.append(meanvar(d))
    F.append(f)
    
pyplot.subplot(311)
m, v = zip(*S)
sd = [i**(0.5) for i in v]
pyplot.plot([-0.01, 1.01], [0,0], 'w,')
pyplot.errorbar(T, m, sd, c='k')
pyplot.xticks([0, 0.2, 0.4, 0.6, 0.8, 1])
pyplot.ylabel("S")

pyplot.subplot(312)
m, v = zip(*D)
sd = [i**(0.5) for i in v]
pyplot.plot([-0.01, 1.01], [0,0], 'w,')
pyplot.errorbar(T, m, sd, c='k')
pyplot.xticks([0, 0.2, 0.4, 0.6, 0.8, 1])
pyplot.ylabel("D")

pyplot.subplot(313)
pyplot.plot([-0.01, 1.01], [0,0], 'w,')
pyplot.plot(T, F, '-ko')
pyplot.xticks([0, 0.2, 0.4, 0.6, 0.8, 1])
pyplot.ylabel("F")
pyplot.xlabel("threshold")

pyplot.savefig('thresholds.png')
pyplot.clf()

# Simulations de coalescence pour aat

align = aligns["aat"]
pol = align.polymorphism()

ps = egglib.simul.CoalesceParamSet(len(align)-1)
mut = egglib.simul.CoalesceFiniteAlleleMutator(pol["thetaW"]*pol["lseff"])

print "testing aat with thetaW"

simuls = egglib.simul.coalesce(ps, mut, 1000)
Pl = 0
Pr = 0
n = 0
for simul in simuls:
    polsim = simul.polymorphism()
    if polsim["S"]>0:
        n+=1
        if polsim["D"] < pol["D"]: Pl+=1
        if polsim["D"] > pol["D"]: Pr+=1

Pl = 1.*Pl/n
Pr = 1.*Pr/n
print Pl, Pr

print "testing aat with S"

mut = egglib.simul.CoalesceFiniteAlleleMutator(0)
mut.fixedNumberOfMutation(pol["S"])
simuls = egglib.simul.coalesce(ps, mut, 1000)
Pl = 0
Pr = 0
n = 0
for simul in simuls:
    polsim = simul.polymorphism()
    if polsim["S"]>0:
        n+=1
        if polsim["D"] < pol["D"]: Pl+=1
        if polsim["D"] > pol["D"]: Pr+=1

Pl = 1.*Pl/n
Pr = 1.*Pr/n
print Pl, Pr

# Generalisation a tous les locus

for locus in aligns:

    align = aligns[locus]
    pol = align.polymorphism()
    
    if pol["S"]==0: continue

    ps = egglib.simul.CoalesceParamSet(len(align)-1)
    mut = egglib.simul.CoalesceFiniteAlleleMutator(pol["thetaW"]*pol["lseff"])

    simuls = egglib.simul.coalesce(ps, mut, 1000)
    Pl = 0
    Pr = 0
    n = 0
    for simul in simuls:
        polsim = simul.polymorphism()
        if polsim["S"]>0:
            n+=1
            if polsim["D"] < pol["D"]: Pl+=1
            if polsim["D"] > pol["D"]: Pr+=1

    Pl = 1.*Pl/n
    Pr = 1.*Pr/n
    print locus, "p-value:", Pl, Pr

# Phylogenie

group1 = group1[:5]
group2 = group2[:5]

skiplist = ["glut", "leg219", "erf", "unk29", "HAPc", "leg391", "fca2",
            "pg3", "l1l", "tc44795"]

for locus in aligns:
    if locus in skiplist: continue
    print "building tree for", locus
    align2 = egglib.Align()
    for i in group1: align2.append(i, aligns[locus].sequenceByName(i))
    for i in group2: align2.append(i, aligns[locus].sequenceByName(i))
    align2.append(outgroup, aligns[locus].sequenceByName(outgroup))
    
    tree, lk = egglib.wrappers.phyml(align2)
    outgroup_node = tree.get_node(outgroup)
    tree.root(outgroup_node)
    print "    test:",
    print tree.findMonophyleticGroup(group1),
    print tree.findMonophyleticGroup(group2)
    

