import os

# slide 3
import egglib
aln1 = egglib.Align()
print aln1.ns, aln1.ls
aln2 = egglib.io.from_fasta("align.fas")
print aln2.ns, aln2.ls

# slide 4
aln = egglib.io.from_fasta("align.fas")
print type(aln)
cnt = egglib.io.from_fasta("align.fas", cls=egglib.Container)
print type(cnt)

# slide 5
aln = egglib.io.from_fasta("align.fas", groups=True)

# slide 6
for sam in aln:
    print sam.name
    print sam.sequence[0]
    print sam.sequence[2:4]
    sam.sequence = 'N' * len(sam.sequence)
    sam.sequence[2:4] = "XX"
    print sam.group[0]
    sam.group = [5]
    sam.group[0] = 999
print aln.to_fasta()
aln = egglib.io.from_fasta("align.fas", groups=True)
sam = aln[0]
print sam.sequence.str()
aln[0] = "new sequence", "TGGCCAAAGGGGTGC", 4
print aln.to_fasta(groups=True)

# slide 7
aln1 = egglib.io.from_fasta("align1.fas")
aln2 = egglib.io.from_fasta("align2.fas")
aln3 = egglib.tools.concat([aln1, aln2], spacer=100, ch="N")
print aln3.to_fasta()

aln = egglib.io.from_fasta("align.fas")
print type(aln), aln.ns, aln.ls
aln = egglib.tools.ungap(aln, 0.5)
print type(aln),aln.ns, aln.ls
cnt = egglib.tools.ungap(aln)
print type(cnt), cnt.ns, [len(i.sequence) for i in cnt]

aln = egglib.io.from_fasta("align.fas")
rf = egglib.tools.ReadingFrame([(1,8), (12,16)])
prot = egglib.tools.translate(aln, frame=rf)
print prot.to_fasta()

sub = aln.extract(0, 10)
print sub.ns, sub.ls
sub = aln.extract([0, 2, 4, 6, 8, 10, 12])
print sub.ns, sub.ls
sub = aln.slice([0, 2, 3, 4])
print sub.ns, sub.ls

# slide 8
aln1 = egglib.io.from_fasta("align.fas")
print aln1.to_fasta()

f = open("align.aln")
content = f.read()
f.close()
aln2 = egglib.io.from_clustal(content)
print aln2.to_fasta()

vcf = egglib.io.VcfParser("dataset.vcf")
print vcf.num_samples
for chrom, pos, nall in vcf:
    variant = vcf.last_variant()
    print variant.samples
    print variant.alleles
    site = vcf.get_genotypes()
    print site

# slide 9
cs = egglib.stats.ComputeStats()
cs.configure(max_missing_freq=0.5)
cs.add_stat("S")
cs.add_stat("Pi")
cs.add_stat("D")
cs.add_stat("D*")
stats = cs.process_align(aln)
print stats

aln = egglib.io.from_fasta("align.fas", groups=True)
struct = egglib.stats.Structure.make_from_dataset(aln, lvl_pop=0)
cs.set_structure(struct)
cs.add_stat("Kst")
stats = cs.process_align(aln)
print stats

struct = egglib.stats.Structure.make_from_dict(pops={0: [0, 3], 1: [1, 2]})
cs.configure(max_missing_freq=0.5)
cs.set_structure(struct)
cs.add_stat("S")
cs.add_stat("Pi")
cs.add_stat("D")
cs.add_stat("D*")
cs.add_stat("Dxy")
cs.add_stat("Kst")
cs.add_stat("WCst")
stats = cs.process_align(aln)
print stats

# slide 10
site = egglib.stats.SiteFrequency.make_from_alleles([[1,0,0,0,1,0,1,1,1,0], [0,1,0,0,0,1,0,0,1,1]])
site = egglib.stats.SiteFrequency.make_from_list([(14, 6), (7, 13), (1, 19)])
site = egglib.stats.SiteFrequency.make_from_dataset(aln, 0)
cs.add_stat("A")
cs.add_stat("R")
cs.add_stat("He")
cs.add_stat("WCst")
stats = cs.process_site(site)
print stats

# slide 11
coding = egglib.stats.CodingDiversity(aln, frame=rf)
aln_S = coding.mk_align_S()
aln_NS = coding.mk_align_NS()
cs = egglib.stats.ComputeStats()
cs.configure(max_missing_freq=0.5)
cs.add_stat("Pi")
print cs.process_align(aln_S)
print cs.process_align(aln_NS)

site1 = egglib.stats.SiteFrequency.make_from_dataset(aln, 0)
site2 = egglib.stats.SiteFrequency.make_from_dataset(aln, 1)
site3 = egglib.stats.SiteFrequency.make_from_dataset(aln, 2)
ehh = egglib.stats.EHH()
ehh.set_core(site1, EHH_thr=0.5)
ehh.load_distant(site2, distance=0.142)
ehh.load_distant(site3, distance=0.457)
print ehh.num_haplotypes
print ehh.get_EHH(0)

# slide 12
simul = egglib.coalesce.Simulator(2, num_chrom=[20, 20], migr=0.5, theta=2.5, recomb=1.2)
simul.params["num_sites"] = 100
simul.params["num_alleles"] = 4
simul.params["N"][1] = 2.5
simul.params["trans_matrix"] = [["",1.0,2.5,1.0],
                                [1.0,"",1.0,2.5],
                                [2.5,1.0,"",1.0],
                                [1.0,2.5,1.0,""]]
simul.params["events"].add(cat="size", T=0.4, idx=1, N=1.0)

# slide 13
aln = simul.simul()
print aln.to_fasta(mapping="ACGT")
for align in simul.iter_simul(1000):
    align.to_fasta("simul.fas")
os.remove("simul.fas")
cstats = egglib.stats.ComputeStats()
cstats.configure(filtr=egglib.stats.filter_default)
d = {0: range(20), 1: range(20, 40)}
structure = egglib.stats.Structure.make_from_dict(pops=d)
cstats.set_structure(structure)
cstats.add_stat("WCst")
for stats in simul.iter_simul(1000, cs=cstats):
    print stats["WCst"]

# slide 14
egglib.wrappers.paths["phyml"] = "/home/stephane/Documents/software/PhyML-3.1/PhyML-3.1_linux64"
egglib.wrappers.paths["clustal"] = "clustalo"
#egglib.wrappers.paths.save() # requires administrator rights
aln = egglib.wrappers.clustal(cnt, num_iter=4)
tree, stats = egglib.wrappers.phyml(aln, model="TN93", boot=100)
print tree
