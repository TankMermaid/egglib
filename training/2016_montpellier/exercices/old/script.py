import egglib

# import the first alignment
aln = egglib.io.from_fasta('align1.fas', groups=True)
print aln.ns
print aln.no
print aln.ls

# create the Structure from level 0
struct = egglib.stats.Structure.make_from_dataset(aln, lvl_pop=0)
print struct.as_dict()

# compute statistics using default parameters and the structure
cs = egglib.stats.ComputeStats()
cs.configure(max_missing_freq=0.0, consider_outgroup_missing=True)
cs.set_structure(struct)
cs.add_stat('S')
cs.add_stat('D')
cs.add_stat('Dxy')
cs.add_stat('K')
cs.add_stat('Snn')
cs.add_stat('WCst')
cs.add_stat('Hsd')
print cs.process_align(aln)

# compute statistics with max missing freq = 25%
cs.configure(max_missing_freq=0.25)
cs.set_structure(struct)
cs.add_stat('S')
cs.add_stat('D')
cs.add_stat('Dxy')
cs.add_stat('K')
cs.add_stat('Snn')
cs.add_stat('WCst')
cs.add_stat('Hsd')
print cs.process_align(aln)

# compute pairwise LD between all sites
print 'compute LD matrix'
pos, mat = egglib.stats.matrix_LD(aln, 'rsq', max_maj=0.8)

# extract data as a two vectors (+ random noise)
x = []
y = []
rnd = egglib.tools.Random()
for i,row in enumerate(mat):
    for j,v in enumerate(row):
        if v is not None:
            x.append(pos[i]-pos[j])
            y.append(v+rnd.normal()*0.005)

# generate a plot
from matplotlib import pyplot
pyplot.plot(x, y, 'ko', mec='k', mfc='None', ms=3, label='align1')
pyplot.xlabel('distance')
pyplot.ylabel('r square')
pyplot.ylim(0, 1)
pyplot.savefig('figure1.png')
pyplot.clf()

# import second alignment and generate LD plot to compare
print 'compute LD matrix for 2nd alignment'
aln2 = egglib.io.from_fasta('align2.fas')
pos, mat = egglib.stats.matrix_LD(aln2, 'rsq', max_maj=0.8)
x = []
y = []
for i,row in enumerate(mat):
    for j,v in enumerate(row):
        if v is not None:
            x.append(pos[i]-pos[j])
            y.append(v+rnd.normal()*0.005)
pyplot.plot(x, y, 'ko', mec='k', mfc='None', ms=3, label='align2')
pyplot.xlabel('distance')
pyplot.ylabel('r square')
pyplot.ylim(0, 1)
pyplot.savefig('figure2.png')
pyplot.clf()

# extract S and NS alignments from align1
rf = egglib.tools.ReadingFrame([(0, 441), (1419, 4014), (5258, 5960), (6605, 8942)])
cd = egglib.stats.CodingDiversity(aln, frame=rf, struct=struct, max_missing=int(0.5*aln.ns))
align_S = cd.mk_align_S()
align_NS = cd.mk_align_NS()
print 'sites syn:', cd.num_sites_S
print 'sites non-syn:', cd.num_sites_NS
print 'S syn:', cd.num_pol_S
print 'S non-syn:', cd.num_pol_NS

# compute statistics
cs = egglib.stats.ComputeStats()
cs.configure(filtr=egglib.stats.Filter(rng=(0, 64)))
cs.set_structure(struct)
cs.add_stat('S')
cs.add_stat('D')
cs.add_stat('Dxy')
cs.add_stat('WCst')
cs.add_stat('thetaW')
cs.add_stat('Pi')

print 'synonymous stats:'
stats = cs.process_align(align_S)
print stats
print '    Pi/site:', stats['Pi']/cd.num_sites_S
print '    thetaW/site:', stats['thetaW']/cd.num_sites_S

print 'non-synonymous stats:'
stats = cs.process_align(align_NS)
print stats
print '    Pi/site:', stats['Pi']/cd.num_sites_NS
print '    thetaW/site:', stats['thetaW']/cd.num_sites_NS

