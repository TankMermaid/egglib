import egglib, glob, random

def mean(val):
    #filter for None
    val = filter(lambda x: x!=None,val)
    #return mean value
    if len(val)==0:
        return None
    return 1.*sum(val)/len(val)


###
print '### exercice 5.1 mean of observed stats ###'
thetaW = []
D      = []
WCst   = []
Snn    = []
Dxy    = []

cs = egglib.stats.ComputeStats()
cs.add_stat('thetaW')
cs.add_stat('D')
cs.add_stat('WCst')
cs.add_stat('Snn')
cs.add_stat('Dxy')
cs.add_stat('ls')

config = []

#loop over loci
for f in glob.iglob('fasta.caicedo/*.fst'):
    aln = egglib.io.from_fasta(f,groups=True)
    struct = egglib.stats.Structure.make_from_dataset(aln,lvl_pop=0)
    cs.set_structure(struct)
    stats = cs.process_align(aln)
    thetaW.append(stats['thetaW']/stats['ls'])
    D.append(stats['D'])
    WCst.append(stats['WCst'])
    Snn.append(stats['Snn'])
    Dxy.append(stats['Dxy'])
    #store configuration of each locus
    config.append([struct.as_dict()[1],aln.ls])

#computation of mean stats over the loci
mthetaW = mean(thetaW)
mD = mean(D)
mWCst = mean(WCst)
mSnn = mean(Snn)
mDxy = mean(Dxy)
###

#write in a file
f = open("obs.txt",'w')
f.write('{0}\n{1}\n{2}\n{3}\n{4}\n'.format(mthetaW,mD,mWCst,mSnn,mDxy))
f.close()
###

###
print '### exercice 5.2  simulations first model ###'

#NREPETS = 10000
NREPETS = 100
cs = egglib.stats.ComputeStats()
cs.configure(filtr=egglib.stats.filter_default)
cs.add_stat('thetaW')
cs.add_stat('D')
cs.add_stat('WCst')
cs.add_stat('Snn')
cs.add_stat('Dxy')
cs.add_stat('ls')

statsDOM1 = open('stats_DOM1_mini.txt','w')
paramsDOM1 = open('params_DOM1_mini.txt','w')
paramsDOM1.write('theta\tT\tS\n')

for rep in range(NREPETS):
    thetaW = []
    D      = []
    WCst   = []
    Snn    = []
    Dxy    = []
    
    theta = random.uniform(0.,0.01)
    T=random.uniform(0.,0.5)
    S=random.uniform(0.,5.)
    
    for conf,ls in config:
        n1 = len(conf[0])
        n2 = len(conf[1])
        sim = egglib.coalesce.Simulator(num_pop=2,num_chrom=[n1,n2],num_sites=ls)
        struct = egglib.stats.Structure.make_from_dict(pops=conf)
        
        sim.params['theta'] = theta*ls
        sim.params['events'].add(cat='bottleneck',T=T,S=S,idx=1)
        sim.params['events'].add('merge',T=T,src=1,dst=0)
        
        aln = sim.simul()
        
        #analysis of polymorphism
        cs.set_structure(struct)
        stats = cs.process_align(aln)
        thetaW.append(stats['thetaW']/stats['ls'])
        D.append(stats['D'])
        WCst.append(stats['WCst'])
        Snn.append(stats['Snn'])
        Dxy.append(stats['Dxy'])
    
    
    mthetaW = mean(thetaW)
    mD = mean(D)
    mWCst = mean(WCst)
    mSnn = mean(Snn)
    mDxy = mean(Dxy)
    
    if None not in [mthetaW,mD,mWCst,mSnn,mDxy]:
        paramsDOM1.write('{0}\t{1}\t{2}\n'.format(theta,T,S))
        statsDOM1.write('{0}\t{1}\t{2}\t{3}\t{4}\n'.format(mthetaW,mD,mWCst,mSnn,mDxy))
paramsDOM1.close()
statsDOM1.close()
###


###
print '### exercice 5.3  simulations second model ###'

#DOM2 model
statsDOM2 = open('stats_DOM2_mini.txt','w')
paramsDOM2 = open('params_DOM2_mini.txt','w')
paramsDOM2.write('theta\tT\tS\tratio\n')

for rep in range(NREPETS):
    thetaW = []
    D      = []
    WCst   = []
    Snn    = []
    Dxy    = []
    
    theta = random.uniform(0.,0.01)
    T=random.uniform(0.,0.5)
    S=random.uniform(0.,5.)
    ratio = random.uniform(0.,5.)
    
    for conf,ls in config:
        n1 = len(conf[0])
        n2 = len(conf[1])
        sim = egglib.coalesce.Simulator(num_pop=2,num_chrom=[n1,n2],num_sites=ls)
        struct = egglib.stats.Structure.make_from_dict(pops=conf)
        
        sim.params['theta'] = theta*ls
        sim.params['events'].add(cat='bottleneck',T=T,S=S,idx=1)
        sim.params['events'].add('merge',T=T,src=1,dst=0)
        sim.params['N'] = [1,ratio]
        
        aln = sim.simul()
        
        #analysis of polymorphism
        cs.set_structure(struct)
        stats = cs.process_align(aln)
        thetaW.append(stats['thetaW']/stats['ls'])
        D.append(stats['D'])
        WCst.append(stats['WCst'])
        Snn.append(stats['Snn'])
        Dxy.append(stats['Dxy'])
    
    mthetaW = mean(thetaW)
    mD = mean(D)
    mWCst = mean(WCst)
    mSnn = mean(Snn)
    mDxy = mean(Dxy)
    
    if None not in [mthetaW,mD,mWCst,mSnn,mDxy]:
        paramsDOM2.write('{0}\t{1}\t{2}\t{3}\n'.format(theta,T,S,ratio))
        statsDOM2.write('{0}\t{1}\t{2}\t{3}\t{4}\n'.format(mthetaW,mD,mWCst,mSnn,mDxy))
paramsDOM2.close()
statsDOM2.close()
###

###
print '### exercice 5.3  simulations third model ###'

#DOM3 model
statsDOM3 = open('stats_DOM3_mini.txt','w')
paramsDOM3 = open('params_DOM3_mini.txt','w')
paramsDOM3.write('theta\tT\S\tM\\n')

for rep in range(NREPETS):
    thetaW = []
    D      = []
    WCst   = []
    Snn    = []
    Dxy    = []
    
    theta = random.uniform(0.,0.01)
    T = random.uniform(0.,0.5)
    S     = random.uniform(0.,5.)
    migr  = random.uniform(0.,5.)
    
    for conf,ls in config:
        n1 = len(conf[0])
        n2 = len(conf[1])
        sim = egglib.coalesce.Simulator(num_pop=2,num_chrom=[n1,n2],num_sites=ls)
        struct = egglib.stats.Structure.make_from_dict(pops=conf)
        
        sim.params['theta']=theta*ls
        sim.params['events'].add(cat='bottleneck',T=T,S=S,idx=1)
        sim.params['events'].add('merge',T=T,src=1,dst=0)
        sim.params['migr_matrix'] = [[None,migr],[migr,None]]
        
        aln = sim.simul()
        
        #analysis of polymorphism
        cs.set_structure(struct)
        stats = cs.process_align(aln)
        thetaW.append(stats['thetaW']/stats['ls'])
        D.append(stats['D'])
        WCst.append(stats['WCst'])
        Snn.append(stats['Snn'])
        Dxy.append(stats['Dxy'])
    
    mthetaW = mean(thetaW)
    mD = mean(D)
    mWCst = mean(WCst)
    mSnn = mean(Snn)
    mDxy = mean(Dxy)
    
    if None not in [mthetaW,mD,mWCst,mSnn,mDxy]:
        paramsDOM3.write('{0}\t{1}\t{2}\t{3}\n'.format(theta,T,S,migr))
        statsDOM3.write('{0}\t{1}\t{2}\t{3}\t{4}\n'.format(mthetaW,mD,mWCst,mSnn,mDxy))
paramsDOM3.close()
statsDOM3.close()
