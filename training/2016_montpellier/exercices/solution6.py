import egglib, glob, os

###
print '### exercice 6.1.1 ###'
print egglib.wrappers.paths
print type(egglib.wrappers.paths)
###

###
print '### exercice 6.1.2 ###'
egglib.wrappers.paths.autodetect(verbose=True)
print egglib.wrappers.paths
###

###
print '### exercice 6.1.3 ###'
egglib.wrappers.paths['phyml'] = '/home/stephane/Documents/software/phyml-20120412/src/phyml'
egglib.wrappers.paths['codeml'] = '/home/stephane/Documents/software/paml-4.8a/src/codeml'
print egglib.wrappers.paths
###

###
print '### exercice 6.2.1 ###'
fnames = glob.glob('aln/*.fas')
print fnames
print 'number of alignments:', len(fnames)
###

###
print '### exercice 6.2.2 ###'
aligns = {}
for fname in fnames:
    name = os.path.basename(fname).split('.')[0]
    aligns[name] = egglib.io.from_fasta(fname, groups=True)
###

###
print '### exercice 6.2.3 ###'
for name in aligns:
    print name, aligns[name].ns, aligns[name].ls
###

###
print '### exercice 6.3.1 ###'
tree, stats = egglib.wrappers.phyml(aligns['locus001'], model='HKY85', rates=4)
###

###
print '### exercice 6.3.2 ###'
print stats
###

###
print '### exercice 6.3.3 ###'
print 'check number of leaves:', tree.num_leaves, aligns['locus001'].ns
f = open('tree.tre', 'w')
f.write(tree.newick() + '\n')
f.close()
###

###
print '### exercice 6.3.4 ###'
for name, aln in aligns.iteritems():
    print 'building tree for', name
    tree, stats = egglib.wrappers.phyml(aln, model='HKY85', rates=4)
    f = open('trees/{0}.tre'.format(name), 'w')
    f.write(tree.newick() + '\n')
    f.close()
###

###
print '### exercice 6.4.1 ###'
mapping = aligns['locus001'].group_mapping()
print mapping
###

###
print '### exercice 6.4.2 ###'
group1 = mapping[1]
group1 = [i.name for i in group1]
print 'group 1:', group1
###

###
print '### exercice 6.4.3 ###'
tree = egglib.Tree('trees/locus001.tre')
print 'find group1:', tree.find_clade(group1)
###

###
print '### exercice 6.5.1 ###'
tree = egglib.Tree('trees/locus003.tre')
print 'find group1:', tree.find_clade(group1)

###
print '### exercice 6.5.2 ###'
print 'find group1 both sides:', tree.find_clade(group1, both_sides=True)
###

###
print '### exercice 6.5.3 ###'
outgroup = tree.find_clade(['ettra', 'suta'])
tree.root(outgroup, reoriente=True)
###

###
print '### exercice 6.5.4 ###'
clade = tree.find_clade(group1)
print 'find group1:', clade
###

###
print '### exercice 6.6.1 ###'
subtree = tree.copy(clade)
print 'leaves in subtree:', subtree.num_leaves
###

###
print '### exercice 6.6.2 ###'
clade.label = '$1'
print tree.newick()
###

###
print '### exercice 6.6.3 ###'
trees = {}
for name in aligns:
    tree = egglib.Tree('trees/{0}.tre'.format(name))
    clade = tree.find_clade(group1)
    print name, clade
    if clade is None:
        outgroup = tree.find_clade(['ettra', 'suta'])
        tree.root(outgroup, reoriente=True)
        clade = tree.find_clade(group1)
        print ' after rerooting -->', clade
    clade.label = '$1'
    trees[name] = tree
###

###
print '### exercice 6.7.1 ###'
for tree in trees.itervalues():
    for node in tree.depth_iter():
        if node.label == '':
            node.label = None
###

###
print '### exercice 6.7.2 ###'
A0 = egglib.wrappers.codeml(aligns['locus001'], trees['locus001'], model='A0')
print 'model A0:', A0
print 'model A0 lnL:', A0['lnL'], A0['np']
###

###
print '### exercice 6.7.3 ###'
A = egglib.wrappers.codeml(aligns['locus001'], trees['locus001'], model='A')
print 'model A lnL:', A['lnL'], A['np']
print 'omega:', A['omega']
print 'LRT:', 2 * (A['lnL'] - A0['lnL']), A['omega'], A['freq']
### 


###
print '### exercice 6.7.4 ###'
for name in sorted(aligns):
    A0 = egglib.wrappers.codeml(aligns[name], trees[name], model='A0')
    A = egglib.wrappers.codeml(aligns[name], trees[name], model='A')
    print name
    print '    LRT:', 2 * (A['lnL'] - A0['lnL'])
    print '    signif.:', 2 * (A['lnL'] - A0['lnL']) > 3.84
    print '    dN/dS table:', A['omega']
    print '    frequencies:', A['freq']
###

print 'note: truly selected loci are 010, 012, 016, 020, 021'
