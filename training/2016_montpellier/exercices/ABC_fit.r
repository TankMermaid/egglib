library(abc)

tol <- 0.0015

# prior bounds
prior1.bounds <- as.matrix(read.table("prior1.txt"))
prior2.bounds <- as.matrix(read.table("prior2.txt"))
prior3.bounds <- as.matrix(read.table("prior3.txt"))

#observed stats 
obs <- read.table("obs.txt")

#simulated parameter values
params1 <- read.table("params_DOM1.txt",header=T)
params2 <- read.table("params_DOM2.txt",header=T)
params3 <- read.table("params_DOM3.txt",header=T)

#simulated summary stats
stats1 <- read.table("stats_DOM1.txt")
stats2 <- read.table("stats_DOM2.txt")
stats3 <- read.table("stats_DOM3.txt")


#fit
results_M1 <- abc(target = obs, param = params1, sumstat = stats1, tol = tol, method = "neuralnet", transf = "logit", logit.bounds = prior1.bounds, numnet = 10, sizenet = 5)
results_M2 <- abc(target = obs, param = params2, sumstat = stats2, tol = tol, method = "neuralnet", transf = "logit", logit.bounds = prior2.bounds, numnet = 10, sizenet = 5)
results_M3 <- abc(target = obs, param = params3, sumstat = stats3, tol = tol, method = "neuralnet", transf = "logit", logit.bounds = prior3.bounds, numnet = 10, sizenet = 5)


#plots
pdf("DOM1_results.pdf")
plot(results_M1,param=params1)
dev.off()

pdf("DOM2_results.pdf")
plot(results_M2,param=params2)
dev.off()

pdf("DOM3_results.pdf")
plot(results_M3,param=params3)
dev.off()


print(summary(results_M1))
print(summary(results_M2))
print(summary(results_M3))


#model choice
nmodel1 <- nrow(stats1)
nmodel2 <- nrow(stats2)
nmodel3 <- nrow(stats3)

tot_stats <- rbind(stats1,stats2,stats3)

index <- c(rep("DOM1", nmodel1), rep("DOM2", nmodel2), rep("DOM3",nmodel3))

post_pr <- postpr(target = obs, index = index, sumstat = tot_stats, tol = tol, method = "neuralnet", corr = TRUE, kernel="epanechnikov", numnet = 10, sizenet = 5, maxit = 5000)

bf <- summary(post_pr)

cv <- cv4postpr(index,tot_stats,nval=50,tol=tol,method="mnlogistic")
png('confusion_matrix.png')
plot(cv)
dev.off()
