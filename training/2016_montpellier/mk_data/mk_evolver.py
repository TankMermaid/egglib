import egglib, subprocess, random, os

templ = """
0              * 0,1:seqs or patters in paml format (mc.paml); 2:paup format (mc.nex)
6426415767     * random number seed (odd number)
{ns} {ncod} 1  * <# seqs>  <# codons>  <# replicates>

-1           * <tree length, use -1 if tree has absolute branch lengths>

{tree}

3                 * number of site classes, & frequencies for site classes
{frq1} {frq2} {frq3}  * must sum to 1.

{tree1}
{tree2}
{tree3}

2.5    * kappa

0.01639344 0.01639344 0.01639344 0.01639344 
0.01639344 0.01639344 0.01639344 0.01639344 
0.01639344 0.01639344 0          0
0.01639344 0.01639344 0          0.01639344 

0.01639344 0.01639344 0.01639344 0.01639344 
0.01639344 0.01639344 0.01639344 0.01639344 
0.01639344 0.01639344 0.01639344 0.01639344 
0.01639344 0.01639344 0.01639344 0.01639344 

0.01639344 0.01639344 0.01639344 0.01639344 
0.01639344 0.01639344 0.01639344 0.01639344 
0.01639344 0.01639344 0.01639344 0.01639344 
0.01639344 0.01639344 0.01639344 0.01639344 

0.01639344 0.01639344 0.01639344 0.01639344 
0.01639344 0.01639344 0.01639344 0.01639344 
0.01639344 0.01639344 0.01639344 0.01639344 
0.01639344 0.01639344 0.01639344 0.01639344 

0    * genetic code (0:universal; 1:mammalian mt; 2-10:see below)


//end of file

============================================================================
MCcodonNSbranches.dat
Notes for using evolver to simulate codon sequences under the branch model, 
with different omega's for different branches. 

Change values of parameters, but do not delete them.  You can add
empty lines, but do not break one line into several lines or merge several 
lines into one line.

Use : to indicate the branch length in the tree topology.  Use # to
indicate the omega ratio for the branch.

64 codon freqs are in fixed order TTT, TTC, TTA, TTG, TCT, TCC, ..., GGG.
Stop codons have frequency 0.

=================!! Check screen output carefully!! ========================
"""

name = 'MCcodonNSbranchsites.dat'
path = '/home/stephane/Documents/software/paml-4.8a/Technical/Simulation/Codon/evolverBS'

nloci = 23
locsel = 9, 11, 15, 19, 20

tree0 = egglib.Tree(string='((krala:0.060617376346,(ettra:0.0484954591919,suta:0.139633079751):0.150749472501):0.82019950592,((cyba:0.1506857569,kola:0.0791612314555):0.309484035805,(ikra:0.0818615473147,atton:0.100391799401):0.159390902522):1.13003124376,((kata:0.0285100555405,tolon:0.0201560637903):0.0707050529787,(osta:0.0703889395529,(mekra:0.0405067475679,trala:0.0708882797999):0.0691507859892):0.0205093921801):0.749299498065);')
target = set(['cyba', 'kola', 'ikra', 'atton'])

for node in tree0.depth_iter():
    if node.parent is not None:
        node.parent_branch /= 4.0

for locus in range(nloci):

    tree = tree0.copy()

    # draw frequencies of sites
    frq1 = random.random() * 0.3 + 0.5
    frq2 = random.random() * (0.98 - frq1)
    frq3 = 1.0 - frq1 - frq2

    # set omega labels to tree
    w1 = random.random() * 0.35
    w2 = random.normalvariate(1.0, 0.1)
    if locus in locsel: w3 = random.expovariate(1/5.0)
    else: w3 = random.normalvariate(1.0, 0.2)

    treelab1 = tree.copy()
    for node in treelab1.depth_iter():
        if node.parent is not None:
            if node.label is None: lbl = ''
            else: lbl = node.label
            node.label = '{0} #{1:.2f}'.format(lbl, w1)

    treelab2 = tree.copy()
    for node in treelab2.depth_iter():
        if node.parent is not None:
            if node.label is None: lbl = ''
            else: lbl = node.label
            node.label = '{0} #{1:.2f}'.format(lbl, w2)

    treelab3 = tree.copy()
    clade = treelab3.find_clade(target)
    for node in treelab3.depth_iter():
        if node.parent is not None:
            if node.label is None: lbl = ''
            else: lbl = node.label
            node.label = '{0} #{1:.2f}'.format(lbl, w1)
    for node in treelab3.depth_iter(clade):
        if node.parent is not None:
            node.label = '{0} #{1:.2f}'.format(node.label.split()[0], w3)

    # create control file
    ns = tree.num_leaves
    ls = random.randint(100, 400)
    f = open(name, 'w')
    f.write(templ.format(
        ns=ns,
        ncod=ls,
        tree=tree.newick(),
        frq1=frq1,
        frq2=frq2,
        frq3=frq3,
        tree1=treelab1.newick(brlens=False),
        tree2=treelab2.newick(brlens=False),
        tree3=treelab3.newick(brlens=False)))
    f.close()

    # run evolver
    p = subprocess.Popen((path,), stdout=subprocess.PIPE)
    p.communicate()

    # get output file
    aln = egglib.Align(nsam=ns, nout=0, nsit=ls*3, init=9999)
    aln.ng = 1
    f = open('mc.paml')
    assert f.readline().strip() == ''
    assert f.readline().strip() == ''
    assert f.readline().split() == [str(ns), str(ls*3)]
    assert f.readline().strip() == ''
    for i in xrange(ns):
        bits = f.readline().split()
        aln[i].name = bits[0]
        if bits[0] in target: aln[i].group[0] = 1
        else: aln[i].group[0] = 2
        aln[i].sequence = ''.join(bits[1:])
    f.close()

    aln.to_fasta(fname='../exercices/aln/locus{0:0>3d}.fas'.format(locus+1), groups=True, linelength=90)

    # clean
    print locus+1, 'done'
    for i in [name, 'siterates.txt', 'ancestral.txt', 'evolver.out', 'mc.paml']:
        if os.path.isfile(i): os.unlink(i)
