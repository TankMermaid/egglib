import egglib, random

sim = egglib.coalesce.Simulator(8)

groups = [0,0,0,0,1,1,1,2]

for i in xrange(8):
    for j in xrange(8):
        if i==j: continue
        g1 = groups[i]
        g2 = groups[j]
        if g1==g2:
            sim.params['migr_matrix'][i,j] = 4.0
        elif g1 == 2 or g2 == 2:
            sim.params['migr_matrix'][i,j] = 0.1
        else:
            assert (g1 == 0 and g2 == 1) or (g1 == 1 and g2 == 0)
            sim.params['migr_matrix'][i,j] = 1.0

print 'migration matrix:'
print '   ',
for i in xrange(8): print '[{0}]'.format(i+1),
print
for i in xrange(8):
    print '[{0}]'.format(i+1),
    for j in xrange(8):
        if i==j: print '   ',
        else: print sim.params['migr_matrix'][i,j],
    print

sim.params['theta'] = 8.0
sim.params['num_sites'] = 1
sim.params['mut_model'] = "SMM"
sim.params['num_indiv'] = [40] * 8
sim.params['s'] = [0.8] * 7 + [0.99]

n = 2 * sum(sim.params['num_indiv'])
data = [[None] * 30 for i in xrange(n)]

for j, aln in enumerate(sim.iter_simul(nrepet=30)):
    offset = int(20 + random.random() * 130)
    for i, item in enumerate(aln):
        assert item.ls == 1
        a = offset + item.sequence[0]
        assert a > 0
        data[i][j] = a

f = open('data1.txt', 'w')
for line in data:
    f.write(' '.join([str(i).rjust(3, '0') for i in line]) + '\n')
f.close()
