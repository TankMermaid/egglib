import egglib, random

cnt = egglib.io.from_fasta('align2.fas', cls=egglib.Container)

for idx, sam in enumerate(cnt):
    points = sorted([random.randint(1, sam.ls-1) for i in range(3)])
    intervals = zip([0] + points, points + [sam.ls])
    intervals.sort(cmp=lambda x,y: cmp(x[1]-x[0], y[1]-y[0]))

    if random.random()<0.5: intervals = intervals[:2]
    else: intervals = intervals[:1]

    for a, b in intervals:
        del sam.sequence[a:b]

    sam.name = 'sample_{0:0>3}'.format(idx+1)
    print sam.name, sam.ls

cnt.to_fasta('sequences.fas')
