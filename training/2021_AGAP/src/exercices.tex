\documentclass{scrartcl}
\input{common}
\title{Exercices avec EggLib}

\begin{document}

\maketitle

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{abstract}

\begin{center}
    \includegraphics[width=0.2\textwidth,keepaspectratio=true]{v3.jpg}
\end{center}

\noindent
Cette formation pr\'esentera les points essentiels pour utiliser la
version 3 de la biblioth\`eque \eggword{EggLib} pour Python. Il est
important de noter que la version actuelle est en pr\'eparation
(b\^eta), ce qui signifie que certains points de syntaxe peuvent \^etre
modifi\'es d'ici la publication de la version 3.0.0 finale
d'\eggword{EggLib}. \\

\noindent
Dans ce document, les exercices sont num\'erot\'es et pr\'esent\'es dans
des cadres soulign\'es d'une barre bleue. Ils sont g\'en\'eralement
suivis d'un paragraphe qui fournit les indications n\'ecessaires pour
les r\'esoudre. Le texte en \eggword{orange} indique les objets se
rapportant \`a \eggword{EggLib}, tandis que les fragments de code Python
donn\'es en exemple sont pr\'esent\'es ainsi~:
\python{print "hello, world!"}. \\

\noindent
De nombreuses r\'ef\'erences sont faites aux fonctions globales de
Python. Vous pouvez consulter leur documentation \`a l'adresse
suivante~: \url{http://docs.python.org/2/library/functions.html}. La
documentation actuelle \linebreak[4] d'\eggword{EggLib} est disponible \`a cette
adresse~: \url{http://mycor.nancy.inra.fr/egglib/}. Les classes
\eggword{Container} et \eggword{Align} sont dans le module
\verb|egglib|, et vous pouvez aussi consulter l'index alphab\'etique pour
retrouver rapidement la documentation d'une classe, fonction, m\'ethode
ou attribut. \\

\noindent
Tous les fichiers de donn\'ees utilis\'es comme exemples et
n\'ecessaires pour accomplir les exercices sont fournis dans le
r\'epertoire \verb|exercices|. Ce r\'epertoire contient \'egalement tous
les corrig\'es sous la forme de scripts (\verb|solution1.py| pour la
s\'erie d'exercices 1, et ainsi de suite).

\end{abstract}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\newpage
\tableofcontents
\newpage

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section*{Avis aux d\'ebutants}

\noindent
Cette formation requiert en principe une connaissance minimale de
Python, c'est \`a dire que les participants devraient savoir comment
\'ecrire et ex\'ecuter des scripts Python. \\

\noindent
Si vous \^etes peu \`a votre aise avec les outils en ligne de commande,
nous vous recommandons d'adopter la strat\'egie suivante~:

\begin{itemize}

    \item Cr\'eez un document texte simple \`a l'int\'erieur du
        r\'epertoire (ou dossier) \verb|exercices| distribu\'e pour la
        formation. Vous pouvez le nommer, par exemple,
        \verb|script.py| (l'extension \verb|.py| est
        recommand\'ee pour plus de clart\'e).

    \item Commencez \`a remplir ce fichier avec les commandes
        sugg\'er\'ees pour l'exercice. Dans le but de tester que tout
        fonctionne correctement, entrez d'abord les commandes
        suivantes~:

        \begin{minted}[frame=leftline]{python}
import egglib
print dir(egglib)
        \end{minted}

    \item D\'emarrez une invite de commande (application \verb|cmd.exe|
        sous Windows, \verb|terminal| sinon.).

    \item Utilisez la commande \verb|cd| (\textit{change directory})
        pour vous d\'eplacer jusqu'au r\'epertoire \verb|exercices|, de
        mani\`ere \`a \^etre au m\^eme niveau que le script que vous
        venez de cr\'eer.

    \item Lancez la commande \verb|python script.py| pour ex\'ecuter le
        script. Le script devrait afficher la liste du contenu du
        package \eggword{EggLib}.

    \item Continuez de remplir le script.

    \item Si vous avez besoin d'ouvrir un fichier qui se trouve dans
        le r\'epertoire d'o\`u vous lancez Python pour ex\'ecuter votre
        script, vous pouvez directement donner \`a Python le nom du
        fichier en question. Sinon, il faut donner aussi le chemin
        (absolu ou relatif) permettant d'arriver jusqu'au fichier depuis
        le point d'o\`u est ex\'ecut\'e le script. Pour importer le
        fichier \verb|align1.fas|, on tapera donc, s'il l'on est dans
        le r\'epertoire o\`u il se trouve~:

        \begin{minted}[frame=leftline]{python}
aln = egglib.io.from_fasta("align1.fas")
        \end{minted}

\end{itemize}

\noindent
Il existe diff\'erents environnements de d\'eveloppement int\'egr\'e
(IDE) facilitant l'\'ecriture et le test de programme en Python et dans
d'autres langages. Ils ne sont pas forc\'ement n\'ecessaires pour effectuer
ces exercices mais s'ils vous simplifient la t\^ache, n'h\'esitez pas
\`a les utiliser~!

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\newpage

\section{Exercices 1 -- Prise en main d'EggLib}

\noindent
Cette s\'erie d'exercices permettra un aper\c{c}u des
fonctionnalit\'es de base d'\eggword{EggLib} et de d\'ecouvrir les 
principes de son interface utilisateur.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Importer un alignment depuis un fichier Fasta}

\begin{task}
Cr\'eer une instance de la classe \eggword{Align} \`a partir de
l'alignement stock\'e au format Fasta dans le fichier \verb|align1.fas|.
\end{task}

\noindent
$\Rightarrow$ Pour cela on utilisera la fonction suivante~:
\eggword{egglib.io.from\_fasta(fname)}, o\`u \python{fname} est le nom du
fichier.

\begin{task}
Quel est le nombre d'\'echantillons pr\'esents dans le groupe principal
(hors groupe externe), ou \emph{ingroup}~? dans le groupe externe
(\emph{outgroup})~? Quelle est la longueur de l'alignement (nombre de
sites)~?
\end{task}

\noindent
$\Rightarrow$ On pr\'ef\'erera les \emph{attributs} (variables
attach\'ees aux objets) \eggword{ns} (\emph{number of samples}),
\eggword{no} (\emph{number of outgroup samples}) et \eggword{ls}
(\emph{length of sequences}). Par ailleurs, Python a une fonction
globale \python{len(obj)} qui donne la longueur d'un objet et qui peut
\^etre utilis\'ee pour les objets \eggword{Align} et \eggword{Container}
(elle renvoie le nombre d'\'echantillons de l'ingroup pour l'objet). \\

\noindent
Le fichier \verb|align1.fas| contient des labels, dans chaque nom
d'\'echantillon, sp\'ecifiant la structure ainsi que la pr\'esence
d'\'echantillons appartenant au groupe externe. Par d\'efaut, ces labels
sont ignor\'es et sont consid\'er\'es comme faisant partie int\'egrante
du nom des \'echantillons.

\begin{task}
Afficher le nom du premier \'echantillon de l'alignement actuel.
Rep\'erer les labels de structure \`a la fin du nom. Puis importer
de nouveau l'alignement en activant l'option qui permet d'importer
les labels de structure. Afficher de nouveau le nom du premier
\'echantillon. Consulter le nombre d'\'echantillons de l'ingroup et de
l'outgroup.
\end{task}

\noindent
$\Rightarrow$ Pour lire le nom d'un \'echantillon \`a une position
\verb|i| donn\'ee de l'alignement, on peut utiliser la \emph{m\'ethode}
(fonction attach\'ee \`a un objet) \eggword{get\_name()}. Pour importer
les labels de structure, il faut passer l'option
\python{groups=True} \`a la fonction \eggword{egglib.io.from\_fasta()},
sachant que la valeur par d\'efaut est \python{False}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Manipuler les \'echantillons}

\begin{task}
Extraire le premier \'echantillon de l'alignement actuel et
examiner la valeur de son attribut \eggword{name}.
\end{task}

\noindent
$\Rightarrow$ Sous Python, l'expression \python{seq[i]} permet
d'extraire l'\'el\'ement \python{i} de la \emph{s\'equence}
\python{seq}, o\`u l'\'el\'ement est identifi\'e par sa position ou
indice (la premi\`ere position \'etant 0). Une s\'equence est tout objet
contenant des \'el\'ements class\'es dans un ordre d\'etermin\'e. Cette
syntaxe fonctionne avec les objets \eggword{Container} et
\eggword{Align}. Les \'el\'ements renvoy\'es (du type
\eggword{SampleView}) repr\'esentent les \'echantillons contenus dans
l'objet.

\begin{task}
Dans une boucle, afficher le nom de tous les \'echantillons du groupe
principal (ingroup) de l'alignement.
\end{task}

\noindent
$\Rightarrow$ Il faut utiliser l'expression
\python{for variable in iterable}, o\`u \python{iterable} est un
un objet qui peut contenir plusieurs
\'el\'ements (ce qui est le cas, entre autres, des s\'equences), et o\`u
\python{variable} est la variable qui contient, \`a chaque tour de la
boucle, l'\'el\'ement courant. \eggword{Container} et
\eggword{Align}, it\'erables et l'it\'eration renvoie
les m\^emes \eggword{SampleView} que l'op\'erateur \verb|[]|, pour tous
les \'echantillons de l'ingroup.

\begin{task}
Dans une autre boucle, faire de m\^eme avec les \'echantillons de
l'ougroup.
\end{task}

\noindent
$\Rightarrow$ Pour cela il faut utiliser un autre it\'erateur~: la
m\'ethode \eggword{iter\_outgroup()} renvoie un it\'erateur parcourant
les \'echantillons de l'outgroup (c'est \`a dire qu'on peut it\'erer
sur la valeur renvoy\'ee par cette m\'ethode). Il existe une autre
m\'ethode, \eggword{iter()}, qui permet d'it\'erer indiff\'eremment sur
l'ingroup et/ou l'outgroup.

\begin{task}
Consid\'erons \`a nouveau le premier \'echantillon de l'alignement.
Afficher \`a nouveau son nom, le modifier, et
v\'erifier que le nom a bien \'et\'e modifi\'e. V\'erifier que le
changement a affect\'e l'objet alignement lui-m\^eme.
\end{task}

\noindent
$\Rightarrow$ L'attribut \eggword{name} des \eggword{SampleView} est
accessible en lecture/\'ecriture et peut donc \^etre modifi\'e. Une
m\'ethode alternative pour acc\'eder au nom des \'echantillons est la
m\'ethode \python{get_name(i)} (aussi pour les
\eggword{Container}) qui acc\`ede au nom de l'\'echantillon \`a la
position \verb|i|. Cela doit vous permettre de r\'ealiser que les
\eggword{SampleView} sont de simples proxy pour manipuler les donn\'ees
contenues dans un \eggword{Align}/\eggword{Container} et ne contiennent
pas eux-m\^emes les donn\'ees.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{\'Editer les s\'equences}

\begin{task}
Afficher le type des objets correspondant \`a l'alignment, \`a un \'echantillon
et \`a sa s\'equence.
\end{task}
\noindent
$\Rightarrow$ La fonction globale de Python \python{type(obj)} peut \^etre
utilis\'ee pour tout objet, y compris les objets d\'efinis dans
\eggword{EggLib}. La s\'equence de l'\'echantillon est stock\'ee dans
l'attribut \eggword{sequence} de cet objet.

\begin{task}
Convertissez la s\'equence en type \mintinline{python}{str}.
\end{task}

\noindent
$\Rightarrow$ Les objets du type \eggword{SequenceView} ne sont pas
automatiquement convertis en cha\^ines de caract\`eres parce qu'ils sont
susceptibles de contenir des valeurs en-dehors de la gamme des
caract\`eres imprimables. Si vous savez que les valeurs contenues dans
le jeu de donn\'ees sont imprimables (typiquement, s'il s'agit de
donn\'ees de s\'equences import\'ees depuis un fichier Fasta), vous
pouvez utiliser la m\'ethode \eggword{string()} des objets
\eggword{SequenceView} pour g\'en\'erer une cha\^ine de caract\`eres.

\begin{task}
Extraire un \'el\'ement de la s\'equence (utiliser un indice inf\'erieur \`a 8942). Extraire les
valeurs correspondant aux dix premi\`eres positions de cette s\'equence
(en une seule fois).
\end{task}

\noindent
$\Rightarrow$ Les objets du type \eggword{SequenceView} permettent
l'acc\`es par indice via l'op\'erateur \verb|[]|. On peut passer un
indice allant de 0 \`a \verb|ls-1| (ou un indice n\'egatif pour compter
\`a partir de la fin, \python{-1} correspondant \`a la derni\`ere
position), mais aussi un \emph{slice}, c'est \`a dire une plage de
valeurs. Les slices sont d\'ecrits dans le tutorial de Python
(\url{http://docs.python.org/2/tutorial/introduction.html#strings}) \`a
propos des cha\^ines de caract\`eres, mais l'explication s'applique \`a
\eggword{SequenceView}, entre autres. \\

\noindent
Vous pouvez observer que les s\'equences ont \'et\'e automatiquement
converties en suite d'entiers. \eggword{Container} et \eggword{Align}
sont con\c{c}us pour accepter toutes les valeurs num\'eriques enti\`eres
(m\^eme n\'egatives), et si des caract\`eres sont fournis, ils sont
cod\'es par leur valeur enti\`ere. Notez que vous si vous entrez
des caract\`eres sp\'eciaux dans vos s\'equences, vous pouvez \^etre
expos\'es \`a des probl\`emes de compatibilit\'e parce que l'encodage
peut varier. Cependant, les caract\`eres ASCII (voir
\url{http://www.asciitable.com}) sont toujours cod\'es par le m\^eme
entier. \\

\noindent
Pour obtenir le code ASCII d'un caract\`ere, utiliser la fonction
globale de Python \python{ord(c)}. Pour faire l'inverse, utiliser
\python{chr(i)}.

\begin{task}
Assigner la valeur \verb|N| \`a la premi\`ere position de la s\'equence
de l'\'echantillon que vous \^etes en train de consid\'erer.
\end{task}

\noindent
$\Rightarrow$ Pour assigner nouvelle valeur \`a n'importe quelle position
d'une s\'equence, on peut tout simplement utiliser la syntaxe
\python{seq[i] = x}. On peut
passer indiff\'eremment une \python{str} ou un entier.

\begin{task}
Remplacer les dix premi\`eres bases de la s\'equence
par la cha\^ine de caract\`eres suivante~: \verb|CGGTGAGCCA|. Essayer
avec une cha\^ine de caract\`eres de longueur diff\'erente. Que se
passe-t-il~?
\end{task}

\noindent
$\Rightarrow$ Pour \'ecrire une plage de valeurs, on peut utiliser les
slices, gr\^ace \`a la syntaxe suivante~: \python{seq[a:b] = values},
o\`u \python{seq} est la s\'equence (ici \c{c}a sera un objet
\eggword{SequenceView}), \python{a} et \python{b} sont les limites du
slice, et \python{values} est une s\'equence contenant les nouvelles
valeurs. Dans le cas d'un \eggword{Align}, aucune op\'eration qui ferait
que diff\'erentes s\'equences aient une longueur diff\'erente n'est
possible. Sachez que cette limitation n'existe pas pour les
\eggword{Container}.

\begin{task}
En utilisant \`a chaque fois une m\'ethode de la classe \eggword{Align},
acc\'eder aux donn\'ees suivantes de l'alignement~: nom du premier
\'echantillon, premi\`ere s\'equence, premier
label de structure du premier \'echantillon, premi\`ere base du premier
\'echantillon, puis modifier cette base, puis v\'erifier que votre
changement a \'et\'e pris en compte.
\end{task}

\noindent
$\Rightarrow$ Pour cela on utilisera les m\'ethodes suivantes des
classes \eggword{Align} et \eggword{Container}~: \eggword{get\_name(i)},
\eggword{get\_sequence(i)}, \eggword{get\_label(i,j)},
\eggword{get\_i(i,j)} et \eggword{set\_i(i,j)}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{\'Editer les alignements}

\begin{task}
Cr\'eer un nouvel objet \eggword{Align} vide.
Stocker le quatri\`eme \'echantillon de l'alignement dans une variable.
L'ajouter \`a l'
\eggword{Align} que nous venons de cr\'eer (ignorer les labels de
structure). Enfin, supprimer cet \'echantillon de l'alignement original.
V\'erifier que les attributs \eggword{ns} des deux objets refl\`etent
bien ces deux op\'erations.
\end{task}

\noindent
$\Rightarrow$ Pour cr\'eer un objet \eggword{Align} sans importer de donn\'ees,
on utilise la syntaxe \python{aln = egglib.Align()}. Pour le reste,
les m\'ethodes \eggword{add\_sample()} et
\eggword{del\_sample()} permettent d'accomplir ces op\'erations.

\begin{itemize}
    \item \eggword{add\_sample()} prend trois arguments~: le nom, la
          s\'equence et les labels de structure. Le nom doit \^etre une
          \python{str}~; la s\'equence un \eggword{SequenceView} ou une
          \python{str} et les labels un \eggword{GroupView} ou une liste
          d'entiers. Les labels peuvent \^etre omis (dans ce cas, la
          valeur par d\'efaut 0 est utilis\'ee).
    \item \eggword{del\_sample()} prend l'indice de l'\'echantillon \`a
          supprimer. Notez que si vous \linebreak[4] supprimez un \'echantillon pour
          lequel il existe un \eggword{SampleView}, il ne faut plus
          utiliser celui-ci.
\end{itemize}

\begin{task}
\`A partir de l'alignement d\'ej\`a disponible, cr\'eer un nouvel
alignement avec la region allant de la position 100 \`a la position 200
(comprises). V\'erifier le nombre d'\'echantillons et la longueur de
l'alignement.
\end{task}

\noindent
$\Rightarrow$ Utiliser la m\'ethode \eggword{extract(start, stop)}
(disponible uniquement pour les \eggword{Align}), qui prend comme arguments la
premi\`ere position \`a extraire et la position \`a laquelle on arr\^ete
d'extraire les donn\'ees (comme pour les slices, cette position n'est
pas extraite).

\begin{task}
Sauvegarder le fichier r\'esultant format Fasta.
\end{task}

\noindent
$\Rightarrow$ Pour exporter les donn\'ees au format Fasta, il faut
utiliser la m\'ethode \eggword{Align.to\_fasta()}, qui prend comme argument
optionnel le nom du fichier de sortie (par d\'efaut, la repr\'esentation Fasta
est renvoy\'ee la forme d'une \python{str} par la fonction).

\begin{task}
De mani\`ere analogue, extraire uniquement les positions 0, 10, 20, 25,
30 et 50. Afficher \'egalement les
dimensions de l'alignement g\'en\'er\'e.
\end{task}

\noindent
$\Rightarrow$ Utiliser la seconde syntaxe de la m\'ethode
\eggword{extract(positions)}, qui accepte une liste d'indices.

\begin{task}
Cr\'eer un nouvel alignement, avec toutes les positions, mais seulement
les \'echantillons 0, 1, 2, 5, 10, 15, 20, 22, 25 et 28 de l'ingroup.
Afficher les dimensions du r\'esultat.
\end{task}

\noindent
$\Rightarrow$ La m\'ethode \eggword{subset(samples)} est disponible pour
les objets \eggword{Align} et \eggword{Container}, et extraie une liste
donn\'ee d'\'echantillons de l'ingroup identifi\'es par leur position
(il est aussi possible d'extraire des \'echantillons de l'outgroup).

\begin{task}
Extraire deux parties de l'alignement~: les positions 0 \`a 4470
(comprises) dans un objet, et toutes les positions suivantes (\`a partir
de 4471) dans un deuxi\`eme objet. Concat\'ener les deux
sous-alignements dans un alignement reconstituant l'alignement original
mais en ins\'erant un bloc de 1000 positions non-d\'etermin\'ees entre
les deux, \`a l'aide de la fonction \eggword{egglib.tools.concat()}.
\end{task}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Traduire des s\'equences codantes}

\begin{task}
On consid\`ere l'alignement original (celui qui \'etait stock\'e dans le
fichier \verb|align1.fas|). G\'en\'erer l'alignement de prot\'eines
correspondant, sachant que les positions des quatre exons sont~:
0-441, 1419-4014, 5258-5960 et
6605-8942, et que le code g\'en\'etique standard peut \^etre utilis\'e.
Afficher ses dimensions et sauvegardez l'alignement de prot\'eines dans
un fichier fasta.
\end{task}

\noindent
$\Rightarrow$ La fonction \eggword{egglib.tools.translate()} permet de
traduire des s\'equences directement. Cependant, comme les s\'equences
codantes sont fragment\'ees (pr\'esence d'introns), il faut fournir \`a
\eggword{translate()} le cadre de lecture, sous la forme d'un objet \linebreak
\eggword{egglib.tools.ReadingFrame}. Ce type d'objets est cr\'e\'e en
appelant le constructeur de la classe en lui fournissant les positions
des exons sous la forme d'une liste de paires de positions. L'\'enonc\'e
de l'exercice fournit les positions que vous devez passer au
constructeur de \eggword{ReadingFrame} (pour chaque exon~: premi\`ere
position et position imm\'ediatement apr\`es la derni\`ere).

\begin{task}
D\'eterminer le nombre de codons stop finaux dans l'alignement (celui
qui a \'et\'e lu \`a partir de \verb|align1.fas|), en utilisant le cadre
de lecture pr\'ec\'edemment fourni. De m\^eme tester si l'alignement
contient au moins un codon stop.
\end{task}

\noindent
$\Rightarrow$ Utiliser les fonctions \eggword{egglib.tools.trailing\_stop()}
et \eggword{egglib.tools.has\_stop()}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\newpage

\section{Exercices 2 -- Calcul de statistiques de diversit\'e nucl\'eotidique}

\noindent
Ces exercices abordent les outils permettant l'analyse du polymorphisme
de s\'equences avec pour objectif le calcul de statistiques. En l'\'etat
actuel, l'essentiel des fonctionnalit\'es est accessible par la classe
\eggword{egglib.stats.ComputeStats}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Calcul de statistiques de base}

\begin{task}
Cr\'eer une instance de la classe \eggword{egglib.stats.ComputeStats} et
programmer le calcul des statistiques suivantes~: \verb|S|, \verb|lseff|,
\verb|thetaW|, \verb|Pi|, \verb|D| et \verb|Hsd|.
\end{task}

\noindent
$\Rightarrow$ La documentation de la classe \eggword{ComputeStats}
contient la liste des statistiques qui peuvent \^etre calcul\'ees.
Les objets \eggword{ComputeStats}
poss\`edent une m\'ethode \eggword{add\_stats()}
(qui peut \^etre appel\'ee plusieurs fois \`a la suite)
qui permet d'ajouter des statistiques \`a calculer.

\begin{task}
Importer l'alignement \verb|align1.fas| (avec les labels de structure).
Utiliser \eggword{ComputeStats} pour calculer les statistiques puis les
stocker dans une variable avant de les afficher.
\end{task}

\noindent
$\Rightarrow$ La m\'ethode \eggword{process\_align()} renvoie un
\python{dict} contenant toutes les statistiques qui ont \'et\'e requises
\`a l'aide de \eggword{add\_stats()}.

\begin{task}
Afficher le nombre de sites utilis\'es dans l'analyse (statistique
\verb|lseff|). En comparaison, afficher le nombre de sites total
de l'alignement. Afficher la statistique \verb|thetaW|, et l'exprimer
par site analys\'e.
\end{task}

\noindent
$\Rightarrow$ La diff\'erence entre les deux valeurs de nombres de sites
correspond aux sites qui ont \'et\'e exclus de l'analyse \`a cause d'un
exc\`es de donn\'ees manquantes (nucl\'eotides non caract\'eris\'es,
ambigu\"{i}t\'es et gaps d'alignement), ou \`a la pr\'esence de plus de deux
all\`eles \`a un site donn\'e.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Prise en compte de la structure}

\begin{task}
Remettre la liste de statistiques \`a calculer \`a z\'ero. Puis calculer
les statistiques suivantes \`a partir
du m\^eme alignement~: \verb|Dj|, \verb|Snn|, \verb|WCst|. Les
statistiques sont-elles calcul\'ees~?
\end{task}

\noindent
$\Rightarrow$ Pour remettre la liste des statistiques \`a z\'ero, utiliser
la m\'ethode \eggword{clear\_stats}.
Les statistiques en question n\'ecessitent une structure. Or \eggword{ComputeStats}
n'examine pas automatiquement les labels de structure pr\'esents dans les
\eggword{Align}~. Comme aucune structure n'est connue, les statistiques
n'ont pas \'et\'e calcult\'ee.

\begin{task}
Cr\'eer une instance de la classe \eggword{Structure} repr\'esentant la
structure en populations de l'alignement contenu dans le fichier
\verb|align1.fas|.
\end{task}

\noindent
$\Rightarrow$ Les objets repr\'esentant les groupes de structure (classe \eggword{Structure})
peuvent \^etre cr\'e\'es \`a partir des labels d'un \eggword{Align} par
la fonction \eggword{egglib.stats.get\_structure()}. En fonction des labels
pr\'esents (plusieurs colonnes de labels peuvent \^etre entr\'ees, y compris
contradictoires) plusieurs structures diff\'erentes peuvent \^etre g\'en\'er\'ees
\`a partir d'une m\^eme liste de labels. Ici on utilisera la premi\`ere
colonne de labels (qui correspond aux populations), que l'on sp\'ecifiera
avec l'option \python{lvl_pop}.

\begin{task}
Calculer les statistiques demand\'ees en utilisant l'objet \eggword{Structure} ainsi
g\'en\'er\'e.
\end{task}

\noindent
$\Rightarrow$ La m\'ethode \eggword{process\_align()} a une
option \verb|struct| pour passer un objet \eggword{Structure}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Statistiques synonymes et non-synonymes}

\begin{task}
Utiliser l'objet \eggword{ReadingFrame} (ou en cr\'eer un nouveau)
correspondant aux bornes des exons de l'alignement provenant du fichier
\verb|align1.fas|.
\end{task}

\noindent
$\Rightarrow$ Se reporter \`a la s\'erie d'exercices pr\'ec\'edente.

\begin{task}
Cr\'eer un objet \eggword{egglib.stats.CodingDiversity} \`a partir de
l'objet \eggword{Align} et du cadre de lecture correspondants.
\end{task}

\noindent
$\Rightarrow$ Le constructeur de la classe prend tous les arguments
n\'ecessaires.

\begin{task}
Afficher le nombre de codons utilis\'es pour l'analyse, les estimations
des nombres de sites nucl\'eotidiques synonymes et non-synonymes, ainsi
que le nombre de sites de codons polymorphes (synonymes et
non-synonymes).
\end{task}

\noindent
$\Rightarrow$ Voir les attributs correspondants des instances de la
classe \eggword{CodingDiversity}.

\begin{task}
Cr\'eer deux \eggword{Align} contenant les sites variables,
respectivement synonymes et non-synonymes. Afficher le nombre
d'\'echantillons et de sites de ces deux objets.
\end{task}

\noindent
$\Rightarrow$ Utiliser les m\'ethodes correspondantes de
\eggword{CodingDiversity}.

\begin{task}
Cr\'eer un nouvel objet \eggword{ComputeStats}. Sp\'ecifier les
statistiques suivantes~: \verb|lseff|, \verb|S|, \verb|D|, \verb|thetaW| et
\verb|Pi| et lancer le calcul pour les deux \eggword{Align}.
Il est n\'ecessaire d'utiliser l'option \verb|filter|.
Stocker les r\'esultats dans deux variables, les afficher et les comparer.
\end{task}

\noindent
$\Rightarrow$ Les \eggword{Align} renvoy\'es par
\eggword{CodingDiversity} contiennent comme donn\'ees des entiers
repr\'esentant les 64 codons possibles (plage de valeurs allant de 0 \`a
63, avec la valeur 64 repr\'esentant toutes les donn\'ees manquantes).
L'objet pr\'e-d\'efini correspondant \`a cette d\'efinition est
\eggword{egglib.stats.filter\_codon}. Cet objet peut \^etre utilis\'e
comme valeur pour l'option \verb|filter| de \eggword{process\_align()}.

\begin{task}
Afficher les valeurs des statistiques \verb|Pi| et \verb|thetaW| par
site. Quelle valeur faut-il utiliser pour diviser~? (Attention, les
\eggword{Align} g\'en\'er\'es par \eggword{CodingDiversity} ne
contiennent que les sites variables.)
\end{task}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\newpage

\section{Exercices 3 -- Analyse de donn\'ees g\'enomiques}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Lire des donn\'ees VCF}

Le format VCF (\emph{variant call format}) permet de stocker des
donn\'ees de diversit\'e g\'enomique (aussi bien des SNP ou des
insertions/d\'el\'etions que des variants structuraux de grande
\'echelle).

\begin{task}
Cr\'eer un objet \eggword{egglib.io.VcfParser} initialis\'e \`a partir
du fichier de donn\'ees \linebreak[4] \verb|genome.vcf|. Quel est le
nombre d'\'echantillons contenus dans ce fichier~? Afficher la liste des
noms d'\'echantillons.
\end{task}

\noindent
$\Rightarrow$ Les objets \eggword{VcfParser} poss\`edent un attribut
\eggword{num\_samples}, ainsi qu'une m\'ethode \eggword{get\_sample()}
pour acc\'eder au nom d'un \'echantillon donn\'e.

\begin{task}
Appeler sa m\'ethode
\eggword{next()} et examiner ce qu'elle renvoie. G\'en\'erer les
donn\'ees pour le premier variant. Quel est le type des objets
utilis\'es pour repr\'esenter un variant~?
\end{task}

\noindent
$\Rightarrow$ Les objets \eggword{VcfParser} peuvent \^etre it\'er\'es,
soit avec le mot-cl\'e \python{for}, soit avec leur m\'ethode
\eggword{next()}. Dans les deux cas, l'utilisateur ne re\c{c}oit qu'un
r\'esum\'e de la ligne lue, et il doit utiliser la m\'ethode
\eggword{last\_variant()} pour extraire toutes les donn\'ees
correspondant \`a la derni\`ere ligne lue.

\begin{task}
Examiner les membres suivants du variant~: \eggword{chromosome},
\eggword{position}, \eggword{num\_alleles}, \eggword{alleles},
\eggword{AA}, \eggword{AC}, \eggword{AN}, \eggword{info}, et, en
particulier (\`a partir de la valeur contenue dans \eggword{info}),
la profondeur de s\'equen\c{c}age.
\end{task}

\begin{task}
Afficher la longueur de l'attribut \eggword{GT} du variant. Comparer
cette valeur avec le nombre d'\'echantillons que nous avons obtenu un
petit peu plus t\^ot. Afficher le premier, puis le troisi\`eme
\'el\'ement de l'attribut \eggword{GT}.
\end{task}

\noindent
$\Rightarrow$ \eggword{GT} contient les g\'enotypes estim\'es \`a partir
des donn\'ees g\'enomiques. Comme pour d'autres valeurs (\eggword{AA},
\eggword{AC}, \eggword{AN}, par exemple), ce champ n'est pas forc\'ement
pr\'esent dans les fichiers VCF. Avant d'acc\'eder \`a ces donn\'ees, il
faut se r\'ef\'erer \`a la documentation du jeu de donn\'ees ou tester
leur pr\'esence. Si \verb|GT| n'est pas pr\'esent dans le fichier VCF mais
que les donn\'ees du type \verb|PL| le sont (il s'agit des probabilit\'es
de chaque g\'enotype possible), il est possible d'effectuer le
\emph{genotype calling} automatiquement (voir l'option \verb|threshold_PL|
du constructeur de \eggword{VcfParser}).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Calculer des statistiques depuis un site d'un VCF}

\begin{task}
Extraire les donn\'ees des g\'enotypes du variant courant du fichier
VCF. Quel est le type renvoy\'e~? Quel est le nombre d'\'echantillons
disponibles pour ce site~?
\end{task}

\noindent
$\Rightarrow$ Si le champ \eggword{GT} est disponible, la m\'ethode
\eggword{VcfParser.get\_genotypes()} permet de cr\'eer un objet
\eggword{Site} contenant les donn\'ees g\'enotypiques (par d\'efaut,
l'option \verb|get_genotypes| est \python{False}, ce qui signifie que
la structure en individus est ignor\'ee et que les donn\'ees sont
import\'ees sous forme d'all\`eles ind\'ependants).
Les objets \eggword{Site} disposent des attributs
\eggword{ns} et \eggword{num\_missing}.

\begin{task}
Cr\'eer un objet \eggword{ComputeStats} et sp\'ecifier les
statistiques suivantes~: \verb|He|, \verb|Fis|, \verb|A| et
\verb|ns_site|. Calculer ces statistiques pour le site.
\end{task}

\noindent
$\Rightarrow$ La m\'ethode \eggword{process\_site()} de
\eggword{ComputeStats} permet d'analyser un site unique.

\begin{task}
Dans une boucle, lire les 22 variants suivants. Afficher les donn\'ees
suivantes pour chaque it\'eration~:

\begin{itemize}
    \item Num\'ero du chromosome.
    \item Position.
    \item Nombre d'all\`eles tel qu'indiqu\'e dans le fichier VCF.
    \item Fr\'equences all\'eliques.
    \item Statistiques telles que calcul\'ees pour le premier site.
\end{itemize}

\noindent
Quel est le probl\`eme avec le dernier site~?
\end{task}

\noindent
$\Rightarrow$ Pour it\'erer sur un nombre fixe de pas, on peut
utiliser la fonction globale \python{range()} qui renvoie une liste
d'entiers. Les trois premiers \'el\'ements sont ceux renvoy\'es par
\eggword{next()}. Vous aurez besoin d'extraire le \eggword{Site}
de chaque site avec l'option \python{get_genotypes=True}

\begin{task}
Extraire le \eggword{Variant} de la ligne courante. Afficher les
all\`eles pr\'esents \`a ce site, le nombre d'all\`eles du dernier objet
\eggword{Site} (correspondant \`a la m\^eme ligne du fichier VCF), ainsi que
la liste de ces all\`eles.
\end{task}

\noindent
$\Rightarrow$ Le nombre d'all\`eles d\'efini dans le VCF (et donc indiqu\'e
par les classes \eggword{VcfParser} et \eggword{Variant}) n'est pas forc\'ement
\'egal aux nombres d'all\`eles effectivement pr\'esents dans les donn\'ees
une fois que le \emph{genotype calling} a \'et\'e effectu\'e.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Analyse de blocs de sites depuis un VCF}

\begin{task}
Cr\'eer une liste vide. Pour les 20 lignes suivantes du VCF,
extraire l'objet \eggword{Site} correspondant \`a chaque ligne et le
charger dans la liste.
\end{task}

\noindent
$\Rightarrow$ Pour cr\'eer une liste vide, on peut utiliser la syntaxe
\python{sites=[]}. Les listes (objets \python{list}) ont une m\'ethode
\python{append(x)} pour ajouter un objet.

\begin{task}
En utilisant \eggword{ComputeStats}, calculer les statistiques suivantes~:
\verb|S|, \verb|nseff|, \verb|lseff|, \verb|D|.
\end{task}

\noindent
$\Rightarrow$ On peut utiliser \eggword{process\_sites()} pour analyser
une liste de sites.

\begin{task}
Cr\'eer une fen\^etre glissante \`a partir de la position actuelle du VCF
en sp\'ecifiant une fen\^etre de 20 sites et un pas de 10 sites.
\end{task}

\noindent
$\Rightarrow$ La m\'ethode \eggword{VcfParser.slider()} renvoie un objet
du type \eggword{VcfWindow}. Il arriver au r\'esultat demand\'e, il faut
utiliser les options \verb|size_as_sites| et \verb|step_as_sites|.
Par d\'efaut, l'objet est renvoy\'e pr\^et \`a
lire des donn\'ees mais n'en contient pas encore.

\begin{task}
Calculer les m\^emes statistiques pour chaque fen\^etre, jusqu'\`a ce que la
fin du contig courant soit atteinte. Afficher le d\'ebut et la fin de chaque
fen\^etre, ainsi que les valeurs des statistiques.
\end{task}

\noindent
$\Rightarrow$ Dans un \python{while}, on utilisera l'attribut
\eggword{good} de l'objet \eggword{VcfWindow} pour v\'erifier que la
boucle peut continuer et
sa m\'ethode \eggword{next()} pour avancer. L'objet \eggword{VcfWindow}
lui-m\^eme peut \^etre utilis\'e comme argument de \eggword{ComputeStats.process\_sites()}.
Les bornes de la fen\^etre peuvent \^etre obtenues avec les deux attributs
\eggword{start\_bound} et \eggword{end\_bound}.

\end{document}
