import egglib

# import structure
pops = {}
with open('strata.txt') as f:
    for line in f:
        line = line.split()
        k = line.pop(0)
        pops[k] = line
print(pops)
rpops = {}
for k, idv in pops.items():
    for i in idv:
        assert i not in rpops
        rpops[i] = k
print(rpops)

# import fasta
aln = egglib.io.from_fasta('list.fasta', alphabet=egglib.alphabets.DNA)
print(aln.ns)
print(aln.ls)

# create structure object
struct = {None: {k: {i: [] for i in idv} for k, idv in pops.items()}}
print(struct)
c = 0
for sam in aln:
    idv, allele = sam.name.split('_')
    k = rpops[idv]
    struct[None][k][idv].append(c)
    c += 1
print(struct)
struct = egglib.struct_from_dict(struct, None)

# compute stats
cs = egglib.stats.ComputeStats(struct=struct)
cs.add_stats('FstWC', 'S', 'D', 'Fis')
print(cs.process_align(aln, max_missing=0.7))

# process sites
cs.clear_stats()
cs.add_stats('FstWC', 'Fis', 'Aing', 'ns_site')
for site in aln.iter_sites():
    print(cs.process_site(site))
