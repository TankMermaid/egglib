ln = 0
with open('chr4B.vcf') as f:
    with open('chr4B.mod.vcf', 'w') as g:
        for line in f:
            ln += 1
            if ln%1000==0: print(f'{ln//1000}K')
            if 'Reference genome' in line:
                line = line.replace('Reference genome', 'Reference_genome')
                print(line.strip())
            g.write(line.rstrip()+'\n')
