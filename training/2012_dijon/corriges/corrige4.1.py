import egglib

cds = egglib.Container('source.fas')
db = egglib.Container('database.fas')
BLASTdb = egglib.wrappers.BLASTdb(db, 'nucl')
BLAST = egglib.wrappers.BLAST()

hits = BLAST.blastn(cds, BLASTdb)
for i in hits:
    for j in hits[i]:
        name = j['subject']
        if name not in cds:
            seq = db.sequenceByName(name)
            if len(seq) > 1500:
                cds.append(name, seq)

cds.write('sequences.fas')

prot = egglib.tools.translate(cds)
protaln = egglib.wrappers.clustal(prot, quiet=False)
aln = egglib.tools.backalign(cds, protaln)

tree, lk = egglib.wrappers.phyml(aln, quiet=False)
tree.write('tree1.tre')
