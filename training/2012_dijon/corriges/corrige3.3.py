import os, egglib

files = os.listdir('fasta')

# redo 2.1

nA = 0
nC = 0
nG = 0
nT = 0

for fname in files:
    aln = egglib.Align('fasta/' + fname)
    for item in aln:
        nA += item.sequence.count('A')
        nC += item.sequence.count('C')
        nG += item.sequence.count('G')
        nT += item.sequence.count('T')

print 'Total number of A:', nA
print 'Total number of C:', nC
print 'Total number of G:', nG
print 'Total number of T:', nT
    
# redo 2.3

for i in os.listdir('fasta2'):
    os.remove('fasta2/' + i)
    
for fname in files:
    aln = egglib.Align('fasta/' + fname)
    for item in aln:
        chars = []
        for char in item.sequence:
            if char in 'ACGT':
                chars.append(char)
            else:
                chars.append('N')
        seq = ''.join(chars)
        item.sequence = seq
    aln.write('fasta2/' + fname)
