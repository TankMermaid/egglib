import os, egglib

files = os.listdir('fasta')

Stotal = 0
fixed = 0

for fname in files:
    aln = egglib.Align('fasta/' +  fname)
    pol = aln.polymorphism()
    print fname, pol['S'], pol['D']
    Stotal += pol['S']
    if pol['S'] == 0:
        fixed += 1

print 'Total S:', Stotal, ' - Loci fixed:', fixed

Stotal = 0
fixed = 0

for fname in files:
    aln = egglib.Align('fasta/' +  fname)
    pol = aln.polymorphism(minimumExploitableData=0.5)
    Stotal += pol['S']
    if pol['S'] == 0:
        fixed += 1

print 'Total S:', Stotal, ' - Loci fixed:', fixed
