import os

countA = 0
countC = 0
countG = 0
countT = 0

files = os.listdir('fasta')

for fname in files:
    f = open('fasta/' + fname)

    for line in f:
        if line[0] != '>':
            countA += line.count('A')
            countC += line.count('C')
            countG += line.count('G')
            countT += line.count('T')
    f.close()
        
print 'Total number of A:', countA
print 'Total number of C:', countC
print 'Total number of G:', countG
print 'Total number of T:', countT
