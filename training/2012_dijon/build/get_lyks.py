import egglib, random

c = egglib.Container('LYK_source.fas')

cds = egglib.Container('/home/demita/Archives/genomes/Mlp/Mlaricis_populina.FrozenGeneCatalog_20110215.CDS.fasta')

cnt = 0
#for i in ['VvLYK2', 'VvLYK3', 'PtLYK3', 'GmLYK3', 'LjLYS7',
#          'MtLYK8', 'VvLYK1', 'PtLYK1', 'PtLYK2', 'MtLYK2',
#          'LjNFR1a', 'GmNFR1a', 'GmNFR1b', 'LjNFR1c', 'MtLYK6',
#          'LjNFR1b', 'MtLYK1', 'GmLYK2', 'MtLYK7', 'GmLYK2b', 'LjLYS6', 'MtLYK9', 'MtLYK4']:

for i in [ 'MtLYK2', 'LjNFR1a', 'GmNFR1a', 'GmNFR1b', 'LjNFR1c', 'MtLYK6',
          'LjNFR1b', 'MtLYK1', 'GmLYK2', 'MtLYK7','MtLYK4']:

    cds.append('', c.sequenceByName(i))
    cnt += 1

print cnt
    
pos = range(len(cds))
random.shuffle(pos)

cds2 = egglib.Container()
for i in range(len(cds)):
    cds2.append(str(i+1).rjust(5, '0'), cds.sequence(pos[i])[:-3])

#for i in [ 'MtLYK2', 'LjNFR1a', 'GmNFR1a', 'GmNFR1b', 'LjNFR1c', 'MtLYK6',
#          'LjNFR1b', 'MtLYK1', 'GmLYK2', 'MtLYK7','MtLYK4']:
#    cds2.append(i, c.sequenceByName(i)[:-3])

cds2.write('Exo4-database.fas')




cds3 = egglib.Container()

for i in [ 'MtLYK3', 'AtCERK1']:
    cds3.append(i, c.sequenceByName(i)[:-3])

cds3.write('Exo4-source.fas')   
