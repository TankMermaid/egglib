import egglib

### 3.1

print '### exercise 3.1.1 ###'
vcf = egglib.io.VcfParser('genome.vcf')
print 'number of samples:', vcf.num_samples
print 'samples:', [j for i in range(vcf.num_samples) for j in vcf.get_sample(i)]
print '### exercise 3.1.2 ###'
print vcf.next()
variant = vcf.last_variant()
print type(variant)

print '### exercise 3.1.3 ###'
print 'chromosome:', variant.chromosome
print 'position:', variant.position
print 'number of alleles:', variant.num_alleles
print 'alleles:', variant.alleles
print 'AA:', variant.AA
print 'AC:', variant.AC
print 'AN:', variant.AN
print 'info:', variant.info
print 'depth:', variant.info['DP']

print '### exercise 3.1.4 ###'
print 'number of items in GT:', len(variant.GT)
print 'first item:', variant.GT[0]
print 'third item:', variant.GT[2]

### 3.2

print '### exercise 3.2.1 ###'
site = vcf.get_genotypes(get_genotypes=True)
print type(site)
print 'number of samples:', site.ns
print 'missing data::', site.num_missing

print '### exercise 3.2.2 ###'
cs = egglib.stats.ComputeStats()
cs.add_stats("He", "Fis", "Aing", "ns_site")
print cs.process_site(site)

print '### exercise 3.2.3 ###'
for i in range(22):
    chrom, pos, nall = vcf.next()
    site = vcf.get_genotypes(get_genotypes=True)
    print chrom, pos, nall, cs.process_site(site)

print '### exercise 3.2.4 ###'
variant = vcf.last_variant()
print variant.alleles
print site.num_alleles
print site.alleles()

### 3.3

print '### exercise 3.3.1 ###'
array = []
for i in range(20):
    vcf.next()
    site = vcf.get_genotypes(get_genotypes=True)
    array.append(site)

print '### exercise 3.3.2 ###'
cs.clear_stats()
cs.add_stats('S', 'nseff', 'lseff', 'D')
print cs.process_sites(array)

print '### exercise 3.3.3 ###'
slider = vcf.slider(20, 10, size_as_sites=True, step_as_sites=True)

print '### exercise 3.3.4 ###'
while slider.good:
    slider.next()
    print slider.start_bound, slider.end_bound, cs.process_sites(slider)
