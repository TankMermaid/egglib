import egglib

### 2.1

print '### exercise 2.1.1 ###'
cs = egglib.stats.ComputeStats()
cs.add_stats('S', 'lseff', 'thetaW', 'Pi', 'D', 'Hsd')

print '### exercise 2.1.2 ###'
aln = egglib.io.from_fasta('align1.fas', groups=True)
stats = cs.process_align(aln)
print stats

print '### exercise 2.1.3 ###'
print 'analyzed sites:', stats['lseff']
print 'align ls:', aln.ls
print 'thetaW/site:', stats['thetaW']/stats['lseff']

### 2.2

print '### exercise 2.2.1 ###'
cs.clear_stats()
cs.add_stats('Dj', 'Snn', 'WCst')
print cs.process_align(aln)

print '### exercise 2.2.2 ###'
struct = egglib.stats.get_structure(aln, lvl_pop=0)
print struct.as_dict()

print '### exercise 2.2.3 ###'
print cs.process_align(aln, struct=struct)

### 2.3

print '### exercise 2.3.1 ###'
rf = egglib.tools.ReadingFrame([(0, 441), (1419, 4014), (5258, 5960), (6605, 8942)])

print '### exercise 2.3.2 ###'
cd = egglib.stats.CodingDiversity(aln, frame=rf)

print '### exercise 2.3.3 ###'
print 'codon sites:', cd.num_codons_eff
print 'sites syn:', cd.num_sites_S
print 'sites non-syn:', cd.num_sites_NS
print 'S syn:', cd.num_pol_S
print 'S non-syn:', cd.num_pol_NS

print '### exercise 2.3.4 ###'
align_S = cd.mk_align_S()
align_NS = cd.mk_align_NS()
print 'align S:', align_S.ns, align_S.ls
print 'align NS:', align_NS.ns, align_NS.ls

print '### exercise 2.3.5 ###'
cs = egglib.stats.ComputeStats()
cs.add_stats('lseff', 'S', 'D', 'thetaW', 'Pi')
statsS = cs.process_align(align_S, filtr=egglib.stats.filter_codon)
statsNS = cs.process_align(align_NS, filtr=egglib.stats.filter_codon)
print 'synonymous stats:'
print statsS
print 'non-synonymous stats:'
print statsNS

print '### exercise 2.3.6 ###'
print 'Pi[S]/site:', statsS['Pi']/cd.num_sites_S
print 'thetaW[S]/site:', statsS['thetaW']/cd.num_sites_S
print 'Pi[NS]/site:', statsNS['Pi']/cd.num_sites_NS
print 'thetaW[NS]/site:', statsNS['thetaW']/cd.num_sites_NS

