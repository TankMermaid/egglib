print('# importing module #')
import egglib
print(egglib.__version__)

print('# import sequences #')
aln = egglib.io.from_fasta("align.fas", alphabet=egglib.alphabets.DNA)

print('# iteration #')
for sam in aln:
    print(sam.name, sam.ls)

print('# edition #')
sam = aln[4]
print(sam.name)
sam.name = 'a name'
print(sam.name)
print(sam.sequence)
print(sam.sequence.string())
print(sam.sequence[0])
print(sam.sequence[:20])
sam.sequence[:20] = '-' * 20
print(sam.sequence.string())

print('# labels #')
aln = egglib.io.from_fasta("align.fas", alphabet=egglib.alphabets.DNA, labels=True)
sam = aln[0]
print(sam.name)
print(sam.labels)
print(len(sam.labels))
print(sam.labels[0], sam.labels[1], sam.labels[2])

print('# structure objects #')
struct = egglib.struct_from_labels(aln, lvl_clust=0, lvl_pop=1, lvl_indiv=2)
print(struct.as_dict())

struct = egglib.struct_from_samplesizes([2, 2], ploidy=2, outgroup=1)
print(struct.as_dict())

labels = ['blue', 'blue', 'red', 'blue', 'red', 'red', 'red', 'blue']
struct = egglib.struct_from_iterable(labels)
print(struct.as_dict())

labels2 = [('blue', 'A'), ('blue', 'A'), ('red', 'B'), ('red', 'B'), ('blue', 'C'), ('blue', 'C'), ('red', 'D'), ('red', 'D'), ('red', 'E'), ('red', 'E'), ('blue', 'F'), ('blue', 'F')]
struct = egglib.struct_from_iterable(labels2, fmt=('P', 'I')    )
print(struct.as_dict())

with open('labels.txt') as f:
    struct = egglib.struct_from_iterable(f, fmt=('P', 'I'), function=str.split)
print(struct.as_dict())

print('# tools examples #')
aln1 = egglib.Align.create([('idv1', 'AAA'), ('idv2', 'CCC'), ('idv3', 'GGG'), ('idv4', 'TTT')], alphabet=egglib.alphabets.DNA)
aln2 = egglib.Align.create([('idv1', 'AAA'), ('idv3', 'GGG'), ('idv4', 'TTT')], alphabet=egglib.alphabets.DNA)
aln3 = egglib.Align.create([('idv1', 'AAA'), ('idv3', 'GGG'), ('idv4', 'TTT'), ('idv2', 'CCC')], alphabet=egglib.alphabets.DNA)
aln4 = egglib.tools.concat(aln1, aln2, aln3)
print(aln1.ls, aln2.ls, aln3.ls, aln4.ls)
print(aln4.fasta())
print(egglib.tools.rc('ACCGTG?MAANRV'))
print(egglib.tools.translate('ATGGGACCGCCATAG'))
aln4.to_codons()
egglib.tools.translate(aln4)

print('# VCF #')
vcf = egglib.io.VcfParser('CEU.exon.2010_03.genotypes.vcf')
print(vcf.num_samples)
print([vcf.get_sample(i) for i in range(vcf.num_samples)])
vcf.readline()
site = vcf.get_genotypes()
print(site.as_list())

print('# VCF compressé #')
import gzip
f = gzip.open('example.vcf.gz','rt')
cache = []
while True:
    line = f.readline()
    if line[:2] == '##': cache.append(line)
    elif line[:1] == '#':
        cache.append(line)
        break
    else: raise IOError('invalid file')
header = ''.join(cache)
print(header)
vcf = egglib.io.VcfStringParser(header)

line = f.readline()
print(vcf.readline(line))
site = vcf.get_genotypes()
print(site.as_list())
    
print('# stats #')
aln = egglib.io.from_fasta('dmi3.fas', alphabet=egglib.alphabets.DNA, labels=True)
cs = egglib.stats.ComputeStats()
cs.add_stats('S', 'Pi','D', 'D*')
stats = cs.process_align(aln, max_missing=0.5)
print(stats)

struct = egglib.struct_from_labels(aln, lvl_pop=0)
cs.configure(struct=struct)
cs.clear_stats()
cs.add_stats('Gst')
print(cs.process_align(aln))

cs.add_stats('Fis')
print(cs.process_align(aln))

print('# VCF sliding window #')
labels = ['a'] * 74 + ['v'] * 40 + ['a'] * 2 + ['v'] * 37
struct = egglib.struct_from_iterable(labels)
cs = egglib.stats.ComputeStats(struct=struct, multi=True)
cs.add_stats('FstWC', 'D', 'S', 'lseff')
vcf = egglib.io.VcfParser('LG15.vcf')
vcf.load_index()
vcf.goto('LG15', 2200000)
sld = vcf.slider(10000, 10000, start=2200000, stop=2250000, max_missing=50, allow_indel=False)
for win in sld:
    print(win.bounds, len(win))
    cs.process_sites(win)
print(cs.results())
