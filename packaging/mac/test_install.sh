# test egglib installation and example script in a container
test="--index-url https://test.pypi.org/simple --no-deps"
for version in "3.7" "3.8" "3.9"
do
    echo ${version}
    rm -fR venv-$version
    python${version} -m venv venv-${version}
    source venv-${version}/bin/activate
    python -m pip install --upgrade pip
    python -m pip install ${test} EggLib
    python test_egglib.py
    rm -fR venv-$version
done
