import sys, subprocess, os, shutil, re, tarfile

def run(*args):
    ret = subprocess.run(args, shell=False)
    if ret.returncode != 0:
        sys.exit('an error occurred')

for version in ['37', '38', '39']:

    # precleaning
    if os.path.isdir(f'venv-{version}'):
        shutil.rmtree(f'venv-{version}')

    # create environment
    print('creating virtual environment')
    run(rf'C:\Users\Stéphane\AppData\Local\Programs\Python\Python{version}\python.exe',
                '-m', 'venv', f'venv-{version}')

    # go to environment
    os.chdir(os.path.join(f'venv-{version}'))

    # running compulation script
    print('testing')
    with open('script.bat', 'w') as f:
        test = "--index-url https://test.pypi.org/simple --no-deps"
        script = ' & '.join([
            'cd Scripts',
            'activate',
            'cd ..',
            'python -m pip install --upgrade pip',
           f'python -m pip install {test} EggLib',
           r'python ..\test_egglib.py'])
        f.write(script)
    run('script.bat')

    # go back and clean
    print('cleaning')
    os.chdir('..')
    if os.path.isdir(f'venv-{version}'):
        shutil.rmtree(f'venv-{version}')
