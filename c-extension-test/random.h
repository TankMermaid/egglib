#ifndef EGGLIB_CLIB_RANDOM_H
#define EGGLIB_CLIB_RANDOM_H

int init(); ///< initialise with default seed (client code is required to call this) return -1 on memory error, 0 otherwise

/// set the pseudorandom number generator seed
void set_seed(unsigned long s);

/// get the seed used to configure the pseudorandom number generator
unsigned long get_seed();

double uniform(); ///< uniform real (32-bit precision)
unsigned long integer_32bit(); ///< uniform integer
int bernoulli(double p); ///< draw in bernoulli
int brand(); ///< draw a boolean
double uniformcl(); ///< uniform on closed interval 
double uniformop(); ///< uniform on open interval
double uniform53(); ///< uniform with increased precision
double erand(double expect); ///< exponential distribution
unsigned int irand(unsigned int ncards); ///< integer with defined range
unsigned int prand(double mean); ///< poisson distribution
unsigned int grand(double param); ///< geometric distribution
double nrand(); ///< normal distribution
double nrandb(double m, double sd, double min, double max); ///< bounded normal
unsigned long binomrand(long n, double p); ///< binomial distribution

#endif
