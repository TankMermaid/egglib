from distutils.core import setup, Extension

NAME = 'egglib_test'

extension = Extension(
    NAME+'.random',
    sources = ['random.c'],
    define_macros=[('PYEGGLIB', '1')])

setup(
    name = NAME,
    version = '1.0',
    description = 'This is a demo package',
    package_dir = {NAME: 'pyfiles'},
    packages = [NAME],
    ext_modules = [extension]
)
