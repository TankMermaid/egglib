#include <stdio.h>
#include "random.h"

int main(int argn, char * argv[]) {
    init();
    printf("default seed: %lu\n", get_seed());
    set_seed(30305495996607);
    printf("initial seed: %lu\n", get_seed());
    printf("32bit-integer: %lu\n", integer_32bit());
    set_seed(706595595404);
    printf("new seed: %lu\n", get_seed());
    printf("bernoulli sequence p=0.0:"); for (unsigned int i=0; i<10; i++) printf(" %d", bernoulli(0.0)); printf("\n");
    printf("bernoulli sequence p=0.1:"); for (unsigned int i=0; i<10; i++) printf(" %d", bernoulli(0.1)); printf("\n");
    printf("bernoulli sequence p=0.5:"); for (unsigned int i=0; i<10; i++) printf(" %d", bernoulli(0.5)); printf("\n");
    printf("bernoulli sequence p=0.9:"); for (unsigned int i=0; i<10; i++) printf(" %d", bernoulli(0.9)); printf("\n");
    printf("bernoulli sequence p=1.0:"); for (unsigned int i=0; i<10; i++) printf(" %d", bernoulli(1.0)); printf("\n");
    printf("brand sequence:"); for (unsigned int i=0; i<15; i++) printf(" %d", brand()); printf("\n");
    printf("uniform: %f\n", uniform());
    printf("closed uniform: %f\n", uniformcl());
    printf("open uniform: %f\n", uniformop());
    printf("53 bit uniform: %f\n", uniform53());
    printf("exponential e=0.1:"); for (unsigned int i=0; i<10; i++) printf(" %.2f", erand(0.1)); printf("\n");
    printf("exponential e=1.0:"); for (unsigned int i=0; i<10; i++) printf(" %.2f", erand(1.0)); printf("\n");
    printf("exponential e=10.0:"); for (unsigned int i=0; i<10; i++) printf(" %.2f", erand(10.0)); printf("\n");
    printf("irand n=4:"); for (unsigned int i=0; i<10; i++) printf(" %d", irand(4)); printf("\n");
    printf("irand n=50:"); for (unsigned int i=0; i<10; i++) printf(" %d", irand(50)); printf("\n");
    printf("poisson m=0.1:"); for (unsigned int i=0; i<10; i++) printf(" %d", prand(0.1)); printf("\n");
    printf("poisson m=1.0:"); for (unsigned int i=0; i<10; i++) printf(" %d", prand(1.0)); printf("\n");
    printf("poisson m=10.0:"); for (unsigned int i=0; i<10; i++) printf(" %d", prand(10.0)); printf("\n");
    printf("geometric p=0.1:"); for (unsigned int i=0; i<10; i++) printf(" %d", grand(0.1)); printf("\n");
    printf("geometric p=0.5:"); for (unsigned int i=0; i<10; i++) printf(" %d", grand(0.5)); printf("\n");
    printf("geometric p=0.9:"); for (unsigned int i=0; i<10; i++) printf(" %d", grand(0.9)); printf("\n");
    printf("normal:"); for (unsigned int i=0; i<10; i++) printf(" %f", nrand()); printf("\n");
    printf("normal bounded m=0.5 sd=1.5, min=-1, max=2:"); for (unsigned int i=0; i<10; i++) printf(" %f", nrandb(0.5, 1.5, -1, 2)); printf("\n");
    printf("binomial n=20 p=0.1:"); for (unsigned int i=0; i<10; i++) printf(" %d", binomrand(20, 0.1)); printf("\n");
    printf("binomial n=20 p=0.5:"); for (unsigned int i=0; i<10; i++) printf(" %d", binomrand(20, 0.5)); printf("\n");
    printf("binomial n=40 p=0.5:"); for (unsigned int i=0; i<10; i++) printf(" %d", binomrand(40, 0.5)); printf("\n");
    return 0;
}
