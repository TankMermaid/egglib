import egglib_test
print('content of package:', [i for i in dir(egglib_test) if i[0] != '_'])
print('content of random module:', [i for i in dir(egglib_test.random) if i[0] != '_'])

egglib_test.random.set_seed(30305495996607)
print('initial seed:', egglib_test.random.get_seed())
print('32bit-integer:', egglib_test.random.integer_32bit())

egglib_test.random.set_seed(19999978)
print('new seed:', egglib_test.random.get_seed())

for p in 0, 0.1, 0.5, 0.9, 1:
    print('bernoulli sequence p={0}: {1}'.format(p, ' '.join(map(str, [int(egglib_test.random.bernoulli(p)) for i in range(10)]))))
print("brand sequence:", " ".join([str(egglib_test.random.boolean()) for i in range(10)]))

print(f"uniform:        {egglib_test.random.uniform():.34f}")
print(f"closed uniform: {egglib_test.random.uniform_closed()}")
print(f"open uniform:   {egglib_test.random.uniform_open()}")
print(f"53 bit uniform: {egglib_test.random.uniform_53bit():.55f}")

for e in 0.1, 1, 10:
    print(f"exponential e={e}: {' '.join([f'{egglib_test.random.exponential(e):.3f}' for i in range(10)])}")
for n in 1, 4, 10:
    print(f"integer n={n}: {' '.join([f'{egglib_test.random.integer(n)}' for i in range(20)])}")

for m in 0.2, 1, 10:
    print(f"poisson m={m}: {' '.join([str(egglib_test.random.poisson(m)) for i in range(20)])}")

for p in 0.1, 0.5, 0.9:
    print(f"geometric p={p}: {' '.join([str(egglib_test.random.geometric(p)) for i in range(20)])}")
    
print(f"normal: {' '.join([f'{egglib_test.random.normal():.3f}' for i in range(10)])}")
print(f"normal bounded m=0.5 sd=1.5, min=-1, max=2: {' '.join([f'{egglib_test.random.normal_bounded(0.5, 1.5, -1, 2):.3f}' for i in range(10)])}")

for n, p in [(20, 0.1), (20, 0.5), (40, 0.5)]:
    print(f"binomial n={n} p={p}: {' '.join([str(egglib_test.random.binomial(n, p)) for i in range(20)])}")


