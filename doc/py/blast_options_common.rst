:param query: input sequence, as a :class:`str`, :class:`.SequenceView`,
    :class:`.SampleView`,  :class:`.Container` or :class:`.Align`
    object. If an EggLib object, the alphabet must be DNA.
:param db: name of a nucleotide database (such as one created with
    :func:`.makeblastdb`. Incompatible with *subject*.
:param subject: can be used alternatively to *db*. Subject sequence
    to search, as a :class:`str` or a :class:`.SequenceView` object.
:param query_loc: location on the query sequence, as a ``(start, stop)`` tuple.
    The stop position is not included in the range passed to the software.
    Not supported if query is a :class:`.Container`.
:param subject_loc: location on the target sequence, as a ``(start, stop)`` tuple.
    The stop position is not included in the range passed to the software.
:param evalue: expect value (E) for saving hits.
:param num_threads: number of CPUs to use in blast search. Can be different
    of 1 only if subject is not used.
:param word_size: length of initial exact match.
