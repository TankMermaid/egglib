:param gapopen: cost to open gaps.
:param gapextend: cost to extend gaps.
:param reward: reward for nucleotide match.
:param penalty: penalty for nucleotide mismatch.  The available
    reward/penalty combinations are listed below, with the
    gapopen/gapextend combinations available for each reward/penalty
    combination. The default gapopen/gapextend combination is indicated
    by an asterix. For megablast, except for (1,-1), the default for
    gapopen/gapextend is left up to the blastn program::

        (1,-2) --  *(5,2)  (2,2)  (1,2)  (0,2)  (3,1)  (2,1)  (1,1)
        (1,-3) --  *(5,2)  (2,2)  (1,2)  (0,2)  (2,1)  (1,1)
        (1,-4) --  *(5,2)  (1,2)  (0,2)  (2,1)  (1,1)
        (2,-3) --   (4,4)  (2,4)  (0,4)  (3,3)  (6,2) *(5,2)  (4,2)  (2,2)
        (4,-5) -- *(12,8)  (6,5)  (5,5)  (4,5)  (3,5)
        (1,-1) --  *(5,2)  (3,2)  (2,2)  (1,2)  (0,2)  (4,1)  (3,1)  (2,1)

    Defaults:

        * megablast:     (1,-2)
        * dc-megablast:  (2,-3)
        * blastn:        (2,-3)
        * blastn-short:  (1,-3)
:param strand: query strand to use: ``"both"``, ``"minus"``, or ``"plus"``.
:param no_dust: prevent DUST filtering (by default, use BLAST's default filtering mode). ``False`` by default unless for blastn-short.
:param no_soft_masking: do not apply filtering locations
    as soft masks (i.e., only for finding initial matches).
    ``False`` by default unless for blastn-short.
:param lcase_masking: use lower case filtering in query and subject
    sequences (not supported when EggLib objects are used because
    the DNA alphabet is case-insensitive and all bases will be passed
    to the blast program as upper case).
:param perc_identity: percent identify cutoff
