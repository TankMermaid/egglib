:param gapopen: cost to open a gap. ``None``: use default
:param gapextend: cost to extend a gap. ``None``: use default
:param matrix: scoring matrix name. Available values are: PAM-30,
    PAM-70, BLOSUM-80, and BLOSUM-62.
:param threshold: minimum word score such that the word is added to
    the BLAST lookup table (>0).
:param seg: filter query sequence with SEG as an integer (0 to
    disable, 1 to enable, or alternatively a tuple with the three
    parameters *window*, *locut*, and *hicut*).
:param window_size: multiple hits window size (use 0 to specify
    1-hit algorithm).
