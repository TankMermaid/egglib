.. _stats:

--------------------
Diversity statistics
--------------------

This module contains tools to analyse diversity based on population
genetics statistics.

The statistics are defined in a :ref:`dedicated section <stats-notice>`.

.. autosummary::
    egglib.stats.ComputeStats
    egglib.stats.EHH
    egglib.stats.pairwise_LD
    egglib.stats.matrix_LD
    egglib.stats.ProbaMisoriented
    egglib.stats.haplotypes_from_align
    egglib.stats.haplotypes_from_sites
    egglib.stats.CodingDiversity
    egglib.stats.paralog_pi
    egglib.stats.ParalogPi

.. autoclass:: egglib.stats.ComputeStats
    :members:

.. autoclass:: egglib.stats.EHH
    :members:

.. autofunction:: egglib.stats.pairwise_LD
.. autofunction:: egglib.stats.matrix_LD

.. autoclass:: egglib.stats.ProbaMisoriented
    :members:

.. autofunction:: egglib.stats.haplotypes_from_align
.. autofunction:: egglib.stats.haplotypes_from_sites

.. autoclass:: egglib.stats.CodingDiversity
    :members:

.. autofunction:: egglib.stats.paralog_pi

.. autoclass:: egglib.stats.ParalogPi
    :members:

