import egglib
vcf = egglib.io.VcfParser('example.vcf')
for chrom, pos, nall in vcf:
    v = vcf.last_variant()
    if 'HQ' in v.format_fields:
        print([i['HQ'] for i in v.samples])
    else:
        print('no data')
#[(51, 51), (51, 51), (None, None)]
#[(58, 50), (65, 3), None]
#[(23, 27), (18, 2), None]
#[(56, 60), (51, 51), None]
#no data




import gzip
f = gzip.open('example.vcf.gz','rt')
cache = []
while True:
    line = f.readline()
    if line[:2] == '##': cache.append(line)
    elif line[:1] == '#':
        cache.append(line)
        break
    else: raise IOError('invalid file')

header = ''.join(cache)
vcf = egglib.io.VcfStringParser(header)
site = egglib.Site()
for line in f:
    print(vcf.readline(line))
    site.from_vcf(vcf)
    print(site.as_list())
#('20', 14369, 2)
#['G', 'G', 'A', 'G', 'A', 'A']
#('20', 17329, 2)
#['T', 'T', 'T', 'A', 'T', 'T']
#('20', 1110695, 3)
#['G', 'T', 'T', 'G', 'T', 'T']
#('20', 1230236, 1)
#['T', 'T', 'T', 'T', 'T', 'T']
#('20', 1234566, 3)
#['GTC', 'G', 'GTC', 'GTCT', 'G', 'G']

