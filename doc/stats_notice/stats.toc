\contentsline {section}{\numberline {1}Single-site statistics}{3}{section.1}
\contentsline {subsection}{\numberline {1.1}Basic statistics}{3}{subsection.1.1}
\contentsline {paragraph}{\nonumberline Note}{4}{section*.2}
\contentsline {subsection}{\numberline {1.2}Weir and Cockerham's \textit {F}-statistics}{5}{subsection.1.2}
\contentsline {paragraph}{\nonumberline Diploid data, one level of structure}{5}{section*.3}
\contentsline {paragraph}{\nonumberline Diploid data, two levels of structure}{6}{section*.4}
\contentsline {paragraph}{\nonumberline Haploid data, one level of structure}{7}{section*.5}
\contentsline {paragraph}{\nonumberline Note}{8}{section*.6}
\contentsline {subsection}{\numberline {1.3}Differentiation statistics}{8}{subsection.1.3}
\contentsline {subsection}{\numberline {1.4}Allele status}{10}{subsection.1.4}
\contentsline {section}{\numberline {2}Multi-site statistics with unphased data}{10}{section.2}
\contentsline {subsection}{\numberline {2.1}Basic statistics}{10}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Neutrality tests without outgroup}{12}{subsection.2.2}
\contentsline {paragraph}{\nonumberline Note}{13}{section*.7}
\contentsline {subsection}{\numberline {2.3}Neutrality tests with outgroup}{13}{subsection.2.3}
\contentsline {paragraph}{\nonumberline Note}{15}{section*.8}
\contentsline {subsection}{\numberline {2.4}Paralog divergence}{15}{subsection.2.4}
\contentsline {paragraph}{\nonumberline Note}{16}{section*.9}
\contentsline {section}{\numberline {3}Multi-site statistics with phased data}{16}{section.3}
\contentsline {subsection}{\numberline {3.1}Basic statistics}{16}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Singleton-based statistics}{17}{subsection.3.2}
\contentsline {paragraph}{\nonumberline Note}{17}{section*.10}
\contentsline {subsection}{\numberline {3.3}Partition-based statistics}{17}{subsection.3.3}
\contentsline {paragraph}{\nonumberline Note}{18}{section*.11}
\contentsline {section}{\numberline {4}Haplotype analysis}{18}{section.4}
\contentsline {paragraph}{\nonumberline Note}{19}{section*.12}
\contentsline {section}{\numberline {5}Linkage disequilibrium analyses}{19}{section.5}
\contentsline {subsection}{\numberline {5.1}Pairwise linkage disequilibrium}{20}{subsection.5.1}
\contentsline {paragraph}{\nonumberline Note}{20}{section*.13}
\contentsline {subsection}{\numberline {5.2}Linkage disequilibrium matrix}{21}{subsection.5.2}
\contentsline {paragraph}{\nonumberline Note}{22}{section*.14}
\contentsline {subsection}{\numberline {5.3}$R_{min}$ and $\mathaccentV {bar}016{r}_d$}{22}{subsection.5.3}
\contentsline {paragraph}{\nonumberline Note}{22}{section*.15}
\contentsline {paragraph}{\nonumberline Note}{23}{section*.16}
\contentsline {section}{\numberline {6}Extended haplotype heterozygosity}{23}{section.6}
\contentsline {subsection}{\numberline {6.1}EHH with phased data}{23}{subsection.6.1}
\contentsline {paragraph}{\nonumberline Variables}{23}{section*.17}
\contentsline {paragraph}{\nonumberline Accessory statistics}{24}{section*.18}
\contentsline {paragraph}{\nonumberline EHH statistics}{25}{section*.19}
\contentsline {paragraph}{\nonumberline Computation}{26}{section*.20}
\contentsline {subsection}{\numberline {6.2}EHH with unphased data}{27}{subsection.6.2}
\contentsline {paragraph}{\nonumberline Variables}{27}{section*.21}
\contentsline {paragraph}{\nonumberline Accessory statistics}{28}{section*.22}
\contentsline {paragraph}{\nonumberline EHHG statistics}{28}{section*.23}
\contentsline {paragraph}{\nonumberline Computation}{29}{section*.24}
\contentsline {section}{\numberline {7}Coding site analysis}{29}{section.7}
\contentsline {section}{\numberline {8}References}{30}{section.8}
