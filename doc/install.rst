************
Installation
************

This page describes the installation procedure of EggLib.

------------
Requirements
------------

EggLib works under Python 3. It is tested and distributed as binaries
for Python 3.7, 3.8, 3.9 and 3.10. Binary distributions are available
for MacOSX (10.9+) and Windows (8 or above).

If you plan to compile EggLib from source, you'll need the development 
libraries for your version of Python. For using any function of the 
:ref:`wrappers <wrappers>` module, the corresponding program must be 
found in your system and EggLib should be configured accordingly (see 
:ref:`paths`).

------------
Installation
------------

EggLib is on the `Python Package Index <https://pypi.org/project/EggLib/>`_
and can be installed using **pip** as::

    pip install egglib --user

The ``--user`` flag tells **pip** to install EggLib in your local user
libraries. You can drop it from a system-wide install or if you're
working in a virtual environment.

.. highlight:: bash

If the ``pip`` command is not directly accessible or if you want to
install EggLib for a specific (non-default) version of Python (for
example, 3.7) you can type::

    python3.7 -m pip install egglib --user

or, on Windows, using the Python Launcher::

    py -3.7 -m pip install egglib --user

You can also:

* Select a specific version of EggLib (for example, 3.0.0): 
  ``pip install egglib==3.0.0``.
* Upgrade to the latest version: ``pip install --upgrade egglib``.
* Remove EggLib: ``pip uninstall egglib``.

Binary packages
===============

Binary packages (binary wheels) are available on **pip** for MacOSX and
Windows under Python 3.7 or above. **pip** should use the package wheel
corresponding to your system. By default, it will attempt to compile
EggLib (which is the normal behaviour on Linux).

Download source and binary packages
===================================

The source package can be downloaded from **pip** using::

    pip download egglib --no-binary :all:

This will download the source package. Letting you access to source 
code. To modify EggLib, it will be much easier to clone EggLib from the 
**git** repository since the source package lacks some development 
scripts. The source package also lets you run the test suite (see 
below), and run an automated script (located in ``packaging/``) to 
generate binary distributions.

To download the binary package corresponding to your system (assuming it
is available), you can just type::

    pip download egglib

Alternatively, all files for the current release are available for
download from `<https://pypi.org/project/EggLib/#files>`_. Older
releases are also archived there.

---------------------------------
Configuring external applications
---------------------------------

If external applications are needed (one is required for every function
of the :ref:`wrappers <wrappers>` module), they must be configured. By
default, EggLib will assume that the corresponding programs are absent
and will not attempt to run them. The best way consists in providing the
command allowing to run the programs to the :mod:`!wrappers` module,
which can be done permanently or temporarily. For details, see
:ref:`paths`.

-----
Tests
-----

The test suite is available from the project package and also from the
source package (using ``pip install egglib``). To run the full list of
tests, use from the project directory (after installing EggLib)::

    python test -ao LOG_FILE

It is possible to run tests separately for different subpackages. Use
``python test -h`` for a manual. If the paths for external applications
have not been provided (see `here <https://egglib.org/py/wrappers.html#paths>`_),
the tests involving them will fail.

Note: currently, two tests implying the random module may fail
occasionally (one yielding an error, the other hanging indefinitively).
